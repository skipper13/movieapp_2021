package com.nicetas.prefs

import android.content.Context
import android.content.SharedPreferences
import androidx.annotation.WorkerThread
import androidx.collection.MutableScatterMap
import androidx.preference.PreferenceManager
import com.nicetas.bus.base.RxBusPublishSubject
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.core.SingleObserver
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.plugins.RxJavaPlugins
import io.reactivex.rxjava3.schedulers.Schedulers

class Prefs(appContext: Context) {

    interface IntKey : PrefsKey
    interface LongKey : PrefsKey
    interface DoubleKey : PrefsKey
    interface FloatKey : PrefsKey
    interface StringKey : PrefsKey
    interface BooleanKey : PrefsKey
    interface StringSetKey : PrefsKey

    class OnPrefsChanged(val key: String?)

    private val prefsDefault by lazy {
        val prefs = PreferenceManager.getDefaultSharedPreferences(appContext)
        prefs.registerOnSharedPreferenceChangeListener(listener)
        prefs
    }
    private val prefsGenres by lazy {
        appContext.getSharedPreferences("genres", Context.MODE_PRIVATE)
    }

    private val bus = RxBusPublishSubject<OnPrefsChanged>()
    private val listener = SharedPreferences.OnSharedPreferenceChangeListener { prefs, key ->
        bus.send(OnPrefsChanged(key = key))
    }

    private val cacheDefault = MutableScatterMap<String, Any>()
    private val cacheGenres = MutableScatterMap<String, Any>()

    fun subscribeOnChanged(): Observable<OnPrefsChanged> {
        return bus.observeEvents(OnPrefsChanged::class.java)
    }

    private fun getPrefs(key: PrefsKey): SharedPreferences {
        return when (key) {
            is PrefsKey.Genres -> prefsGenres
            else -> prefsDefault
        }
    }

    private fun getCache(key: PrefsKey): MutableScatterMap<String, Any> {
        return when (key) {
            is PrefsKey.Genres -> cacheGenres
            else -> cacheDefault
        }
    }

    fun editGenres(): Editor {
        return Editor(prefsGenres.edit(), cacheGenres)
    }

    fun edit(): Editor {
        return Editor(prefsDefault.edit(), cacheDefault)
    }

    fun getBoolean(key: BooleanKey, defValue: Boolean): Boolean {
        val cacheMap = getCache(key)
        val cache = cacheMap[key.name]
        if (cache is Boolean) {
            return cache
        }
        val value = getPrefs(key).getBoolean(key.name, defValue)
        cacheMap.put(key.name, value)
        return value
    }

    fun getInt(key: IntKey, defValue: Int): Int {
        val cacheMap = getCache(key)
        val cache = cacheMap[key.name]
        if (cache is Int) {
            return cache
        }
        val value = getPrefs(key).getInt(key.name, defValue)
        cacheMap.put(key.name, value)
        return value
    }

    fun getLong(key: LongKey, defValue: Long): Long {
        val cacheMap = getCache(key)
        val cache = cacheMap[key.name]
        if (cache is Long) {
            return cache
        }
        val value = getPrefs(key).getLong(key.name, defValue)
        cacheMap.put(key.name, value)
        return value
    }

    fun getFloat(key: FloatKey, defValue: Float): Float {
        val cacheMap = getCache(key)
        val cache = cacheMap[key.name]
        if (cache is Float) {
            return cache
        }
        val value = getPrefs(key).getFloat(key.name, defValue)
        cacheMap.put(key.name, value)
        return value
    }

    fun getString(key: StringKey, defValue: String?): String? {
        val cacheMap = getCache(key)
        val cache = cacheMap[key.name]
        if (cache is String) {
            return cache
        }
        val value = getPrefs(key).getString(key.name, defValue)
        if (value != null) {
            cacheMap.put(key.name, value)
        }
        return value
    }

    fun getStringSet(key: StringSetKey, defValues: Set<String>?): Set<String>? {
        return getPrefs(key).getStringSet(key.name, defValues)
    }

    fun getDouble(key: DoubleKey, defValue: Double): Double {
        val cacheMap = getCache(key)
        val cache = cacheMap[key.name]
        if (cache is Long) {
            return java.lang.Double.longBitsToDouble(cache)
        }
        val value = getPrefs(key).getLong(key.name, java.lang.Double.doubleToRawLongBits(defValue))
        cacheMap.put(key.name, value)
        return java.lang.Double.longBitsToDouble(value)
    }

    fun contains(key: PrefsKey): Boolean {
        return getPrefs(key).contains(key.name)
    }


    class Editor(
        private val editor: SharedPreferences.Editor,
        private val cache: MutableScatterMap<String, Any>,
    ) {

        fun putBoolean(key: BooleanKey, value: Boolean): Editor {
            cache[key.name] = value
            editor.putBoolean(key.name, value)
            return this
        }

        fun putInt(key: IntKey, value: Int): Editor {
            cache[key.name] = value
            editor.putInt(key.name, value)
            return this
        }

        fun putLong(key: LongKey, value: Long): Editor {
            cache[key.name] = value
            editor.putLong(key.name, value)
            return this
        }

        fun putFloat(key: FloatKey, value: Float): Editor {
            cache[key.name] = value
            editor.putFloat(key.name, value)
            return this
        }

        fun putString(key: StringKey, value: String): Editor {
            cache[key.name] = value
            editor.putString(key.name, value)
            return this
        }

        fun putStringSet(key: StringSetKey, values: Set<String>?): Editor {
            editor.putStringSet(key.name, values)
            return this
        }

        fun putDouble(key: DoubleKey, double: Double): Editor {
            val value = java.lang.Double.doubleToRawLongBits(double)
            cache[key.name] = value
            editor.putLong(key.name, value)
            return this
        }

        fun remove(key: PrefsKey): Editor {
            cache.remove(key.name)
            editor.remove(key.name)
            return this
        }

        fun clear(): Editor {
            cache.clear()
            editor.clear()
            return this
        }

        @WorkerThread
        fun commit(): Boolean {
            return editor.commit()
        }

        fun commitIO(action: Editor.() -> Unit) {
            RxJavaPlugins.onAssembly(SingleEdit(this, action))
                .subscribeOn(Schedulers.io())
                .subscribe()
        }
    }


    class SingleEdit(
        private val editor: Editor,
        private val action: Editor.() -> Unit,
    ) : Single<Boolean>() {
        override fun subscribeActual(observer: SingleObserver<in Boolean>) {
            val d = Disposable.empty()
            observer.onSubscribe(d)
            if (!d.isDisposed) {
                action.invoke(editor)
                val value = editor.commit()
                if (!d.isDisposed) {
                    observer.onSuccess(value)
                }
            }
        }
    }

}
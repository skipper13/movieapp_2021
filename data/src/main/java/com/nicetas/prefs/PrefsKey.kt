package com.nicetas.prefs

import com.nicetas.repos.network.LanguageProvider

interface PrefsKey {

    val name: String

    object SearchKitSelectedId : Prefs.IntKey {
        override val name = "search_kit_selected_id"
    }

    object ImgBaseUrl : Prefs.StringKey {
        override val name = "img_base_url"
    }

    object IsKitAnimationHided : Prefs.BooleanKey {
        override val name = "is_kit_animation_hided"
    }

    object IsOfflineMode : Prefs.BooleanKey {
        override val name = "is_offline_mode"
    }

    object WatchlistSort : Prefs.IntKey {
        override val name = "watchlist_sort"
    }

    object WatchlistIsDesc : Prefs.BooleanKey {
        override val name = "watchlist_is_desc"
    }

    object Genres : Prefs.StringKey {
        override val name get() = "genres_${LanguageProvider.language}"
    }

    object DateFormatOrder : Prefs.IntKey {
        const val D_M_Y = 1
        const val M_D_Y = 2
        const val Y_M_D = 3
        override val name = "date_format_order"
    }

    object DateFormatSeparator : Prefs.StringKey {
        override val name = "date_format_separator"
    }

    object IsSearchOptionsTipsShown : Prefs.BooleanKey {
        override val name = "is_search_options_tips_shown"
    }

    object IsMovieInfoTipsShown : Prefs.BooleanKey {
        override val name = "is_movie_info_tips_shown"
    }

    object ShowPostersForSearch : Prefs.BooleanKey {
        override val name = "show_posters_for_search"
    }

}
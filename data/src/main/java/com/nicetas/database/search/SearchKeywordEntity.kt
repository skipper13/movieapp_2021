package com.nicetas.database.search

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.nicetas.models.IdName
import com.nicetas.models.extensions.ifBlankF

@Entity(tableName = "search_keyword_entity")
class SearchKeywordEntity(
    @PrimaryKey
    @ColumnInfo(name = "id")
    val id: Int,
    @ColumnInfo(name = "name")
    val name: String,
) : IdName {

    override fun identifier() = id

    override fun iName() = name.ifBlankF { findLocal() }.ifBlankF { "$id" }

    override fun toString(): String {
        return "id = $id, name = $name, iName = ${iName()};"
    }

    private fun findLocal(): String {
        return when (id) {
            1720 -> "tyrannosaurus rex"
            10542 -> "based on toy"
            210024 -> "anime"
            else -> ""
        }
    }

}
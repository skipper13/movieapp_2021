package com.nicetas.database.search

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.nicetas.models.IdName
import com.nicetas.models.extensions.ifBlankF

@Entity(tableName = "search_company_entity")
class SearchCompanyEntity(
    @PrimaryKey
    @ColumnInfo(name = "id")
    val id: Int,
    @ColumnInfo(name = "name")
    val name: String,
) : IdName {

    override fun identifier() = id

    override fun iName() = name.ifBlankF { findLocal() }.ifBlankF { "$id" }

    override fun toString(): String {
        return "id = $id, name = $name, iName = ${iName()};"
    }

    private fun findLocal(): String {
        return when (id) {
            2 -> "Walt Disney Pictures"
            3 -> "Pixar"
            521 -> "DreamWorks Animation"
            882 -> "TOHO"
            2073 -> "KADOKAWA"
            2251 -> "Sony Pictures Animation"
            2785 -> "Warner Bros. Animation"
            2875 -> "Ikiru Films"
            4859 -> "Nickelodeon Animation Studio"
            5024 -> "Mainframe Entertainment"
            6125 -> "Walt Disney Animation Studios"
            6220 -> "Mattel"
            6329 -> "MTV Entertainment Studios"
            6704 -> "Illumination"
            10342 -> "Studio Ghibli"
            11749 -> "20th Century Fox Animation"
            12502 -> "Bandai Namco Entertainment"
            13240 -> "Bron Studios"
            25120 -> "Warner Bros. Pictures Animation"
            31922 -> "DNEG"
            49983 -> "Mattel Entertainment"
            77973 -> "Lord Miller"
            137586 -> "Mattel Television"
            else -> ""
        }
    }

}
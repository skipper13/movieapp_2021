package com.nicetas.database.search

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.rxjava3.core.Single

@Dao
interface SearchKitsDao {

    @Query("SELECT * FROM search_kit_entity")
    fun getAllKits(): Single<List<SearchKitEntity>>

    @Query("SELECT * FROM search_kit_entity WHERE id = :kitId")
    fun getKit(kitId: Int): Single<List<SearchKitEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertKit(kit: SearchKitEntity): Single<Long>

    @Query("DELETE FROM search_kit_entity WHERE id = :kitId")
    fun deleteKit(kitId: Int): Single<Int>

    @Query("SELECT * FROM search_keyword_entity WHERE id=:id")
    fun getKeyword(id: Int): Single<List<SearchKeywordEntity>>

    @Query("SELECT * FROM search_keyword_entity WHERE id IN (:ids)")
    fun getKeywords(ids: List<Int>): Single<List<SearchKeywordEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertKeyword(keyword: SearchKeywordEntity): Single<Long>

//    @Insert(onConflict = OnConflictStrategy.REPLACE)
//    fun insertKeywords(keyword: List<SearchKeywordEntity>): Single<List<Long>>

    @Query("SELECT * FROM search_company_entity WHERE id=:id")
    fun getCompany(id: Int): Single<List<SearchCompanyEntity>>

    @Query("SELECT * FROM search_company_entity WHERE id IN (:ids)")
    fun getCompanies(ids: List<Int>): Single<List<SearchCompanyEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCompany(company: SearchCompanyEntity): Single<Long>

//    @Insert(onConflict = OnConflictStrategy.REPLACE)
//    fun insertCompanies(companies: List<SearchCompanyEntity>): Single<List<Long>>

}
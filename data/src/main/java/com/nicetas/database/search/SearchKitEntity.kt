package com.nicetas.database.search

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.nicetas.models.genres.Genre
import com.nicetas.repos.search.SortBy
import java.time.LocalDate

@Entity(tableName = "search_kit_entity")
class SearchKitEntity(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    val id: Int = 0,
    @ColumnInfo(name = "icon_id")
    val iconId: Int,
    @ColumnInfo(name = "title")
    val title: String,
    @ColumnInfo(name = "release_date_from")
    val releaseDateFrom: LocalDate?,
    @ColumnInfo(name = "release_date_until")
    val releaseDateUntil: LocalDate?,
    @ColumnInfo(name = "vote_average_gte")
    val voteAverageMin: Double?,
    @ColumnInfo(name = "vote_average_lte")
    val voteAverageMax: Double?,
    @ColumnInfo(name = "vote_count_gte")
    val voteCountMin: Int,
    @ColumnInfo(name = "sort_by")
    val sortBy: SortBy,
    @ColumnInfo(name = "with_origin_country")
    val withOriginCountry: String?,
    @ColumnInfo(name = "certification", defaultValue = "")
    val certification: HashSet<String>,
    @ColumnInfo(name = "certification_country")
    val certificationCountry: String?,
    @ColumnInfo(name = "with_runtime_gte")
    val withRuntimeMinimum: Int?,
    @ColumnInfo(name = "with_runtime_lte")
    val withRuntimeMaximum: Int?,
    @ColumnInfo(name = "with_genres")
    val withGenresIds: HashSet<Int>,
    @ColumnInfo(name = "is_with_genres_operator_or")
    val isWithGenresOperatorOR: Boolean,
    @ColumnInfo(name = "without_genres")
    val withoutGenresIds: HashSet<Int>,
    @ColumnInfo(name = "with_companies")
    val withCompanies: HashSet<Int>,
    @ColumnInfo(name = "is_with_companies_operator_or")
    val isWithCompaniesOperatorOR: Boolean,
    @ColumnInfo(name = "without_companies")
    val withoutCompanies: HashSet<Int>,
    @ColumnInfo(name = "with_keywords")
    val withKeywords: HashSet<Int>,
    @ColumnInfo(name = "is_with_keywords_operator_or")
    val isWithKeywordsOperatorOR: Boolean,
    @ColumnInfo(name = "without_keywords")
    val withoutKeywords: HashSet<Int>,
) {

    object IconId {
        const val POPULAR = 0
        const val TOP = 1
        const val ANIMATION = 2
        const val UPCOMING = 18
    }

    companion object {
        const val ID_POPULAR = Int.MAX_VALUE - 1001
        const val ID_TOP = Int.MAX_VALUE - 1002
        const val ID_UPCOMING = Int.MAX_VALUE - 1003
        const val ID_ANIMATION = Int.MAX_VALUE - 1004

        fun createPopular() = SearchKitEntity(
            id = ID_POPULAR,
            iconId = IconId.POPULAR,
            title = "Popular",
            withGenresIds = HashSet<Int>(),
            isWithGenresOperatorOR = false,
            withoutGenresIds = withoutHorror(),
            voteCountMin = 50,
            voteAverageMin = 6.0,
            voteAverageMax = null,
            releaseDateFrom = null,
            releaseDateUntil = null,
            sortBy = SortBy.POPULARITY_DESC,
            withOriginCountry = null,
            withRuntimeMinimum = null,
            withRuntimeMaximum = null,
            withCompanies = HashSet<Int>(),
            isWithCompaniesOperatorOR = true,
            withoutCompanies = HashSet<Int>(),
            withKeywords = HashSet<Int>(),
            isWithKeywordsOperatorOR = true,
            withoutKeywords = HashSet<Int>(),
            certification = HashSet<String>(),
            certificationCountry = null,
        )

        fun createTop() = SearchKitEntity(
            id = ID_TOP,
            iconId = IconId.TOP,
            title = "Top",
            withGenresIds = HashSet<Int>(),
            isWithGenresOperatorOR = false,
            withoutGenresIds = withoutHorror(),
            voteCountMin = 300,
            voteAverageMin = 6.0,
            voteAverageMax = null,
            releaseDateFrom = null,
            releaseDateUntil = null,
            sortBy = SortBy.VOTE_AVERAGE_DESC,
            withOriginCountry = null,
            withRuntimeMinimum = null,
            withRuntimeMaximum = null,
            withCompanies = HashSet<Int>(),
            isWithCompaniesOperatorOR = true,
            withoutCompanies = HashSet<Int>(),
            withKeywords = HashSet<Int>(),
            isWithKeywordsOperatorOR = true,
            withoutKeywords = HashSet<Int>(),
            certification = HashSet<String>(),
            certificationCountry = null,
        )

        fun createUpcoming() = SearchKitEntity(
            id = ID_UPCOMING,
            iconId = IconId.UPCOMING,
            title = "Upcoming",
            withGenresIds = HashSet<Int>(),
            isWithGenresOperatorOR = false,
            withoutGenresIds = withoutHorror(),
            voteCountMin = 0,
            voteAverageMin = null,
            voteAverageMax = null,
            releaseDateFrom = LocalDate.now(),
            releaseDateUntil = null,
            sortBy = SortBy.POPULARITY_DESC,
            withOriginCountry = null,
            withRuntimeMinimum = null,
            withRuntimeMaximum = null,
            withCompanies = HashSet<Int>(),
            isWithCompaniesOperatorOR = true,
            withoutCompanies = HashSet<Int>(),
            withKeywords = HashSet<Int>(),
            isWithKeywordsOperatorOR = true,
            withoutKeywords = HashSet<Int>(),
            certification = HashSet<String>(),
            certificationCountry = null,
        )

        fun createAnimation() = SearchKitEntity(
            id = ID_ANIMATION,
            iconId = IconId.ANIMATION,
            title = "Animation",
            withGenresIds = HashSet<Int>().apply { add(Genre.ANIMATION_ID) },
            isWithGenresOperatorOR = false,
            withoutGenresIds = withoutHorror(),
            voteCountMin = 150,
            voteAverageMin = 6.0,
            voteAverageMax = null,
            releaseDateFrom = null,
            releaseDateUntil = null,
            sortBy = SortBy.PRIMARY_RELEASE_DATE_DESC,
            withOriginCountry = null,
            withRuntimeMinimum = 50,
            withRuntimeMaximum = null,
            withCompanies = HashSet<Int>().apply {
                add(3) // Pixar
                add(6125) // Walt Disney Animation Studios
                add(521) // DreamWorks Animation
                add(6704) // Illumination
                add(31922) // DNEG
                add(77973) // Lord Miller
                add(2875) // Ikiru Films
            },
            isWithCompaniesOperatorOR = true,
            withoutCompanies = HashSet<Int>().apply {
                add(25120) // Warner Bros. Pictures Animation
                add(2785) // Warner Bros. Animation
                add(4859) // Nickelodeon Animation Studio
                add(13240) // Bron Studios
                add(6329) // MTV Entertainment Studios
            },
            withKeywords = HashSet<Int>(),
            isWithKeywordsOperatorOR = true,
            withoutKeywords = HashSet<Int>().apply {
                add(210024) // anime
                add(10542) // based on toy
                add(1720) // tyrannosaurus rex
            },
            certification = HashSet<String>(),
            certificationCountry = null,
        )

        private fun withoutHorror(): HashSet<Int> {
            return HashSet<Int>().apply { add(Genre.HORROR_ID) }
        }

    }
}
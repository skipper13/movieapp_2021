package com.nicetas.database.watchlist

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single

@Dao
interface WatchListDao {

    @Query("SELECT * FROM MovieToWatchEntity")
    fun getAll(): Single<List<MovieToWatchEntity>>

    @Query("SELECT EXISTS(SELECT * FROM MovieToWatchEntity WHERE movie_id = :movieId)")
    fun isInWatchList(movieId: Int): Single<Boolean>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(movie: MovieToWatchEntity): Completable

    @Query("DELETE FROM MovieToWatchEntity WHERE movie_id = :movieId")
    fun delete(movieId: Int): Completable

}
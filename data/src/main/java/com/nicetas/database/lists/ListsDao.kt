package com.nicetas.database.lists

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single

@Dao
interface ListsDao {

    @Query("SELECT * FROM ListsEntity")
    fun getAll(): Single<List<ListsEntity>>

    @Query("SELECT EXISTS(SELECT * FROM ListsEntity WHERE list_id = :listId)")
    fun isInLists(listId: Int): Single<Boolean>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(list: ListsEntity): Completable

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(list: ListsEntity): Completable

    @Query("DELETE FROM ListsEntity WHERE list_id = :listId")
    fun delete(listId: Int): Completable

}
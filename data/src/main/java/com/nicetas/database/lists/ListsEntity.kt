package com.nicetas.database.lists

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class ListsEntity(
    @PrimaryKey
    @ColumnInfo(name = "list_id")
    val id: Int,
    @ColumnInfo(name = "name")
    val name: String,
    @ColumnInfo(name = "backdrop_path")
    val backdropPath: String?,
    @ColumnInfo(name = "item_count")
    val itemCount: Int,
)
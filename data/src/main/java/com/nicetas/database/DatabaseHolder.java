package com.nicetas.database;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.nicetas.database.lists.ListsDao;
import com.nicetas.database.lists.ListsEntity;
import com.nicetas.database.search.SearchCompanyEntity;
import com.nicetas.database.search.SearchKeywordEntity;
import com.nicetas.database.search.SearchKitEntity;
import com.nicetas.database.search.SearchKitsDao;
import com.nicetas.database.watchlist.MovieToWatchEntity;
import com.nicetas.database.watchlist.WatchListDao;

public class DatabaseHolder {

    @NonNull
    public AppDatabase room;

    public DatabaseHolder(Context appContext) {
        room = Room.databaseBuilder(appContext, AppDatabase.class, "appDB")
                .addMigrations(getMigration_1_2())
                .addMigrations(getMigration_2_3())
                .addMigrations(getMigration_3_4())
                .addMigrations(getMigration_4_5())
                .addMigrations(getMigration_5_6())
                .addMigrations(getMigration_6_7())
                .addMigrations(getMigration_7_8())
                .addMigrations(getMigration_8_9())
                .addMigrations(getMigration_9_10())
                .build();
    }

    @TypeConverters(DbTypeConverters.class)
    @Database(
            entities = {
                    MovieToWatchEntity.class,
                    SearchKitEntity.class,
                    ListsEntity.class,
                    SearchKeywordEntity.class,
                    SearchCompanyEntity.class,
            },
            version = 10
    )
    public abstract static class AppDatabase extends RoomDatabase {

        public abstract WatchListDao watchListDao();

        public abstract SearchKitsDao searchKitsDao();

        public abstract ListsDao listsDao();
    }

    private Migration getMigration_1_2() {
        return new Migration(1, 2) {
            @Override
            public void migrate(@NonNull SupportSQLiteDatabase db) {
                db.execSQL("ALTER TABLE `SearchKitEntity` ADD COLUMN `with_origin_country` TEXT");
            }
        };
    }

    private Migration getMigration_2_3() {
        return new Migration(2, 3) {
            @Override
            public void migrate(@NonNull SupportSQLiteDatabase db) {
                db.execSQL("""
                        CREATE TABLE `search_keyword_entity`
                        (`id` INTEGER NOT NULL, `name` TEXT NOT NULL, PRIMARY KEY(`id`))
                        """);
            }
        };
    }

    private Migration getMigration_3_4() {
        return new Migration(3, 4) {
            @Override
            public void migrate(@NonNull SupportSQLiteDatabase db) {
                // Make `vote_average_gte` nullable and insert `vote_average_lte`
                db.execSQL(""" 
                        CREATE TABLE `search_kit_entity` (
                         `id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                         `icon_id` INTEGER NOT NULL,
                         `title` TEXT NOT NULL,
                         `release_date_from` TEXT,
                         `release_date_until` TEXT,
                         `vote_average_gte` REAL,
                         `vote_average_lte` REAL,
                         `vote_count_gte` INTEGER NOT NULL,
                         `sort_by` TEXT NOT NULL,
                         `with_runtime_gte` INTEGER,
                         `with_runtime_lte` INTEGER,
                         `with_genres` TEXT NOT NULL,
                         `without_genres` TEXT NOT NULL,
                         `with_companies` TEXT NOT NULL,
                         `without_companies` TEXT NOT NULL,
                         `with_keywords` TEXT NOT NULL,
                         `without_keywords` TEXT NOT NULL,
                         `with_origin_country` TEXT
                        )""");
                // Copy the data
                db.execSQL("""
                        INSERT INTO `search_kit_entity` (id, icon_id, title,
                         release_date_from, release_date_until,
                         vote_average_gte, vote_count_gte, sort_by,
                         with_runtime_gte, with_runtime_lte,
                         with_genres, without_genres,
                         with_companies, without_companies,
                         with_keywords, without_keywords,
                         with_origin_country)
                        SELECT id, icon_id, title,
                         release_date_from, release_date_until,
                         vote_average_gte, vote_count_gte, sort_by,
                         with_runtime_gte, with_runtime_lte,
                         with_genres, without_genres,
                         with_companies, without_companies,
                         with_keywords, without_keywords,
                         with_origin_country
                        FROM `SearchKitEntity`
                        """);
                // Remove the old table
                db.execSQL("DROP TABLE `SearchKitEntity`");
            }
        };
    }

    private Migration getMigration_4_5() {
        return new Migration(4, 5) {
            @Override
            public void migrate(@NonNull SupportSQLiteDatabase db) {
                db.execSQL("DELETE FROM `search_kit_entity` WHERE id <= 3");
            }
        };
    }

    private Migration getMigration_5_6() {
        return new Migration(5, 6) {
            @Override
            public void migrate(@NonNull SupportSQLiteDatabase db) {
                db.execSQL("""
                        ALTER TABLE `search_kit_entity`
                        ADD COLUMN `is_with_keywords_operator_or` INTEGER DEFAULT 1 NOT NULL
                        """);
            }
        };
    }

    private Migration getMigration_6_7() {
        return new Migration(6, 7) {
            @Override
            public void migrate(@NonNull SupportSQLiteDatabase db) {
                db.execSQL("""
                        ALTER TABLE `search_kit_entity`
                        ADD COLUMN `is_with_companies_operator_or` INTEGER DEFAULT 1 NOT NULL
                        """);
                db.execSQL("""
                        CREATE TABLE `search_company_entity`
                        (`id` INTEGER NOT NULL, `name` TEXT NOT NULL, PRIMARY KEY(`id`))
                        """);
            }
        };
    }

    private Migration getMigration_7_8() {
        return new Migration(7, 8) {
            @Override
            public void migrate(@NonNull SupportSQLiteDatabase db) {
                db.execSQL("ALTER TABLE `search_kit_entity` ADD COLUMN `certification_country` TEXT");
                db.execSQL("""
                        ALTER TABLE `search_kit_entity`
                        ADD COLUMN `certification` TEXT NOT NULL DEFAULT ''
                        """);
            }
        };
    }

    private Migration getMigration_8_9() {
        return new Migration(8, 9) {
            @Override
            public void migrate(@NonNull SupportSQLiteDatabase db) {
                db.execSQL("""
                        ALTER TABLE `search_kit_entity`
                        ADD COLUMN `is_with_genres_operator_or` INTEGER DEFAULT 0 NOT NULL
                        """);
            }
        };
    }

    private Migration getMigration_9_10() {
        return new Migration(9, 10) {
            @Override
            public void migrate(@NonNull SupportSQLiteDatabase db) {
                db.execSQL("ALTER TABLE `MovieToWatchEntity` ADD COLUMN `poster_path` TEXT");
            }
        };
    }

}
package com.nicetas.database

import androidx.room.TypeConverter
import com.nicetas.GsonProvider
import com.nicetas.models.extensions.isBlankF
import com.nicetas.repos.search.SortBy
import java.time.LocalDate
import java.time.LocalDateTime

class DbTypeConverters {

    private val gson = GsonProvider()

    @TypeConverter
    fun fromLocalDate(date: LocalDate?): String {
        return gson.toJson(date)
    }

    @TypeConverter
    fun toLocalDate(value: String): LocalDate? {
        return gson.fromJson(value)
    }

    @TypeConverter
    fun fromLocalDateTime(date: LocalDateTime?): String {
        return gson.toJson(date)
    }

    @TypeConverter
    fun toLocalDateTime(value: String): LocalDateTime? {
        return gson.fromJson(value)
    }

    @TypeConverter
    fun fromSortBy(sortBy: SortBy): String {
        return sortBy.serverName
    }

    @TypeConverter
    fun toSortBy(value: String): SortBy {
        return SortBy.parse(value)
    }

    @TypeConverter
    fun fromIntHashSet(hashSet: HashSet<Int>): String {
        return hashSet.joinToString("|")
    }

    @TypeConverter
    fun toIntHashSet(text: String): HashSet<Int> {
        val set = HashSet<Int>()
        if (text.isBlankF()) {
            return set
        }
        text.split("|").forEach { number ->
            set.add(number.toInt())
        }
        return set
    }

    @TypeConverter
    fun fromStringHashSet(hashSet: HashSet<String>): String {
        return hashSet.joinToString("|")
    }

    @TypeConverter
    fun toStringHashSet(text: String): HashSet<String> {
        val set = HashSet<String>()
        if (text.isBlankF()) {
            return set
        }
        text.split("|").forEach { substring ->
            set.add(substring)
        }
        return set
    }

}
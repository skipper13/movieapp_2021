package com.nicetas.domain

import com.nicetas.models.extensions.DateOrder
import com.nicetas.prefs.Prefs
import com.nicetas.prefs.PrefsKey
import com.nicetas.prefs.PrefsKey.DateFormatOrder.D_M_Y
import com.nicetas.prefs.PrefsKey.DateFormatOrder.M_D_Y
import com.nicetas.prefs.PrefsKey.DateFormatOrder.Y_M_D
import io.reactivex.rxjava3.core.Maybe
import io.reactivex.rxjava3.core.Observable

class DateFormatInteractor(
    private val prefs: Prefs,
) {

    var dateOrder = DateOrder.DayMonthYear
    var dateSeparator = '.'

    fun subscribeOnPrefsChanged(): Observable<Unit> {
        return prefs.subscribeOnChanged()
            .flatMapMaybe { event ->
                val isOrder = event.key == PrefsKey.DateFormatOrder.name
                val isSeparator = event.key == PrefsKey.DateFormatSeparator.name
                if (isOrder) {
                    updateDateOrder()
                }
                if (isSeparator) {
                    updateDateSeparator()
                }
                if (isOrder || isSeparator) {
                    Maybe.just(Unit)
                } else {
                    Maybe.never()
                }
            }
    }

    private fun updateDateOrder() {
        val order = prefs.getInt(PrefsKey.DateFormatOrder, D_M_Y)
        dateOrder = when (order) {
            M_D_Y -> DateOrder.MonthDayYear
            Y_M_D -> DateOrder.YearMonthDay
            else -> DateOrder.DayMonthYear
        }
    }

    private fun updateDateSeparator() {
        dateSeparator = (prefs.getString(PrefsKey.DateFormatSeparator, ".") ?: ".")[0]
    }

    fun updateFromPrefs() {
        updateDateOrder()
        updateDateSeparator()
    }

}
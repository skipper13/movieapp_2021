package com.nicetas

import android.content.Context


fun Context.readAssetsFile(fileName: String): String {
    return assets.open(fileName).bufferedReader().use { it.readText() }
}
package com.nicetas.bus

import com.nicetas.bus.base.RxBusPublishSubject
import io.reactivex.rxjava3.core.Observable

class WatchListUpdateBus() {

    sealed class Event(val movieId: Int)

    class OnRemove(movieId: Int) : Event(movieId)
    class OnAdd(movieId: Int) : Event(movieId)

    private val bus = RxBusPublishSubject<Event>()

    fun subscribeOnWatchlist(): Observable<Event> {
        return bus.observeEvents(Event::class.java)
    }

    fun onRemove(movieId: Int) {
        bus.send(OnRemove(movieId))
    }

    fun onAdd(movieId: Int) {
        bus.send(OnAdd(movieId))
    }

}
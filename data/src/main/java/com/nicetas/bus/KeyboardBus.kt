package com.nicetas.bus

import com.nicetas.bus.base.RxBusPublishSubject
import io.reactivex.rxjava3.core.Observable

class KeyboardBus() {

    class OnKeyboard(val isOpen: Boolean)

    private val bus = RxBusPublishSubject<OnKeyboard>()

    fun subscribeOnKeyboard(): Observable<OnKeyboard> {
        return bus.observeEvents(OnKeyboard::class.java)
    }

    fun onOpen() {
        bus.send(OnKeyboard(isOpen = true))
    }

    fun onClose() {
        bus.send(OnKeyboard(isOpen = false))
    }

}
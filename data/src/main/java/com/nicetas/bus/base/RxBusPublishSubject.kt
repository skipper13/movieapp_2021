package com.nicetas.bus.base

import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.subjects.PublishSubject

class RxBusPublishSubject<in T : Any> {

    private val subject = PublishSubject.create<T>().toSerialized()

    fun <E : T> send(event: E) {
        subject.onNext(event)
    }

    fun send(throwable: Throwable) {
        subject.onError(throwable)
    }

    fun <E : T> observeEvents(eventClass: Class<E>): Observable<E> {
        return subject.ofType(eventClass)
    }
}
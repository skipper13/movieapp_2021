package com.nicetas

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.TypeAdapter
import com.google.gson.reflect.TypeToken
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonToken
import com.google.gson.stream.JsonWriter
import com.nicetas.models.extensions.isBlankF
import com.nicetas.models.extensions.toLocalDate
import java.time.LocalDate
import java.time.LocalDateTime

class GsonProvider() {

    val gson: Gson = GsonBuilder()
        .registerTypeAdapter(LocalDate::class.java, LocalDateAdapter())
        .registerTypeAdapter(LocalDateTime::class.java, LocalDateTimeAdapter())
        .create()

    inline fun <reified T> fromJson(json: String): T {
        return gson.fromJson(json, object : TypeToken<T>() {}.type)
    }

    fun toJson(src: Any?): String {
        return gson.toJson(src)
    }

    private class LocalDateAdapter : TypeAdapter<LocalDate?>() {
        override fun read(jsonReader: JsonReader): LocalDate? {
            when (jsonReader.peek()) {
                JsonToken.STRING -> {
                    val dateStr = jsonReader.nextString()
                    if (dateStr.isBlankF()) {
                        return null
                    } else {
                        return dateStr.toLocalDate()
                    }
                }
                else -> {
                    jsonReader.skipValue()
                    return null
                }
            }
        }

        override fun write(out: JsonWriter, value: LocalDate?) {
            out.value(value?.toString())
        }
    }

    private class LocalDateTimeAdapter : TypeAdapter<LocalDateTime?>() {
        override fun read(jsonReader: JsonReader): LocalDateTime? {
            when (jsonReader.peek()) {
                JsonToken.STRING -> {
                    val dateStr = jsonReader.nextString()
                    if (dateStr.isBlankF()) {
                        return null
                    } else {
                        return LocalDateTime.parse(dateStr)
                    }
                }
                else -> {
                    jsonReader.skipValue()
                    return null
                }
            }
        }

        override fun write(out: JsonWriter, value: LocalDateTime?) {
            out.value(value?.toString())
        }
    }

}
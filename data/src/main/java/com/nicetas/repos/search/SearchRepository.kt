package com.nicetas.repos.search

import android.content.Context
import com.nicetas.GsonProvider
import com.nicetas.database.DatabaseHolder
import com.nicetas.database.search.SearchCompanyEntity
import com.nicetas.database.search.SearchKeywordEntity
import com.nicetas.database.search.SearchKitEntity
import com.nicetas.models.movies.MoviesList
import com.nicetas.models.movies.companies.SearchCompanies
import com.nicetas.models.movies.keywords.SearchKeywords
import com.nicetas.prefs.Prefs
import com.nicetas.prefs.PrefsKey
import com.nicetas.repos.network.LanguageProvider
import com.nicetas.repos.network.RetrofitHolder
import com.nicetas.repos.network.SuccessResponse
import com.nicetas.repos.network.successOrError
import io.reactivex.rxjava3.core.Single

open class SearchRepository(
    private val appContext: Context,
    private val retrofit: RetrofitHolder,
    private val db: DatabaseHolder,
    private val gson: GsonProvider,
    private val prefs: Prefs,
) {

    companion object {
        const val DEFAULT_KIT_ID = SearchKitEntity.ID_POPULAR
    }

    val searchKit by lazy { SearchKit() }
    private var searchKitDiscover: SearchKit? = null
    var currentSearchOptionsTab = 1

    private fun service() = retrofit.searchService
    private fun apiKey() = retrofit.apiKey
    protected fun language() = LanguageProvider.language

    fun getSelectedKitId(): Int {
        return prefs.getInt(PrefsKey.SearchKitSelectedId, DEFAULT_KIT_ID)
    }

    fun saveSelectedKitId(id: Int): Single<Boolean> {
        return Single.fromCallable {
            prefs.edit()
                .putInt(PrefsKey.SearchKitSelectedId, id)
                .commit()
        }
    }

    fun getUserSearchKits(): Single<List<SearchKitEntity>> {
        return db.room.searchKitsDao().getAllKits()
    }

    private fun initSearchKit(): Single<SearchKit> {
        return Single.fromCallable {
            val selectedId = getSelectedKitId()

            val defaultKit = when (selectedId) {
                SearchKitEntity.ID_POPULAR -> SearchKitEntity.createPopular()
                SearchKitEntity.ID_TOP -> SearchKitEntity.createTop()
                SearchKitEntity.ID_UPCOMING -> SearchKitEntity.createUpcoming()
                SearchKitEntity.ID_ANIMATION -> SearchKitEntity.createAnimation()
                else -> null
            }
            return@fromCallable Pair(selectedId, defaultKit)

        }.flatMap { pair ->

            val defaultKit = pair.second
            if (defaultKit != null) {
                searchKit.update(defaultKit)
                updateSearchKitDiscover()

                return@flatMap Single.just(searchKitDiscover!!)

            } else {
                return@flatMap db.room.searchKitsDao().getKit(pair.first).map { kitsList ->
                    val kit = if (kitsList.isNotEmpty()) {
                        kitsList[0]
                    } else {
                        SearchKitEntity.createPopular() // fallback
                    }
                    searchKit.update(kit)
                    updateSearchKitDiscover()

                    return@map searchKitDiscover!!
                }
            }
        }
    }

    fun updateSearchKitDiscover() {
        searchKitDiscover = gson.fromJson<SearchKit>(gson.toJson(searchKit))
    }

    fun addSearchKit(kit: SearchKitEntity): Single<Long> {
        return db.room.searchKitsDao().insertKit(kit)
    }

    fun deleteSearchKit(kitId: Int): Single<Int> {
        return Single.fromCallable {
            if (kitId == SearchKitEntity.ID_ANIMATION) {
                prefs.edit()
                    .putBoolean(PrefsKey.IsKitAnimationHided, true)
                    .commit()
            }
        }.flatMap {
            db.room.searchKitsDao().deleteKit(kitId)
        }
    }

    fun saveKeywordForKits(keyword: SearchKeywordEntity): Single<Long> {
        return db.room.searchKitsDao().insertKeyword(keyword)
    }

    fun getKeywordForKits(id: Int): Single<SearchKeywordEntity> {
        return db.room.searchKitsDao().getKeyword(id).map { keywordList ->
            if (keywordList.isEmpty()) {
                SearchKeywordEntity(id = id, name = "")
            } else {
                keywordList[0]
            }
        }
    }

    fun getKeywordsForKits(ids: List<Int>): Single<List<SearchKeywordEntity>> {
        return db.room.searchKitsDao().getKeywords(ids)
    }

    fun saveCompanyForKits(company: SearchCompanyEntity): Single<Long> {
        return db.room.searchKitsDao().insertCompany(company)
    }

    fun getCompanyForKits(id: Int): Single<SearchCompanyEntity> {
        return db.room.searchKitsDao().getCompany(id).map { companyList ->
            if (companyList.isEmpty()) {
                SearchCompanyEntity(id = id, name = "")
            } else {
                companyList[0]
            }
        }
    }

    fun getCompaniesForKits(ids: List<Int>): Single<List<SearchCompanyEntity>> {
        return db.room.searchKitsDao().getCompanies(ids)
    }

    open fun searchMovie(query: String, page: Int): Single<SuccessResponse<MoviesList>> {
        val response = service().searchMovie(
            apiKey = apiKey(),
            language = language(),
            query = query,
            page = page,
        )
        return response.successOrError()
    }

    fun searchKeyword(query: String, page: Int): Single<SuccessResponse<SearchKeywords>> {
        val response = service().searchKeyword(
            apiKey = apiKey(),
            query = query,
            page = page,
        )
        return response.successOrError()
    }

    fun searchCompany(query: String, page: Int): Single<SuccessResponse<SearchCompanies>> {
        val response = service().searchCompany(
            apiKey = apiKey(),
            query = query,
            page = page,
        )
        return response.successOrError()
    }

    open fun discoverMovie(page: Int): Single<SuccessResponse<MoviesList>> {
        return if (searchKitDiscover != null) {
            Single.just(searchKitDiscover!!)
        } else {
            initSearchKit()
        }.flatMap { options ->
            discoverMovie(page, options)
        }
    }

    private fun discoverMovie(
        page: Int,
        options: SearchKit,
    ): Single<SuccessResponse<MoviesList>> {
        val runtimeMin = options.withRuntimeMinimum
        val runtimeMax = options.withRuntimeMaximum

        val response = service().discoverMovie(
            apiKey = apiKey(),
            language = language(),
            page = page,
            minVoteAverage = options.voteAverageMin,
            maxVoteAverage = options.voteAverageMax,
            minVoteCount = options.voteCountMin,
            withGenres = if (options.withGenresIds.isNotEmpty()) {
                val operator = if (options.isWithGenresOperatorOR) "|" else ","
                options.withGenresIds.joinToString(separator = operator)
            } else {
                null
            },
            withoutGenres = if (options.withoutGenresIds.isNotEmpty()) {
                options.withoutGenresIds.joinToString(separator = ",")
            } else {
                null
            },
            sortBy = options.sortBy.serverName,
            releaseDateFrom = if (options.releaseDateFrom != null) {
                options.releaseDateFrom!!.toString()
            } else {
                null
            },
            releaseDateUntil = if (options.releaseDateUntil != null) {
                options.releaseDateUntil!!.toString()
            } else {
                null
            },
            withCompanies = if (options.withCompanies.isNotEmpty()) {
                val operator = if (options.isWithCompaniesOperatorOR) "|" else ","
                options.withCompanies.joinToString(separator = operator)
            } else {
                null
            },
            withoutCompanies = if (options.withoutCompanies.isNotEmpty()) {
                options.withoutCompanies.joinToString(separator = ",")
            } else {
                null
            },
            withRuntimeMinimum = if (runtimeMin != null && runtimeMin > 0) {
                runtimeMin
            } else {
                null
            },
            withRuntimeMaximum = if (runtimeMax != null && runtimeMax > 0) {
                runtimeMax
            } else {
                null
            },
            withKeywords = if (options.withKeywords.isNotEmpty()) {
                val operator = if (options.isWithKeywordsOperatorOR) "|" else ","
                options.withKeywords.joinToString(separator = operator)
            } else {
                null
            },
            withoutKeywords = if (options.withoutKeywords.isNotEmpty()) {
                options.withoutKeywords.joinToString(separator = ",")
            } else {
                null
            },
            withOriginCountry = options.withOriginCountry,
            certification = options.certification.joinToString(separator = "|"),
            certificationCountry = options.certificationCountry,
        )
        return response.successOrError()
    }

}
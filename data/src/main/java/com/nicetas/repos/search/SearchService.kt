package com.nicetas.repos.search

import com.nicetas.models.movies.MoviesList
import com.nicetas.models.movies.companies.SearchCompanies
import com.nicetas.models.movies.keywords.SearchKeywords
import io.reactivex.rxjava3.core.Single
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface SearchService {

    @GET("discover/movie")
    fun discoverMovie(
        @Query("api_key") apiKey: String,
        @Query("page") page: Int,
        @Query("language") language: String,
        @Query("vote_count.gte") minVoteCount: Int?,
        @Query("vote_average.gte") minVoteAverage: Double?,
        @Query("vote_average.lte") maxVoteAverage: Double?,
        @Query("with_genres") withGenres: String?,
        @Query("without_genres") withoutGenres: String?,
        @Query("sort_by") sortBy: String,
        @Query("primary_release_date.gte") releaseDateFrom: String?,
        @Query("primary_release_date.lte") releaseDateUntil: String?,
        @Query("with_companies") withCompanies: String?,
        @Query("without_companies") withoutCompanies: String?,
        @Query("with_runtime.gte") withRuntimeMinimum: Int?,
        @Query("with_runtime.lte") withRuntimeMaximum: Int?,
        @Query("with_keywords") withKeywords: String?,
        @Query("without_keywords") withoutKeywords: String?,
        @Query("with_origin_country") withOriginCountry: String?,
        @Query("certification") certification: String?,
        @Query("certification_country") certificationCountry: String?,
    ): Single<Response<MoviesList>>

    @GET("search/movie")
    fun searchMovie(
        @Query("api_key") apiKey: String,
        @Query("query") query: String,
        @Query("page") page: Int,
        @Query("language") language: String?,
    ): Single<Response<MoviesList>>

    @GET("search/keyword")
    fun searchKeyword(
        @Query("api_key") apiKey: String,
        @Query("query") query: String,
        @Query("page") page: Int,
    ): Single<Response<SearchKeywords>>

    @GET("search/company")
    fun searchCompany(
        @Query("api_key") apiKey: String,
        @Query("query") query: String,
        @Query("page") page: Int,
    ): Single<Response<SearchCompanies>>

    /*
    @GET("search/person")
    fun searchPerson(
        @Query("api_key") apiKey: String,
        @Query("query") query: String,
        @Query("page") page: Int,
        @Query("language") language: String?,
    ): Single<SearchPersonList>

    @GET("search/multi")
    fun searchMulti(
        @Query("api_key") apiKey: String,
        @Query("query") query: String,
        @Query("page") page: Int,
        @Query("language") language: String?,
    ): Single<MultiSearchList>
     */

}
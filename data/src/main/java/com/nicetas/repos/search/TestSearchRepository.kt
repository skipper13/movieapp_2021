package com.nicetas.repos.search

import android.content.Context
import com.nicetas.GsonProvider
import com.nicetas.prefs.Prefs
import com.nicetas.database.DatabaseHolder
import com.nicetas.models.movies.MoviesList
import com.nicetas.readAssetsFile
import com.nicetas.repos.network.RetrofitHolder
import com.nicetas.repos.network.SuccessResponse
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.internal.operators.single.SingleFromCallable

class TestSearchRepository(
    private val appContext: Context,
    retrofit: RetrofitHolder,
    db: DatabaseHolder,
    private val gson: GsonProvider,
    private val prefs: Prefs,
) : SearchRepository(
    appContext = appContext,
    retrofit = retrofit,
    db = db,
    gson = gson,
    prefs = prefs,
) {

    override fun searchMovie(query: String, page: Int): Single<SuccessResponse<MoviesList>> {
        return SingleFromCallable {
            val fileName = if (query.contains("test")) {
                "responses/search/search_movie_page_${page}.json"
            } else {
                "responses/search/search_movie_empty.json"
            }
            val response = appContext.readAssetsFile(fileName)
            val body = gson.fromJson<MoviesList>(response)
            SuccessResponse.createTest(body)
        }
    }

    override fun discoverMovie(page: Int): Single<SuccessResponse<MoviesList>> {
        return SingleFromCallable {
            val language = when (language()) {
                "ru" -> "ru"
                else -> "en"
            }
            val response =
                appContext.readAssetsFile("responses/search/discover_movie_page_${page}_$language.json")
            val body = gson.fromJson<MoviesList>(response)
            SuccessResponse.createTest(body)
        }
    }

}
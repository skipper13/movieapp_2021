package com.nicetas.repos.search

enum class SortBy(val type: String, val isDesc: Boolean) {

    POPULARITY_DESC(type = "popularity", isDesc = true),
    PRIMARY_RELEASE_DATE_DESC(type = "primary_release_date", isDesc = true),
    VOTE_AVERAGE_DESC(type = "vote_average", isDesc = true),
    VOTE_COUNT_DESC(type = "vote_count", isDesc = true),
    REVENUE_DESC(type = "revenue", isDesc = true),
    TITLE_DESC(type = "title", isDesc = true),

    POPULARITY_ASC(type = "popularity", isDesc = false),
    PRIMARY_RELEASE_DATE_ASC(type = "primary_release_date", isDesc = false),
    VOTE_AVERAGE_ASC(type = "vote_average", isDesc = false),
    VOTE_COUNT_ASC(type = "vote_count", isDesc = false),
    REVENUE_ASC(type = "revenue", isDesc = false),
    TITLE_ASC(type = "title", isDesc = false);

    val serverName: String
        get() {
            val sort = if (isDesc) "desc" else "asc"
            return "$type.$sort"
        }

    companion object {
        fun parse(value: String?): SortBy {
            return entries.firstOrNull { it.serverName == value } ?: POPULARITY_DESC
        }
    }

}
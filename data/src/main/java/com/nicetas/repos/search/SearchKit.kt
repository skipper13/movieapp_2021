package com.nicetas.repos.search

import com.nicetas.database.search.SearchKitEntity
import java.time.LocalDate

class SearchKit(
    var id: Int = 0,
    var iconId: Int = 0,
    var title: String = "",

    var withGenresIds: HashSet<Int> = hashSetOf(),
    var isWithGenresOperatorOR: Boolean = false,
    var withoutGenresIds: HashSet<Int> = hashSetOf(),

    var voteCountMin: Int = 300,
    var voteAverageMin: Double? = null,
    var voteAverageMax: Double? = null,
    var releaseDateFrom: LocalDate? = null,
    var releaseDateUntil: LocalDate? = null,
    var sortBy: SortBy = SortBy.POPULARITY_DESC,
    var withOriginCountry: String? = null, // iso_3166_1  "US"
    var certification: HashSet<String> = hashSetOf(),
    var certificationCountry: String? = null,

    var withRuntimeMinimum: Int? = null,
    var withRuntimeMaximum: Int? = null,

    var withCompanies: HashSet<Int> = hashSetOf(),
    var isWithCompaniesOperatorOR: Boolean = true,
    var withoutCompanies: HashSet<Int> = hashSetOf(),

    var withKeywords: HashSet<Int> = hashSetOf(),
    var isWithKeywordsOperatorOR: Boolean = true,
    var withoutKeywords: HashSet<Int> = hashSetOf(),
) {
    fun update(entity: SearchKitEntity) {
        id = entity.id
        iconId = entity.iconId
        title = entity.title

        withGenresIds = entity.withGenresIds
        isWithGenresOperatorOR = entity.isWithGenresOperatorOR
        withoutGenresIds = entity.withoutGenresIds

        voteCountMin = entity.voteCountMin
        voteAverageMin = entity.voteAverageMin
        voteAverageMax = entity.voteAverageMax
        releaseDateFrom = entity.releaseDateFrom
        releaseDateUntil = entity.releaseDateUntil
        sortBy = entity.sortBy
        withOriginCountry = entity.withOriginCountry
        certification = entity.certification
        certificationCountry = entity.certificationCountry

        withRuntimeMinimum = entity.withRuntimeMinimum
        withRuntimeMaximum = entity.withRuntimeMaximum

        withKeywords = entity.withKeywords
        isWithKeywordsOperatorOR = entity.isWithKeywordsOperatorOR
        withoutKeywords = entity.withoutKeywords

        withCompanies = entity.withCompanies
        isWithCompaniesOperatorOR = entity.isWithCompaniesOperatorOR
        withoutCompanies = entity.withoutCompanies
    }

    fun toEntity(): SearchKitEntity {
        return SearchKitEntity(
            title = title,
            iconId = iconId,
            withGenresIds = withGenresIds,
            isWithGenresOperatorOR = isWithGenresOperatorOR,
            withoutGenresIds = withoutGenresIds,
            voteCountMin = voteCountMin,
            voteAverageMin = voteAverageMin,
            voteAverageMax = voteAverageMax,
            releaseDateFrom = releaseDateFrom,
            releaseDateUntil = releaseDateUntil,
            sortBy = sortBy,
            withOriginCountry = withOriginCountry,
            certification = certification,
            certificationCountry = certificationCountry,
            withRuntimeMinimum = withRuntimeMinimum,
            withRuntimeMaximum = withRuntimeMaximum,
            withCompanies = withCompanies,
            isWithCompaniesOperatorOR = isWithCompaniesOperatorOR,
            withoutCompanies = withoutCompanies,
            withKeywords = withKeywords,
            isWithKeywordsOperatorOR = isWithKeywordsOperatorOR,
            withoutKeywords = withoutKeywords,
        )
    }

}
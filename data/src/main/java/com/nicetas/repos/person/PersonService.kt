package com.nicetas.repos.person

import com.nicetas.models.persons.Person
import com.nicetas.models.persons.images.PersonImages
import io.reactivex.rxjava3.core.Single
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface PersonService {

    @GET("person/{id}")
    fun getActorInfo(
        @Path(value = "id", encoded = false) personId: Int,
        @Query("api_key") apiKey: String,
        @Query("language") language: String?,
        @Query("append_to_response") appendToResponse: String?,
    ): Single<Response<Person>>

    @GET("person/{id}/images")
    fun getActorImages(
        @Path(value = "id", encoded = false) personId: Int,
        @Query("api_key") apiKey: String,
    ): Single<Response<PersonImages>>

    /*
    @GET("person/{id}/movie_credits")
    fun getActorMovieCredits(
        @Path(value = "id", encoded = false) personId: Int,
        @Query("api_key") apiKey: String,
        @Query("language") language: String?,
    ): Single<Response<PersonCredits>>
     */

}
package com.nicetas.repos.person

import android.content.Context
import com.nicetas.GsonProvider
import com.nicetas.models.persons.Person
import com.nicetas.models.persons.images.PersonImages
import com.nicetas.readAssetsFile
import com.nicetas.repos.network.RetrofitHolder
import com.nicetas.repos.network.SuccessResponse
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.internal.operators.single.SingleFromCallable

class TestPersonRepository(
    private val appContext: Context,
    retrofit: RetrofitHolder,
    private val gson: GsonProvider,
) : PersonRepository(
    retrofit = retrofit,
) {

    /*
     * person_56322.json (Amy Poehler, "Joy" in "Inside out")
     * person_1087262.json (Leah Lewis, "Ember Lumen" in "Elemental")
     */
    override fun loadActorInfo(actorId: Int): Single<SuccessResponse<Person>> {
        return SingleFromCallable {
            val response = appContext.readAssetsFile("responses/person/person_$actorId.json")
            val body = gson.fromJson<Person>(response)
            SuccessResponse.createTest(body)
        }
    }

    override fun loadActorImages(actorId: Int): Single<SuccessResponse<PersonImages>> {
        return SingleFromCallable {
            val response = appContext.readAssetsFile("responses/person/person_${actorId}_images.json")
            val body = gson.fromJson<PersonImages>(response)
            SuccessResponse.createTest(body)
        }
    }

    /*
    override fun loadActorMovieCredits(actorId: Int): Single<SuccessResponse<PersonCredits>> {
        return SingleFromCallable {
            val response = appContext.readAssetsFile("responses/person/person_${actorId}_credits.json")
            val body = gson.fromJson<PersonCredits>(response)
            SuccessResponse.createTest(body)
        }
    }
     */

}
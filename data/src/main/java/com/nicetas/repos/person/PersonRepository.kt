package com.nicetas.repos.person

import androidx.collection.MutableIntObjectMap
import com.nicetas.models.persons.Person
import com.nicetas.models.persons.images.PersonImages
import com.nicetas.repos.CacheByLanguage
import com.nicetas.repos.network.LanguageProvider
import com.nicetas.repos.network.RetrofitHolder
import com.nicetas.repos.network.SuccessResponse
import com.nicetas.repos.network.successOrError
import io.reactivex.rxjava3.core.Single

open class PersonRepository(
    private val retrofit: RetrofitHolder,
) {

    private val actorsCache = CacheByLanguage<Person>()
    private val actorImagesCache = MutableIntObjectMap<PersonImages>()

    private fun service() = retrofit.personService
    private fun apiKey() = retrofit.apiKey
    private fun language() = LanguageProvider.language

    fun getActorFromCache(actorId: Int): Person? {
        return actorsCache.get(actorId)
    }

    open fun loadActorInfo(actorId: Int): Single<SuccessResponse<Person>> {
        return service().getActorInfo(
            apiKey = apiKey(),
            personId = actorId,
            language = language(),
            appendToResponse = "movie_credits",
        ).successOrError()
            .map { response ->
                actorsCache.put(actorId, response.body)
                response
            }
    }

    fun getActorImagesFromCache(actorId: Int): PersonImages? {
        return actorImagesCache[actorId]
    }

    open fun loadActorImages(actorId: Int): Single<SuccessResponse<PersonImages>> {
        return service().getActorImages(
            apiKey = apiKey(),
            personId = actorId,
        ).successOrError()
            .map { response ->
                actorImagesCache.put(actorId, response.body)
                response
            }
    }

}
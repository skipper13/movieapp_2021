package com.nicetas.repos

import androidx.collection.MutableIntObjectMap
import com.nicetas.repos.network.LanguageProvider

class CacheByLanguage<T>() {

    private val cacheEn by lazy { MutableIntObjectMap<T>() }
    private val cacheFr by lazy { MutableIntObjectMap<T>() }
    private val cacheDe by lazy { MutableIntObjectMap<T>() }
    private val cacheIt by lazy { MutableIntObjectMap<T>() }
    private val cachePt by lazy { MutableIntObjectMap<T>() }
    private val cacheRu by lazy { MutableIntObjectMap<T>() }
    private val cacheSr by lazy { MutableIntObjectMap<T>() }
    private val cacheEs by lazy { MutableIntObjectMap<T>() }
    private val cacheTr by lazy { MutableIntObjectMap<T>() }

    fun get(id: Int): T? {
        return when (LanguageProvider.language) {
            "en" -> cacheEn[id]
            "fr" -> cacheFr[id]
            "de" -> cacheDe[id]
            "it" -> cacheIt[id]
            "pt" -> cachePt[id]
            "ru" -> cacheRu[id]
            "sr" -> cacheSr[id]
            "es" -> cacheEs[id]
            "tr" -> cacheTr[id]
            else -> null
        }
    }

    fun put(id: Int, value: T) {
        when (LanguageProvider.language) {
            "en" -> cacheEn[id] = value
            "fr" -> cacheFr[id] = value
            "de" -> cacheDe[id] = value
            "it" -> cacheIt[id] = value
            "pt" -> cachePt[id] = value
            "ru" -> cacheRu[id] = value
            "sr" -> cacheSr[id] = value
            "es" -> cacheEs[id] = value
            "tr" -> cacheTr[id] = value
        }
    }

}
package com.nicetas.repos.list

import com.nicetas.database.DatabaseHolder
import com.nicetas.database.lists.ListsEntity
import com.nicetas.models.lists.ListDetails
import com.nicetas.repos.network.LanguageProvider
import com.nicetas.repos.network.RetrofitHolder
import com.nicetas.repos.network.SuccessResponse
import com.nicetas.repos.network.successOrError
import com.nicetas.repos.search.SortBy
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single

open class ListRepository(
    private val retrofit: RetrofitHolder,
    private val db: DatabaseHolder,
) {

    private fun service() = retrofit.listService
    private fun apiKey() = retrofit.apiKey
    private fun language() = LanguageProvider.language

    open fun getDetails(
        sortBy: SortBy?,
        listId: Int,
        page: Int,
    ): Single<SuccessResponse<ListDetails>> {
        return service().getDetails(
            listId = listId,
            apiKey = apiKey(),
            language = language(),
            page = page,
            sortBy = sortBy?.serverName,
        ).successOrError()
    }

    open fun getLists(): Single<List<ListsEntity>> {
        return db.room.listsDao().getAll()
    }

    open fun isInLists(listId: Int): Single<Boolean> {
        return db.room.listsDao().isInLists(listId)
    }

    fun insertList(list: ListsEntity): Completable {
        return db.room.listsDao().insert(list)
    }

    open fun updateList(list: ListsEntity): Completable {
        return db.room.listsDao().update(list)
    }

    fun deleteList(listId: Int): Completable {
        return db.room.listsDao().delete(listId)
    }

}
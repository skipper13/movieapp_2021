package com.nicetas.repos.list

import android.content.Context
import com.nicetas.GsonProvider
import com.nicetas.database.DatabaseHolder
import com.nicetas.database.lists.ListsEntity
import com.nicetas.models.lists.ListDetails
import com.nicetas.readAssetsFile
import com.nicetas.repos.network.RetrofitHolder
import com.nicetas.repos.network.SuccessResponse
import com.nicetas.repos.search.SortBy
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.internal.operators.single.SingleFromCallable

class TestListRepository(
    private val appContext: Context,
    retrofit: RetrofitHolder,
    db: DatabaseHolder,
    private val gson: GsonProvider,
) : ListRepository(
    retrofit = retrofit,
    db = db,
) {

    private val testListId = 12345678

    override fun getDetails(
        sortBy: SortBy?,
        listId: Int,
        page: Int,
    ): Single<SuccessResponse<ListDetails>> {
        return SingleFromCallable {
            val fileName = "responses/lists/list_${listId}_page_${page}.json"
            val response = appContext.readAssetsFile(fileName)
            val body = gson.fromJson<ListDetails>(response)
            SuccessResponse.createTest(body)
        }
    }

    override fun getLists(): Single<List<ListsEntity>> {
        val item = ListsEntity(
            id = testListId,
            backdropPath = "/9PqD3wSIjntyJDBzMNuxuKHwpUD.jpg",
            name = "Animation",
            itemCount = 183,
        )
        return Single.just(listOf(item))
    }

    override fun isInLists(listId: Int): Single<Boolean> {
        return if (listId == testListId) {
            Single.just(true)
        } else {
            super.isInLists(listId)
        }
    }

    override fun updateList(list: ListsEntity): Completable {
        return if (list.id == testListId) {
            Completable.complete()
        } else {
            super.updateList(list)
        }
    }

}
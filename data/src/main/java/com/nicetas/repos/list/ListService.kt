package com.nicetas.repos.list

import com.nicetas.models.lists.ListDetails
import io.reactivex.rxjava3.core.Single
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ListService {

    @GET("https://api.themoviedb.org/4/list/{id}")
    fun getDetails(
        @Path(value = "id", encoded = false) listId: Int,
        @Query("page") page: Int,
        @Query("sort_by") sortBy: String?,
        @Query("api_key") apiKey: String,
        @Query("language") language: String,
    ): Single<Response<ListDetails>>

//    @POST("https://api.themoviedb.org/4/list/{id}/items")
//    fun addItems(
//        @Path(value = "id", encoded = false) listId: Int,
//        @Body body: AddItemsToListBody,
//    ): Single<Response<AddItemsToListResponse>>

}
package com.nicetas.repos.genre

import android.content.Context
import com.nicetas.GsonProvider
import com.nicetas.prefs.Prefs
import com.nicetas.models.genres.Genre
import com.nicetas.models.genres.Genres
import com.nicetas.readAssetsFile
import com.nicetas.repos.network.RetrofitHolder
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.internal.operators.single.SingleFromCallable

class TestGenresRepository(
    private val appContext: Context,
    retrofit: RetrofitHolder,
    private val gson: GsonProvider,
    prefs: Prefs,
) : GenresRepository(
    appContext = appContext,
    retrofit = retrofit,
    gson = gson,
    prefs = prefs,
) {

    override fun getGenres(): Single<List<Genre>> {
        return SingleFromCallable {
            val language = when (language()) {
                "ru" -> "ru"
                "de" -> "de"
                else -> "en"
            }
            val response = appContext.readAssetsFile("responses/genres/genres_$language.json")
            gson.fromJson<Genres>(response).genres
        }
    }

}
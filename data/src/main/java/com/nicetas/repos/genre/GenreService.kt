package com.nicetas.repos.genre

import com.nicetas.models.genres.Genres
import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface GenreService {

    @GET("genre/movie/list")
    fun getMoviesGenres(
        @Query("api_key") apiKey: String,
        @Query("language") language: String?,
    ): Single<Genres>

}
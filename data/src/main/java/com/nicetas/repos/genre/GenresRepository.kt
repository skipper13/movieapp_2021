package com.nicetas.repos.genre

import android.content.Context
import com.nicetas.GsonProvider
import com.nicetas.models.genres.Genre
import com.nicetas.prefs.Prefs
import com.nicetas.prefs.PrefsKey
import com.nicetas.repos.network.LanguageProvider
import com.nicetas.repos.network.RetrofitHolder
import io.reactivex.rxjava3.core.Single

open class GenresRepository(
    private val appContext: Context,
    private val gson: GsonProvider,
    private val prefs: Prefs,
    private val retrofit: RetrofitHolder,
) {

    private fun service() = retrofit.genreService
    private fun apiKey() = retrofit.apiKey
    protected fun language() = LanguageProvider.language

    open fun getGenres(): Single<List<Genre>> {
        return Single.fromCallable {
            val json = prefs.getString(PrefsKey.Genres, null)
            if (json != null) gson.fromJson<List<Genre>>(json) else emptyList()
        }
            .flatMap { localGenres ->
                if (localGenres.isNotEmpty()) {
                    return@flatMap Single.just(localGenres)
                }
                service().getMoviesGenres(apiKey = apiKey(), language = language())
                    .map { serverGenres ->
                        val genres = serverGenres.genres ?: emptyList()
                        if (genres.isNotEmpty()) {
                            prefs.editGenres()
                                .putString(PrefsKey.Genres, gson.toJson(genres))
                                .commit()
                        }
                        genres
                    }
            }
    }

}
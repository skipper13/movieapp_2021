package com.nicetas.repos.network

import android.annotation.SuppressLint
import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import android.os.Build
import androidx.core.content.ContextCompat.getSystemService

// https://developer.android.com/training/monitoring-device-state/connectivity-status-type

class Connectivity(appContext: Context) {

    companion object {
        var isInternetAvailable = true
            private set
    }

    interface Callback {
        fun onAvailable(network: Network)
        fun onLost(network: Network)
    }

    private val connectivityManager =
        getSystemService(appContext, ConnectivityManager::class.java) as ConnectivityManager

    private val listeners = ArrayList<Callback>()
    private var wasInit = false

    private val networkCallback = object : ConnectivityManager.NetworkCallback() {
        override fun onAvailable(network: Network) {
            isInternetAvailable = true
            for (listener in listeners) {
                listener.onAvailable(network)
            }
        }

        override fun onLost(network: Network) {
            isInternetAvailable = false
            for (listener in listeners) {
                listener.onLost(network)
            }
        }
    }

    fun init() {
        if (wasInit) {
            return
        }
        wasInit = true

        isInternetAvailable = isInternetConnected()

        val networkRequest = NetworkRequest.Builder()
            .addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
            .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
            .addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR)
            .build()

        connectivityManager.requestNetwork(networkRequest, networkCallback)
    }

    fun addListener(callback: Callback) {
        listeners.add(callback)
    }

    @SuppressLint("MissingPermission") // Actually, permission is specified.
    private fun isInternetConnected(): Boolean {
        val cm = connectivityManager
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            cm.activeNetwork != null && cm.getNetworkCapabilities(cm.activeNetwork) != null
        } else {
            cm.activeNetworkInfo != null && cm.activeNetworkInfo!!.isConnectedOrConnecting
        }
    }

}
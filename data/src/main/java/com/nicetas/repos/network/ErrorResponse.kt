package com.nicetas.repos.network

class ErrorResponse(
    val responseCode: Int,
    val serverCode: Int,
    errorMessage: String,
) : RuntimeException(errorMessage)
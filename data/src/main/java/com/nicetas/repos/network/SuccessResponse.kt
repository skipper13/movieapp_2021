package com.nicetas.repos.network

import retrofit2.Response

class SuccessResponse<T> private constructor(
    val body: T,
    val code: Int,
    val message: String,
) {
    companion object {
        fun <T> create(response: Response<T>): SuccessResponse<T> {
            return SuccessResponse(
                body = response.body()!!,
                code = response.code(),
                message = response.message(),
            )
        }
        fun <T> createTest(body: T): SuccessResponse<T> {
            return SuccessResponse(
                body = body,
                code = 200,
                message = "ok",
            )
        }
    }
}
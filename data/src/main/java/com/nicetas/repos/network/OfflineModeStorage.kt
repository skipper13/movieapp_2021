package com.nicetas.repos.network

import com.nicetas.prefs.Prefs
import com.nicetas.prefs.PrefsKey

class OfflineModeStorage(
    private val prefs: Prefs,
) {

    companion object {
        var isOfflineMode = false
            private set
    }

    fun init() {
        isOfflineMode = prefs.getBoolean(PrefsKey.IsOfflineMode, false)
    }

    fun updateOfflineMode(isOffline: Boolean) {
        isOfflineMode = isOffline

        prefs.edit().commitIO {
            putBoolean(PrefsKey.IsOfflineMode, isOffline)
        }
    }

}
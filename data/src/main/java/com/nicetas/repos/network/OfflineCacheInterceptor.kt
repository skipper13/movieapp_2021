package com.nicetas.repos.network

import okhttp3.CacheControl
import okhttp3.Interceptor
import okhttp3.Interceptor.*
import okhttp3.Response
import java.io.IOException

class OfflineCacheInterceptor() : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Chain): Response {
        val builder = chain.request().newBuilder()

        if (Connectivity.isInternetAvailable.not() || OfflineModeStorage.isOfflineMode) {
            builder.cacheControl(CacheControl.FORCE_CACHE)
        }

        return chain.proceed(builder.build())
    }

}
package com.nicetas.repos.network

import io.reactivex.rxjava3.core.Single
import org.json.JSONObject
import retrofit2.Response


fun <T> Single<Response<T>>.successOrError(): Single<SuccessResponse<T>> {
    return this.flatMap { response ->
        if (response.isSuccessful) {
            Single.just(SuccessResponse.create(response))
        } else {
            val text = response.errorBody()!!.string()
            if (text.isNotEmpty() && text[0] == '{') {
                val errorJson = JSONObject(text)
                val exception = ErrorResponse(
                    responseCode = response.code(),
                    serverCode = errorJson.optInt("status_code", -1),
                    errorMessage = errorJson.optString("status_message", response.message()),
                )
                Single.error<SuccessResponse<T>>(exception)
            } else {
                val exception = ErrorResponse(
                    responseCode = response.code(),
                    serverCode = -1,
                    errorMessage = response.message(),
                )
                Single.error<SuccessResponse<T>>(exception)
            }
        }
    }
}

// https://developer.themoviedb.org/docs/errors

/* 401 {
      "success": false
      "status_code": 3
      "status_message": "Authentication failed: You do not have permissions to access the service."
    }
*/
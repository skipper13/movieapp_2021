package com.nicetas.repos.network

import android.content.Context
import com.chuckerteam.chucker.api.ChuckerInterceptor
import com.nicetas.repos.configuration.ConfigService
import com.nicetas.repos.genre.GenreService
import com.nicetas.repos.list.ListService
import com.nicetas.repos.movie.MovieService
import com.nicetas.repos.person.PersonService
import com.nicetas.repos.search.SearchService
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitHolder(
    appContext: Context,
    isDebug: Boolean,
) {
    companion object {
        private const val API_URL = "https://api.themoviedb.org/3/"
    }

    private var retrofit: Retrofit

    val apiKey = "95a363ee61e43ce245060c9f77aff160"

    init {
        val cache = Cache(
            directory = appContext.cacheDir,
            maxSize = 50L * 1024L * 1024L // 50 MiB
        )
        val okHttpBuilder = OkHttpClient.Builder()
            .cache(cache)
            .addInterceptor(OfflineCacheInterceptor())

        if (isDebug) {
            val loggingInterceptor = HttpLoggingInterceptor().apply {
                level = HttpLoggingInterceptor.Level.BASIC
            }
            okHttpBuilder.addInterceptor(loggingInterceptor)

            val chuckerInterceptor = ChuckerInterceptor.Builder(appContext)
                .build()

            okHttpBuilder.addInterceptor(chuckerInterceptor)
        }

        retrofit = Retrofit.Builder()
            .baseUrl(API_URL)
            .client(okHttpBuilder.build())
            .validateEagerly(isDebug)
            .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    val searchService: SearchService by lazy { retrofit.create(SearchService::class.java) }
    val movieService: MovieService by lazy { retrofit.create(MovieService::class.java) }
    val personService: PersonService by lazy { retrofit.create(PersonService::class.java) }
    val listService: ListService by lazy { retrofit.create(ListService::class.java) }
    val genreService: GenreService by lazy { retrofit.create(GenreService::class.java) }
    val configService: ConfigService by lazy { retrofit.create(ConfigService::class.java) }

}
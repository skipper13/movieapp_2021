package com.nicetas.repos.configuration

import android.content.Context
import androidx.collection.MutableObjectList
import androidx.collection.ObjectList
import com.nicetas.GsonProvider
import com.nicetas.models.configuration.ConfigurationTmdb
import com.nicetas.models.configuration.CountryTmdb
import com.nicetas.models.extensions.isNotBlankF
import com.nicetas.prefs.Prefs
import com.nicetas.prefs.PrefsKey
import com.nicetas.readAssetsFile
import com.nicetas.repos.network.LanguageProvider
import com.nicetas.repos.network.RetrofitHolder
import com.nicetas.repos.network.successOrError
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.internal.operators.single.SingleFromCallable

open class ConfigRepository(
    private val appContext: Context,
    private val prefs: Prefs,
    private val gson: GsonProvider,
    private val retrofit: RetrofitHolder,
) {

    private val countries = MutableObjectList<CountryTmdb>(0)
    private var countriesLanguage = ""

    private fun service() = retrofit.configService
    private fun apiKey() = retrofit.apiKey
    private fun language() = LanguageProvider.language

    fun getOrLoadImgBaseUrl(): Single<String> {
        return Single.fromCallable {
            prefs.getString(PrefsKey.ImgBaseUrl, "")!!
        }.flatMap { url ->
            if (url.isNotBlankF()) {
                Single.just(url)
            } else {
                loadConfig().map { it.images.secureBaseUrl }
            }
        }
    }

    open fun loadConfig(): Single<ConfigurationTmdb> {
        return service().getConfiguration(apiKey = apiKey())
            .successOrError()
            .map { response ->
                val config = response.body
                prefs.edit()
                    .putString(PrefsKey.ImgBaseUrl, config.images.secureBaseUrl)
                    .commit()
                config
            }
    }

    fun findCountryName(iso_3166_1: String /* "US" */): String? {
        for (i in 0 until countries.size) {
            val country = countries[i]
            if (country.iso_3166_1 == iso_3166_1) {
                return country.nativeName
            }
        }
        return null
    }

    fun getOrReadCountries(): Single<ObjectList<CountryTmdb>> {
        return SingleFromCallable {
            if (countriesLanguage != language()) {
                val fileName = when (language()) {
                    "fr" -> "responses/countries/countries_fr.json"
                    "de" -> "responses/countries/countries_de.json"
                    "it" -> "responses/countries/countries_it.json"
                    "pt" -> "responses/countries/countries_pt.json"
                    "ru" -> "responses/countries/countries_ru.json"
                    "sr" -> "responses/countries/countries_sr.json"
                    "es" -> "responses/countries/countries_es.json"
                    "tr" -> "responses/countries/countries_tr.json"
                    else -> "responses/countries/countries_en.json"
                }
                val json = appContext.readAssetsFile(fileName)
                val countries: List<CountryTmdb> = gson.fromJson(json)
                this.countries.clear()
                this.countries.addAll(countries)
                countriesLanguage = language()
            }
            countries
        }
    }

}
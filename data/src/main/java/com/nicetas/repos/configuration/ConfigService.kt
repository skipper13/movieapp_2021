package com.nicetas.repos.configuration

import com.nicetas.models.configuration.ConfigurationTmdb
import com.nicetas.models.configuration.CountryTmdb
import io.reactivex.rxjava3.core.Single
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ConfigService {

    @GET("configuration")
    fun getConfiguration(
        @Query("api_key") apiKey: String,
    ): Single<Response<ConfigurationTmdb>>

    @GET("configuration/countries")
    fun getCountries(
        @Query("api_key") apiKey: String,
        @Query("language") language: String?,
    ): Single<Response<CountryTmdb>>

}
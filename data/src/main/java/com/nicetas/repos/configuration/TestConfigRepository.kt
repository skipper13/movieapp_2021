package com.nicetas.repos.configuration

import android.content.Context
import com.nicetas.GsonProvider
import com.nicetas.prefs.Prefs
import com.nicetas.models.configuration.ConfigurationTmdb
import com.nicetas.readAssetsFile
import com.nicetas.repos.network.RetrofitHolder
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.internal.operators.single.SingleFromCallable
import java.util.concurrent.TimeUnit

class TestConfigRepository(
    private val appContext: Context,
    retrofit: RetrofitHolder,
    private val gson: GsonProvider,
    prefs: Prefs,
) : ConfigRepository(
    appContext = appContext,
    retrofit = retrofit,
    prefs = prefs,
    gson = gson,
) {

    override fun loadConfig(): Single<ConfigurationTmdb> {
        return SingleFromCallable {
            val response = appContext.readAssetsFile("responses/configuration.json")
            val config = gson.fromJson<ConfigurationTmdb>(response)
            config
        }.delay(10, TimeUnit.SECONDS)
    }

}
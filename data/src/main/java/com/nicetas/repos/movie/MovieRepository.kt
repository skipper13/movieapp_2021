package com.nicetas.repos.movie

import androidx.collection.MutableIntObjectMap
import com.nicetas.database.DatabaseHolder
import com.nicetas.database.watchlist.MovieToWatchEntity
import com.nicetas.models.movies.Movie
import com.nicetas.models.movies.extended.credits.MovieCredits
import com.nicetas.models.movies.extended.images.MovieImages
import com.nicetas.models.movies.extended.videos.MovieVideos
import com.nicetas.repos.CacheByLanguage
import com.nicetas.repos.network.LanguageProvider
import com.nicetas.repos.network.RetrofitHolder
import com.nicetas.repos.network.SuccessResponse
import com.nicetas.repos.network.successOrError
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single

open class MovieRepository(
    private val retrofit: RetrofitHolder,
    private val db: DatabaseHolder,
) {

    private val moviesCache = CacheByLanguage<Movie>()
    private val creditsCache = MutableIntObjectMap<MovieCredits>()
    private val imagesCache = MutableIntObjectMap<MovieImages>()
    private val videosCache = MutableIntObjectMap<MovieVideos>()

    private fun service() = retrofit.movieService
    private fun apiKey() = retrofit.apiKey
    private fun language() = LanguageProvider.language

    fun getMovieFromCache(movieId: Int): Movie? {
        return moviesCache.get(movieId)
    }

    open fun loadMovie(movieId: Int): Single<SuccessResponse<Movie>> {
        return service().getMovie(
            apiKey = apiKey(),
            language = language(),
            movieId = movieId,
            appendToResponse = "keywords,external_ids",
        ).successOrError()
            .map { response ->
                moviesCache.put(movieId, response.body)
                response
            }
    }

    fun getMovieCreditsFromCache(movieId: Int): MovieCredits? {
        return creditsCache[movieId]
    }

    open fun loadMovieCredits(movieId: Int): Single<SuccessResponse<MovieCredits>> {
        return service().getMovieCredits(
            apiKey = apiKey(),
            movieId = movieId,
        ).successOrError()
            .map { response ->
                creditsCache[movieId] = response.body
                response
            }
    }

    fun getMovieImagesFromCache(movieId: Int): MovieImages? {
        return imagesCache[movieId]
    }

    open fun loadMovieImages(movieId: Int): Single<SuccessResponse<MovieImages>> {
        return service().getMovieImages(
            apiKey = apiKey(),
            movieId = movieId,
        ).successOrError()
            .map { response ->
                imagesCache[movieId] = response.body
                response
            }
    }

    fun getMovieVideosFromCache(movieId: Int): MovieVideos? {
        return videosCache[movieId]
    }

    open fun loadMovieVideos(movieId: Int): Single<SuccessResponse<MovieVideos>> {
        return service().getMovieVideos(
            apiKey = apiKey(),
            movieId = movieId,
        ).successOrError()
            .map { response ->
                videosCache[movieId] = response.body
                response
            }
    }

    fun getWatchList(): Single<List<MovieToWatchEntity>> {
        return db.room.watchListDao().getAll()
    }

    fun isInWatchList(movieId: Int): Single<Boolean> {
        return db.room.watchListDao().isInWatchList(movieId)
    }

    fun insertMovieToWatch(movie: MovieToWatchEntity): Completable {
        return db.room.watchListDao().insert(movie)
    }

    fun deleteMovieToWatch(movieId: Int): Completable {
        return db.room.watchListDao().delete(movieId)
    }

}
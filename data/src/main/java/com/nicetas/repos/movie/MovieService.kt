package com.nicetas.repos.movie

import com.nicetas.models.movies.Movie
import com.nicetas.models.movies.extended.credits.MovieCredits
import com.nicetas.models.movies.extended.images.MovieImages
import com.nicetas.models.movies.extended.videos.MovieVideos
import io.reactivex.rxjava3.core.Single
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MovieService {

    @GET("movie/{id}")
    fun getMovie(
        @Path(value = "id", encoded = false) movieId: Int,
        @Query("api_key") apiKey: String,
        @Query("language") language: String,
        @Query("append_to_response") appendToResponse: String,
    ): Single<Response<Movie>>

    @GET("movie/{id}/credits")
    fun getMovieCredits(
        @Path(value = "id", encoded = false) movieId: Int,
        @Query("api_key") apiKey: String,
    ): Single<Response<MovieCredits>>

    @GET("movie/{id}/images")
    fun getMovieImages(
        @Path(value = "id", encoded = false) movieId: Int,
        @Query("api_key") apiKey: String,
    ): Single<Response<MovieImages>>

    @GET("movie/{id}/videos")
    fun getMovieVideos(
        @Path(value = "id", encoded = false) movieId: Int,
        @Query("api_key") apiKey: String,
    ): Single<Response<MovieVideos>>

    /*
    @GET("movie/{id}/external_ids")
    fun getMovieExternalIds(
        @Path(value = "id", encoded = false) movieId: Int,
        @Query("api_key") apiKey: String,
    ): Single<Response<MovieExternalIds>>

    @GET("movie/{id}/keywords")
    fun getMovieKeywords(
        @Path(value = "id", encoded = false) movieId: Int,
        @Query("api_key") apiKey: String,
    ): Single<Response<MovieKeywords>>

    @GET("movie/{id}/similar")
    fun getSimilar(
        @Path(value = "id", encoded = false) movieId: Int,
        @Query("api_key") apiKey: String,
        @Query("page") page: Int,
        @Query("language") language: String,
    ): Single<MoviesList>

    @GET("movie/popular")
    fun getPopular(
        @Query("api_key") apiKey: String,
        @Query("page") page: Int,
        @Query("language") language: String,
    ): Single<MoviesList>

    @GET("movie/top_rated")
    fun getTopRated(
        @Query("api_key") apiKey: String,
        @Query("page") page: Int,
        @Query("language") language: String,
    ): Single<MoviesList>

    @GET("movie/now_playing")
    fun getNowPlaying(
        @Query("api_key") apiKey: String,
        @Query("page") page: Int,
        @Query("language") language: String,
    ): Single<MoviesList>

    @GET("movie/upcoming")
    fun getUpcoming(
        @Query("api_key") apiKey: String,
        @Query("page") page: Int,
        @Query("language") language: String,
    ): Single<MoviesList>
     */

}
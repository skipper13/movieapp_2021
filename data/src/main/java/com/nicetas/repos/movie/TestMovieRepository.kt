package com.nicetas.repos.movie

import android.content.Context
import com.nicetas.GsonProvider
import com.nicetas.database.DatabaseHolder
import com.nicetas.models.movies.Movie
import com.nicetas.models.movies.extended.credits.MovieCredits
import com.nicetas.models.movies.extended.images.MovieImages
import com.nicetas.models.movies.extended.videos.MovieVideos
import com.nicetas.readAssetsFile
import com.nicetas.repos.network.RetrofitHolder
import com.nicetas.repos.network.SuccessResponse
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.internal.operators.single.SingleFromCallable

class TestMovieRepository(
    private val appContext: Context,
    retrofit: RetrofitHolder,
    db: DatabaseHolder,
    private val gson: GsonProvider,
) : MovieRepository(
    retrofit = retrofit,
    db = db,
) {

    override fun loadMovie(movieId: Int): Single<SuccessResponse<Movie>> {
        return SingleFromCallable {
            val response = appContext.readAssetsFile("responses/movie/movie_$movieId.json")
            val body = gson.fromJson<Movie>(response)
            SuccessResponse.createTest(body)
        }
    }

    override fun loadMovieCredits(movieId: Int): Single<SuccessResponse<MovieCredits>> {
        return SingleFromCallable {
            val response = appContext.readAssetsFile("responses/movie/movie_${movieId}_credits.json")
            val body = gson.fromJson<MovieCredits>(response)
            SuccessResponse.createTest(body)
        }
    }

    override fun loadMovieImages(movieId: Int): Single<SuccessResponse<MovieImages>> {
        return SingleFromCallable {
            val response = appContext.readAssetsFile("responses/movie/movie_${movieId}_images.json")
            val body = gson.fromJson<MovieImages>(response)
            SuccessResponse.createTest(body)
        }
    }

    override fun loadMovieVideos(movieId: Int): Single<SuccessResponse<MovieVideos>> {
        return SingleFromCallable {
            val response = appContext.readAssetsFile("responses/movie/movie_${movieId}_videos.json")
            val body = gson.fromJson<MovieVideos>(response)
            SuccessResponse.createTest(body)
        }
    }

    /*
    override fun loadMovieExternalIds(movieId: Int): Single<SuccessResponse<MovieExternalIds>> {
        return SingleFromCallable {
            val response = appContext.readAssetsFile("responses/movie/movie_${movieId}_external_ids.json")
            val body = gson.fromJson<MovieExternalIds>(response)
            SuccessResponse.createTest(body)
        }
    }
     */

}
package com.nicetas.movieapp3

import android.annotation.SuppressLint
import android.app.Application
import com.nicetas.movieapp3.di.DataModule
import com.nicetas.movieapp3.di.MockSettings
import com.nicetas.movieapp3.di.appModule
import com.nicetas.movieapp3.di.viewModelsModule
import com.nicetas.repos.network.OfflineModeStorage
import org.koin.android.ext.android.inject
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class App : Application() {

    companion object {
        @SuppressLint("StaticFieldLeak")
        lateinit var instance: App
            private set
    }

    private val offlineModeStorage by inject<OfflineModeStorage>()

    override fun onCreate() {
        super.onCreate()
        instance = this
        startKoin {
            androidContext(this@App)
            modules(
                appModule,
                viewModelsModule,
                DataModule(mockSettings()).module,
            )
        }
        offlineModeStorage.init()
    }

    private fun mockSettings(): MockSettings {
        if (BuildConfig.CAN_USE_MOCK.not()) {
            return MockSettings.Builder().build()
        }
        return MockSettings.Builder()
//            .config()
//            .search()
//            .movie()
//            .person()
//            .genres()
//            .list()
            .build()
    }

}
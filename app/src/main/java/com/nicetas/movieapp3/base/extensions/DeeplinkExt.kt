package com.nicetas.movieapp3.base.extensions

import com.nicetas.models.extensions.isNotBlankF


fun String.getIdFromDeeplink(): Int {
    if (this.isNotBlankF() && this[0].isDigit()) {
        return this.takeWhile { it.isDigit() }.toInt()
    } else {
        return 0
    }
}
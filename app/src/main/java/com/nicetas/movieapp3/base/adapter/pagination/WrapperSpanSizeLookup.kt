package com.nicetas.movieapp3.base.adapter.pagination

import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.GridLayoutManager.SpanSizeLookup

internal class WrapperSpanSizeLookup(
    val wrappedSpanSizeLookup: SpanSizeLookup,
    private val layoutManager: GridLayoutManager,
    private val wrapperAdapter: WrapperAdapter,
) : SpanSizeLookup() {

    override fun getSpanSize(position: Int): Int {
        return if (wrapperAdapter.isLoadingRow(position) || wrapperAdapter.isErrorRow(position)) {
            layoutManager.spanCount
        } else {
            wrappedSpanSizeLookup.getSpanSize(position)
        }
    }

}
package com.nicetas.movieapp3.base

import android.content.ContentValues
import android.content.Context
import android.graphics.Bitmap
import android.media.MediaScannerConnection
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.MediaStore
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStream

class ImageSaveHelper {

    // https://stackoverflow.com/a/57357571
    fun saveImage(
        context: Context,
        bitmap: Bitmap,
        folderName: String,
        fileName: String
    ): Uri? {
        var fos: OutputStream? = null
        var imageFile: File? = null
        var imageUri: Uri? = null
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                val contentValues = ContentValues().apply {
                    put(MediaStore.MediaColumns.DISPLAY_NAME, fileName)
                    put(MediaStore.MediaColumns.MIME_TYPE, "image/jpeg")
                    put(
                        MediaStore.MediaColumns.RELATIVE_PATH,
                        Environment.DIRECTORY_PICTURES + File.separator + folderName
                    )
                }
                val resolver = context.contentResolver
                imageUri = resolver.insert(
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    contentValues
                )
                if (imageUri == null) {
                    Logger.d("Failed to create new MediaStore record.")
                    return null
                }
                fos = resolver.openOutputStream(imageUri)

            } else {

                val pathName = StringBuilder()
                    .append(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).path)
                    .append(File.separator)
                    .append(folderName)
                    .toString()

                val imagesDir = File(pathName)
                if (!imagesDir.exists()) {
                    imagesDir.mkdir()
                }
                imageFile = File(imagesDir, "$fileName.jpg")
                fos = FileOutputStream(imageFile)
            }
            if (fos == null || !bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos)) {
                Logger.d("Failed to save bitmap.")
                return null
            }
            fos.flush()
        } finally {
            fos?.close()
        }
        if (imageFile != null) { //pre Q
            MediaScannerConnection.scanFile(context, arrayOf(imageFile.toString()), null, null)
            imageUri = Uri.fromFile(imageFile)
        }
        return imageUri
    }

}
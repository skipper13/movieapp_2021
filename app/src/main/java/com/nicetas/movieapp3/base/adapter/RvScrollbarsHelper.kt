package com.nicetas.movieapp3.base.adapter

import android.util.AttributeSet
import android.util.Xml
import com.nicetas.movieapp3.App
import com.nicetas.movieapp3.R
import org.xmlpull.v1.XmlPullParser

/**
 * There is no way to enable scrollbars for RecyclerView in code.
 * But we can parse attribute from xml file and cache it.
 * android:scrollbars="vertical"
 */
object RvScrollbarsHelper {

    val attrs by lazy { getAttributeSet() }

    // https://stackoverflow.com/a/47925553
    private fun getAttributeSet(): AttributeSet? {
        var attr: AttributeSet? = null
        try {
            val parser: XmlPullParser =
                App.instance.resources.getXml(R.xml.attrs_scrollbars_for_recycler_view)
            try {
                parser.next()
                parser.nextTag()
            } catch (e: Exception) {
                /* no-op */
            }
            attr = Xml.asAttributeSet(parser)
            return attr
        } catch (e: Exception) {
            return attr
        }
    }

}
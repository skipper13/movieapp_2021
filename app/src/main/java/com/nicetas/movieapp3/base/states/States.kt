package com.nicetas.movieapp3.base.states

import android.content.Context
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat.getColor
import com.google.android.material.button.MaterialButton
import com.nicetas.movieapp3.R
import com.nicetas.movieapp3.base.extensions.dp

class States(private val viewState: ViewState) {

    fun createLoadView(
        contentView: View,
    ): View {
        val view = ProgressBar(contentView.context, null, android.R.attr.progressBarStyle)
        view.layoutParams = when (contentView.parent) {
            is FrameLayout -> FrameLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT)
                .apply { gravity = Gravity.CENTER }

            is RelativeLayout -> RelativeLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT)
                .apply { addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE) }

            is CoordinatorLayout -> CoordinatorLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT)
                .apply { gravity = Gravity.CENTER }

            is ConstraintLayout -> ConstraintLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT)
                .apply {
                    topToTop = ConstraintLayout.LayoutParams.PARENT_ID
                    bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID
                    verticalBias = 0.5f
                    horizontalBias = 0.5f
                }
            else -> ViewGroup.MarginLayoutParams(MATCH_PARENT, WRAP_CONTENT)
        }
        view.indeterminateDrawable.colorFilter = PorterDuffColorFilter(
            getColor(view.context, R.color.yellow_CAC683),
            PorterDuff.Mode.SRC_IN
        )
        return view
    }

    fun createLoadDelayView(
        context: Context,
        delayMs: Long,
    ): View {
        val vgRoot = FrameLayout(context)
        vgRoot.layoutParams = ViewGroup.LayoutParams(MATCH_PARENT, MATCH_PARENT)

        val progressBar = LoadDelayProgressBar(context, null, delayMs).apply {
            layoutParams = FrameLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT)
                .apply { gravity = Gravity.CENTER }
        }
        vgRoot.addView(progressBar)
        return vgRoot
    }

    fun createErrorView(
        context: Context,
    ): View {
        val vgRoot = LinearLayout(context).apply {
            layoutParams = ViewGroup.MarginLayoutParams(MATCH_PARENT, MATCH_PARENT)
            orientation = LinearLayout.VERTICAL
            gravity = Gravity.CENTER
        }

        viewState.tvError = TextView(context).apply {
            setPadding(16f.dp, 16f.dp, 16f.dp, 16f.dp)
            gravity = Gravity.CENTER
            textSize = 16f
            setTextColor(getColor(context, R.color.white))
            text = context.getString(R.string.error_state)
        }
        vgRoot.addView(viewState.tvError)

        viewState.btnRetry = MaterialButton(context).apply {
            layoutParams = LinearLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT)
            setBackgroundColor(getColor(context, R.color.yellow_CAC683))
            setRippleColorResource(R.color.grey_B6B6B6)
            setTextColor(getColor(context, R.color.black))
            isAllCaps = false
            text = context.getString(R.string.error_state_view_reload)
        }
        vgRoot.addView(viewState.btnRetry)

        return vgRoot
    }

}
package com.nicetas.movieapp3.base.states

import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.FrameLayout
import android.widget.ProgressBar
import android.widget.TextView
import androidx.collection.MutableIntObjectMap
import androidx.core.view.isVisible
import androidx.lifecycle.LifecycleOwner
import com.nicetas.movieapp3.base.extensions.onClickDebounce

class ViewState private constructor(contentView: View) {

    companion object State {
        private const val CONTENT = 1
        private const val LOAD = 2
        private const val ERROR = 3
        private const val EMPTY = 4
    }

    class Builder(private val contentView: View) {

        private val viewState = ViewState(contentView)
        private val states = States(viewState)

        /** [ProgressBar] */
        fun load(): Builder {
            val loadView = states.createLoadView(contentView)
            viewState.addState(LOAD, loadView)
            return this
        }

        /** [LoadDelayProgressBar] */
        fun loadDelay(delay: Long = LoadDelayProgressBar.DELAY_MS): Builder {
            val loadView = states.createLoadDelayView(contentView.context, delay)
            viewState.addState(LOAD, loadView)
            return this
        }

        fun error(onClick: () -> Unit): Builder {
            val errorView = states.createErrorView(contentView.context)
            viewState.addState(ERROR, errorView)
            viewState.btnRetry?.onClickDebounce { onClick() }
            return this
        }

        fun empty(view: View): Builder {
            viewState.addState(EMPTY, view)
            return this
        }

        fun state(state: Int, view: View): Builder {
            viewState.addState(state, view)
            return this
        }

        fun build() = viewState
    }

    private val states = MutableIntObjectMap<View>(4)
    private val parent: ViewGroup
    private var currentView = contentView

    /** TextView that is used when error state massage display */
    var tvError: TextView? = null

    /** Button that is used when error state is display */
    var btnRetry: Button? = null

    init {
        states[CONTENT] = contentView
        parent = contentView.parent as ViewGroup
    }

    fun addState(state: Int, view: View) {
        if (state in states) {
            throw IllegalStateException("State $state already added")
        }
        view.isVisible = false
        states[state] = view
    }

    fun content() {
        state(CONTENT)
    }

    fun load() {
        state(LOAD)
    }

    fun error(error: CharSequence) {
        tvError?.text = error
        state(ERROR)
    }

    fun empty() {
        state(EMPTY)
    }

    fun state(state: Int) {
        val target = states[state] ?: throw IllegalStateException("State $state was not added yet")

        if (target.parent != parent) {
            parent.addView(target)
        }
        if (currentView != target) {
            currentView.isVisible = false
            target.isVisible = true
            showLoadDelayIfNeed(state, target)
            currentView = target
        } else {
            // If target was gone initially and e.g. after returning from deeper fragment, then switch target visibility to visible
            target.isVisible = true
        }
    }

    /**
     * [LoadDelayProgressBar] is created in [States.createLoadDelayView].
     * Currently its parent is [FrameLayout].
     * When switch to content, visibility of [FrameLayout] changes to gone, not [LoadDelayProgressBar]'s itself.
     * If we would change the visibility of the [LoadDelayProgressBar] itself,
     * it would be displayed at the same time with the content.
     */
    private fun showLoadDelayIfNeed(state: Int, target: View) {
        if (state == LOAD && target is FrameLayout) {
            val child = target.getChildAt(0)
            if (child is LoadDelayProgressBar) {
                child.isVisible = false
                child.show()
            }
        }
    }

    fun observeLce(lce: Lce, viewLifecycleOwner: LifecycleOwner) {
        observeLce(lce, viewLifecycleOwner, null)
    }

    fun observeLce(
        lce: Lce,
        viewLifecycleOwner: LifecycleOwner,
        callback: ((Lce.State, ViewState) -> Unit)?,
    ) {
        lce.liveData.observe(viewLifecycleOwner) { state ->
            when (state) {
                is Lce.Loading -> load()
                is Lce.Content -> content()
                is Lce.Error -> error(state.message(currentView.context))
                is Lce.Empty -> empty()
                else -> callback?.invoke(state, this)
            }
        }
    }

}
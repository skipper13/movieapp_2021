package com.nicetas.movieapp3.base.extensions

import android.content.Context
import android.graphics.LinearGradient
import android.graphics.Shader
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.core.content.ContextCompat.getColor
import androidx.core.view.WindowInsetsCompat
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.nicetas.movieapp3.R


inline fun View.onClickDebounce(
    delayMs: Long = 750L,
    crossinline callback: (View?) -> Unit
) {
    setOnClickListener(object : View.OnClickListener {
        private var notClicked = true
        override fun onClick(view: View) {
            if (notClicked) {
                notClicked = false
                callback(view)
                view.postDelayed({ notClicked = true }, delayMs)
            }
        }
    })
}

fun View?.showKeyboard() {
    this ?: return
    if (requestFocus()) {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(this, InputMethodManager.SHOW_IMPLICIT)
    }
}

fun View?.hideKeyboard(clearFocus: Boolean = true) {
    this ?: return
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(windowToken, 0)
    if (clearFocus) {
        clearFocus()
    }
}


fun View.snackBarTMDB(
    text: String,
    @BaseTransientBottomBar.Duration duration: Int,
): Snackbar {
    val textColors = intArrayOf(
        getColor(context, R.color.green_90CEA1),
        getColor(context, R.color.blue_3CBEC9),
        getColor(context, R.color.blue_00B3E5),
    )
    val snackBar = Snackbar.make(this, text, duration)

    val textView = snackBar.view.findViewById<TextView>(R.id.snackbar_text)
    // https://stackoverflow.com/a/52289927
    val width = textView.paint.measureText(text)
    textView.paint.shader = LinearGradient(
        0F, 0F, width, textView.textSize,
        textColors, null, Shader.TileMode.CLAMP
    )

    snackBar.setBackgroundTint(getColor(context, R.color.grey_333333))
    snackBar.setActionTextColor(getColor(context, R.color.yellow_CAC683))
    return snackBar
}

fun WindowInsetsCompat.statusBarHeight(): Int {
    return getInsets(WindowInsetsCompat.Type.statusBars()).top
}
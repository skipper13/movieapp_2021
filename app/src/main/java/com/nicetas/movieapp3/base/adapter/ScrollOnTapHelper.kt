package com.nicetas.movieapp3.base.adapter

import android.os.Bundle
import androidx.core.view.isGone
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearSmoothScroller
import androidx.recyclerview.widget.RecyclerView

class ScrollOnTapHelper(savedInstanceState: Bundle?) {

    companion object {
        private const val MAX_ITEMS_FOR_SMOOTH = 10
        private const val STATE_SCROLL_POS_ON_TAP = "STATE_SCROLL_POS_ON_TAP"
    }

    var savedBottomPos: Int

    init {
        savedBottomPos = savedInstanceState?.getInt(STATE_SCROLL_POS_ON_TAP, 0) ?: 0
    }

    fun onSaveInstanceState(outState: Bundle) {
        outState.putInt(STATE_SCROLL_POS_ON_TAP, savedBottomPos)
    }

    fun scroll(rvContent: RecyclerView) {
        if (rvContent.isGone || rvContent.scrollState != RecyclerView.SCROLL_STATE_IDLE) {
            return
        }

        val layoutManager = rvContent.layoutManager as? GridLayoutManager
            ?: return

        val firstVisiblePos = layoutManager.findFirstCompletelyVisibleItemPosition()
            .takeIf { it != RecyclerView.NO_POSITION }
            ?: layoutManager.findFirstVisibleItemPosition()

        val lastVisiblePos = layoutManager.findLastCompletelyVisibleItemPosition()
            .takeIf { it != RecyclerView.NO_POSITION }
            ?: layoutManager.findLastVisibleItemPosition()

        when {
            firstVisiblePos == 0 -> {
                if (savedBottomPos > MAX_ITEMS_FOR_SMOOTH) {
                    rvContent.scrollToPosition(savedBottomPos)
                } else {
                    val smoothScroller = object : LinearSmoothScroller(rvContent.context) {
                        override fun getVerticalSnapPreference(): Int {
                            return SNAP_TO_ANY
                        }
                    }
                    smoothScroller.targetPosition = savedBottomPos
                    layoutManager.startSmoothScroll(smoothScroller)
                }
            }
            firstVisiblePos >= MAX_ITEMS_FOR_SMOOTH -> {
                // Don't use firstVisiblePos, otherwise on some devices will be wrong position
                savedBottomPos = lastVisiblePos
                rvContent.scrollToPosition(0)
            }
            else -> {
                // Don't use firstVisiblePos, otherwise on some devices will be wrong position
                savedBottomPos = lastVisiblePos
                val smoothScroller = object : LinearSmoothScroller(rvContent.context) {
                    override fun getVerticalSnapPreference(): Int {
                        return SNAP_TO_ANY
                    }
                }
                smoothScroller.targetPosition = 0
                layoutManager.startSmoothScroll(smoothScroller)
            }
        }
    }

}
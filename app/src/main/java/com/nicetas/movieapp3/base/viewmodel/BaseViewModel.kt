package com.nicetas.movieapp3.base.viewmodel

import androidx.annotation.CallSuper
import androidx.lifecycle.AndroidViewModel
import com.nicetas.movieapp3.App
import com.nicetas.movieapp3.base.states.Lce
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.disposables.Disposable

abstract class BaseViewModel : AndroidViewModel(App.instance) {

    protected val disposables = CompositeDisposable()
    val lce = Lce()

    operator fun CompositeDisposable.plusAssign(subscription: Disposable) {
        add(subscription)
    }

    @CallSuper
    override fun onCleared() {
        disposables.clear()
        super.onCleared()
    }

}
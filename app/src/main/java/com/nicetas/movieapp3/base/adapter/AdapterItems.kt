package com.nicetas.movieapp3.base.adapter

import androidx.collection.MutableObjectList
import androidx.collection.ObjectList
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView

/*
Inconsistency detected
https://code.google.com/p/android/issues/detail?id=77846
https://code.google.com/p/android/issues/detail?id=77232
http://stackoverflow.com/questions/30220771/recyclerview-inconsistency-detected-invalid-item-position
http://stackoverflow.com/questions/26827222/how-to-change-contents-of-recyclerview-while-scrolling
*/
class AdapterItems<Model>(
    private val adapter: RecyclerView.Adapter<RecyclerView.ViewHolder>,
) {

    val list = MutableObjectList<Model>()

    fun update(newItems: ObjectList<Model>, callback: BaseDiffUtilCallback<Model>) {
        callback.setItems(oldItems = list, newItems = newItems)
        val diffResult = DiffUtil.calculateDiff(callback)
        list.clear()
        list.addAll(newItems)
        diffResult.dispatchUpdatesTo(adapter)
    }

    fun <T : Model> add(item: T, index: Int) {
        val initialSize = list.size
        list.add(index, item)

        // We do it that way to prevent some issues. Check links from header of file.
        if (initialSize == 0) {
            // noinspection NotifyDataSetChanged
            adapter.notifyDataSetChanged()
        } else {
            adapter.notifyItemInserted(index)
        }
    }

    fun <T : Model> add(item: T): Int {
        val position = list.size
        add(item, position)
        return position
    }

    // TODO NR: 24.07.2024  does not allow to specify  <T : Model>
//    fun <T : Model> addAll(items: ObjectList<T>, index: Int)

    fun addAll(items: ObjectList<Model>, index: Int) {
        val initialSize = list.size

        if (items.isEmpty()) {
            // just notify "AdapterDataObserver" in "RecyclerPaginate" to remove loading row
            adapter.notifyItemChanged(index, -1234)
            return
        } else {
            list.addAll(index, items)
        }

        // We do it that way to prevent some issues. Check links from header of file.
        if (initialSize == 0) {
            // noinspection NotifyDataSetChanged
            adapter.notifyDataSetChanged()
        } else {
            adapter.notifyItemRangeInserted(index, items.size)
        }
    }

    fun addAll(items: ObjectList<Model>) {
        addAll(items, list.size)
    }

    fun <T : Model> replace(item: T, position: Int, payload: Any?): Model? {
        val oldItem = list.set(position, item)
        if (payload == null) {
            adapter.notifyItemChanged(position)
        } else {
            adapter.notifyItemChanged(position, payload)
        }
        return oldItem
    }

    fun <T : Model> replace(item: T, position: Int): Model? {
        return replace(item, position, null)
    }

    fun clear() {
        val size = list.size
        list.clear()
        adapter.notifyItemRangeRemoved(0, size)
    }

    fun replaceAll(items: ObjectList<Model>) {
        list.clear()
        list.addAll(items)
        // noinspection NotifyDataSetChanged
        adapter.notifyDataSetChanged()
    }

    fun remove(position: Int): Model? {
        if (position < 0 || position > list.size) {
            return null
        }
        val removedItem = list.removeAt(position)

        // We do it that way to prevent some issues. Check links from header of file.
        if (list.isEmpty()) {
            // noinspection NotifyDataSetChanged
            adapter.notifyDataSetChanged()
        } else {
            adapter.notifyItemRemoved(position)
        }
        return removedItem
    }

}
package com.nicetas.movieapp3.base.fragment

import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.annotation.ColorInt
import androidx.annotation.LayoutRes
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.nicetas.movieapp3.R
import com.nicetas.movieapp3.ui.main.MainActivity

abstract class BaseFragment(@LayoutRes layoutId: Int) : Fragment(layoutId) {

    constructor() : this(0)

    val mainActivity: MainActivity?
        get() = activity as? MainActivity

    var isBottomNavigationVisible: Boolean
        get() = mainActivity?.isBottomNavigationVisible ?: false
        set(value) {
            mainActivity?.isBottomNavigationVisible = value
        }

    var isBottomNavigationLockedHidden: Boolean
        get() = mainActivity?.isBottomNavigationLockedHidden ?: false
        set(value) {
            mainActivity?.isBottomNavigationLockedHidden = value
        }

    protected val isKeyboardOpen: Boolean
        get() = mainActivity?.isKeyboardOpen ?: false

    protected val isInMultiWindowMode: Boolean
        get() {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                return activity?.isInMultiWindowMode == true
            }
            return false
        }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindToolbar(view)
    }

    private fun bindToolbar(view: View) {
        val toolbar = view.findViewById<Toolbar>(R.id.toolbar)
        if (toolbar != null) {
            val appBarConfiguration = AppBarConfiguration(
                setOf(R.id.searchScreen, R.id.watchlistScreen,  R.id.aboutScreen)
            )
            toolbar.setupWithNavController(findNavController(), appBarConfiguration)
            mainActivity?.setSupportActionBar(toolbar)
        }
    }

    fun setToolbarTitle(title: String?) {
        mainActivity?.supportActionBar?.title = title
    }

    fun setStatusBarColor(@ColorInt color: Int) {
        mainActivity?.statusBarColor = color
    }

}
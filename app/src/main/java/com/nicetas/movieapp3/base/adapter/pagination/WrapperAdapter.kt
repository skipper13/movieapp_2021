package com.nicetas.movieapp3.base.adapter.pagination

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class WrapperAdapter(
    val wrappedAdapter: RecyclerView.Adapter<RecyclerView.ViewHolder>,
    private val loadingListItemCreator: LoadingListItemCreator,
    private val errorListItemCreator: ErrorListItemCreator,
    private val onRetryClick: () -> Unit,
    private val getErrorText: () -> CharSequence,
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        private const val ITEM_VIEW_TYPE_LOADING = Int.MAX_VALUE - 500 // Magic
        private const val ITEM_VIEW_TYPE_ERROR = Int.MAX_VALUE - 501 // Magic
    }

    init {
        setHasStableIds(wrappedAdapter.hasStableIds())
    }

    private var isDisplayLoadingRow = true
    var isDisplayErrorRow = false
        private set

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            ITEM_VIEW_TYPE_LOADING -> {
                loadingListItemCreator.onCreateViewHolder(parent, viewType)
            }
            ITEM_VIEW_TYPE_ERROR -> {
                errorListItemCreator.onCreateViewHolder(
                    parent = parent,
                    viewType = viewType,
                    onRetryClick = {
                        displayLoadingRow(true)
                        onRetryClick()
                    },
                )
            }
            else -> {
                wrappedAdapter.onCreateViewHolder(parent, viewType)
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when {
            isErrorRow(position) -> {
                errorListItemCreator.onBindViewHolder(holder, position, getErrorText)
            }
            isLoadingRow(position) -> {
                loadingListItemCreator.onBindViewHolder(holder, position)
            }
            else -> {
                wrappedAdapter.onBindViewHolder(holder, position)
            }
        }
    }

    override fun getItemCount(): Int {
        return if (isDisplayLoadingRow || isDisplayErrorRow) {
            wrappedAdapter.itemCount + 1
        } else {
            wrappedAdapter.itemCount
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when {
            isLoadingRow(position) -> ITEM_VIEW_TYPE_LOADING
            isErrorRow(position) -> ITEM_VIEW_TYPE_ERROR
            else -> wrappedAdapter.getItemViewType(position)
        }
    }

    override fun getItemId(position: Int): Long {
        return if (isLoadingRow(position) || isErrorRow(position)) {
            RecyclerView.NO_ID
        } else {
            wrappedAdapter.getItemId(position)
        }
    }

    fun displayLoadingRow(displayLoadingRow: Boolean) {
        if (isDisplayErrorRow) {
            isDisplayErrorRow = false
            isDisplayLoadingRow = displayLoadingRow
            if (displayLoadingRow) {
                // show loading instead of error
                notifyItemChanged(wrappedAdapter.itemCount)
            } else {
                // just remove error
                notifyItemRemoved(wrappedAdapter.itemCount)
            }
            return
        }
        if (isDisplayLoadingRow != displayLoadingRow) {
            isDisplayLoadingRow = displayLoadingRow
            if (displayLoadingRow) {
                notifyItemInserted(wrappedAdapter.itemCount)
            } else {
                notifyItemRemoved(wrappedAdapter.itemCount)
            }
        }
    }

    fun displayErrorRow() {
        if (isDisplayErrorRow) {
            return
        }
        isDisplayErrorRow = true

        if (isDisplayLoadingRow) {
            isDisplayLoadingRow = false
            notifyItemChanged(wrappedAdapter.itemCount)
            return
        }
        notifyItemInserted(wrappedAdapter.itemCount)
    }

    fun isLoadingRow(position: Int): Boolean {
        return isDisplayLoadingRow && position == itemCount - 1
    }

    fun isErrorRow(position: Int): Boolean {
        return isDisplayErrorRow && position == itemCount - 1
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        wrappedAdapter.onAttachedToRecyclerView(recyclerView)
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        super.onDetachedFromRecyclerView(recyclerView)
        wrappedAdapter.onDetachedFromRecyclerView(recyclerView)
    }

}
package com.nicetas.movieapp3.base

import android.app.Activity
import android.os.Build
import com.nicetas.movieapp3.base.extensions.dp
import com.nicetas.movieapp3.base.extensions.isLandscape

class MoviesSpanCount {

    fun get(activity: Activity, isPosters: Boolean): Int {

        val isInMultiWindowMode =
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.N && activity.isInMultiWindowMode

        val isLandscape = activity.isLandscape()

        val metrics = activity.resources.displayMetrics
        val isHeightTablet = metrics.heightPixels >= 900F.dp
        val isWidthTablet = metrics.widthPixels >= 840F.dp
        val isTablet = (isLandscape && isWidthTablet) || isHeightTablet

        return if (isTablet) {
            if (isPosters) {
                if (isLandscape) {
                    if (isInMultiWindowMode) 3 else 5
                } else {
                    3
                }
            } else {
                if (isLandscape) 3 else 2
            }
        } else {
            if (isPosters) {
                if (isLandscape) {
                    if (isInMultiWindowMode) 3 else 4
                } else {
                    2
                }
            } else {
                if (isLandscape) 2 else 1
            }
        }
    }

}
package com.nicetas.movieapp3.base.adapter.pagination

import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.recyclerview.widget.RecyclerView

/** RecyclerView creator that will be called to create and bind loading list item  */
interface LoadingListItemCreator {
    /**
     * Create new loading list item [androidx.recyclerview.widget.RecyclerView.ViewHolder].
     *
     * @param parent   parent ViewGroup.
     * @param viewType type of the loading list item.
     * @return ViewHolder that will be used as loading list item.
     */
    fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder

    /**
     * Bind the loading list item.
     *
     * @param holder   loading list item ViewHolder.
     * @param position loading list item position.
     */
    fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int)

    companion object {
        val DEFAULT: LoadingListItemCreator = object : LoadingListItemCreator {
            override fun onCreateViewHolder(
                parent: ViewGroup,
                viewType: Int
            ): RecyclerView.ViewHolder {
                val view = ProgressBar(parent.context, null, android.R.attr.progressBarStyle)
                view.layoutParams = RecyclerView.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
                )
                return object : RecyclerView.ViewHolder(view) {}
            }

            override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {
                // No binding for default loading row
            }
        }
    }
}
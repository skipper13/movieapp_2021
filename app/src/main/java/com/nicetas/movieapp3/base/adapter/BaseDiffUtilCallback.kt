package com.nicetas.movieapp3.base.adapter

import androidx.collection.ObjectList
import androidx.recyclerview.widget.DiffUtil

abstract class BaseDiffUtilCallback<T>() : DiffUtil.Callback() {

    private lateinit var oldItems: ObjectList<T>
    private lateinit var newItems: ObjectList<T>

    fun setItems(oldItems: ObjectList<T>, newItems: ObjectList<T>) {
        this.oldItems = oldItems
        this.newItems = newItems
    }

    abstract fun areItemsTheSame(oldItem: T, newItem: T): Boolean

    abstract fun areContentsTheSame(oldItem: T, newItem: T): Boolean

    override fun getChangePayload(oldPos: Int, newPos: Int): Any? = -1

    final override fun getOldListSize(): Int = oldItems.size

    final override fun getNewListSize(): Int = newItems.size

    final override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return areItemsTheSame(
            oldItem = oldItems[oldItemPosition],
            newItem = newItems[newItemPosition]
        )
    }

    final override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return areContentsTheSame(
            oldItem = oldItems[oldItemPosition],
            newItem = newItems[newItemPosition]
        )
    }

}
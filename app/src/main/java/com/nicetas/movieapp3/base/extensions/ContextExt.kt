package com.nicetas.movieapp3.base.extensions

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.res.Configuration
import android.content.res.Resources
import android.net.Uri
import androidx.core.content.ContextCompat


/*
 * http://kotlinextensions.com/
 */
fun Context.browse(url: String, newTask: Boolean = false): Boolean {
    try {
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse(url)
        if (newTask) {
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        }
        startActivity(intent)
        return true
    } catch (e: ActivityNotFoundException) {
        return false
    }
}

fun Context.share(text: String) {
    val intent = Intent().apply {
        action = Intent.ACTION_SEND
        putExtra(Intent.EXTRA_TEXT, text)
        type = "text/plain"
    }
    startActivity(Intent.createChooser(intent, null))
}

fun Context.setClipboard(text: CharSequence) {
    val manager =
        ContextCompat.getSystemService(this, ClipboardManager::class.java) as ClipboardManager
    val clip = ClipData.newPlainText("", text)
    manager.setPrimaryClip(clip)
}

fun Context.isPortrait(): Boolean {
    return resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT
}

fun Context.isLandscape(): Boolean {
    return resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE
}

fun Resources.isPortrait(): Boolean {
    return configuration.orientation == Configuration.ORIENTATION_PORTRAIT
}

fun Resources.isLandscape(): Boolean {
    return configuration.orientation == Configuration.ORIENTATION_LANDSCAPE
}

/**
 * @return true if orientation lock requested and landscape orientation will be changed to portrait
 */
fun Activity.lockOrientationForTips(): Boolean {
    val heightPixels = resources.displayMetrics.heightPixels

    val isLandscapeSmallHeight = isLandscape() && heightPixels < 480F.dp

    val needLockOrientation = requestedOrientation != ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
            && (isPortrait() || isLandscapeSmallHeight)

    if (needLockOrientation) {
        // noinspection SourceLockedOrientationActivity
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        return isLandscape()
    }
    return false
}
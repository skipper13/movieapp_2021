package com.nicetas.movieapp3.base.states

import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.nicetas.movieapp3.R
import com.nicetas.repos.network.Connectivity
import com.nicetas.repos.network.OfflineModeStorage

class Lce() {

    interface State

    object Loading : State
    object Content : State
    object Empty : State

    class Error(private val message: String = "") : State {

        // CacheInterceptor - package okhttp3.internal.cache
        private val isCacheError: Boolean
            // "HTTP 504 Unsatisfiable Request (only-if-cached)" or without "HTTP 504"
            get() = message.contains("Unsatisfiable Request (only-if-cached)", ignoreCase = true)

        private val isConnectionError: Boolean
            get() = message.contains("Unable to resolve host")
                    && message.contains("No address associated with hostname")

        fun message(context: Context): String {
            if (OfflineModeStorage.isOfflineMode && isCacheError) {
                return context.getString(R.string.error_offline_mode_enabled)
            }
            if (Connectivity.isInternetAvailable) {
                if (isCacheError) {
                    return context.getString(R.string.error_сonnection_restored)
                }
                if (isConnectionError) {
                    return context.getString(R.string.error_please_check_сonnection)
                }
                return message
            } else {
                return context.getString(R.string.error_no_connection)
            }
        }
    }

    val liveData = MutableLiveData<State>()

    fun loading() {
        liveData.value = Loading
    }

    fun content() {
        liveData.value = Content
    }

    fun empty() {
        liveData.value = Empty
    }

    fun error(message: String) {
        liveData.value = Error(message = message)
    }

    fun state(state: State) {
        liveData.value = state
    }

}
package com.nicetas.movieapp3.base.extensions


inline fun <reified T> Any.privateField(name: String): T {
    val field = this::class.java.getDeclaredField(name)
    field.isAccessible = true
    return field.get(this) as T
}
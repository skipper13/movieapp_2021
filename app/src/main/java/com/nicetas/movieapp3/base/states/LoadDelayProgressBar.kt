package com.nicetas.movieapp3.base.states

import android.content.Context
import android.util.AttributeSet
import android.widget.ProgressBar
import androidx.appcompat.view.ContextThemeWrapper
import androidx.core.widget.ContentLoadingProgressBar

/**
 * @see [ContentLoadingProgressBar]
 */
class LoadDelayProgressBar(
    context: Context,
    attrs: AttributeSet?,
    private val delayMs: Long,
) : ProgressBar(
    ContextThemeWrapper(context, android.R.style.Widget_Material_ProgressBar),
    attrs,
    0
) {

    companion object {
        const val DELAY_MS = 500L
    }

    constructor(context: Context) : this(context, null, DELAY_MS)

    // These field should only be accessed on the UI thread.
    private var mPostedShow: Boolean = false

    private val mDelayedShow = Runnable {
        mPostedShow = false
        visibility = VISIBLE
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        removeCallbacks()
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        removeCallbacks()
    }

    private fun removeCallbacks() {
        removeCallbacks(mDelayedShow)
    }

    fun show() {
        if (!mPostedShow) {
            mPostedShow = true
            postDelayed(mDelayedShow, delayMs)
        }
    }

}
package com.nicetas.movieapp3.base.adapter.pagination

import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.AdapterDataObserver
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.nicetas.movieapp3.R

class RecyclerPaginate(
    private val recyclerView: RecyclerView,
    private val callbacks: Callbacks,
    private val loadingTriggerThreshold: Int,
    addLoadingListItem: Boolean,
    loadingListItemCreator: LoadingListItemCreator,
    errorListItemCreator: ErrorListItemCreator,
    onRetryClick: () -> Unit,
    getErrorText: () -> CharSequence,
) : Paginate() {

    private lateinit var wrapperAdapter: WrapperAdapter
    private var wrapperSpanSizeLookup: WrapperSpanSizeLookup? = null
    private var mDataObserver: AdapterDataObserver? = null

    private val mOnScrollListener: RecyclerView.OnScrollListener =
        object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                // Each time when list is scrolled check if end of the list is reached
                checkEndOffset()
            }
        }

    private fun initDataObserver(): AdapterDataObserver {
        mDataObserver = object : AdapterDataObserver() {
            override fun onChanged() {
                wrapperAdapter.notifyDataSetChanged()
                onAdapterDataChanged()
            }

            override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
                wrapperAdapter.notifyItemRangeInserted(positionStart, itemCount)
                onAdapterDataChanged()
            }

            override fun onItemRangeChanged(positionStart: Int, itemCount: Int) {
                wrapperAdapter.notifyItemRangeChanged(positionStart, itemCount)
                onAdapterDataChanged()
            }

            override fun onItemRangeChanged(positionStart: Int, itemCount: Int, payload: Any?) {
                wrapperAdapter.notifyItemRangeChanged(positionStart, itemCount, payload)
                onAdapterDataChanged()
            }

            override fun onItemRangeRemoved(positionStart: Int, itemCount: Int) {
                wrapperAdapter.notifyItemRangeRemoved(positionStart, itemCount)
                onAdapterDataChanged()
            }

            override fun onItemRangeMoved(fromPosition: Int, toPosition: Int, itemCount: Int) {
                wrapperAdapter.notifyItemMoved(fromPosition, toPosition)
                onAdapterDataChanged()
            }
        }
        return mDataObserver!!
    }

    init {
        // Attach scrolling listener in order to perform end offset check on each scroll event
        recyclerView.addOnScrollListener(mOnScrollListener)

        if (addLoadingListItem) {
            val adapter = recyclerView.adapter!!
            // Wrap existing adapter with new WrapperAdapter that will add loading row
            wrapperAdapter = WrapperAdapter(
                wrappedAdapter = adapter,
                loadingListItemCreator = loadingListItemCreator,
                errorListItemCreator = errorListItemCreator,
                onRetryClick = onRetryClick,
                getErrorText = getErrorText,
            )
            wrapperAdapter.displayLoadingRow(callbacks.hasLoadedAllItems().not())
            adapter.registerAdapterDataObserver(initDataObserver())
            recyclerView.adapter = wrapperAdapter

            // For GridLayoutManager use separate/customisable span lookup for loading row
            val layoutManager = recyclerView.layoutManager
            if (layoutManager is GridLayoutManager) {
                wrapperSpanSizeLookup = WrapperSpanSizeLookup(
                    wrappedSpanSizeLookup = layoutManager.spanSizeLookup,
                    layoutManager = layoutManager,
                    wrapperAdapter = wrapperAdapter,
                )
                layoutManager.spanSizeLookup = wrapperSpanSizeLookup
            }
        }
    }

    override fun setHasMoreDataToLoad(hasMoreDataToLoad: Boolean) {
        wrapperAdapter.displayLoadingRow(hasMoreDataToLoad)
    }

    override fun setError() {
        wrapperAdapter.displayErrorRow()
    }

    override fun unbind() {
        recyclerView.removeOnScrollListener(mOnScrollListener)
        val adapter = recyclerView.adapter
        if (adapter is WrapperAdapter) {
            val originalAdapter = wrapperAdapter.wrappedAdapter
            mDataObserver?.let { dataObserver ->
                originalAdapter.unregisterAdapterDataObserver(dataObserver)
            }
            mDataObserver = null
        }
    }

    private fun checkEndOffset() {
        val layoutManager = recyclerView.layoutManager
        val totalItemCount = layoutManager?.itemCount ?: 0
        val visibleItemCount = recyclerView.childCount
        val firstVisibleItemPosition: Int

        firstVisibleItemPosition = if (layoutManager is LinearLayoutManager) {
            layoutManager.findFirstVisibleItemPosition()

        } else if (layoutManager is StaggeredGridLayoutManager) {
            // https://code.google.com/p/android/issues/detail?id=181461
            if (layoutManager.childCount > 0) {
                layoutManager.findFirstVisibleItemPositions(null)[0]
            } else {
                0
            }
        } else {
            throw IllegalStateException("LayoutManager needs to subclass LinearLayoutManager or StaggeredGridLayoutManager")
        }

        // Check if end of the list is reached (counting threshold) or if there is no items at all
        if (totalItemCount - visibleItemCount <= firstVisibleItemPosition + loadingTriggerThreshold || totalItemCount == 0) {
            // Call load more only if loading is not currently in progress and if there is more items to load
            if (wrapperAdapter.isDisplayErrorRow.not()
                && callbacks.isLoading().not()
                && callbacks.hasLoadedAllItems().not()
            ) {
                callbacks.onLoadMore()
            }
        }
    }

    private fun onAdapterDataChanged() {
        wrapperAdapter.displayLoadingRow(!callbacks.hasLoadedAllItems())
        checkEndOffset()
    }

    class Builder(private val recyclerView: RecyclerView, private val callbacks: Callbacks) {
        private var loadingTriggerThreshold = 5
        private var addLoadingListItem = true
        private var loadingListItemCreator: LoadingListItemCreator? = null
        private var errorListItemCreator: ErrorListItemCreator? = null
        private var onRetryClick: (() -> Unit)? = null
        private var getErrorText: (() -> CharSequence)? = null
        /**
         * Set the offset from the end of the list at which the load more event needs to be triggered. Default offset
         * if 5.
         *
         * @param threshold number of items from the end of the list.
         * @return [Builder]
         */
        fun setLoadingTriggerThreshold(threshold: Int): Builder {
            loadingTriggerThreshold = threshold
            return this
        }

        /**
         * Setup loading row. If loading row is used original adapter set on RecyclerView will be wrapped with
         * internal adapter that will add loading row as the last item in the list. Paginate will observer the
         * changes upon original adapter and remove loading row if there is no more data to load. By default loading
         * row will be added.
         *
         * @param addLoadingListItem true if loading row needs to be added, false otherwise.
         * @return [Builder]
         * @see {@link Callbacks.hasLoadedAllItems
         * @see {@link Builder.setLoadingListItemCreator
         */
        fun addLoadingListItem(addLoadingListItem: Boolean): Builder {
            this.addLoadingListItem = addLoadingListItem
            return this
        }

        /**
         * Set custom loading list item creator. If no creator is set default one will be used.
         *
         * @param creator Creator that will ne called for inflating and binding loading list item.
         * @return [Builder]
         */
        fun setLoadingListItemCreator(creator: LoadingListItemCreator?): Builder {
            loadingListItemCreator = creator
            return this
        }

        fun setErrorListItemCreator(creator: ErrorListItemCreator?): Builder {
            errorListItemCreator = creator
            return this
        }

        fun onRetryClick(onClick: () -> Unit): Builder {
            this.onRetryClick = onClick
            return this
        }

        fun errorText(text: () -> CharSequence): Builder {
            this.getErrorText = text
            return this
        }

        /**
         * Create pagination functionality upon RecyclerView.
         *
         * @return [Paginate] instance.
         */
        fun build(): Paginate {
            checkNotNull(recyclerView.adapter) { "Adapter needs to be set!" }
            checkNotNull(recyclerView.layoutManager) { "LayoutManager needs to be set on the RecyclerView" }
            return RecyclerPaginate(
                recyclerView = recyclerView,
                callbacks = callbacks,
                loadingTriggerThreshold = loadingTriggerThreshold,
                addLoadingListItem = addLoadingListItem,
                loadingListItemCreator = loadingListItemCreator
                    ?: LoadingListItemCreator.DEFAULT,
                errorListItemCreator = errorListItemCreator
                    ?: ErrorListItemCreator.DEFAULT,
                onRetryClick = onRetryClick ?: { /* no-op */ },
                getErrorText = getErrorText
                    ?: { recyclerView.context.getString(R.string.error_state) }
            )
        }
    }

}
package com.nicetas.movieapp3.base.adapter

class ClickDebounce(
    private val delay: Long = 170
) {

    private var lastClickTime = 0L

    fun canClick(): Boolean {
        val now = System.currentTimeMillis()
        if (now - lastClickTime < delay) {
            return false
        }
        lastClickTime = now
        return true
    }

}
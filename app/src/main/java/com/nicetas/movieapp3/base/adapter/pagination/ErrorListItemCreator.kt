package com.nicetas.movieapp3.base.adapter.pagination

import android.view.Gravity
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat.getColor
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.button.MaterialButton
import com.nicetas.movieapp3.R
import com.nicetas.movieapp3.base.extensions.dp
import com.nicetas.movieapp3.base.extensions.onClickDebounce

/** RecyclerView creator that will be called to create and bind error list item  */
interface ErrorListItemCreator {

    fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int,
        onRetryClick: () -> Unit,
    ): RecyclerView.ViewHolder

    fun onBindViewHolder(
        holder: RecyclerView.ViewHolder?,
        position: Int,
        getErrorText: () -> CharSequence,
    )

    companion object {
        val DEFAULT: ErrorListItemCreator = object : ErrorListItemCreator {

            private var tvError: TextView? = null

            override fun onCreateViewHolder(
                parent: ViewGroup,
                viewType: Int,
                onRetryClick: () -> Unit,
            ): RecyclerView.ViewHolder {
                val view = LinearLayout(parent.context).apply {
                    layoutParams = ViewGroup.MarginLayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT
                    )
                    orientation = LinearLayout.VERTICAL
                    gravity = Gravity.CENTER
                }

                tvError = TextView(view.context).apply {
                    setPadding(16f.dp, 16f.dp, 16f.dp, 16f.dp)
                    gravity = Gravity.CENTER
                    textSize = 16f
                    setTextColor(getColor(context, R.color.white))
                }
                view.addView(tvError)

                val btnRetry = MaterialButton(view.context).apply {
                    layoutParams = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                    )
                    setBackgroundColor(getColor(context, R.color.yellow_CAC683))
                    setRippleColorResource(R.color.grey_B6B6B6)
                    isAllCaps = false
                    text = context.getString(R.string.error_state_view_reload)
                    setTextColor(getColor(context, R.color.black))
                    onClickDebounce { onRetryClick() }
                }
                view.addView(btnRetry)
                return object : RecyclerView.ViewHolder(view) {}
            }

            override fun onBindViewHolder(
                holder: RecyclerView.ViewHolder?,
                position: Int,
                getErrorText: () -> CharSequence,
            ) {
                tvError?.text = getErrorText.invoke()
            }
        }
    }
}
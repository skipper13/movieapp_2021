package com.nicetas.movieapp3.di

import com.nicetas.repos.movie.TestMovieRepository
import com.nicetas.repos.search.TestSearchRepository
import com.nicetas.repos.genre.TestGenresRepository
import com.nicetas.repos.person.TestPersonRepository
import com.nicetas.repos.configuration.TestConfigRepository
import com.nicetas.repos.list.TestListRepository

class MockSettings private constructor() {

    var mockConfigRepo: Boolean = false
        private set
    var mockSearchRepo: Boolean = false
        private set
    var mockMovieRepo: Boolean = false
        private set
    var mockPersonRepo: Boolean = false
        private set
    var mockGenresRepo: Boolean = false
        private set
    var mockListRepo: Boolean = false
        private set


    class Builder {

        private val mockSettings = MockSettings()

        /** @see TestConfigRepository */
        fun config(): Builder {
            mockSettings.mockConfigRepo = true
            return this
        }

        /** @see TestSearchRepository */
        fun search(): Builder {
            mockSettings.mockSearchRepo = true
            return this
        }

        /** @see TestMovieRepository */
        fun movie(): Builder {
            mockSettings.mockMovieRepo = true
            return this
        }

        /** @see TestPersonRepository */
        fun person(): Builder {
            mockSettings.mockPersonRepo = true
            return this
        }

        /** @see TestGenresRepository */
        fun genres(): Builder {
            mockSettings.mockGenresRepo = true
            return this
        }

        /** @see TestListRepository */
        fun list(): Builder {
            mockSettings.mockListRepo = true
            return this
        }

        fun build(): MockSettings {
            return mockSettings
        }
    }

}
package com.nicetas.movieapp3.di

import com.nicetas.movieapp3.ui.about.AboutViewModel
import com.nicetas.movieapp3.ui.extended.actor.ActorInfoViewModel
import com.nicetas.movieapp3.ui.extended.actor.cast.ActorCastViewModel
import com.nicetas.movieapp3.ui.extended.actor.photo.ActorPhotosViewModel
import com.nicetas.movieapp3.ui.extended.credits.CreditsViewModel
import com.nicetas.movieapp3.ui.extended.date.DateFormatViewModel
import com.nicetas.movieapp3.ui.extended.images.MovieImagesViewModel
import com.nicetas.movieapp3.ui.extended.info.MovieInfoViewModel
import com.nicetas.movieapp3.ui.extended.tabs.MovieTabsViewModel
import com.nicetas.movieapp3.ui.extended.videos.MovieVideosViewModel
import com.nicetas.movieapp3.ui.fullscreen.FullscreenImagesViewModel
import com.nicetas.movieapp3.ui.lists.ListsViewModel
import com.nicetas.movieapp3.ui.lists.list.ListViewModel
import com.nicetas.movieapp3.ui.main.MainViewModel
import com.nicetas.movieapp3.ui.search.SearchViewModel
import com.nicetas.movieapp3.ui.search.options.SearchOptionsViewModel
import com.nicetas.movieapp3.ui.search.options.keywords.search.SearchWordViewModel
import com.nicetas.movieapp3.ui.search.options.kit.SearchKitsViewModel
import com.nicetas.movieapp3.ui.search.options.kit.create.CreateSearchKitViewModel
import com.nicetas.movieapp3.ui.search.options.origincountry.OriginCountriesViewModel
import com.nicetas.movieapp3.ui.watchlist.WatchListViewModel
import com.nicetas.repos.network.OfflineModeStorage
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelsModule = module {

    viewModel {
        MainViewModel(
            keyboardBus = get(),
        )
    }
    viewModel {
        WatchListViewModel(
            movieRepository = get(),
            watchListUpdateBus = get(),
            prefs = get(),
        )
    }
    viewModel {
        AboutViewModel(
            configRepository = get(),
            offlineModeStorage = get<OfflineModeStorage>(),
            prefs = get(),
        )
    }
    viewModel {
        MovieTabsViewModel(
            movieRepository = get(),
            watchListUpdateBus = get(),
        )
    }
    viewModel {
        MovieInfoViewModel(
            configRepository = get(),
            movieRepository = get(),
            dateFormat = get(),
            prefs = get(),
        )
    }
    viewModel {
        CreditsViewModel(
            movieRepository = get(),
        )
    }
    viewModel {
        MovieImagesViewModel(
            movieRepository = get(),
        )
    }
    viewModel {
        MovieVideosViewModel(
            movieRepository = get(),
        )
    }
    viewModel {
        ActorInfoViewModel(
            personRepository = get(),
            dateFormat = get(),
        )
    }
    viewModel {
        ActorPhotosViewModel(
            personRepository = get(),
        )
    }
    viewModel {
        ActorCastViewModel(
            personRepository = get(),
        )
    }
    viewModel {
        SearchViewModel(
            configRepository = get(),
            searchRepository = get(),
            movieRepository = get(),
            keyboardBus = get(),
            watchListUpdateBus = get(),
            prefs = get(),
        )
    }
    viewModel {
        SearchOptionsViewModel(
            searchRepository = get(),
            genresRepository = get(),
            configRepository = get(),
            dateFormat = get(),
            prefs = get(),
        )
    }
    viewModel {
        SearchKitsViewModel(
            searchRepository = get(),
            prefs = get(),
        )
    }
    viewModel {
        CreateSearchKitViewModel(
            searchRepository = get(),
        )
    }
    viewModel {
        FullscreenImagesViewModel()
    }

    viewModel {
        ListsViewModel(
            listRepository = get(),
        )
    }
    viewModel {
        ListViewModel(
            configRepository = get(),
            listRepository = get(),
            movieRepository = get(),
            watchListUpdateBus = get(),
        )
    }
    viewModel {
        OriginCountriesViewModel(
            configRepository = get(),
        )
    }
    viewModel {
        SearchWordViewModel(
            searchRepository = get(),
        )
    }
    viewModel {
        DateFormatViewModel(
            prefs = get(),
        )
    }
}
package com.nicetas.movieapp3.di

import com.nicetas.database.DatabaseHolder
import com.nicetas.movieapp3.BuildConfig
import com.nicetas.repos.network.Connectivity
import com.nicetas.repos.network.OfflineModeStorage
import com.nicetas.repos.network.RetrofitHolder
import org.koin.dsl.module

val appModule = module {
    single {
        RetrofitHolder(
            appContext = get(),
            isDebug = BuildConfig.DEBUG,
        )
    }
    single {
        DatabaseHolder(get())
    }
    single {
        Connectivity(appContext = get())
    }
    single {
        OfflineModeStorage(prefs = get())
    }
}
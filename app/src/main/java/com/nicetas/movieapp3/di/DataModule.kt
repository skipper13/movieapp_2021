package com.nicetas.movieapp3.di

import com.nicetas.GsonProvider
import com.nicetas.prefs.Prefs
import com.nicetas.bus.KeyboardBus
import com.nicetas.bus.WatchListUpdateBus
import com.nicetas.domain.DateFormatInteractor
import com.nicetas.repos.configuration.ConfigRepository
import com.nicetas.repos.configuration.TestConfigRepository
import com.nicetas.repos.genre.GenresRepository
import com.nicetas.repos.genre.TestGenresRepository
import com.nicetas.repos.list.ListRepository
import com.nicetas.repos.list.TestListRepository
import com.nicetas.repos.movie.MovieRepository
import com.nicetas.repos.movie.TestMovieRepository
import com.nicetas.repos.person.PersonRepository
import com.nicetas.repos.person.TestPersonRepository
import com.nicetas.repos.search.SearchRepository
import com.nicetas.repos.search.TestSearchRepository
import org.koin.core.module.Module
import org.koin.dsl.module

class DataModule(
    private val mockSettings: MockSettings,
) {

    val module: Module
        get() = module {
            single {
                if (mockSettings.mockMovieRepo) {
                    TestMovieRepository(
                        appContext = get(),
                        retrofit = get(),
                        db = get(),
                        gson = get(),
                    )
                } else {
                    MovieRepository(
                        retrofit = get(),
                        db = get(),
                    )
                }
            }

            single {
                if (mockSettings.mockPersonRepo) {
                    TestPersonRepository(
                        appContext = get(),
                        retrofit = get(),
                        gson = get(),
                    )
                } else {
                    PersonRepository(
                        retrofit = get(),
                    )
                }
            }

            single {
                if (mockSettings.mockGenresRepo) {
                    TestGenresRepository(
                        appContext = get(),
                        retrofit = get(),
                        gson = get(),
                        prefs = get(),
                    )
                } else {
                    GenresRepository(
                        appContext = get(),
                        retrofit = get(),
                        gson = get(),
                        prefs = get(),
                    )
                }
            }

            single {
                if (mockSettings.mockSearchRepo) {
                    TestSearchRepository(
                        appContext = get(),
                        retrofit = get(),
                        db = get(),
                        gson = get(),
                        prefs = get(),
                    )
                } else {
                    SearchRepository(
                        appContext = get(),
                        retrofit = get(),
                        db = get(),
                        gson = get(),
                        prefs = get(),
                    )
                }
            }

            single {
                if (mockSettings.mockConfigRepo) {
                    TestConfigRepository(
                        appContext = get(),
                        retrofit = get(),
                        gson = get(),
                        prefs = get(),
                    )
                } else {
                    ConfigRepository(
                        appContext = get(),
                        retrofit = get(),
                        gson = get(),
                        prefs = get(),
                    )
                }
            }

            single {
                if (mockSettings.mockListRepo) {
                    TestListRepository(
                        appContext = get(),
                        retrofit = get(),
                        db = get(),
                        gson = get(),
                    )
                } else {
                    ListRepository(
                        retrofit = get(),
                        db = get(),
                    )
                }
            }

            single { KeyboardBus() }
            single { WatchListUpdateBus() }
            single { GsonProvider() }
            single { Prefs(appContext = get()) }

            factory { DateFormatInteractor(prefs = get()) }
        }

}
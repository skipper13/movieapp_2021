package com.nicetas.movieapp3.ui.extended.actor

import androidx.collection.MutableObjectList
import androidx.collection.ObjectList
import androidx.lifecycle.MutableLiveData
import com.nicetas.domain.DateFormatInteractor
import com.nicetas.models.persons.Person
import com.nicetas.movieapp3.base.Logger
import com.nicetas.movieapp3.base.viewmodel.BaseViewModel
import com.nicetas.movieapp3.ui.extended.actor.adapter.KnownForHolder
import com.nicetas.repos.network.LanguageProvider
import com.nicetas.repos.person.PersonRepository
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.internal.operators.single.SingleFromCallable
import io.reactivex.rxjava3.schedulers.Schedulers
import java.util.Optional

class ActorInfoViewModel(
    private val personRepository: PersonRepository,
    private val dateFormat: DateFormatInteractor,
) : BaseViewModel() {

    val onGetActor = MutableLiveData<Person>()

    private var wasInit = false
    private var language = ""
    private val cast = MutableObjectList<Any>()
    var moviesScrollOffset = 0

    fun onViewCreated(actorId: Int) {
        if (wasInit && language == LanguageProvider.language) {
            return
        }
        wasInit = true
        language = LanguageProvider.language
        subscribeOnDateFormat()
        loadActor(actorId)
    }

    override fun onCleared() {
        super.onCleared()
        cast.clear()
    }

    fun getCast(): ObjectList<Any> = cast
    fun dateOrder() = dateFormat.dateOrder
    fun dateSeparator() = dateFormat.dateSeparator

    fun loadActor(actorId: Int) {
        dateFormat.updateFromPrefs()

        lce.loading()
        disposables += SingleFromCallable {
            val cache = personRepository.getActorFromCache(actorId)
            Optional.ofNullable(cache)
        }.flatMap { optional ->
            if (optional.isEmpty) {
                personRepository.loadActorInfo(actorId).map { it.body }
            } else {
                Single.just(optional.get())
            }
        }.map { actor ->
            val movies = actor.movieCredits.cast?.sortedByDescending { it.popularity }
                ?: emptyList()

            cast.clear()

            for (i in movies.indices) {
                val movie = movies[i]
                val item = KnownForHolder.Item(
                    movieId = movie.id,
                    title = movie.title,
                    posterPath = movie.posterPath,
                    backdropPath = movie.backdropPath,
                    releaseDate = movie.releaseDate,
                    voteAverage = movie.voteAverage,
                    character = movie.character,
                )
                cast.add(item)
            }
            actor
        }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ actor ->
                onGetActor.value = actor
                lce.content()
            }, { error ->
                Logger.e(error)
                lce.error(message = error.message ?: "")
            })
    }

    private fun subscribeOnDateFormat() {
        disposables += dateFormat.subscribeOnPrefsChanged()
            .subscribeOn(AndroidSchedulers.mainThread())
            .subscribe({
                onGetActor.value = onGetActor.value
            }, { error ->
                Logger.e(error)
            })
    }

}
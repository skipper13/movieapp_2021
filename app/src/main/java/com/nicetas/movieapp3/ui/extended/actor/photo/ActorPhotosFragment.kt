package com.nicetas.movieapp3.ui.extended.actor.photo

import android.os.Bundle
import android.view.View
import android.view.ViewGroup.MarginLayoutParams
import androidx.core.content.ContextCompat.getColor
import androidx.core.view.ViewCompat
import androidx.core.view.updateLayoutParams
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.nicetas.movieapp3.R
import com.nicetas.movieapp3.base.adapter.GridSpacingItemDecoration
import com.nicetas.movieapp3.base.extensions.dp
import com.nicetas.movieapp3.base.extensions.isLandscape
import com.nicetas.movieapp3.base.extensions.statusBarHeight
import com.nicetas.movieapp3.base.fragment.BaseFragment
import com.nicetas.movieapp3.base.states.ViewState
import com.nicetas.movieapp3.databinding.FragmentActorPhotosBinding
import com.nicetas.movieapp3.ui.ImageUrl
import com.nicetas.movieapp3.ui.extended.actor.photo.adapter.ActorPhotosAdapter
import com.nicetas.movieapp3.ui.fullscreen.FullscreenImagesFragment
import org.koin.androidx.viewmodel.ext.android.viewModel

class ActorPhotosFragment : BaseFragment(R.layout.fragment_actor_photos) {

    companion object {
        private const val ARG_ACTOR_ID = "ARG_ACTOR_ID"
        private const val ARG_ACTOR_NAME = "ARG_ACTOR_NAME"

        fun args(
            actorId: Int,
            name: String,
        ) = Bundle(2).apply {
            putInt(ARG_ACTOR_ID, actorId)
            putString(ARG_ACTOR_NAME, name)
        }
    }

    private var _binding: FragmentActorPhotosBinding? = null
    private val binding
        get() = _binding!!

    private val viewModel: ActorPhotosViewModel by viewModel()

    private var adapter: ActorPhotosAdapter? = null

    private val actorId: Int by lazy { requireArguments().getInt(ARG_ACTOR_ID) }
    private val actorName: String by lazy { requireArguments().getString(ARG_ACTOR_NAME, "") }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentActorPhotosBinding.bind(view)

        setStatusBarColor(getColor(requireContext(), R.color.colorPrimary))
        setToolbarTitle(actorName)

        ViewCompat.setOnApplyWindowInsetsListener(binding.root) { _, insets ->
            val statusBarHeight = insets.statusBarHeight()
            binding.appBar.updateLayoutParams<MarginLayoutParams> {
                topMargin = statusBarHeight
            }
            binding.rvContent.updateLayoutParams<MarginLayoutParams> {
                bottomMargin = statusBarHeight
            }
            insets
        }

        initViewState()
        initRecyclerView()
        setupObservers()
        viewModel.onViewCreated(actorId)
    }

    override fun onDestroyView() {
        adapter?.onDestroyView()
        adapter = null
        binding.rvContent.adapter = null
        super.onDestroyView()
        _binding = null
    }

    private fun initViewState() {
        ViewState.Builder(binding.rvContent)
            .loadDelay(100)
            .error(onClick = { viewModel.loadImages() })
            .empty(binding.tvEmptyState)
            .build()
            .observeLce(viewModel.lce, viewLifecycleOwner)
    }

    private fun initRecyclerView() {
        adapter = ActorPhotosAdapter(
            onPhotoClick = { position ->
                val args = FullscreenImagesFragment.args(
                    title = actorName,
                    imageSize = ImageUrl.w500,
                    images = viewModel.photos,
                    position = position
                )
                findNavController().navigate(R.id.fullscreenImagesScreen, args)
            }
        )
        binding.rvContent.adapter = adapter

        val spanCount = if (resources.isLandscape()) 3 else 2
        binding.rvContent.layoutManager = GridLayoutManager(requireContext(), spanCount)
        binding.rvContent.addItemDecoration(GridSpacingItemDecoration(spanCount, 4f.dp, false))
    }

    private fun setupObservers() {
        viewModel.onGetImages.observe(viewLifecycleOwner) { images ->
            adapter?.items?.replaceAll(images)
        }
    }

}
package com.nicetas.movieapp3.ui.search.options.kit.create

import androidx.collection.MutableObjectList
import androidx.collection.ObjectList
import com.nicetas.database.search.SearchKitEntity
import com.nicetas.movieapp3.base.Logger
import com.nicetas.movieapp3.base.viewmodel.BaseViewModel
import com.nicetas.movieapp3.base.viewmodel.LiveEvent
import com.nicetas.movieapp3.ui.search.options.kit.create.adapter.KitIcon
import com.nicetas.movieapp3.ui.search.options.kit.create.adapter.KitIconHolder
import com.nicetas.repos.search.SearchRepository
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.internal.operators.single.SingleFromCallable
import io.reactivex.rxjava3.schedulers.Schedulers

class CreateSearchKitViewModel(
    private val searchRepository: SearchRepository,
) : BaseViewModel() {

    val closeScreen = LiveEvent<Unit>()
    val showIcons = LiveEvent<ObjectList<KitIconHolder.Item>>()
    val updateSelected = LiveEvent<Pair<Int, Int>>()

    private var wasInit = false
    private var iconId = SearchKitEntity.IconId.POPULAR
    private val icons = MutableObjectList<KitIconHolder.Item>()
    var title = ""

    fun onViewCreated() {
        if (wasInit) {
            showIcons.setValue(icons)
            return
        }
        wasInit = true
        initIcons()
    }

    override fun onCleared() {
        super.onCleared()
        icons.clear()
    }

    private fun initIcons() {
        disposables += SingleFromCallable {
            for (i in 0 until KitIcon.getIconsCount()) {
                icons.add(KitIconHolder.Item(id = i, isSelected = i == iconId))
            }
            icons
        }
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ items ->
                showIcons.setValue(items)
            }, { error ->
                Logger.e(error)
            })
    }

    fun create() {
        searchRepository.searchKit.id = 0
        searchRepository.searchKit.iconId = iconId
        searchRepository.searchKit.title = title

        val newKit = searchRepository.searchKit.toEntity()

        disposables += searchRepository.addSearchKit(kit = newKit)
            .flatMap { id ->
                searchRepository.searchKit.id = id.toInt()
                searchRepository.saveSelectedKitId(id.toInt())
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ _ ->
                closeScreen.setValue(Unit)
            }, { error ->
                Logger.e(error)
            })
    }

    fun updateSelectedIcon(iconId: Int) {
        val indexOld = icons.indexOfFirst { it.id == this.iconId }
        val indexNew = icons.indexOfFirst { it.id == iconId }
        if (indexOld != -1 && indexNew != -1) {
            icons[indexOld].isSelected = false
            icons[indexNew].isSelected = true

            updateSelected.setValue(Pair(this.iconId, iconId))
            this.iconId = iconId
        }
    }

}
package com.nicetas.movieapp3.ui.extended.actor.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.nicetas.models.persons.Person
import com.nicetas.movieapp3.base.adapter.AdapterItems
import com.nicetas.movieapp3.base.adapter.ClickDebounce

class ActorInfoAdapter(
    private var inflater: LayoutInflater?,
    private val callbackInfo: ActorInfoHolder.Callback,
    private val saveMoviesScrollOffset: (Int) -> Unit,
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        private const val TYPE_ACTOR_INFO = 0
    }

    private val debounce = ClickDebounce()
    val items = AdapterItems<Any>(this)

    fun onDestroyView() {
        items.list.clear()
        inflater = null
    }

    override fun getItemCount(): Int = items.list.size

    override fun getItemViewType(position: Int): Int {
        return when (items.list[position]) {
            is Person -> TYPE_ACTOR_INFO
            else -> throw IllegalStateException("item is null for position = $position")
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_ACTOR_INFO -> ActorInfoHolder(
                parent = parent,
                inflater = inflater,
                debounce = debounce,
                callback = callbackInfo,
            )
            else -> throw IllegalStateException("Unknown holder type")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = items.list[position]
        when (holder) {
            is ActorInfoHolder -> holder.bind(item as Person)
        }
    }

    override fun onViewRecycled(holder: RecyclerView.ViewHolder) {
        when (holder) {
            is ActorInfoHolder -> {
                val rv = holder.binding.rvKnownForMovies
                saveMoviesScrollOffset(rv.computeHorizontalScrollOffset())
            }
        }
    }

}
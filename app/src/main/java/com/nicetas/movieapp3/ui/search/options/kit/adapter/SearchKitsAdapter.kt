package com.nicetas.movieapp3.ui.search.options.kit.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.nicetas.movieapp3.base.adapter.ClickDebounce
import com.nicetas.movieapp3.base.adapter.AdapterItems

class SearchKitsAdapter(
    private val kitCallback: SearchKitHolder.Callback,
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        private const val TYPE_OPTION = 0
    }

    private val debounce = ClickDebounce()
    val items = AdapterItems<Any>(this)

    override fun getItemCount(): Int = items.list.size

    override fun getItemViewType(position: Int): Int {
        return when (items.list[position]) {
            is SearchKitHolder.Item -> TYPE_OPTION
            else -> throw IllegalStateException("item is null for position = $position")
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_OPTION -> SearchKitHolder(
                parent = parent,
                debounce = debounce,
                callback = kitCallback,
            )
            else -> throw IllegalStateException("Unknown holder type")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = items.list[position]
        when (holder) {
            is SearchKitHolder -> holder.bind(item as SearchKitHolder.Item)
        }
    }

}


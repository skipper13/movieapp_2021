package com.nicetas.movieapp3.ui.fullscreen

import com.nicetas.movieapp3.base.viewmodel.BaseViewModel

class FullscreenImagesViewModel() : BaseViewModel() {

    private var wasInit = false
    var position: Int = 0

    fun onCreate(initialPosition: Int) {
        if (wasInit) {
            return
        }
        wasInit = true
        position = initialPosition
    }

}
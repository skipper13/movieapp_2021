package com.nicetas.movieapp3.ui.extended.tabs.views

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.Toolbar
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.appbar.CollapsingToolbarLayout
import com.google.android.material.tabs.TabLayout

interface ViewsProviderMovieTabs {

    val coordinator: CoordinatorLayout
    val ablExtended: AppBarLayout
    val ctlExtended: CollapsingToolbarLayout
    val ivMovie: AppCompatImageView
    val toolbar: Toolbar
    val tabLayout: TabLayout
    val viewPager: ViewPager2

    fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
    ): View

    fun onViewCreated(view: View)

    fun onDestroyView()

}
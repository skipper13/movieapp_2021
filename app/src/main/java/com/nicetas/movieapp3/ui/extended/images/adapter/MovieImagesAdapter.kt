package com.nicetas.movieapp3.ui.extended.images.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.nicetas.movieapp3.base.adapter.ClickDebounce
import com.nicetas.movieapp3.base.adapter.AdapterItems

class MovieImagesAdapter(
    private val onImageClick: MovieImagesHolder.Callback,
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        private const val TYPE_IMAGE = 0
    }

    private val debounce = ClickDebounce()
    val items = AdapterItems<Any>(this)

    override fun getItemCount(): Int = items.list.size

    override fun getItemViewType(position: Int): Int {
        return when (items.list[position]) {
            is MovieImagesHolder.Item -> TYPE_IMAGE
            else -> throw IllegalStateException("item is null for position = $position")
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_IMAGE -> MovieImagesHolder(
                parent = parent,
                debounce = debounce,
                callback = onImageClick,
            )
            else -> throw IllegalStateException("Unknown holder type")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = items.list[position]
        when (holder) {
            is MovieImagesHolder -> holder.bind(item as MovieImagesHolder.Item)
        }
    }

}
package com.nicetas.movieapp3.ui.search.options.certification.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.StringRes
import androidx.recyclerview.widget.RecyclerView
import com.nicetas.movieapp3.R
import com.nicetas.movieapp3.base.adapter.ClickDebounce
import com.nicetas.movieapp3.databinding.ItemCertificationCountryBinding
import com.nicetas.movieapp3.ui.search.options.certification.Certification

class CertificationCountryHolder(
    parent: ViewGroup,
    private var binding: ItemCertificationCountryBinding = ItemCertificationCountryBinding.inflate(
        LayoutInflater.from(parent.context),
        parent,
        false
    ),
    debounce: ClickDebounce,
    private val callback: Callback,
) : RecyclerView.ViewHolder(binding.root) {

    interface Callback {
        fun onClick()
    }

    class Item(
        var countryId: String,
    )

    private lateinit var item: Item
    private val context: Context
        get() = itemView.context

    init {
        binding.root.setOnClickListener {
            if (debounce.canClick()) {
                callback.onClick()
            }
        }
    }

    fun bind(item: Item) {
        this.item = item

        @StringRes val titleRes = when (item.countryId) {
            Certification.Germany.getId() -> R.string.certification_dialog_title_germany
            Certification.UnitedStates.getId() -> R.string.certification_dialog_title_united_states
            Certification.Australia.getId() -> R.string.certification_dialog_title_australia
            else -> throw IllegalStateException()
        }
        binding.btnCountry.text = context.getString(titleRes)
    }
}
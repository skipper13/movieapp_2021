package com.nicetas.movieapp3.ui.extended.tabs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.MarginLayoutParams
import android.widget.LinearLayout
import androidx.core.content.ContextCompat.getColor
import androidx.core.view.MenuProvider
import androidx.core.view.ViewCompat
import androidx.core.view.updateLayoutParams
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.navigation.fragment.findNavController
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.google.android.material.tabs.TabLayoutMediator
import com.nicetas.models.extensions.toLocalDate
import com.nicetas.movieapp3.R
import com.nicetas.movieapp3.base.adapter.enforceSingleScrollDirection
import com.nicetas.movieapp3.base.adapter.recyclerView
import com.nicetas.movieapp3.base.extensions.getIdFromDeeplink
import com.nicetas.movieapp3.base.extensions.statusBarHeight
import com.nicetas.movieapp3.base.fragment.BaseFragment
import com.nicetas.movieapp3.ui.ImageUrl
import com.nicetas.movieapp3.ui.extended.credits.CreditsFragment
import com.nicetas.movieapp3.ui.extended.images.MovieImagesFragment
import com.nicetas.movieapp3.ui.extended.info.MovieInfoFragment
import com.nicetas.movieapp3.ui.extended.tabs.views.ViewsCodeMovieTabs
import com.nicetas.movieapp3.ui.extended.tabs.views.ViewsProviderMovieTabs
import com.nicetas.movieapp3.ui.extended.videos.MovieVideosFragment
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.time.LocalDate


class MovieTabsFragment : BaseFragment() {

    companion object {
        private const val ARG_MOVIE_ID_DEEPLINK = "ARG_MOVIE_ID_DEEPLINK"
        private const val ARG_MOVIE_ID = "ARG_MOVIE_ID"
        private const val ARG_MOVIE_TITLE = "ARG_MOVIE_TITLE"
        private const val ARG_MOVIE_BACKDROP = "ARG_MOVIE_BACKDROP"
        private const val ARG_MOVIE_POSTER = "ARG_MOVIE_POSTER"
        private const val ARG_MOVIE_RELEASE_DATE = "ARG_MOVIE_RELEASE_DATE"
        private const val ARG_MOVIE_VOTE_AVERAGE = "ARG_MOVIE_VOTE_AVERAGE"

        fun args(
            movieId: Int,
            title: String?,
            backdropPath: String?,
            posterPath: String?,
            releaseDate: LocalDate?,
            voteAverage: Double?,
        ) = Bundle().apply {
            putInt(ARG_MOVIE_ID, movieId)
            putString(ARG_MOVIE_TITLE, title)
            putString(ARG_MOVIE_BACKDROP, backdropPath)
            putString(ARG_MOVIE_POSTER, posterPath)
            releaseDate?.let {
                putString(ARG_MOVIE_RELEASE_DATE, releaseDate.toString())
            }
            voteAverage?.let {
                putDouble(ARG_MOVIE_VOTE_AVERAGE, voteAverage)
            }
        }
    }

    private enum class Tab(val pos: Int) {
        Info(0),
        Credits(1),
        Backdrops(2),
        Videos(3),
    }

    private var _viewsHolder: ViewsProviderMovieTabs? = null
    private val viewsHolder: ViewsProviderMovieTabs
        get() = _viewsHolder!!

    private val viewModel: MovieTabsViewModel by viewModel()

    private val movieId: Int by lazy {
        val args = requireArguments()
        if (args.containsKey(ARG_MOVIE_ID)) {
            args.getInt(ARG_MOVIE_ID)
        } else {
            args.getString(ARG_MOVIE_ID_DEEPLINK, "0").getIdFromDeeplink()
        }
    }
    private var title: String = ""
    private var backdropPath: String? = null
    private var posterPath: String? = null
    private var releaseDate: LocalDate? = null
    private var voteAverage: Double? = null

    private var menuItemAddToWatch: MenuItem? = null
    private var menuItemRemoveFromWatch: MenuItem? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (movieId == 0) {
            findNavController().navigateUp()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // ViewsBindingMovieTabs or ViewsCodeMovieTabs
        _viewsHolder = ViewsCodeMovieTabs(movieId)
        return viewsHolder.onCreateView(inflater, container)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewsHolder.onViewCreated(view)
        setupInfoFromArgs()
        setupOptionsMenu()
        setupViews()
        setupObservers()
        viewModel.onViewCreated(movieId)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        viewsHolder.onDestroyView()
        _viewsHolder = null
        menuItemAddToWatch = null
        menuItemRemoveFromWatch = null
    }

    private fun setupViews() {
        setStatusBarColor(getColor(requireContext(), R.color.grey_424242_alpha_30))

        ViewCompat.setOnApplyWindowInsetsListener(viewsHolder.coordinator) { _, insets ->
            val statusBarHeight = insets.statusBarHeight()
            viewsHolder.toolbar.updateLayoutParams<MarginLayoutParams> {
                topMargin = statusBarHeight
            }
            insets
        }

        /* Problem: collapsed content blink after returning to this fragment or when rotate device.
           WA: Initial animation duration is "0", and now set default duration */
        viewsHolder.ctlExtended.post {
            _viewsHolder?.ctlExtended?.scrimAnimationDuration = 600
        }

        viewsHolder.viewPager.adapter = object : FragmentStateAdapter(this) {
            override fun createFragment(position: Int): Fragment {
                return when (position) {
                    Tab.Info.pos -> MovieInfoFragment.newInstance(movieId)
                    Tab.Credits.pos -> CreditsFragment.newInstance(movieId)
                    Tab.Backdrops.pos -> MovieImagesFragment.newInstance(movieId, title)
                    Tab.Videos.pos -> MovieVideosFragment.newInstance(movieId)
                    else -> throw IllegalStateException("no fragment for pos: $position")
                }
            }

            override fun getItemCount(): Int = 4
        }
        TabLayoutMediator(viewsHolder.tabLayout, viewsHolder.viewPager) { tab, position ->
            tab.text = when (position) {
                Tab.Info.pos -> getString(R.string.movie_tabs_info)
                Tab.Credits.pos -> getString(R.string.movie_tabs_cast)
                Tab.Backdrops.pos -> getString(R.string.movie_tabs_backdrops)
                Tab.Videos.pos -> getString(R.string.movie_tabs_videos)
                else -> throw IllegalStateException("no fragment for pos: $position")
            }
        }.attach()
        allowScrollableTabsWithFillGravity()
        viewsHolder.viewPager.recyclerView.enforceSingleScrollDirection()
    }

    private fun setupOptionsMenu() {
        requireActivity().addMenuProvider(object : MenuProvider {
            override fun onCreateMenu(menu: Menu, menuInflater: MenuInflater) {
                menuInflater.inflate(R.menu.menu_movie_tabs, menu)

                val isInit = viewModel.isInWatchlist.isInitialized

                menuItemAddToWatch = menu.findItem(R.id.action_add_to_watchlist).apply {
                    if (isInit) {
                        isVisible = viewModel.isInWatchlist.value!!.not()
                    } else {
                        isVisible = false
                    }
                }
                menuItemRemoveFromWatch = menu.findItem(R.id.action_remove_from_watchlist).apply {
                    if (isInit) {
                        isVisible = viewModel.isInWatchlist.value!!
                    } else {
                        isVisible = false
                    }
                }
            }

            override fun onMenuItemSelected(menuItem: MenuItem): Boolean {
                return when (menuItem.itemId) {
                    R.id.action_add_to_watchlist -> {
                        viewModel.addToWatchlist(
                            movieId = movieId,
                            title = title,
                            backdropPath = backdropPath,
                            posterPath = posterPath,
                            releaseDate = releaseDate,
                            voteAverage = voteAverage,
                        )
                        true
                    }
                    R.id.action_remove_from_watchlist -> {
                        viewModel.removeFromWatchlist(movieId)
                        true
                    }
                    else -> false
                }
            }
        }, viewLifecycleOwner, Lifecycle.State.RESUMED)
    }

    private fun setupInfoFromArgs() {
        val args = requireArguments()

        title = args.getString(ARG_MOVIE_TITLE, "")
        viewsHolder.ctlExtended.title = title

        backdropPath = args.getString(ARG_MOVIE_BACKDROP, null)
        setBackdrop()

        val dateStr = args.getString(ARG_MOVIE_RELEASE_DATE, null)
        releaseDate = dateStr?.toLocalDate()

        val vote = args.getDouble(ARG_MOVIE_VOTE_AVERAGE, -1.0)
        if (vote != -1.0) {
            voteAverage = vote
        }
    }

    fun updateInfoIfMissing(
        title: String?,
        backdropPath: String?,
        releaseDate: LocalDate?,
        voteAverage: Double?,
    ) {
        // To avoid blinking in case of different image. Just for deeplink.
        if (this.backdropPath == null) {
            this.backdropPath = backdropPath
            setBackdrop()
        }
        title?.let {
            this.title = title
            /* Do not change title via "supportActionBar" here, because there is no
            guarantee of title update after language change */
            viewsHolder.ctlExtended.title = title
        }

        this.releaseDate = releaseDate
        this.voteAverage = voteAverage
    }

    private fun setupObservers() {
        viewModel.isInWatchlist.observe(viewLifecycleOwner) { isInWatchlist ->
            menuItemAddToWatch?.isVisible = isInWatchlist.not()
            menuItemRemoveFromWatch?.isVisible = isInWatchlist
        }
    }

    private fun setBackdrop() {
        Glide.with(requireContext())
            .load(ImageUrl.w500(backdropPath))
            .transition(DrawableTransitionOptions.withCrossFade())
            .centerCrop()
            .into(viewsHolder.ivMovie)
    }

    // https://stackoverflow.com/a/39018131
    private fun allowScrollableTabsWithFillGravity() {
        val slidingTabStrip = viewsHolder.tabLayout.getChildAt(0) as ViewGroup

        for (i in 0 until viewsHolder.tabLayout.tabCount) {
            val tab = slidingTabStrip.getChildAt(i)
            val layoutParams = tab.layoutParams as LinearLayout.LayoutParams
            layoutParams.weight = 1f
            tab.layoutParams = layoutParams
        }
    }

}
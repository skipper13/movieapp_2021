package com.nicetas.movieapp3.ui.extended.date

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import com.nicetas.movieapp3.R
import com.nicetas.movieapp3.databinding.FragmentDateFormatDialogBinding
import com.nicetas.prefs.PrefsKey.DateFormatOrder.D_M_Y
import com.nicetas.prefs.PrefsKey.DateFormatOrder.M_D_Y
import com.nicetas.prefs.PrefsKey.DateFormatOrder.Y_M_D
import org.koin.androidx.viewmodel.ext.android.viewModel

class DateFormatDialog : DialogFragment() {

    companion object {
        const val FRAGMENT_RESULT_KEY = "FRAGMENT_RESULT_KEY_DateFormatDialog"

        fun newInstance() = DateFormatDialog()
    }

    private var _binding: FragmentDateFormatDialogBinding? = null
    private val binding
        get() = _binding!!

    private val viewModel: DateFormatViewModel by viewModel()

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        _binding = FragmentDateFormatDialogBinding.inflate(layoutInflater)

        binding.btnDot.setOnClickListener {
            setTextToOrderButtons(".")
        }
        binding.btnSlash.setOnClickListener {
            setTextToOrderButtons("/")
        }
        binding.btnHyphen.setOnClickListener {
            setTextToOrderButtons("-")
        }

        return AlertDialog.Builder(requireActivity())
            .setTitle(R.string.date_format_title)
            .setView(binding.root)
            .setPositiveButton(android.R.string.ok) { _, _ ->
                saveFormat()
            }
            .setNegativeButton(android.R.string.cancel, null)
            .create()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupObservers()
        viewModel.onViewCreated()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun setupObservers() {
        viewModel.update.observe(viewLifecycleOwner) { format ->
            when (format.separator) {
                "-" -> binding.btnHyphen.isChecked = true
                "/" -> binding.btnSlash.isChecked = true
                else -> binding.btnDot.isChecked = true
            }
            setTextToOrderButtons(format.separator)
            when (format.order) {
                M_D_Y -> binding.btnMonthDayYear.isChecked = true
                Y_M_D -> binding.btnYearMonthDay.isChecked = true
                else -> binding.btnDayMonthYear.isChecked = true
            }
        }
        viewModel.onRestoreScreen.observe(viewLifecycleOwner) { separator ->
            setTextToOrderButtons(separator)
        }
    }

    private fun setTextToOrderButtons(separator: String) {
        viewModel.selectedSeparator = separator
        binding.btnDayMonthYear.text = getString(R.string.date_format_day_month_year, separator)
        binding.btnMonthDayYear.text = getString(R.string.date_format_month_day_year, separator)
        binding.btnYearMonthDay.text = getString(R.string.date_format_year_month_day, separator)
    }

    private fun saveFormat() {
        val order = when {
            binding.btnMonthDayYear.isChecked -> M_D_Y
            binding.btnYearMonthDay.isChecked -> Y_M_D
            else -> D_M_Y
        }
        val separator = when {
            binding.btnHyphen.isChecked -> "-"
            binding.btnSlash.isChecked -> "/"
            else -> "."
        }
        viewModel.saveFormat(order, separator)
    }

}
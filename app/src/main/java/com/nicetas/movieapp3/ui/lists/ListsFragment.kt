package com.nicetas.movieapp3.ui.lists

import android.os.Bundle
import android.view.View
import android.view.ViewGroup.MarginLayoutParams
import androidx.core.content.ContextCompat.getColor
import androidx.core.view.ViewCompat
import androidx.core.view.updateLayoutParams
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.nicetas.movieapp3.R
import com.nicetas.movieapp3.base.adapter.GridSpacingItemDecoration
import com.nicetas.movieapp3.base.MoviesSpanCount
import com.nicetas.movieapp3.base.adapter.ScrollOnTapHelper
import com.nicetas.movieapp3.base.extensions.dp
import com.nicetas.movieapp3.base.extensions.statusBarHeight
import com.nicetas.movieapp3.base.fragment.BaseFragment
import com.nicetas.movieapp3.base.states.ViewState
import com.nicetas.movieapp3.databinding.FragmentListsBinding
import com.nicetas.movieapp3.ui.lists.adapter.ListsAdapter
import com.nicetas.movieapp3.ui.lists.list.ListFragment
import org.koin.androidx.viewmodel.ext.android.viewModel

class ListsFragment : BaseFragment(R.layout.fragment_lists) {

    private var _binding: FragmentListsBinding? = null
    private val binding
        get() = _binding!!

    private val viewModel: ListsViewModel by viewModel()

    private var adapter: ListsAdapter? = null
    private var scrollOnTapHelper: ScrollOnTapHelper? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentListsBinding.bind(view)
        scrollOnTapHelper = ScrollOnTapHelper(savedInstanceState)
        initViewState()
        setupViews()
        initRecyclerView()
        setupObservers()
        viewModel.onViewCreated()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        scrollOnTapHelper?.onSaveInstanceState(outState)
    }

    override fun onDestroyView() {
        adapter?.run { items.list.clear() }
        adapter = null
        binding.rvContent.adapter = null
        scrollOnTapHelper = null
        super.onDestroyView()
        _binding = null
    }

    private fun initViewState() {
        ViewState.Builder(binding.rvContent)
            .loadDelay()
            .error(onClick = { viewModel.reloadMovies() })
            .empty(binding.tvEmptyState)
            .build()
            .observeLce(viewModel.lce, viewLifecycleOwner)
    }

    private fun setupViews() {
        setStatusBarColor(getColor(requireContext(), R.color.colorPrimary))
        setToolbarTitle(getString(R.string.lists_title))

        ViewCompat.setOnApplyWindowInsetsListener(binding.root) { _, insets ->
            val statusBarHeight = insets.statusBarHeight()
            binding.appBar.updateLayoutParams<MarginLayoutParams> {
                topMargin = statusBarHeight
            }
            binding.rvContent.updateLayoutParams<MarginLayoutParams> {
                bottomMargin = statusBarHeight
            }
            insets
        }
        binding.toolbar.setOnClickListener {
            scrollOnTapHelper?.scroll(binding.rvContent)
        }
    }

    private fun initRecyclerView() {
        adapter = ListsAdapter(
            listsCallback = { listId, name ->
                val args = ListFragment.args(
                    listId = listId,
                    name = name,
                )
                findNavController().navigate(R.id.action_lists_to_list, args)
            }
        )
        binding.rvContent.adapter = adapter
        binding.rvContent.itemAnimator = null

        val spanCount = MoviesSpanCount().get(requireActivity(), false)
        binding.rvContent.layoutManager = GridLayoutManager(requireContext(), spanCount)
        binding.rvContent.addItemDecoration(GridSpacingItemDecoration(spanCount, 4f.dp, false))
    }

    private fun setupObservers() {
        viewModel.onGetLists.observe(viewLifecycleOwner) { lists ->
            adapter?.update(lists)
        }
    }

}
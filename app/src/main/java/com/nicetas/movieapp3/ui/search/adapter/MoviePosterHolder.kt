package com.nicetas.movieapp3.ui.search.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.nicetas.movieapp3.R
import com.nicetas.movieapp3.base.adapter.ClickDebounce
import com.nicetas.movieapp3.databinding.ItemMoviePosterBinding
import com.nicetas.movieapp3.ui.ImageUrl
import java.time.LocalDate

class MoviePosterHolder(
    parent: ViewGroup,
    inflater: LayoutInflater,
    private var binding: ItemMoviePosterBinding =
        ItemMoviePosterBinding.inflate(inflater, parent, false),
    debounce: ClickDebounce,
    callback: MovieHolder.Callback,
    private val voteFormatter: MovieHolder.VoteFormatter,
) : RecyclerView.ViewHolder(binding.root) {

    private lateinit var item: MovieHolder.Item

    init {
        binding.root.setOnClickListener {
            if (debounce.canClick()) {
                callback.onMovieClick(item)
            }
        }
        binding.ivBookmark.setOnClickListener {
            if (debounce.canClick()) {
                callback.removeFromBookmarks(item)
            }
        }
    }

    fun bind(item: MovieHolder.Item) {
        this.item = item

        binding.tvTitle.text = item.title

        bindReleaseDate(item.releaseDate)
        bindVoteAverage(item.voteAverage)

        Glide.with(binding.ivPoster)
            .load(ImageUrl.w342(item.posterPath))
            .transition(DrawableTransitionOptions.withCrossFade())
            .placeholder(R.drawable.movie_image_placeholder)
            .centerCrop()
            .into(binding.ivPoster)

        binding.ivBookmark.isVisible = item.isInWatchlist
    }

    private fun bindVoteAverage(voteAverage: Double?) {
        if (voteAverage != null && voteAverage > 0.0) {
            binding.ivVoteAverage.isVisible = true
            binding.tvVoteAverage.isVisible = true
            binding.tvVoteAverage.text = voteFormatter.format(voteAverage)
        } else {
            binding.tvVoteAverage.isInvisible = true
            binding.ivVoteAverage.isInvisible = true
        }
    }

    private fun bindReleaseDate(releaseDate: LocalDate?) {
        if (releaseDate == null) {
            binding.ivReleaseDate.isVisible = false
            binding.tvReleaseDate.isVisible = false
        } else {
            binding.ivReleaseDate.isVisible = true
            binding.tvReleaseDate.isVisible = true
            binding.tvReleaseDate.text = releaseDate.year.toString()
        }
    }

}
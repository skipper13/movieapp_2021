package com.nicetas.movieapp3.ui.search.options.kit

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.setFragmentResult
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.nicetas.movieapp3.R
import com.nicetas.movieapp3.base.adapter.RvScrollbarsHelper
import com.nicetas.movieapp3.base.extensions.dp
import com.nicetas.movieapp3.ui.search.options.kit.adapter.SearchKitHolder
import com.nicetas.movieapp3.ui.search.options.kit.adapter.SearchKitsAdapter
import org.koin.androidx.viewmodel.ext.android.viewModel

class SearchKitsDialog : DialogFragment() {

    companion object {
        const val FRAGMENT_RESULT_KEY = "FRAGMENT_RESULT_KEY_SearchKitsDialog"

        fun newInstance() = SearchKitsDialog()
    }

    private val viewModel: SearchKitsViewModel by viewModel()

    private var recyclerView: RecyclerView? = null
    private var adapter: SearchKitsAdapter? = null
    private val resultBundle = Bundle()

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        recyclerView = RecyclerView(requireContext(), RvScrollbarsHelper.attrs).apply {
            id = viewModel.recyclerViewId
            layoutParams = ViewGroup.LayoutParams(MATCH_PARENT, WRAP_CONTENT)
            setPadding(0, 8F.dp, 0, 8F.dp)
            clipToPadding = false
            overScrollMode = View.OVER_SCROLL_IF_CONTENT_SCROLLS
            layoutManager = LinearLayoutManager(requireContext())
        }

        adapter = SearchKitsAdapter(
            kitCallback = object : SearchKitHolder.Callback {
                override fun onKitClick(id: Int) {
                    viewModel.selectKit(id)
                }

                override fun deleteKit(item: SearchKitHolder.Item) {
                    AlertDialog.Builder(requireActivity())
                        .setTitle(R.string.search_kits_delete)
                        .setMessage(item.text)
                        .setPositiveButton(android.R.string.ok) { _, _ ->
                            viewModel.deleteKit(item.id)
                        }
                        .setNegativeButton(android.R.string.cancel, null)
                        .show()
                }
            },
        )
        recyclerView!!.adapter = adapter

        return AlertDialog.Builder(requireActivity())
            .setView(recyclerView)
            .setNeutralButton(R.string.search_kits_create) { _, _ ->
                findNavController().navigate(R.id.createKitScreen)
                dismiss()
            }
            .setNegativeButton(android.R.string.cancel, null)
            .create()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return recyclerView!!
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupObservers()
        viewModel.onViewCreated()
    }

    override fun onDestroyView() {
        adapter?.run { items.list.clear() }
        adapter = null
        super.onDestroyView()
        recyclerView = null
    }

    private fun setupObservers() {
        viewModel.updateOptionsScreen.observe(viewLifecycleOwner) {
            setFragmentResult(FRAGMENT_RESULT_KEY, resultBundle)
        }
        viewModel.showOptions.observe(viewLifecycleOwner) { items ->
            adapter?.items?.replaceAll(items)
        }
        viewModel.closeDialog.observe(viewLifecycleOwner) {
            setFragmentResult(FRAGMENT_RESULT_KEY, resultBundle)
            dismiss()
        }
    }

}
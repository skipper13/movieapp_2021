package com.nicetas.movieapp3.ui.extended.images

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.widget.FrameLayout
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.nicetas.movieapp3.R
import com.nicetas.movieapp3.base.adapter.GridSpacingItemDecoration
import com.nicetas.movieapp3.base.adapter.RvScrollbarsHelper
import com.nicetas.movieapp3.base.extensions.dp
import com.nicetas.movieapp3.base.extensions.isLandscape
import com.nicetas.movieapp3.base.fragment.BaseFragment
import com.nicetas.movieapp3.base.states.ViewState
import com.nicetas.movieapp3.ui.ImageUrl
import com.nicetas.movieapp3.ui.extended.images.adapter.MovieImagesAdapter
import com.nicetas.movieapp3.ui.fullscreen.FullscreenImagesFragment
import org.koin.androidx.viewmodel.ext.android.viewModel

class MovieImagesFragment : BaseFragment() {

    companion object {
        private const val ARG_MOVIE_ID = "ARG_MOVIE_ID"
        private const val ARG_MOVIE_TITLE = "ARG_MOVIE_TITLE"

        fun newInstance(
            movieId: Int,
            title: String,
        ) = MovieImagesFragment().apply {
            arguments = Bundle(2).apply {
                putInt(ARG_MOVIE_ID, movieId)
                putString(ARG_MOVIE_TITLE, title)
            }
        }
    }

    private val viewModel: MovieImagesViewModel by viewModel()

    private var rvContent: RecyclerView? = null
    private var adapter: MovieImagesAdapter? = null

    private val movieId: Int by lazy { requireArguments().getInt(ARG_MOVIE_ID) }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val vgRoot = FrameLayout(requireContext())
        vgRoot.layoutParams = ViewGroup.LayoutParams(MATCH_PARENT, MATCH_PARENT)

        rvContent = RecyclerView(requireContext(), RvScrollbarsHelper.attrs).apply {
            id = viewModel.recyclerViewId
            layoutParams = ViewGroup.LayoutParams(MATCH_PARENT, MATCH_PARENT)
            setPadding(0, 8F.dp, 0, 8F.dp)
            clipToPadding = false
        }
        vgRoot.addView(rvContent)
        return vgRoot
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewState()
        initRecyclerView()
        setupObservers()
        viewModel.onViewCreated(movieId)
    }

    override fun onDestroyView() {
        adapter?.run { items.list.clear() }
        adapter = null
        rvContent?.adapter = null
        rvContent = null
        super.onDestroyView()
    }

    private fun initViewState() {
        ViewState.Builder(rvContent!!)
            .load()
            .error(onClick = { viewModel.loadImages(movieId) })
            .build()
            .observeLce(viewModel.lce, viewLifecycleOwner)
    }

    private fun initRecyclerView() {
        adapter = MovieImagesAdapter(
            onImageClick = { position ->
                val args = FullscreenImagesFragment.args(
                    title = requireArguments().getString(ARG_MOVIE_TITLE, ""),
                    imageSize = ImageUrl.w780,
                    images = viewModel.backdrops,
                    position = position
                )
                findNavController().navigate(R.id.fullscreenImagesScreen, args)
            }
        )
        val rvContent = rvContent ?: return
        rvContent.adapter = adapter

        val spanCount = if (resources.isLandscape()) 2 else 1
        rvContent.layoutManager = GridLayoutManager(requireContext(), spanCount)
        rvContent.addItemDecoration(GridSpacingItemDecoration(spanCount, 4f.dp, false))
    }

    private fun setupObservers() {
        viewModel.onGetImages.observe(viewLifecycleOwner) { images ->
            adapter?.items?.replaceAll(images)
        }
    }

}
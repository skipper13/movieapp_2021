package com.nicetas.movieapp3.ui.sort

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.nicetas.movieapp3.base.adapter.ClickDebounce
import com.nicetas.movieapp3.databinding.ItemSortByHeaderBinding

class SortByHeaderHolder(
    parent: ViewGroup,
    private var binding: ItemSortByHeaderBinding = ItemSortByHeaderBinding.inflate(
        LayoutInflater.from(parent.context),
        parent,
        false
    ),
    debounce: ClickDebounce,
    callback: Callback,
) : RecyclerView.ViewHolder(binding.root) {

    fun interface Callback {
        fun onRadioClick(isDescending: Boolean)
    }

    class Item(val isDescending: Boolean)

    init {
        binding.radioButtonAsc.setOnClickListener {
            if (debounce.canClick()) {
                callback.onRadioClick(isDescending = false)
            }
        }
        binding.radioButtonDesc.setOnClickListener {
            if (debounce.canClick()) {
                callback.onRadioClick(isDescending = true)
            }
        }
    }

    fun bind(item: Item) {
        binding.radioButtonAsc.isChecked = item.isDescending.not()
        binding.radioButtonDesc.isChecked = item.isDescending
    }

}
package com.nicetas.movieapp3.ui.extended.actor.photo.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.nicetas.movieapp3.base.adapter.AdapterItems
import com.nicetas.movieapp3.base.adapter.ClickDebounce

class ActorPhotosAdapter(
    private val onPhotoClick: ActorPhotoHolder.Callback,
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        private const val TYPE_PHOTO = 0
    }

    private val debounce = ClickDebounce()
    val items = AdapterItems<Any>(this)

    fun onDestroyView() {
        items.list.clear()
    }

    override fun getItemCount(): Int = items.list.size

    override fun getItemViewType(position: Int): Int {
        return when (items.list[position]) {
            is ActorPhotoHolder.Item -> TYPE_PHOTO
            else -> throw IllegalStateException("item is null for position = $position")
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_PHOTO -> ActorPhotoHolder(
                parent = parent,
                debounce = debounce,
                callback = onPhotoClick,
            )
            else -> throw IllegalStateException("Unknown holder type")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = items.list[position]
        when (holder) {
            is ActorPhotoHolder -> holder.bind(item as ActorPhotoHolder.Item)
        }
    }

}
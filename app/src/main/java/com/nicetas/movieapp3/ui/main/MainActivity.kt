package com.nicetas.movieapp3.ui.main

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.view.MenuItem
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.isVisible
import androidx.core.view.updateLayoutParams
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.snackbar.Snackbar
import com.nicetas.movieapp3.R
import com.nicetas.movieapp3.base.extensions.snackBarTMDB
import com.nicetas.movieapp3.databinding.ActivityMainBinding
import com.nicetas.repos.network.Connectivity
import com.nicetas.repos.network.LanguageProvider
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

/*
 * If you're using Compose with setApplicationLocales(), you must extend your activity from
 * AppCompatActivity. Otherwise, setting the app locale won't work.
 * https://developer.android.com/guide/topics/resources/app-languages#androidx-impl
 */
class MainActivity : AppCompatActivity() {

    private val viewModel: MainViewModel by viewModel()
    private val connectivity by inject<Connectivity>()

    private lateinit var binding: ActivityMainBinding

    var isBottomNavigationVisible: Boolean
        get() = binding.bottomNavigationView.isVisible
        set(value) {
            binding.bottomNavigationView.isVisible = value
        }

    var isBottomNavigationLockedHidden = false

    var isKeyboardOpen = false
        private set

    var statusBarColor = R.color.colorPrimary
        set(value) {
            binding.vStatusBar.setBackgroundColor(value)
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initConnectivity()
        setupInsets()
        LanguageProvider.language = getString(R.string.request_lang)
        setupNavigation()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressedDispatcher.onBackPressed()
        return true
    }

    private fun setupNavigation() {
        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.fragmentNavHost) as NavHostFragment

        val navController = navHostFragment.navController

        binding.bottomNavigationView.setupWithNavController(navController)

        binding.bottomNavigationView.setOnItemReselectedListener { item: MenuItem ->
            // Pop the back stack to the start destination of the current tab
            when (item.itemId) {
                R.id.search -> navController.popBackStack(R.id.searchScreen, false)
                R.id.movies -> navController.popBackStack(R.id.watchlistScreen, false)
                R.id.profile -> Unit
            }
        }

        val shortcut = intent.extras?.getString("shortcut")
        when (shortcut) {
            "Watchlist" -> {
                binding.bottomNavigationView.selectedItemId = R.id.movies
            }
        }
    }

    fun gotoSearchTab() {
        binding.bottomNavigationView.selectedItemId = R.id.search
    }

    private fun setupInsets() {
        window.statusBarColor = Color.TRANSPARENT
        WindowCompat.setDecorFitsSystemWindows(window, false)

        ViewCompat.setOnApplyWindowInsetsListener(binding.root) { view, insets ->
            val ime = WindowInsetsCompat.Type.ime()
            val systemBars = WindowInsetsCompat.Type.systemBars()
            val systemBarsInsets = insets.getInsets(systemBars)

            binding.vStatusBar.updateLayoutParams {
                height = systemBarsInsets.top
            }

            view.updateLayoutParams<ViewGroup.MarginLayoutParams> {
                // For windowSoftInputMode="adjustResize" work
                bottomMargin = insets.getInsets(ime).bottom

                if (systemBarsInsets.bottom == 0) {
                    if (systemBarsInsets.left > systemBarsInsets.right) {
                        rightMargin = 0
                        leftMargin = systemBarsInsets.left
                    } else {
                        leftMargin = 0
                        rightMargin = systemBarsInsets.right
                    }
                }
            }

            val isKeyboardOpen = insets.isVisible(ime)
            if (isKeyboardOpen != this.isKeyboardOpen) {
                this.isKeyboardOpen = isKeyboardOpen
                viewModel.updateKeyboardVisibility(isKeyboardOpen)
            }
            if (isBottomNavigationLockedHidden.not()) {
                binding.bottomNavigationView.isVisible = isKeyboardOpen.not()
            }

            insets
        }
    }

    private val writeSettingsLauncher = registerForActivityResult(
        ActivityResultContracts.StartActivityForResult()
    ) { result ->
        if (result.resultCode != Activity.RESULT_OK
            || Build.VERSION.SDK_INT != Build.VERSION_CODES.M
        ) {
            return@registerForActivityResult
        }
        if (Settings.System.canWrite(this)) {
            connectivity.init()
        }
    }

    private fun initConnectivity() {
        if (Build.VERSION.SDK_INT != Build.VERSION_CODES.M || Settings.System.canWrite(this)) {
            connectivity.init()
            return
        }
        binding.root.snackBarTMDB(
            text = "Internet connection check needs modifying system settings",
            duration = Snackbar.LENGTH_INDEFINITE
        )
            .setAction("Open settings") {
                val intent = Intent(
                    Settings.ACTION_MANAGE_WRITE_SETTINGS,
                    Uri.parse("package:" + applicationInfo.packageName)
                )
                writeSettingsLauncher.launch(intent)
            }
            .show()
    }

}
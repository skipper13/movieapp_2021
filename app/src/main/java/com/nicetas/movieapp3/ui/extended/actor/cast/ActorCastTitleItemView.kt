package com.nicetas.movieapp3.ui.extended.actor.cast

import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.nicetas.movieapp3.ui.theme.ColorCompose
import com.nicetas.movieapp3.ui.theme.MovieApp3Theme

@Composable
fun ActorCastTitleItemView(text: String) {
    Text(
        text = text,
        color = ColorCompose.white,
        style = MaterialTheme.typography.bodyMedium,
        fontSize = 18.sp,
        modifier = Modifier
            .padding(start = 16.dp, top = 8.dp)
            .height(40.dp)
            .wrapContentHeight(align = Alignment.CenterVertically)
    )
}


@Preview
@Composable
private fun ActorCastTitleItemPreview() {
    MovieApp3Theme {
        ActorCastTitleItemView(
            text = "2014",
        )
    }
}
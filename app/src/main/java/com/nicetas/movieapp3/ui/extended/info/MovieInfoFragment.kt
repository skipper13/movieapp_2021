package com.nicetas.movieapp3.ui.extended.info

import android.animation.LayoutTransition
import android.content.res.Resources
import android.graphics.Rect
import android.os.Build
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.appcompat.content.res.AppCompatResources
import androidx.appcompat.content.res.AppCompatResources.getDrawable
import androidx.core.content.ContextCompat.getColor
import androidx.core.view.isVisible
import androidx.core.view.postDelayed
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.nicetas.models.extensions.format
import com.nicetas.models.extensions.isNullOrBlankF
import com.nicetas.models.movies.Movie
import com.nicetas.models.movies.keywords.Keyword
import com.nicetas.movieapp3.R
import com.nicetas.movieapp3.base.extensions.dp
import com.nicetas.movieapp3.base.extensions.onClickDebounce
import com.nicetas.movieapp3.base.extensions.setClipboard
import com.nicetas.movieapp3.base.fragment.BaseFragment
import com.nicetas.movieapp3.base.states.ViewState
import com.nicetas.movieapp3.databinding.FragmentMovieInfoBinding
import com.nicetas.movieapp3.ui.ImageUrl
import com.nicetas.movieapp3.ui.extended.date.DateFormatDialog
import com.nicetas.movieapp3.ui.extended.info.links.LinksAdapter
import com.nicetas.movieapp3.ui.extended.tabs.MovieTabsFragment
import com.nicetas.movieapp3.ui.fullscreen.FullscreenImagesFragment
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.time.LocalDate
import java.util.Locale


class MovieInfoFragment : BaseFragment(R.layout.fragment_movie_info) {

    companion object {
        private const val ARG_MOVIE_ID = "ARG_MOVIE_ID"
        private const val NOT_AVAILABLE = "n/a"

        fun newInstance(movieId: Int) = MovieInfoFragment().apply {
            arguments = Bundle(1).apply {
                putInt(ARG_MOVIE_ID, movieId)
            }
        }
    }

    private var _binding: FragmentMovieInfoBinding? = null
    private val binding
        get() = _binding!!

    private val viewModel: MovieInfoViewModel by viewModel()
    private var adapterLinks: LinksAdapter? = null
    private var chipKeywordFake: Chip? = null
    private var tips: MovieInfoTips? = null

    private val movieId: Int by lazy { requireArguments().getInt(ARG_MOVIE_ID) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentMovieInfoBinding.bind(view)
        setupViews()
        setupObservers()
        initViewState()
        viewModel.onViewCreated(movieId)
    }

    override fun onDestroyView() {
        adapterLinks?.run { items.list.clear() }
        adapterLinks = null
        tips?.onDestroyView()
        tips = null
        super.onDestroyView()
        chipKeywordFake = null
        _binding = null
    }

    private fun initViewState() {
        ViewState.Builder(binding.scrollViewContent)
            .load()
            .error(onClick = { viewModel.loadMovieInfo(movieId) })
            .build()
            .observeLce(viewModel.lce, viewLifecycleOwner)
    }

    private fun setupViews() {
        binding.vShowKeywords.onClickDebounce {
            binding.vShowKeywords.isVisible = false
            binding.vKeywordRightGradient.isVisible = false
            binding.chipsKeywords.isSingleLine = false
            binding.chipsKeywords.removeViews(binding.chipsKeywords.childCount - 1, 1)
        }

        binding.rvLinks.addOnItemTouchListener(object : RecyclerView.SimpleOnItemTouchListener() {
            override fun onInterceptTouchEvent(rv: RecyclerView, e: MotionEvent): Boolean {
                if (e.action == MotionEvent.ACTION_MOVE) {
                    rv.parent.requestDisallowInterceptTouchEvent(true)
                }
                return false
            }
        })
        adapterLinks = LinksAdapter()
        binding.rvLinks.adapter = adapterLinks

        // Because of textIsSelectable="true" click from setOnClickListener works only with second tap
        val releaseDateGestureDetector =
            GestureDetector(requireContext(), object : GestureDetector.SimpleOnGestureListener() {
                override fun onSingleTapConfirmed(e: MotionEvent): Boolean {
                    DateFormatDialog.newInstance().show(parentFragmentManager, null)
                    return super.onSingleTapConfirmed(e)
                }
            })
        // noinspection ClickableViewAccessibility
        binding.tvReleaseDate.setOnTouchListener { v, event ->
            releaseDateGestureDetector.onTouchEvent(event)
        }
    }

    private fun setupObservers() {
        viewModel.onGetMovie.observe(viewLifecycleOwner) { movie ->
            setData(movie)
        }
        viewModel.updateReleaseDate.observe(viewLifecycleOwner) { releaseDate ->
            bindReleaseDate(releaseDate)
        }
        viewModel.showTips.observe(viewLifecycleOwner) {
            // ".post" to wait for locations of views on screen
            binding.root.post {
                _binding ?: return@post
                tips = MovieInfoTips(
                    fragment = this,
                    scrollView = binding.scrollViewContent,
                    tvReleaseDate = binding.tvReleaseDate,
                    vgKeywords = binding.vgKeywords,
                    vgLinks = binding.vgLinks,
                    vBottomSpaceForTips = binding.vBottomSpaceForTips,
                    onTipsEnd = {
                        viewModel.onTipsEnd()
                    }
                )
                tips!!.showTips()
            }
        }
    }

    private fun setData(movie: Movie) {
        (parentFragment as? MovieTabsFragment)?.updateInfoIfMissing(
            title = movie.title,
            backdropPath = movie.backdropPath,
            releaseDate = movie.releaseDate,
            voteAverage = movie.voteAverage,
        )

        Glide.with(requireContext())
            .load(ImageUrl.w342(movie.posterPath))
            .placeholder(getDrawable(requireContext(), R.drawable.placeholder_130x200))
            .transition(DrawableTransitionOptions.withCrossFade())
            .centerCrop()
            .into(binding.ivPoster)

        if (movie.posterPath != null) {
            binding.ivPoster.onClickDebounce {
                val args = FullscreenImagesFragment.args(
                    title = movie.title ?: "",
                    imageSize = ImageUrl.w780,
                    images = listOf(movie.posterPath!!)
                )
                findNavController().navigate(R.id.fullscreenImagesScreen, args)
            }
        }

        bindTagline(movie.tagline)

        binding.tvTitle.text = getTextOrNA(R.string.movie_info_title, movie.title)

        binding.tvOriginalTitle.text =
            getTextOrNA(R.string.movie_info_original_title, movie.originalTitle)

        bindReleaseDate(movie.releaseDate)

        binding.tvGenres.text = getTextOrNA(
            R.string.movie_info_genres,
            movie.genres.joinToString { it.name }
        )
        binding.tvCountries.text = getTextOrNA(
            R.string.movie_info_countries,
            movie.productionCountries.joinToString { it.name }
        )
        binding.tvProductionCompanies.text = getTextOrNA(
            R.string.movie_info_production_companies,
            movie.productionCompanies.joinToString { it.name }
        )

        setFormattedSum(binding.tvBudget, movie.budget)
        setFormattedSum(binding.tvRevenue, movie.revenue)

        bindRuntime(movie.runtime)

        binding.tvPopularity.text = DecimalFormat("#.##").format(movie.popularity ?: 0.0)
        // noinspection SetTextI18n
        binding.tvVotes.text = "${movie.voteAverage ?: 0.0} (${movie.voteCount ?: 0})"

        if (movie.overview.isNullOrBlankF()) {
            binding.tvOverview.isVisible = false
            binding.vDividerOverview.root.isVisible = false
        } else {
            binding.tvOverview.text = movie.overview
        }

        bindKeywords(movie.keywords.keywords)

        adapterLinks?.fill(
            context = requireContext(),
            movieId = movie.id,
            imdbId = movie.imdbId,
            ids = movie.externalIds,
        )
    }

    private fun bindTagline(tagline: String?) {
        if (tagline.isNullOrBlankF()) {
            binding.tvTagline.isVisible = false
            return
        }
        binding.tvTagline.text = tagline
    }

    private fun bindReleaseDate(releaseDate: LocalDate?) {
        if (releaseDate == null) {
            binding.tvReleaseDate.text = NOT_AVAILABLE
            return
        }
        binding.tvReleaseDate.text = releaseDate.format(
            order = viewModel.dateOrder(),
            separator = viewModel.dateSeparator(),
        )
    }

    private fun bindRuntime(runtime: Int?) {
        if (runtime == null || runtime == 0) {
            binding.tvRuntime.text = NOT_AVAILABLE
            return
        }
        if (runtime > 59) {
            val hours = runtime / 60
            val min = runtime - (hours * 60)
            binding.tvRuntime.text = getString(R.string.movie_info_runtime_h_m, hours, min)
        } else {
            binding.tvRuntime.text = getString(R.string.movie_info_runtime_m, runtime)
        }
    }

    private fun getTextOrNA(@StringRes stringRes: Int, string: String?): SpannableString {
        val str = if (string.isNullOrBlankF()) NOT_AVAILABLE else string

        val span = SpannableString(getString(stringRes, str))
        span.setSpan(
            ForegroundColorSpan(getColor(requireContext(), R.color.grey_969696)),
            0, span.indexOf(str),
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        return span
    }

    private fun setFormattedSum(textView: TextView, sum: Long?) {
        textView.text = if (sum != null && sum > 0L) {
            "${getFormattedSum(sum)} $"    // 1000000 -> 1'000'000
        } else {
            NOT_AVAILABLE
        }
    }

    private fun getFormattedSum(sum: Long): String {
        return DecimalFormat("#,##0", DecimalFormatSymbols(Locale.UK).apply {
            groupingSeparator = '\''
        }).format(sum)
    }

    private fun bindKeywords(keywords: List<Keyword>) {
        if (keywords.isEmpty()) {
            binding.vgKeywords.isVisible = false
            binding.vDividerKeywords.root.isVisible = false
            return
        }

        binding.chipsKeywords.layoutTransition.disableTransitionType(LayoutTransition.APPEARING)

        for (i in keywords.indices) {
            val keyword = keywords[i]

            val chip = Chip(context).apply {
                layoutParams = ChipGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, 28F.dp)
                chipBackgroundColor = AppCompatResources
                    .getColorStateList(context, R.color.grey_606060)
                setEnsureMinTouchTargetSize(false)
                setTextColor(getColor(context, R.color.grey_d9d9d9))
                setPadding(0, 0, 0, 0)
                text = keyword.name
                setOnClickListener { setClipboard(keyword.name) }
            }
            binding.chipsKeywords.addView(chip)
        }
        // This chip helps to decide if need to show [vShowKeywords] or not.
        // This chip will be removed on [vShowKeywords] click.
        chipKeywordFake = Chip(context).apply {
            layoutParams = ChipGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, 28F.dp)
            chipBackgroundColor = AppCompatResources
                .getColorStateList(context, R.color.cardview_dark_background)
            isClickable = false
            setEnsureMinTouchTargetSize(false)
        }
        binding.chipsKeywords.addView(chipKeywordFake)

        binding.chipsKeywords.postDelayed(100) {
            if (isFakeChipInScreenWidthBounds(chipKeywordFake)) {
                _binding?.vShowKeywords?.isVisible = false
            }
        }
    }

    private fun setClipboard(text: String) {
        requireContext().setClipboard(text)
        // https://developer.android.com/develop/ui/views/touch-and-input/copy-paste#duplicate-notifications
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.S_V2) {
            Toast.makeText(context, "Copied: \"$text\"", Toast.LENGTH_SHORT).show()
        }
    }

    private fun isFakeChipInScreenWidthBounds(view: View?): Boolean {
        if (view == null || !view.isShown) {
            return false
        }
        val actualPosition = Rect()
        view.getGlobalVisibleRect(actualPosition)
        val displayMetrics = Resources.getSystem().displayMetrics
        val screenDoubleHeight = displayMetrics.heightPixels + displayMetrics.heightPixels
        val screen = Rect(0, 0, displayMetrics.widthPixels, screenDoubleHeight)
        return actualPosition.intersect(screen)
    }

}
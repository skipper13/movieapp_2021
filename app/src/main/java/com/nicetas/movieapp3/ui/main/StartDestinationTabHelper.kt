package com.nicetas.movieapp3.ui.main

import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment

class StartDestinationTabHelper {

    fun onBackPressedDispatcher(fragment: Fragment) {
        val isDeeplink = fragment.activity?.intent?.data != null
        if (isDeeplink) {
            // Let user go back to first opened screen from deeplink for app close possibility.
            // Otherwise there will be an infinite cycle (tab1 <-> tab2 or 3) on back-pressed.
            return
        }
        fragment.requireActivity().onBackPressedDispatcher.addCallback(
            owner = fragment,
            onBackPressedCallback = object : OnBackPressedCallback(enabled = true) {
                override fun handleOnBackPressed() {
                    (fragment.activity as? MainActivity)?.gotoSearchTab()
                }
            }
        )
    }

}
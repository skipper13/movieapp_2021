package com.nicetas.movieapp3.ui.tv

import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.view.View
import android.view.ViewGroup.MarginLayoutParams
import androidx.annotation.StringRes
import androidx.appcompat.content.res.AppCompatResources.getDrawable
import androidx.core.content.ContextCompat.getColor
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.isVisible
import androidx.core.view.updateLayoutParams
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.nicetas.models.extensions.format
import com.nicetas.models.extensions.isNullOrBlankF
import com.nicetas.models.extensions.toLocalDate
import com.nicetas.movieapp3.R
import com.nicetas.movieapp3.base.extensions.onClickDebounce
import com.nicetas.movieapp3.base.fragment.BaseFragment
import com.nicetas.movieapp3.databinding.FragmentTvInfoBinding
import com.nicetas.movieapp3.ui.ImageUrl
import com.nicetas.movieapp3.ui.fullscreen.FullscreenImagesFragment
import java.text.DecimalFormat
import java.time.LocalDate

class TvInfoFragment : BaseFragment(R.layout.fragment_tv_info) {

    companion object {
        private const val ARG_TV_ID = "ARG_TV_ID"
        private const val ARG_TV_NAME = "ARG_TV_NAME"
        private const val ARG_TV_ORIGINAL_NAME = "ARG_TV_ORIGINAL_NAME"
        private const val ARG_TV_OVERVIEW = "ARG_TV_OVERVIEW"
        private const val ARG_TV_POSTER_PATH = "ARG_TV_POSTER_PATH"
        private const val ARG_TV_POPULARITY = "ARG_TV_POPULARITY"
        private const val ARG_TV_VOTE_AVERAGE = "ARG_TV_VOTE_AVERAGE"
        private const val ARG_TV_VOTE_COUNT = "ARG_TV_VOTE_COUNT"
        private const val ARG_TV_FIRST_AIR_DATE = "ARG_TV_FIRST_AIR_DATE"
        private const val NOT_AVAILABLE = "n/a"

        fun args(
            tvId: Int,
            name: String,
            originalName: String?,
            overview: String?,
            posterPath: String?,
            popularity: Double,
            voteAverage: Double,
            voteCount: Int,
            firstAirDate: LocalDate?,
        ) = Bundle(9).apply {
            putInt(ARG_TV_ID, tvId)
            putString(ARG_TV_NAME, name)
            putString(ARG_TV_ORIGINAL_NAME, originalName)
            putString(ARG_TV_OVERVIEW, overview)
            putString(ARG_TV_POSTER_PATH, posterPath)
            putDouble(ARG_TV_POPULARITY, popularity)
            putDouble(ARG_TV_VOTE_AVERAGE, voteAverage)
            putInt(ARG_TV_VOTE_COUNT, voteCount)
            putString(ARG_TV_FIRST_AIR_DATE, firstAirDate?.toString())
        }
    }

    private var _binding: FragmentTvInfoBinding? = null
    private val binding
        get() = _binding!!

    private val args: Bundle
        get() = requireArguments()

    private val tvId: Int by lazy { args.getInt(ARG_TV_ID) }
    private val name: String by lazy { args.getString(ARG_TV_NAME, "") }
    private val originalName: String? by lazy { args.getString(ARG_TV_ORIGINAL_NAME) }
    private val overview: String? by lazy { args.getString(ARG_TV_OVERVIEW) }
    private val posterPath: String? by lazy { args.getString(ARG_TV_POSTER_PATH) }
    private val popularity: Double by lazy { args.getDouble(ARG_TV_POPULARITY, 0.0) }
    private val voteAverage: Double by lazy { args.getDouble(ARG_TV_VOTE_AVERAGE, 0.0) }
    private val voteCount: Int by lazy { args.getInt(ARG_TV_VOTE_COUNT) }
    private val firstAirDate: String? by lazy { args.getString(ARG_TV_FIRST_AIR_DATE) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentTvInfoBinding.bind(view)
        setStatusBarColor(getColor(requireContext(), R.color.colorPrimary))

        ViewCompat.setOnApplyWindowInsetsListener(binding.root) { _, insets ->
            val statusBarHeight = insets.getInsets(WindowInsetsCompat.Type.statusBars()).top
            binding.appBar.updateLayoutParams<MarginLayoutParams> {
                topMargin = statusBarHeight
            }
            binding.scrollViewContent.updateLayoutParams<MarginLayoutParams> {
                bottomMargin = statusBarHeight
            }
            insets
        }
        setToolbarTitle(name)
        setData()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun setData() {
        Glide.with(requireContext())
            .load(ImageUrl.w342(posterPath))
            .placeholder(getDrawable(requireContext(), R.drawable.placeholder_130x200))
            .transition(DrawableTransitionOptions.withCrossFade())
            .centerCrop()
            .into(binding.ivPoster)

        if (posterPath != null) {
            binding.ivPoster.onClickDebounce {
                val args = FullscreenImagesFragment.args(
                    title = name,
                    imageSize = ImageUrl.w780,
                    images = listOf(posterPath!!)
                )
                findNavController().navigate(R.id.fullscreenImagesScreen, args)
            }
        }

        binding.tvTitle.text = getTextOrNA(R.string.movie_info_title, name)

        binding.tvOriginalTitle.text =
            getTextOrNA(R.string.movie_info_original_title, originalName)

        bindFirstAirDate()

        binding.tvPopularity.text = DecimalFormat("#.##").format(popularity)
        // noinspection SetTextI18n
        binding.tvVotes.text = "$voteAverage (${voteCount})"

        if (overview.isNullOrBlankF()) {
            binding.tvOverview.isVisible = false
        } else {
            binding.tvOverview.text = overview
        }
    }

    private fun bindFirstAirDate() {
        if (firstAirDate == null) {
            binding.tvFirstAirDate.text = NOT_AVAILABLE
            return
        }
        binding.tvFirstAirDate.text = firstAirDate!!.toLocalDate().format()
    }

    private fun getTextOrNA(@StringRes stringRes: Int, string: String?): SpannableString {
        val str = if (string.isNullOrBlankF()) NOT_AVAILABLE else string

        val span = SpannableString(getString(stringRes, str))
        span.setSpan(
            ForegroundColorSpan(getColor(requireContext(), R.color.grey_969696)),
            0, span.indexOf(str),
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        return span
    }

}
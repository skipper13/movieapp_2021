package com.nicetas.movieapp3.ui.search.options.keywords.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.nicetas.movieapp3.base.adapter.ClickDebounce
import com.nicetas.movieapp3.databinding.ItemKeywordBinding

class KeywordHolder(
    parent: ViewGroup,
    private var binding: ItemKeywordBinding = ItemKeywordBinding.inflate(
        LayoutInflater.from(parent.context),
        parent,
        false
    ),
    debounce: ClickDebounce,
    callback: Callback,
) : RecyclerView.ViewHolder(binding.root) {

    fun interface Callback {
        fun onDeleteClick(id: Int)
    }

    class Item(
        val id: Int,
        val text: String,
    )

    private lateinit var item: Item

    init {
        binding.root.setOnCloseIconClickListener {
            if (debounce.canClick()) {
                callback.onDeleteClick(id = item.id)
            }
        }
    }

    fun bind(item: Item) {
        this.item = item
        binding.root.text = item.text
    }

}
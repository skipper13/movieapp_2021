package com.nicetas.movieapp3.ui.watchlist.sort

import androidx.annotation.IntDef
import com.nicetas.movieapp3.ui.watchlist.sort.WatchlistSort.Companion.DATE_ADDED
import com.nicetas.movieapp3.ui.watchlist.sort.WatchlistSort.Companion.RATING
import com.nicetas.movieapp3.ui.watchlist.sort.WatchlistSort.Companion.RELEASE_DATE
import com.nicetas.movieapp3.ui.watchlist.sort.WatchlistSort.Companion.TITLE

@Target(AnnotationTarget.TYPE)
@Retention(AnnotationRetention.SOURCE)
@IntDef(DATE_ADDED, RELEASE_DATE, TITLE, RATING)
annotation class WatchlistSort {
    companion object {
        const val DATE_ADDED = 0
        const val RELEASE_DATE = 1
        const val TITLE = 2
        const val RATING = 3

        const val DEFAULT = DATE_ADDED
    }
}
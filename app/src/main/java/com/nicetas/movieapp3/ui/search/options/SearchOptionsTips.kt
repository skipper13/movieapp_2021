@file:Suppress("INVISIBLE_MEMBER", "INVISIBLE_REFERENCE")

package com.nicetas.movieapp3.ui.search.options

import android.content.pm.ActivityInfo
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.google.android.material.button.MaterialButton
import com.nicetas.movieapp3.R
import com.nicetas.movieapp3.base.adapter.ClickDebounce
import com.nicetas.movieapp3.base.extensions.dp
import com.nicetas.movieapp3.base.extensions.lockOrientationForTips
import com.nicetas.movieapp3.base.extensions.privateField
import com.takusemba.spotlight.OnSpotlightListener
import com.takusemba.spotlight.Spotlight
import com.takusemba.spotlight.SpotlightView
import com.takusemba.spotlight.Target
import com.takusemba.spotlight.shape.RoundedRectangle

class SearchOptionsTips(
    private val fragment: Fragment,
    private val frameLayoutContainer: FrameLayout,
    private val tvSearchKits: TextView?,
    private val vgStars: ViewGroup?,
    private val onTipsEnd: () -> Unit,
) {

    private val activity: FragmentActivity?
        get() = fragment.activity

    private var spotlight: Spotlight? = null

    fun onDestroyView() {
        spotlight?.finish()
        spotlight = null
        if (activity?.requestedOrientation == ActivityInfo.SCREEN_ORIENTATION_PORTRAIT) {
            activity!!.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED
        }
    }

    fun showTips() {
        if (activity == null || activity!!.lockOrientationForTips()) {
            return
        }

        val targets = ArrayList<Target>(2)

        val shape1 = RoundedRectangle(
            height = tvSearchKits!!.height * 1.2F,
            width = tvSearchKits.width.toFloat() - 24F.dp,
            radius = 36F,
        )

        val overlay1 = fragment.layoutInflater.inflate(R.layout.layout_search_options_tip_1, null)
        val target1 = Target.Builder()
            .setAnchor(tvSearchKits)
            .setShape(shape1)
            .setOverlay(overlay1)
            .build()

        targets.add(target1)

        val shape2 = RoundedRectangle(
            height = vgStars!!.height * 1.3F,
            width = vgStars.width.toFloat() - 12F.dp,
            radius = 36F,
        )

        val overlay2 = fragment.layoutInflater.inflate(R.layout.layout_search_options_tip_2, null)
        val target2 = Target.Builder()
            .setAnchor(vgStars)
            .setShape(shape2)
            .setOverlay(overlay2)
            .build()

        targets.add(target2)

        spotlight = Spotlight.Builder(fragment.requireActivity())
            .setTargets(targets)
            .setContainer(frameLayoutContainer)
            .setBackgroundColorRes(R.color.black_alpha_45)
            .setDuration(700L)
            .setOnSpotlightListener(object : OnSpotlightListener {
                override fun onStarted() = Unit

                override fun onEnded() {
                    if (activity == null) {
                        return
                    }
                    spotlight = null
                    onTipsEnd.invoke()
                    activity!!.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED
                }
            })
            .build()

        val spotlightView = spotlight!!.privateField<SpotlightView>("spotlight")
        spotlightView.setOnClickListener(View.OnClickListener {
            /* prevent clicking through overlay */
        })

        spotlight?.start()

        val clickDebounce = ClickDebounce(1000)

        overlay1.findViewById<MaterialButton>(R.id.btnNext).setOnClickListener {
            if (clickDebounce.canClick()) {
                spotlight?.next()
            }
        }
        overlay1.findViewById<MaterialButton>(R.id.btnClose).setOnClickListener {
            if (clickDebounce.canClick()) {
                spotlight?.finish()
            }
        }
        overlay2.findViewById<MaterialButton>(R.id.btnClose).setOnClickListener {
            if (clickDebounce.canClick()) {
                spotlight?.finish()
            }
        }
    }

}
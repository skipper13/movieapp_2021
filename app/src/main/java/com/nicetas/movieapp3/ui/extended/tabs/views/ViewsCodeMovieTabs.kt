package com.nicetas.movieapp3.ui.extended.tabs.views

import android.graphics.BlendMode
import android.graphics.BlendModeColorFilter
import android.graphics.PorterDuff
import android.os.Build
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.Toolbar
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat.getColor
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.appbar.CollapsingToolbarLayout
import com.google.android.material.tabs.TabLayout
import com.nicetas.movieapp3.R
import com.nicetas.movieapp3.base.adapter.recyclerView
import com.nicetas.movieapp3.base.extensions.dp

class ViewsCodeMovieTabs(
    private val movieId: Int,
) : ViewsProviderMovieTabs {

    companion object {
        private const val PARALLAX = CollapsingToolbarLayout.LayoutParams.COLLAPSE_MODE_PARALLAX
    }

    override lateinit var coordinator: CoordinatorLayout
    override lateinit var ablExtended: AppBarLayout
    override lateinit var ctlExtended: CollapsingToolbarLayout
    override lateinit var ivMovie: AppCompatImageView
    override lateinit var toolbar: Toolbar
    override lateinit var tabLayout: TabLayout
    override lateinit var viewPager: ViewPager2

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
    ): View {
        coordinator = CoordinatorLayout(container!!.context).apply {
            id = R.id.coordinator
            layoutParams = ViewGroup.LayoutParams(MATCH_PARENT, MATCH_PARENT)
        }

        //region AppBarLayout
        ablExtended = AppBarLayout(coordinator.context).apply {
            id = R.id.ablExtended
            layoutParams = CoordinatorLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT)
            stateListAnimator = null
        }

        //region CollapsingToolbarLayout
        ctlExtended = CollapsingToolbarLayout(ablExtended.context).apply {
            id = R.id.ctlExtended
            layoutParams = AppBarLayout.LayoutParams(MATCH_PARENT, MATCH_PARENT)
                .apply {
                    scrollFlags = (AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL
                            or AppBarLayout.LayoutParams.SCROLL_FLAG_EXIT_UNTIL_COLLAPSED
                            or AppBarLayout.LayoutParams.SCROLL_FLAG_SNAP)
                }
            scrimAnimationDuration = 0
            setBackgroundColor(getColor(context, R.color.white_alpha_27))
            setContentScrimColor(getColor(context, R.color.colorPrimary))
            expandedTitleGravity = Gravity.BOTTOM or Gravity.CENTER
            expandedTitleMarginBottom = 20F.dp
            expandedTitleTextSize = 20F.dp.toFloat()
            setCollapsedTitleTextColor(getColor(context, R.color.white))
            setExpandedTitleColor(getColor(context, R.color.white))

            ivMovie = AppCompatImageView(context).apply {
                id = R.id.ivMovie
                layoutParams = CollapsingToolbarLayout.LayoutParams(MATCH_PARENT, 170F.dp)
                    .apply {
                        collapseMode = PARALLAX
                        parallaxMultiplier = 0.5F
                    }
            }
            addView(ivMovie)

            val viewBottomShadow = View(context).apply {
                layoutParams = CollapsingToolbarLayout.LayoutParams(MATCH_PARENT, 170F.dp)
                    .apply {
                        collapseMode = PARALLAX
                        parallaxMultiplier = 0.5F
                    }
                setBackgroundResource(R.drawable.title_expanded_gradient)
            }
            addView(viewBottomShadow)

            toolbar = Toolbar(context).apply {
                id = R.id.toolbar
                layoutParams = CollapsingToolbarLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT)
                    .apply {
                        collapseMode = CollapsingToolbarLayout.LayoutParams.COLLAPSE_MODE_PIN
                    }
            }
            addView(toolbar)
        }
        //endregion CollapsingToolbarLayout
        ablExtended.addView(ctlExtended)

        tabLayout = TabLayout(ablExtended.context).apply {
            id = R.id.tabLayout
            layoutParams = AppBarLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT)
            tabGravity = TabLayout.GRAVITY_FILL
            tabMode = TabLayout.MODE_SCROLLABLE
            setBackgroundColor(getColor(context, R.color.colorPrimary))
            setSelectedTabIndicatorColor(getColor(context, R.color.grey_9e9e9e))
            setTabTextColors(
                getColor(context, R.color.grey_969696),
                getColor(context, R.color.white)
            )
        }
        ablExtended.addView(tabLayout)

        //endregion AppBarLayout
        coordinator.addView(ablExtended)

        viewPager = ViewPager2(coordinator.context).apply {
            id = R.id.viewPager
            layoutParams = CoordinatorLayout.LayoutParams(MATCH_PARENT, MATCH_PARENT).apply {
                behavior = AppBarLayout.ScrollingViewBehavior()
            }
            recyclerView.overScrollMode = View.OVER_SCROLL_NEVER
        }
        coordinator.addView(viewPager)

        return coordinator
    }

    override fun onViewCreated(view: View) {
        val navigationIcon = toolbar.navigationIcon
        val whiteColor = getColor(view.context, R.color.white)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            navigationIcon?.colorFilter = BlendModeColorFilter(whiteColor, BlendMode.SRC)
        } else {
            navigationIcon?.setColorFilter(whiteColor, PorterDuff.Mode.SRC_ATOP)
        }
    }

    override fun onDestroyView() {
        /* no-op */
    }

}
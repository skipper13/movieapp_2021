package com.nicetas.movieapp3.ui.search.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.nicetas.movieapp3.R
import com.nicetas.movieapp3.base.adapter.ClickDebounce
import com.nicetas.movieapp3.base.extensions.isLandscape
import com.nicetas.movieapp3.databinding.ItemMovieBinding
import com.nicetas.movieapp3.ui.ImageUrl
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.time.LocalDate
import java.util.Locale

class MovieHolder(
    parent: ViewGroup,
    inflater: LayoutInflater,
    private var binding: ItemMovieBinding = ItemMovieBinding.inflate(inflater, parent, false),
    debounce: ClickDebounce,
    private val callback: Callback,
    private val voteFormatter: VoteFormatter,
) : RecyclerView.ViewHolder(binding.root) {

    class DifHelper {
        fun areItemsTheSame(oldItem: Item, newItem: Item): Boolean {
            return oldItem.movieId == newItem.movieId
        }

        fun areContentsTheSame(oldItem: Item, newItem: Item): Boolean {
            return oldItem.title == newItem.title
                    && oldItem.voteAverage == newItem.voteAverage
                    && oldItem.releaseDate == newItem.releaseDate
                    && oldItem.backdropPath == newItem.backdropPath
                    && oldItem.isInWatchlist == newItem.isInWatchlist
        }
    }

    class VoteFormatter() {
        private val voteFormat: DecimalFormat =
            DecimalFormat("#.0", DecimalFormatSymbols(Locale.UK).apply {
                groupingSeparator = '.'
            })

        fun format(number: Double): String = voteFormat.format(number)
    }

    interface Callback {
        val isMultiWindow: Boolean
        fun onMovieClick(movie: Item)
        fun removeFromBookmarks(movie: Item)
    }

    open class Item(
        open val movieId: Int,
        open val title: String,
        open val voteAverage: Double?,
        open val releaseDate: LocalDate?,
        open val backdropPath: String?,
        open val posterPath: String?,
        open var isInWatchlist: Boolean,
    )

    private lateinit var item: Item

    private val context: Context
        get() = itemView.context

    init {
        binding.root.setOnClickListener {
            if (debounce.canClick()) {
                callback.onMovieClick(item)
            }
        }
        binding.ivBookmark.setOnClickListener {
            if (debounce.canClick()) {
                callback.removeFromBookmarks(item)
            }
        }
    }

    fun bind(item: Item) {
        this.item = item

        binding.tvTitle.text = item.title

        bindReleaseDate(item.releaseDate)
        bindVoteAverage(item.voteAverage)

        Glide.with(context)
            .load(ImageUrl.w500(item.backdropPath))
            .transition(DrawableTransitionOptions.withCrossFade())
            .placeholder(R.drawable.movie_image_placeholder)
            .centerCrop()
            .into(binding.ivPoster)

        binding.ivBookmark.isVisible = item.isInWatchlist
    }

    private fun bindVoteAverage(voteAverage: Double?) {
        if (voteAverage != null && voteAverage > 0.0) {
            binding.tvVoteAverage.isVisible = true
            binding.tvVoteAverage.text = voteFormatter.format(voteAverage)
        } else {
            binding.tvVoteAverage.isInvisible = true
        }
        if (callback.isMultiWindow && context.isLandscape()) {
            binding.tvVoteAverage.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null)
        }
    }

    private fun bindReleaseDate(releaseDate: LocalDate?) {
        if (releaseDate == null) {
            binding.tvReleaseDate.isVisible = false
        } else {
            binding.tvReleaseDate.isVisible = true
            binding.tvReleaseDate.text = releaseDate.year.toString()
        }
        if (callback.isMultiWindow && context.isLandscape()) {
            binding.tvReleaseDate.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null)
        }
    }
}
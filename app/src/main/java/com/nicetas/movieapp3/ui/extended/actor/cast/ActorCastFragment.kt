package com.nicetas.movieapp3.ui.extended.actor.cast

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.core.content.ContextCompat.getColor
import androidx.navigation.findNavController
import com.nicetas.movieapp3.R
import com.nicetas.movieapp3.base.fragment.BaseFragment
import com.nicetas.movieapp3.ui.extended.tabs.MovieTabsFragment
import com.nicetas.movieapp3.ui.theme.MovieApp3Theme
import org.koin.androidx.viewmodel.ext.android.viewModel

class ActorCastFragment : BaseFragment() {

    companion object {
        private const val ARG_ACTOR_ID = "ARG_ACTOR_ID"
        private const val ARG_ACTOR_NAME = "ARG_ACTOR_NAME"

        fun args(
            actorId: Int,
            name: String,
        ) = Bundle(2).apply {
            putInt(ARG_ACTOR_ID, actorId)
            putString(ARG_ACTOR_NAME, name)
        }
    }

    private val actorId: Int by lazy { requireArguments().getInt(ARG_ACTOR_ID) }
    private val actorName: String by lazy { requireArguments().getString(ARG_ACTOR_NAME, "") }

    private val viewModel: ActorCastViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                MovieApp3Theme {
                    ActorCastScreen(
                        viewModel = viewModel,
                        onMovieClick = { movie ->
                            val args = MovieTabsFragment.args(
                                movieId = movie.movieId,
                                title = movie.title,
                                backdropPath = movie.backdropPath,
                                posterPath = movie.posterPath,
                                releaseDate = movie.releaseDate,
                                voteAverage = movie.voteAverage,
                            )
                            findNavController().navigate(R.id.movieTabs, args)
                        },
                        isMultiWindow = { isInMultiWindowMode },
                        onUpClick = {
                            findNavController().navigateUp()
                        },
                    )
                }
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViews()
        viewModel.onViewCreated(actorId, actorName)
    }

    private fun setupViews() {
        setToolbarTitle(actorName)
        setStatusBarColor(getColor(requireContext(), R.color.colorPrimary))
    }

}
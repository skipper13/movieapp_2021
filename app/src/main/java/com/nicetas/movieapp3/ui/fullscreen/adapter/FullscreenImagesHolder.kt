package com.nicetas.movieapp3.ui.fullscreen.adapter

import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.nicetas.movieapp3.base.photoview.PhotoView
import com.nicetas.movieapp3.ui.ImageUrl

class FullscreenImagesHolder(
    parent: ViewGroup,
    private var photoView: PhotoView = PhotoView(parent.context).apply {
        layoutParams = ViewGroup.LayoutParams(MATCH_PARENT, MATCH_PARENT)
    },
    private val imageSize: String,
    private val onImageClick: () -> Unit,
) : RecyclerView.ViewHolder(photoView) {

    init {
        photoView.setOnClickListener {
            onImageClick()
        }
    }

    fun bind(imagePath: String) {
        Glide.with(photoView.context)
            .load("${ImageUrl.baseUrl}${imageSize}${imagePath}")
            .transition(DrawableTransitionOptions.withCrossFade())
            .into(photoView)
    }
}
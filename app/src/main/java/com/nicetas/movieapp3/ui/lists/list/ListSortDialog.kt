package com.nicetas.movieapp3.ui.lists.list

import android.app.Dialog
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.content.res.AppCompatResources.getDrawable
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.setFragmentResult
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.nicetas.movieapp3.R
import com.nicetas.movieapp3.base.extensions.dp
import com.nicetas.movieapp3.ui.sort.SortByAdapter
import com.nicetas.movieapp3.ui.sort.SortByHeaderHolder
import com.nicetas.movieapp3.ui.sort.SortByHolder
import com.nicetas.repos.search.SortBy

class ListSortDialog : DialogFragment() {

    companion object {
        private const val SORT_TYPE = "sort_type"
        const val FRAGMENT_RESULT_KEY = "FRAGMENT_RESULT_KEY_ListSortDialog"
        private const val ARG_SELECTED_SORT = "ARG_SELECTED_SORT"
        private const val STATE_IS_DESC = "STATE_IS_DESC"
        private const val ORIGINAL_ORDER_ID = ""

        fun newInstance(sortBy: SortBy?) = ListSortDialog().apply {
            arguments = Bundle(1).apply {
                putString(ARG_SELECTED_SORT, sortBy?.serverName)
            }
        }

        fun sortBy(bundle: Bundle): SortBy? {
            val value = bundle.getString(SORT_TYPE)
            return if (value == null) null else SortBy.parse(value)
        }
    }

    private val selectedSortBy: SortBy? by lazy {
        val value = requireArguments().getString(ARG_SELECTED_SORT)
        if (value == null) null else SortBy.parse(value)
    }

    private var adapter: SortByAdapter<String>? = null
    private var isDesc = true

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val recyclerView = RecyclerView(requireContext()).apply {
            layoutParams = ViewGroup.LayoutParams(MATCH_PARENT, MATCH_PARENT)
            setPadding(0, 8F.dp, 0, 8F.dp)
            clipToPadding = false
            overScrollMode = View.OVER_SCROLL_NEVER
        }

        val isSelectedDesc = selectedSortBy?.isDesc ?: true
        isDesc = savedInstanceState?.getBoolean(STATE_IS_DESC, isSelectedDesc)
            ?: isSelectedDesc

        adapter = SortByAdapter(
            selectedId = selectedSortBy?.type ?: ORIGINAL_ORDER_ID,
            onItemClick = { position, item ->
                val bundle = Bundle(1).apply {
                    val sortBy = SortBy.entries.find { it.type == item.id && it.isDesc == isDesc }
                    putString(SORT_TYPE, sortBy?.serverName)
                }
                setFragmentResult(FRAGMENT_RESULT_KEY, bundle)
                dismiss()
            },
            onRadioClick = { isDescending ->
                adapter?.onRadioClick(recyclerView, isDescending)
                isDesc = isDescending
            }
        )
        fillAdapter(adapter!!)

        recyclerView.layoutManager = LinearLayoutManager(requireContext())
        recyclerView.adapter = adapter

        return AlertDialog.Builder(requireActivity())
            .setTitle(R.string.sort_dialog_title)
            .setView(recyclerView)
            .create()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putBoolean(STATE_IS_DESC, isDesc)
    }

    override fun onDestroyView() {
        adapter?.run { items.list.clear() }
        adapter = null
        super.onDestroyView()
    }

    private fun fillAdapter(adapter: SortByAdapter<String>) {
        adapter.items.add(SortByHeaderHolder.Item(isDesc))

        val originalOrder = SortByHolder.Item(
            text = getString(R.string.list_dialog_sort_by_original_order),
            icon = getDrawable(requireContext(), R.drawable.ic_list_sort_original_order)!!,
            id = ORIGINAL_ORDER_ID,
        )
        adapter.items.add(originalOrder)

        val popularity = SortByHolder.Item(
            text = getString(R.string.list_dialog_sort_by_popularity),
            icon = getDrawable(requireContext(), R.drawable.ic_list_sort_popularity)!!,
            id = SortBy.POPULARITY_DESC.type,
        )
        adapter.items.add(popularity)

        val releaseDate = SortByHolder.Item(
            text = getString(R.string.list_dialog_sort_by_release_date),
            icon = getDrawable(requireContext(), R.drawable.ic_list_sort_release_date)!!,
            id = SortBy.PRIMARY_RELEASE_DATE_DESC.type,
        )
        adapter.items.add(releaseDate)

        val voteAverage = SortByHolder.Item(
            text = getString(R.string.list_dialog_sort_by_rating),
            icon = getDrawable(requireContext(), R.drawable.ic_list_sort_rating)!!,
            id = SortBy.VOTE_AVERAGE_DESC.type,
        )
        adapter.items.add(voteAverage)

        val title = SortByHolder.Item(
            text = getString(R.string.list_dialog_sort_by_title),
            icon = getDrawable(requireContext(), R.drawable.ic_list_sort_title)!!,
            id = SortBy.TITLE_DESC.type,
        )
        adapter.items.add(title)

        /*  Currently server returns list sorted by "popularity" instead of "revenue"

        val revenue = SortByHolder.Item(
            text = getString(R.string.list_dialog_sort_by_revenue),
            icon = getDrawable(requireContext(), R.drawable.ic_list_sort_revenue)!!,
            id = SortBy.REVENUE_DESC.type,
        )
        adapter.items.add(revenue)
         */

        val voteCount = SortByHolder.Item(
            text = getString(R.string.list_dialog_sort_by_vote_count),
            icon = getDrawable(requireContext(), R.drawable.ic_list_sort_vote_count)!!,
            id = SortBy.VOTE_COUNT_DESC.type,
        )
        adapter.items.add(voteCount)
    }

}
package com.nicetas.movieapp3.ui.extended.credits.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.content.res.AppCompatResources.getDrawable
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.nicetas.movieapp3.R
import com.nicetas.movieapp3.databinding.ItemCrewBinding
import com.nicetas.movieapp3.ui.ImageUrl

class CrewHolder(
    parent: ViewGroup,
    inflater: LayoutInflater,
    private var binding: ItemCrewBinding = ItemCrewBinding.inflate(inflater, parent, false),
) : RecyclerView.ViewHolder(binding.root) {

    class Item(
        val id: Int,
        val name: String,
        val job: String,
        val department: String,
        val profilePath: String?,
    )

    private lateinit var item: Item

    fun bind(item: Item) {
        this.item = item
        val context = itemView.context

        binding.tvName.text = item.name
        binding.tvJob.text = item.job
        binding.tvDepartment.text = item.department

        Glide.with(context)
            .load(ImageUrl.w342(item.profilePath))
            .transition(DrawableTransitionOptions.withCrossFade())
            .placeholder(getDrawable(context, R.drawable.placeholder_135x200))
            .centerCrop()
            .into(binding.ivPhoto)
    }

}
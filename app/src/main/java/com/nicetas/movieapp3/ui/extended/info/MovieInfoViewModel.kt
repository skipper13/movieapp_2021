package com.nicetas.movieapp3.ui.extended.info

import androidx.lifecycle.MutableLiveData
import com.nicetas.domain.DateFormatInteractor
import com.nicetas.models.movies.Movie
import com.nicetas.movieapp3.base.Logger
import com.nicetas.movieapp3.base.viewmodel.BaseViewModel
import com.nicetas.movieapp3.base.viewmodel.LiveEvent
import com.nicetas.prefs.Prefs
import com.nicetas.prefs.PrefsKey
import com.nicetas.repos.configuration.ConfigRepository
import com.nicetas.repos.movie.MovieRepository
import com.nicetas.repos.network.LanguageProvider
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import java.time.LocalDate

class MovieInfoViewModel(
    private val configRepository: ConfigRepository,
    private val movieRepository: MovieRepository,
    private val dateFormat: DateFormatInteractor,
    private val prefs: Prefs,
) : BaseViewModel() {

    val onGetMovie = MutableLiveData<Movie>()
    val updateReleaseDate = LiveEvent<LocalDate>()
    val showTips = LiveEvent<Unit>()

    private var wasInit = false
    private var language = ""

    fun dateOrder() = dateFormat.dateOrder
    fun dateSeparator() = dateFormat.dateSeparator

    fun onViewCreated(movieId: Int) {
        if (wasInit && language == LanguageProvider.language) {
            checkTips()
            return
        }
        wasInit = true
        language = LanguageProvider.language
        subscribeOnDateFormat()
        loadMovieInfo(movieId)
    }

    fun loadMovieInfo(movieId: Int) {
        dateFormat.updateFromPrefs()

        movieRepository.getMovieFromCache(movieId)?.let { movie ->
            onGetMovie.value = movie
            lce.content()
            checkTips()
            return
        }
        lce.loading()
        disposables += movieRepository.loadMovie(movieId)
            .flatMap { movie ->
                configRepository.getOrReadCountries().map {
                    val countries = movie.body.productionCountries
                    for (i in countries.indices) {
                        val country = countries[i]
                        val name = configRepository.findCountryName(country.iso_3166_1)
                        if (name != null) {
                            country.name = name
                        }
                    }
                    return@map movie
                }
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ response ->
                onGetMovie.value = response.body!!
                lce.content()
                checkTips()
            }, { error ->
                Logger.e(error)
                lce.error(message = error.message ?: "")
            })
    }

    private fun checkTips() {
        val isTipsShown = prefs.getBoolean(PrefsKey.IsMovieInfoTipsShown, false)
        if (isTipsShown.not()) {
            showTips.setValue(Unit)
        }
    }

    private fun subscribeOnDateFormat() {
        disposables += dateFormat.subscribeOnPrefsChanged()
            .subscribeOn(AndroidSchedulers.mainThread())
            .subscribe({
                onGetMovie.value?.releaseDate?.let { date ->
                    updateReleaseDate.setValue(date)
                }
            }, { error ->
                Logger.e(error)
            })
    }

    fun onTipsEnd() {
        prefs.edit().commitIO {
            putBoolean(PrefsKey.IsMovieInfoTipsShown, true)
        }
    }

}
package com.nicetas.movieapp3.ui.search.options.keywords.search

import androidx.annotation.IntDef
import com.nicetas.movieapp3.ui.search.options.keywords.search.SearchWord.Companion.COMPANIES_WITH
import com.nicetas.movieapp3.ui.search.options.keywords.search.SearchWord.Companion.COMPANIES_WITHOUT
import com.nicetas.movieapp3.ui.search.options.keywords.search.SearchWord.Companion.KEYWORDS_WITH
import com.nicetas.movieapp3.ui.search.options.keywords.search.SearchWord.Companion.KEYWORDS_WITHOUT

@Target(AnnotationTarget.TYPE)
@Retention(AnnotationRetention.SOURCE)
@IntDef(KEYWORDS_WITH, KEYWORDS_WITHOUT, COMPANIES_WITH, COMPANIES_WITHOUT)
annotation class SearchWord {
    companion object {
        const val KEYWORDS_WITH = 0
        const val KEYWORDS_WITHOUT = 1
        const val COMPANIES_WITH = 2
        const val COMPANIES_WITHOUT = 3

        fun isKeyword(type: @SearchWord Int): Boolean {
            return type == KEYWORDS_WITH || type == KEYWORDS_WITHOUT
        }

        fun isCompany(type: @SearchWord Int): Boolean {
            return type == COMPANIES_WITH || type == COMPANIES_WITHOUT
        }
    }
}
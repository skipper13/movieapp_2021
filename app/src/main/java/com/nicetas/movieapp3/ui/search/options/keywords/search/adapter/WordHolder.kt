package com.nicetas.movieapp3.ui.search.options.keywords.search.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.nicetas.movieapp3.base.adapter.ClickDebounce
import com.nicetas.movieapp3.databinding.ItemWordBinding

class WordHolder(
    parent: ViewGroup,
    private var binding: ItemWordBinding = ItemWordBinding.inflate(
        LayoutInflater.from(parent.context),
        parent,
        false
    ),
    debounce: ClickDebounce,
    callback: Callback,
) : RecyclerView.ViewHolder(binding.root) {

    fun interface Callback {
        fun onClick(item: Item)
    }

    class Item(
        val id: Int,
        val text: String,
        var isSelected: Boolean,
    ) {
        override fun toString(): String =
            "${Integer.toHexString(hashCode())},  id = $id, text = $text, isSelected = $isSelected"
    }

    private lateinit var item: Item

    init {
        binding.root.setOnClickListener {
            if (debounce.canClick()) {
                callback.onClick(item)
            }
        }
    }

    fun bind(item: Item) {
        this.item = item
        binding.tvName.text = item.text
        binding.checkBox.isChecked = item.isSelected
    }

}
package com.nicetas.movieapp3.ui.search

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup.MarginLayoutParams
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.content.res.AppCompatResources.getDrawable
import androidx.appcompat.view.menu.MenuBuilder
import androidx.core.content.ContextCompat.getColor
import androidx.core.view.MenuProvider
import androidx.core.view.ViewCompat
import androidx.core.view.updateLayoutParams
import androidx.fragment.app.setFragmentResultListener
import androidx.lifecycle.Lifecycle
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.animation.MotionSpec
import com.nicetas.movieapp3.R
import com.nicetas.movieapp3.base.adapter.GridSpacingItemDecoration
import com.nicetas.movieapp3.base.MoviesSpanCount
import com.nicetas.movieapp3.base.adapter.ScrollOnTapHelper
import com.nicetas.movieapp3.base.adapter.pagination.Paginate
import com.nicetas.movieapp3.base.extensions.dp
import com.nicetas.movieapp3.base.extensions.onClickDebounce
import com.nicetas.movieapp3.base.extensions.statusBarHeight
import com.nicetas.movieapp3.base.fragment.BaseFragment
import com.nicetas.movieapp3.base.states.ViewState
import com.nicetas.movieapp3.databinding.FragmentSearchBinding
import com.nicetas.movieapp3.ui.extended.tabs.MovieTabsFragment
import com.nicetas.movieapp3.ui.search.adapter.MovieHolder
import com.nicetas.movieapp3.ui.search.adapter.SearchMoviesAdapter
import com.nicetas.movieapp3.ui.search.options.SearchOptionsFragment
import org.koin.androidx.viewmodel.ext.android.viewModel


class SearchFragment : BaseFragment(R.layout.fragment_search) {

    private var _binding: FragmentSearchBinding? = null
    private val binding
        get() = _binding!!

    private val viewModel: SearchViewModel by viewModel()

    private var adapter: SearchMoviesAdapter? = null
    private var layoutManager: GridLayoutManager? = null
    private var gridDecor: GridSpacingItemDecoration? = null
    private var menuItemGridType: MenuItem? = null
    private var paginate: Paginate? = null
    private var searchHelper: SearchHelper? = null
    private var scrollOnTapHelper: ScrollOnTapHelper? = null

    private val paginateCallbacks = object : Paginate.Callbacks {
        override fun onLoadMore() = viewModel.loadMoreMovies(isFullReload = false)
        override fun isLoading() = viewModel.isLoading
        override fun hasLoadedAllItems() = viewModel.hasLoadedAllItems
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setFragmentResultListener(SearchOptionsFragment.FRAGMENT_RESULT_KEY) { requestKey, bundle ->
            val isNeedReload = SearchOptionsFragment.isNeedReload(bundle)
            if (isNeedReload) {
                viewModel.discover()
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentSearchBinding.bind(view)
        scrollOnTapHelper = ScrollOnTapHelper(savedInstanceState)
        initViewState()
        setupOptionsMenu()
        initRecyclerView()
        setupViews()
        setupObservers()
        setListeners()
        viewModel.onViewCreated()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        scrollOnTapHelper?.onSaveInstanceState(outState)
    }

    override fun onDestroyView() {
        paginate?.unbind()
        paginate = null
        adapter?.onDestroyView()
        adapter = null
        binding.rvContent.adapter = null
        layoutManager = null
        gridDecor = null
        menuItemGridType = null
        searchHelper = null
        super.onDestroyView()
        scrollOnTapHelper = null
        _binding = null
    }

    private fun setupOptionsMenu() {
        requireActivity().addMenuProvider(object : MenuProvider {
            override fun onCreateMenu(menu: Menu, menuInflater: MenuInflater) {
                menuInflater.inflate(R.menu.menu_search, menu)

                menuItemGridType = menu.findItem(R.id.action_grid_type)
                if (viewModel.updateSpanCount.isInitialized) {
                    updateMenuItemGridType()
                }

                // noinspection RestrictedApi
                if (menu is MenuBuilder) {
                    menu.setOptionalIconsVisible(true)
                }
                searchHelper = SearchHelper().apply {
                    init(
                        activity = (activity as AppCompatActivity),
                        item = menu.findItem(R.id.action_search),
                        onSearchRequest = { text ->
                            viewModel.search(text)
                        },
                        getSearchText = { viewModel.searchText }
                    )
                }
            }

            override fun onMenuItemSelected(menuItem: MenuItem): Boolean {
                return when (menuItem.itemId) {
                    R.id.action_search -> true
                    R.id.action_grid_type -> {
                        viewModel.onGridTypeClick()
                        true
                    }
                    else -> false
                }
            }
        }, viewLifecycleOwner, Lifecycle.State.RESUMED)
    }

    private fun initViewState() {
        ViewState.Builder(binding.rvContent)
            .load()
            .error(onClick = { viewModel.reloadMovies() })
            .empty(binding.tvEmptyState)
            .build()
            .observeLce(viewModel.lce, viewLifecycleOwner)
    }

    private fun setupViews() {
        setStatusBarColor(getColor(requireContext(), R.color.colorPrimary))
        setToolbarTitle(getString(R.string.search_title))

        ViewCompat.setOnApplyWindowInsetsListener(binding.root) { _, insets ->
            val statusBarHeight = insets.statusBarHeight()
            binding.appBar.updateLayoutParams<MarginLayoutParams> {
                topMargin = statusBarHeight
            }
            binding.rvContent.updateLayoutParams<MarginLayoutParams> {
                bottomMargin = statusBarHeight
            }
            insets
        }

        binding.toolbar.setOnClickListener {
            scrollOnTapHelper?.scroll(binding.rvContent)
        }
    }

    private fun initRecyclerView() {
        adapter = SearchMoviesAdapter(
            inflater = layoutInflater,
            isPosters = { viewModel.isPosters },
            movieCallback = object : MovieHolder.Callback {

                override val isMultiWindow: Boolean
                    get() = isInMultiWindowMode

                override fun onMovieClick(movie: MovieHolder.Item) {
                    val args = MovieTabsFragment.args(
                        movieId = movie.movieId,
                        title = movie.title,
                        backdropPath = movie.backdropPath,
                        releaseDate = movie.releaseDate,
                        posterPath = movie.posterPath,
                        voteAverage = movie.voteAverage,
                    )
                    findNavController().navigate(R.id.movieTabs, args)
                }

                override fun removeFromBookmarks(movie: MovieHolder.Item) {
                    AlertDialog.Builder(requireContext())
                        .setTitle(R.string.search_remove_from_bookmarks)
                        .setMessage(movie.title)
                        .setPositiveButton(android.R.string.ok) { _, _ ->
                            viewModel.removeFromWatchList(movie.movieId)
                        }
                        .setNegativeButton(android.R.string.cancel, null)
                        .show()
                }
            }
        )

        binding.rvContent.adapter = adapter

        val context = requireContext()
        val defaultSpanCount = GridLayoutManager.DEFAULT_SPAN_COUNT

        layoutManager = GridLayoutManager(context, defaultSpanCount)
        binding.rvContent.layoutManager = layoutManager

        gridDecor = GridSpacingItemDecoration(defaultSpanCount, 4f.dp, false)
        binding.rvContent.addItemDecoration(gridDecor!!)

        val fab = binding.fab
        fab.showMotionSpec = MotionSpec.createFromResource(context, R.animator.fab_show_anim)
        fab.hideMotionSpec = MotionSpec.createFromResource(context, R.animator.fab_hide_anim)

        binding.rvContent.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (isKeyboardOpen.not() && dy < 0 && fab.isShown().not()) {
                    fab.show()
                } else if (dy > 0 && fab.isShown()) {
                    fab.hide()
                }
            }
        })
    }

    private fun initPaginate() {
        if (paginate == null) {
            paginate = Paginate.with(binding.rvContent, paginateCallbacks)
                .setLoadingTriggerThreshold(3)
                .addLoadingListItem(true)
                .onRetryClick { viewModel.loadMoreMovies(isFullReload = false) }
                .errorText { viewModel.paginationError?.message(requireContext()) ?: "" }
                .build()
        }
    }

    private fun setupObservers() {
        viewModel.onGetMovies.observe(viewLifecycleOwner) { result ->
            if (result.isFullReload) {
                adapter?.items?.replaceAll(result.items)

                binding.rvContent.scrollToPosition(0)

                scrollOnTapHelper?.savedBottomPos = 0
                binding.appBar.setExpanded(true, false)
            } else {
                adapter?.items?.addAll(result.items)
            }
            initPaginate()
        }
        viewModel.onMovieFromWatchlistRemoved.observe(viewLifecycleOwner) { movieId ->
            adapter?.removeMovieFromWatchlist(movieId)
        }
        viewModel.isKeyboardOpen.observe(viewLifecycleOwner) { isOpen ->
            if (isOpen) {
                binding.fab.hide()
            } else {
                binding.fab.show()
            }
        }
        viewModel.showPaginationError.observe(viewLifecycleOwner) {
            paginate?.setError()
        }
        viewModel.updateSpanCount.observe(viewLifecycleOwner) {
            val spanCount = MoviesSpanCount().get(requireActivity(), viewModel.isPosters)
            gridDecor?.spanCount = spanCount
            layoutManager?.spanCount = spanCount
            updateMenuItemGridType()
        }
    }

    private fun setListeners() {
        binding.fab.onClickDebounce {
            findNavController().navigate(R.id.action_to_search_options)
        }
    }

    private fun updateMenuItemGridType() {
        menuItemGridType?.title = if (viewModel.isPosters) {
            getString(R.string.search_action_grid_backdrops)
        } else {
            getString(R.string.search_action_grid_posters)
        }
        menuItemGridType?.icon = if (viewModel.isPosters) {
            getDrawable(requireContext(), R.drawable.ic_search_list)
        } else {
            getDrawable(requireContext(), R.drawable.ic_search_grid)
        }
    }

}
package com.nicetas.movieapp3.ui.extended.actor.photo

import androidx.collection.MutableObjectList
import androidx.collection.ObjectList
import androidx.lifecycle.MutableLiveData
import com.nicetas.movieapp3.base.Logger
import com.nicetas.movieapp3.base.viewmodel.BaseViewModel
import com.nicetas.movieapp3.ui.extended.actor.ActorInfoFragment
import com.nicetas.movieapp3.ui.extended.actor.photo.adapter.ActorPhotoHolder
import com.nicetas.repos.person.PersonRepository
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.internal.operators.single.SingleFromCallable
import io.reactivex.rxjava3.schedulers.Schedulers
import java.util.Optional

class ActorPhotosViewModel(
    private val personRepository: PersonRepository,
) : BaseViewModel() {

    val onGetImages = MutableLiveData<ObjectList<Any>>()

    private var wasInit = false
    private var actorId = 0
    val photos = arrayListOf<String>()

    fun onViewCreated(actorId: Int) {
        if (wasInit) {
            return
        }
        wasInit = true
        this.actorId = actorId
        loadImages()
    }

    override fun onCleared() {
        super.onCleared()
        photos.clear()
    }

    fun loadImages() {
        lce.loading()
        disposables += SingleFromCallable {
            val cache = personRepository.getActorImagesFromCache(actorId = actorId)
            Optional.ofNullable(cache)
        }.flatMap { optional ->
            if (optional.isEmpty) {
                personRepository.loadActorImages(actorId).map { it.body }
            } else {
                Single.just(optional.get())
            }
        }.map { images ->
            val photos = images.images

            this@ActorPhotosViewModel.photos.apply {
                clear()
                addAll(photos.map { it.filePath })
            }

            val items = MutableObjectList<Any>(photos.size)
            for (i in photos.indices) {
                items += ActorPhotoHolder.Item(filePath = photos[i].filePath)
            }
            items
        }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ items ->
                onGetImages.value = items
                if (items.isEmpty()) {
                    /** actually can't happen, because of "actor.profilePath != null" check in
                     * [ActorInfoFragment.setActor] */
                    lce.empty()
                } else {
                    lce.content()
                }
            }, { error ->
                Logger.e(error)
                lce.error(message = error.message ?: "")
            })
    }

}
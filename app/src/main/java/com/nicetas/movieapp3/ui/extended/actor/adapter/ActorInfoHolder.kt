package com.nicetas.movieapp3.ui.extended.actor.adapter

import android.content.Context
import android.view.GestureDetector
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.ViewGroup
import androidx.appcompat.content.res.AppCompatResources.getDrawable
import androidx.collection.ObjectList
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.nicetas.models.extensions.DateOrder
import com.nicetas.models.extensions.format
import com.nicetas.models.extensions.isNotNullOrBlankF
import com.nicetas.models.persons.Person
import com.nicetas.models.search.person.Gender
import com.nicetas.movieapp3.R
import com.nicetas.movieapp3.base.adapter.ClickDebounce
import com.nicetas.movieapp3.base.extensions.browse
import com.nicetas.movieapp3.base.extensions.share
import com.nicetas.movieapp3.databinding.ItemActorInfoCardBinding
import com.nicetas.movieapp3.ui.ImageUrl

class ActorInfoHolder(
    parent: ViewGroup,
    private var inflater: LayoutInflater?,
    var binding: ItemActorInfoCardBinding = ItemActorInfoCardBinding.inflate(
        inflater ?: /*cannot happen*/ LayoutInflater.from(parent.context),
        parent,
        false
    ),
    debounce: ClickDebounce,
    private val callback: Callback,
) : RecyclerView.ViewHolder(binding.root) {

    interface Callback {
        fun onPhotoClick(actorId: Int, name: String)
        fun onMovieClick(movie: KnownForHolder.Item)
        fun onAllMoviesClick(actorId: Int, name: String)
        fun getCast(): ObjectList<Any>
        fun getMoviesScrollOffset(): Int
        fun showDateFormatDialog()
        fun getDateOrder(): DateOrder
        fun getDateSeparator(): Char
    }

    private lateinit var actor: Person
    private var adapter: KnownForAdapter? = null

    private val context: Context
        get() = binding.root.context

    init {
        binding.ivPhoto.setOnClickListener {
            if (debounce.canClick() && actor.profilePath != null) {
                callback.onPhotoClick(actor.id, actor.name)
            }
        }

        binding.btnAllMovies.setOnClickListener {
            if (debounce.canClick()) {
                callback.onAllMoviesClick(actor.id, actor.name)
            }
        }

        adapter = KnownForAdapter(
            inflater = inflater,
            debounce = debounce,
            onMovieClick = { movie ->
                callback.onMovieClick(movie)
            }
        )
        inflater = null
        binding.rvKnownForMovies.adapter = adapter

        initTmdb(debounce)
        initImdb(debounce)

        // Because of textIsSelectable="true" click from setOnClickListener works only with second tap
        val releaseDateGestureDetector =
            GestureDetector(context, object : GestureDetector.SimpleOnGestureListener() {
                override fun onSingleTapConfirmed(e: MotionEvent): Boolean {
                    callback.showDateFormatDialog()
                    return super.onSingleTapConfirmed(e)
                }
            })
        // noinspection ClickableViewAccessibility
        binding.tvBirthday.setOnTouchListener { v, event ->
            releaseDateGestureDetector.onTouchEvent(event)
        }
    }

    fun bind(actor: Person) {
        this.actor = actor

        Glide.with(context)
            .load(ImageUrl.w342(actor.profilePath))
            .transition(DrawableTransitionOptions.withCrossFade())
            .placeholder(getDrawable(context, R.drawable.placeholder_130x200))
            .centerCrop()
            .into(binding.ivPhoto)

        binding.tvPlaceOfBirth.text = actor.placeOfBirth

        binding.tvBiography.text = actor.biography

        binding.tvKnownFor.text = if (actor.gender == Gender.FEMALE) {
            context.getString(R.string.actor_known_for_female)
        } else {
            context.getString(R.string.actor_known_for_male)
        }

        bindBirthAndDeathDays(actor)

        binding.ivImdb.isVisible = actor.imdbId.isNotNullOrBlankF()

        adapter?.items?.replaceAll(callback.getCast())

        (binding.rvKnownForMovies.layoutManager as? LinearLayoutManager)
            ?.scrollToPositionWithOffset(0, -callback.getMoviesScrollOffset())
    }

    private fun bindBirthAndDeathDays(actor: Person) = with(actor) {
        val order = callback.getDateOrder()
        val separator = callback.getDateSeparator()

        val text = if (birthday != null && deathday != null) {
            "${birthday.format(order, separator)} - ${deathday.format(order, separator)}"
        } else if (birthday != null) {
            birthday.format(order, separator)
        } else {
            null
        }
        binding.tvBirthday.text = text
    }

    private fun initTmdb(debounce: ClickDebounce) {
        val tmdbUrl = "https://www.themoviedb.org/person/"
        binding.ivTmdb.setOnClickListener {
            if (debounce.canClick()) {
                context.browse(tmdbUrl + actor.id)
            }
        }
        binding.ivTmdb.setOnLongClickListener {
            context.share(tmdbUrl + actor.id)
            true
        }
    }

    private fun initImdb(debounce: ClickDebounce) {
        val imdbUrl = "https://www.imdb.com/name/"
        binding.ivImdb.setOnClickListener {
            if (debounce.canClick()) {
                context.browse(imdbUrl + actor.imdbId)
            }
        }
        binding.ivImdb.setOnLongClickListener {
            context.share(imdbUrl + actor.imdbId)
            true
        }
    }

}
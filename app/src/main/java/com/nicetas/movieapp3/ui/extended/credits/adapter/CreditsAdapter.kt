package com.nicetas.movieapp3.ui.extended.credits.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.collection.ObjectList
import androidx.recyclerview.widget.RecyclerView
import com.nicetas.movieapp3.base.adapter.BaseDiffUtilCallback
import com.nicetas.movieapp3.base.adapter.ClickDebounce
import com.nicetas.movieapp3.base.adapter.AdapterItems

class CreditsAdapter(
    private var inflater: LayoutInflater?,
    private val onCastClick: CastHolder.Callback,
    private val onDepartmentClick: DepartmentHolder.Callback,
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        private const val TYPE_CAST = 0
        private const val TYPE_CREW = 1
        private const val TYPE_DEPARTMENT = 2
    }

    class DiffUtilCallback() : BaseDiffUtilCallback<Any>() {

        override fun areItemsTheSame(oldItem: Any, newItem: Any): Boolean {
            if (oldItem is CastHolder.Item && newItem is CastHolder.Item) {
                return oldItem.actorId == newItem.actorId
            }
            if (oldItem is DepartmentHolder.Item && newItem is DepartmentHolder.Item) {
                return oldItem.department == newItem.department
            }
            if (oldItem is CrewHolder.Item && newItem is CrewHolder.Item) {
                return oldItem.id == newItem.id
            }
            return false
        }

        override fun areContentsTheSame(oldItem: Any, newItem: Any): Boolean {
            if (oldItem is CastHolder.Item && newItem is CastHolder.Item) {
                return oldItem.name == newItem.name
                        && oldItem.character == newItem.character
                        && oldItem.profilePath == newItem.profilePath
            }
            if (oldItem is DepartmentHolder.Item && newItem is DepartmentHolder.Item) {
                return oldItem.isExpanded == newItem.isExpanded
            }
            if (oldItem is CrewHolder.Item && newItem is CrewHolder.Item) {
                return oldItem.name == newItem.name
                        && oldItem.job == newItem.job
                        && oldItem.department == newItem.department
                        && oldItem.profilePath == newItem.profilePath
            }
            return false
        }
    }

    private val debounce = ClickDebounce()
    private val diffUtilCallback = DiffUtilCallback()
    val items = AdapterItems<Any>(this)

    fun update(newItems: ObjectList<Any>) {
        items.update(newItems, diffUtilCallback)
    }

    fun onDestroyView() {
        items.list.clear()
        inflater = null
    }

    override fun getItemCount(): Int = items.list.size

    override fun getItemViewType(position: Int): Int {
        return when (items.list[position]) {
            is CastHolder.Item -> TYPE_CAST
            is CrewHolder.Item -> TYPE_CREW
            is DepartmentHolder.Item -> TYPE_DEPARTMENT
            else -> throw IllegalStateException("item is null for position = $position")
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_CAST -> CastHolder(
                parent = parent,
                inflater = inflater ?: /*cannot happen*/ LayoutInflater.from(parent.context),
                debounce = debounce,
                callback = onCastClick,
            )
            TYPE_CREW -> CrewHolder(
                parent = parent,
                inflater = inflater ?: /*cannot happen*/ LayoutInflater.from(parent.context),
            )
            TYPE_DEPARTMENT -> DepartmentHolder(
                parent = parent,
                inflater = inflater ?: /*cannot happen*/ LayoutInflater.from(parent.context),
                debounce = debounce,
                callback = onDepartmentClick,
            )
            else -> throw IllegalStateException("Unknown holder type")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = items.list[position]
        when (holder) {
            is CastHolder -> holder.bind(item as CastHolder.Item)
            is CrewHolder -> holder.bind(item as CrewHolder.Item)
            is DepartmentHolder -> holder.bind(item as DepartmentHolder.Item)
        }
    }

}
package com.nicetas.movieapp3.ui.extended.info.links

import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.FrameLayout
import androidx.appcompat.content.res.AppCompatResources.getColorStateList
import androidx.core.content.ContextCompat.getColor
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.chip.Chip
import com.nicetas.movieapp3.R
import com.nicetas.movieapp3.base.adapter.ClickDebounce
import com.nicetas.movieapp3.base.extensions.browse
import com.nicetas.movieapp3.base.extensions.dp
import com.nicetas.movieapp3.base.extensions.share

class LinkHolder(
    parent: ViewGroup,
    // @layout/item_link_preview
    private var chip: Chip = Chip(parent.context).apply {
        layoutParams = FrameLayout.LayoutParams(WRAP_CONTENT, 48F.dp).apply {
            chipBackgroundColor = getColorStateList(context, R.color.grey_606060)
            chipMinHeight = 28F.dp.toFloat()
            setTextColor(getColor(context, R.color.grey_d9d9d9))
            setMarginEnd(8F.dp)
        }
    },
    debounce: ClickDebounce,
) : RecyclerView.ViewHolder(chip) {

    class Item(
        val text: String,
        val link: String,
    )

    private lateinit var item: Item

    init {
        chip.setOnClickListener {
            if (debounce.canClick()) {
                chip.context.browse(item.link)
            }
        }
        chip.setOnLongClickListener {
            chip.context.share(item.link)
            true
        }
    }

    fun bind(item: Item) {
        this.item = item
        chip.text = item.text
    }

}
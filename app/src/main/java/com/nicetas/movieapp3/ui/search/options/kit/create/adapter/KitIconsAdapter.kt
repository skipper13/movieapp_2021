package com.nicetas.movieapp3.ui.search.options.kit.create.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.nicetas.movieapp3.base.adapter.AdapterItems

class KitIconsAdapter(
    private val iconCallback: KitIconHolder.Callback,
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        private const val TYPE_ICON = 0
    }

    val items = AdapterItems<Any>(this)

    fun updateSelected(oldId: Int, newId: Int) {
        val indexOld = items.list.indexOfFirst { it is KitIconHolder.Item && it.id == oldId }
        val indexNew = items.list.indexOfFirst { it is KitIconHolder.Item && it.id == newId }
        if (indexOld != -1 && indexNew != -1) {
            (items.list[indexOld] as KitIconHolder.Item).isSelected = false
            (items.list[indexNew] as KitIconHolder.Item).isSelected = true
            notifyItemChanged(indexOld)
            notifyItemChanged(indexNew)
        }
    }

    override fun getItemCount(): Int = items.list.size

    override fun getItemViewType(position: Int): Int {
        return when (items.list[position]) {
            is KitIconHolder.Item -> TYPE_ICON
            else -> throw IllegalStateException("item is null for position = $position")
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_ICON -> KitIconHolder(
                parent = parent,
                callback = iconCallback,
            )
            else -> throw IllegalStateException("Unknown holder type")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = items.list[position]
        when (holder) {
            is KitIconHolder -> holder.bind(item as KitIconHolder.Item)
        }
    }

}


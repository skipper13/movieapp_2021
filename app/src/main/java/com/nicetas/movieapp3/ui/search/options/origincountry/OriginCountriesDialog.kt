package com.nicetas.movieapp3.ui.search.options.origincountry

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.setFragmentResult
import com.nicetas.movieapp3.R
import com.nicetas.movieapp3.databinding.FragmentOriginCountriesDialogBinding
import com.nicetas.movieapp3.ui.search.options.origincountry.adapter.OriginCountriesAdapter
import com.nicetas.movieapp3.ui.search.options.origincountry.adapter.OriginCountryHolder
import me.zhanghai.android.fastscroll.FastScrollerBuilder
import org.koin.androidx.viewmodel.ext.android.viewModel

class OriginCountriesDialog : DialogFragment() {

    companion object {
        const val FRAGMENT_RESULT_KEY = "FRAGMENT_RESULT_KEY_OriginCountriesDialog"
        private const val ARG_SELECTED_COUNTRY_ISO = "ARG_SELECTED_COUNTRY_ISO"
        private const val ARG_SELECTED_COUNTRY_NAME = "ARG_SELECTED_COUNTRY_NAME"

        fun newInstance(selectedCountryIso: String?) = OriginCountriesDialog().apply {
            arguments = Bundle(1).apply {
                putString(ARG_SELECTED_COUNTRY_ISO, selectedCountryIso)
            }
        }

        fun countryISO(bundle: Bundle): String? {
            return bundle.getString(ARG_SELECTED_COUNTRY_ISO)
        }

        fun countryName(bundle: Bundle): String? {
            return bundle.getString(ARG_SELECTED_COUNTRY_NAME)
        }
    }

    private var _binding: FragmentOriginCountriesDialogBinding? = null
    private val binding
        get() = _binding!!

    private val viewModel: OriginCountriesViewModel by viewModel()

    private val selectedCountryIso: String? by lazy {
        requireArguments().getString(ARG_SELECTED_COUNTRY_ISO)
    }

    private var adapter: OriginCountriesAdapter? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        _binding = FragmentOriginCountriesDialogBinding.inflate(layoutInflater)

        adapter = OriginCountriesAdapter(object : OriginCountryHolder.Callback {
            override fun onClick(item: OriginCountryHolder.Item) {
                val bundle = Bundle(2).apply {
                    putString(ARG_SELECTED_COUNTRY_ISO, item.iso_3166_1)
                    putString(ARG_SELECTED_COUNTRY_NAME, item.text)
                }
                setFragmentResult(FRAGMENT_RESULT_KEY, bundle)
                dismiss()
            }

            override fun isSelected(id: String): Boolean {
                return id == selectedCountryIso
            }
        })
        binding.rvItems.adapter = adapter
        FastScrollerBuilder(binding.rvItems).build()

        return AlertDialog.Builder(requireActivity())
            .setView(binding.root)
            .setNeutralButton(R.string.reset_selected) { _, _ ->
                val bundle = Bundle(2).apply {
                    putString(ARG_SELECTED_COUNTRY_ISO, null)
                    putString(ARG_SELECTED_COUNTRY_NAME, null)
                }
                setFragmentResult(FRAGMENT_RESULT_KEY, bundle)
            }
            .setNegativeButton(android.R.string.cancel, null)
            .create()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupObservers()
        viewModel.onViewCreated()
    }

    override fun onDestroyView() {
        adapter?.run { items.list.clear() }
        adapter = null
        binding.rvItems.adapter = null
        super.onDestroyView()
        _binding = null
    }

    private fun setupObservers() {
        viewModel.showCountries.observe(viewLifecycleOwner) { items ->
            adapter?.items?.replaceAll(items)
        }
    }

}
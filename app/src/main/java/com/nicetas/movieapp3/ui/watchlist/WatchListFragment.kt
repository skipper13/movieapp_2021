package com.nicetas.movieapp3.ui.watchlist

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup.MarginLayoutParams
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat.getColor
import androidx.core.view.MenuProvider
import androidx.core.view.ViewCompat
import androidx.core.view.postDelayed
import androidx.core.view.updateLayoutParams
import androidx.fragment.app.setFragmentResultListener
import androidx.lifecycle.Lifecycle
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.nicetas.movieapp3.R
import com.nicetas.movieapp3.base.adapter.GridSpacingItemDecoration
import com.nicetas.movieapp3.base.MoviesSpanCount
import com.nicetas.movieapp3.base.adapter.ScrollOnTapHelper
import com.nicetas.movieapp3.base.extensions.dp
import com.nicetas.movieapp3.base.extensions.statusBarHeight
import com.nicetas.movieapp3.base.fragment.BaseFragment
import com.nicetas.movieapp3.base.states.ViewState
import com.nicetas.movieapp3.databinding.FragmentWatchListBinding
import com.nicetas.movieapp3.ui.extended.tabs.MovieTabsFragment
import com.nicetas.movieapp3.ui.main.StartDestinationTabHelper
import com.nicetas.movieapp3.ui.search.adapter.MovieHolder
import com.nicetas.movieapp3.ui.watchlist.adapter.WatchListAdapter
import com.nicetas.movieapp3.ui.watchlist.sort.WatchlistSortDialog
import org.koin.androidx.viewmodel.ext.android.viewModel

class WatchListFragment : BaseFragment(R.layout.fragment_watch_list) {

    private var _binding: FragmentWatchListBinding? = null
    private val binding
        get() = _binding!!

    private val viewModel: WatchListViewModel by viewModel()

    private var adapter: WatchListAdapter? = null
    private val startDestinationTabHelper = StartDestinationTabHelper()
    private var scrollOnTapHelper: ScrollOnTapHelper? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setFragmentResultListener(WatchlistSortDialog.FRAGMENT_RESULT_KEY) { requestKey, bundle ->
            val sort = WatchlistSortDialog.sort(bundle)
            val isDescending = WatchlistSortDialog.isDescending(bundle)
            viewModel.changeSort(sort, isDescending)
        }
        startDestinationTabHelper.onBackPressedDispatcher(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentWatchListBinding.bind(view)
        scrollOnTapHelper = ScrollOnTapHelper(savedInstanceState)
        initViewState()
        setupOptionsMenu()
        setupViews()
        initRecyclerView()
        setupObservers()
        viewModel.onViewCreated()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        scrollOnTapHelper?.onSaveInstanceState(outState)
    }

    override fun onDestroyView() {
        adapter?.onDestroyView()
        adapter = null
        binding.rvContent.adapter = null
        scrollOnTapHelper = null
        super.onDestroyView()
        _binding = null
    }

    private fun setupOptionsMenu() {
        requireActivity().addMenuProvider(object : MenuProvider {
            override fun onCreateMenu(menu: Menu, menuInflater: MenuInflater) {
                menuInflater.inflate(R.menu.menu_watchlist, menu)
            }

            override fun onMenuItemSelected(menuItem: MenuItem): Boolean {
                return when (menuItem.itemId) {
                    R.id.action_sort_type -> {
                        val dialog = WatchlistSortDialog.newInstance(
                            selectedSort = viewModel.sort,
                            isDescending = viewModel.isDescending,
                        )
                        dialog.show(parentFragmentManager, null)
                        true
                    }
                    R.id.action_lists -> {
                        findNavController().navigate(R.id.listsScreen)
                        true
                    }
                    else -> false
                }
            }
        }, viewLifecycleOwner, Lifecycle.State.RESUMED)
    }

    private fun setupViews() {
        setStatusBarColor(getColor(requireContext(), R.color.colorPrimary))
        setToolbarTitle(getString(R.string.watchlist_title))

        ViewCompat.setOnApplyWindowInsetsListener(binding.root) { _, insets ->
            val statusBarHeight = insets.statusBarHeight()
            binding.appBar.updateLayoutParams<MarginLayoutParams> {
                topMargin = statusBarHeight
            }
            binding.rvContent.updateLayoutParams<MarginLayoutParams> {
                bottomMargin = statusBarHeight
            }
            insets
        }
        binding.toolbar.setOnClickListener {
            scrollOnTapHelper?.scroll(binding.rvContent)
        }
    }

    private fun initViewState() {
        ViewState.Builder(binding.rvContent)
            .loadDelay()
            .error(onClick = { viewModel.reloadMovies() })
            .empty(binding.tvEmptyState)
            .build()
            .observeLce(viewModel.lce, viewLifecycleOwner)
    }

    private fun initRecyclerView() {
        adapter = WatchListAdapter(
            inflater = layoutInflater,
            movieCallback = object : MovieHolder.Callback {
                override val isMultiWindow: Boolean
                    get() = isInMultiWindowMode

                override fun onMovieClick(movie: MovieHolder.Item) {
                    val args = MovieTabsFragment.args(
                        movieId = movie.movieId,
                        title = movie.title,
                        backdropPath = movie.backdropPath,
                        releaseDate = movie.releaseDate,
                        posterPath = movie.posterPath,
                        voteAverage = movie.voteAverage,
                    )
                    findNavController().navigate(R.id.movieTabs, args)
                }

                override fun removeFromBookmarks(movie: MovieHolder.Item) {
                    AlertDialog.Builder(requireContext())
                        .setTitle(R.string.search_remove_from_bookmarks)
                        .setMessage(movie.title)
                        .setPositiveButton(android.R.string.ok) { _, _ ->
                            viewModel.removeFromWatchList(movie.movieId)
                        }
                        .setNegativeButton(android.R.string.cancel, null)
                        .show()
                }
            }
        )

        binding.rvContent.adapter = adapter
        binding.rvContent.itemAnimator = null

        val spanCount = MoviesSpanCount().get(requireActivity(), false)
        binding.rvContent.layoutManager = GridLayoutManager(requireContext(), spanCount)
        binding.rvContent.addItemDecoration(GridSpacingItemDecoration(spanCount, 4f.dp, false))
    }

    private fun setupObservers() {
        viewModel.onGetMovies.observe(viewLifecycleOwner) { movies ->
            adapter?.update(movies)
        }
        viewModel.removeMovieById.observe(viewLifecycleOwner) { movieId ->
            binding.rvContent.itemAnimator = object : DefaultItemAnimator() {
                override fun onRemoveFinished(item: RecyclerView.ViewHolder?) {
                    _binding?.rvContent?.postDelayed(700L) {
                        _binding?.rvContent?.itemAnimator = null
                    }
                }

            }
            adapter?.removeByMovieId(movieId)
        }
        viewModel.scrollToTop.observe(viewLifecycleOwner) {
            binding.rvContent.scrollToPosition(0)
            scrollOnTapHelper?.savedBottomPos = 0
            binding.appBar.setExpanded(true, false)
        }
    }

}
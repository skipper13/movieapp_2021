package com.nicetas.movieapp3.ui.search.options

import android.content.Context
import android.text.SpannableStringBuilder
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat.getColor
import androidx.core.text.color
import com.nicetas.movieapp3.R
import com.nicetas.repos.network.LanguageProvider
import com.nicetas.repos.search.SortBy

class SortByHelper(
    private val context: Context,
) {

    @StringRes
    private fun getTypeText(sort: SortBy): Int {
        return when (sort) {
            SortBy.POPULARITY_DESC, SortBy.POPULARITY_ASC -> {
                R.string.discover_sort_by_popularity
            }
            SortBy.PRIMARY_RELEASE_DATE_DESC, SortBy.PRIMARY_RELEASE_DATE_ASC -> {
                R.string.discover_sort_by_release_date
            }
            SortBy.VOTE_AVERAGE_DESC, SortBy.VOTE_AVERAGE_ASC -> {
                R.string.discover_sort_by_vote_average
            }
            SortBy.TITLE_DESC, SortBy.TITLE_ASC -> {
                R.string.discover_sort_by_title
            }
            SortBy.REVENUE_DESC, SortBy.REVENUE_ASC -> {
                R.string.discover_sort_by_revenue
            }
            SortBy.VOTE_COUNT_DESC, SortBy.VOTE_COUNT_ASC -> {
                R.string.discover_sort_by_vote_count
            }
        }
    }

    fun getButtonText(sort: SortBy): CharSequence {
        val type = context.getString(getTypeText(sort))
        return when (LanguageProvider.language) {
            "tr" -> {
                SpannableStringBuilder()
                    .color(getColor(context, R.color.yellow_CAC683)) {
                        append(type)
                    }
                    .append(" ")
                    .append(context.getString(R.string.discover_btn_sort_by))
            }
            else -> {
                SpannableStringBuilder()
                    .append(context.getString(R.string.discover_btn_sort_by))
                    .append(" ")
                    .color(getColor(context, R.color.yellow_CAC683)) {
                        append(type)
                    }
            }
        }
    }

}
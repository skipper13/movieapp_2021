package com.nicetas.movieapp3.ui.search.options.kit.create.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.content.res.AppCompatResources.getDrawable
import androidx.core.content.ContextCompat.getColor
import androidx.recyclerview.widget.RecyclerView
import com.nicetas.movieapp3.R
import com.nicetas.movieapp3.databinding.ItemKitIconBinding

class KitIconHolder(
    parent: ViewGroup,
    private var binding: ItemKitIconBinding = ItemKitIconBinding.inflate(
        LayoutInflater.from(parent.context),
        parent,
        false
    ),
    callback: Callback,
) : RecyclerView.ViewHolder(binding.root) {

    fun interface Callback {
        fun onIconClick(id: Int)
    }

    class Item(
        val id: Int,
        var isSelected: Boolean,
    )

    private lateinit var item: Item
    private val context: Context
        get() = itemView.context

    init {
        binding.root.setOnClickListener {
            callback.onIconClick(item.id)
        }
    }

    fun bind(item: Item) {
        this.item = item

        val icon = getDrawable(context, KitIcon.getIconById(item.id))
        binding.ivIcon.setImageDrawable(icon)

        if (item.isSelected) {
            binding.root.setBackgroundColor(getColor(context, R.color.yellow_B9B574))
        } else {
            binding.root.background = null
        }
    }
}
package com.nicetas.movieapp3.ui.sort

import android.content.Context
import android.graphics.drawable.Drawable
import android.os.Build
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.DrawableRes
import androidx.appcompat.content.res.AppCompatResources.getDrawable
import androidx.recyclerview.widget.RecyclerView
import com.nicetas.movieapp3.R
import com.nicetas.movieapp3.base.adapter.ClickDebounce
import com.nicetas.movieapp3.databinding.ItemSortByBinding

class SortByHolder<T>(
    private val selectedId: T,
    parent: ViewGroup,
    private var binding: ItemSortByBinding = ItemSortByBinding.inflate(
        LayoutInflater.from(parent.context),
        parent,
        false
    ),
    debounce: ClickDebounce,
    callback: Callback<T>,
) : RecyclerView.ViewHolder(binding.root) {

    fun interface Callback<T> {
        fun onItemClick(position: Int, item: Item<T>)
    }

    class Item<T>(
        val text: String,
        val icon: Drawable,
        val id: T,
    )

    private lateinit var item: Item<T>
    private val context: Context
        get() = itemView.context

    init {
        binding.root.setOnClickListener {
            if (debounce.canClick()) {
                callback.onItemClick(bindingAdapterPosition, item)
            }
        }
    }

    @Suppress("unchecked_cast")
    fun bind(item: Any) {
        this.item = item as Item<T>
        binding.ivIcon.setImageDrawable(item.icon)
        binding.tvTitle.text = item.text

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            binding.root.background = getDrawable(context, R.drawable.dialog_list_ripple)
            if (item.id == selectedId) {
                binding.root.foreground = getDrawable(context, R.drawable.dialog_list_checked)
            } else {
                binding.root.foreground = null
            }
        } else {
            @DrawableRes val resId = if (item.id == selectedId) {
                R.drawable.dialog_list_checked
            } else {
                R.drawable.dialog_list_ripple
            }
            binding.root.background = getDrawable(context, resId)
        }
    }
}
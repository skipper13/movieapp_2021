package com.nicetas.movieapp3.ui.extended.tabs.views

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.Toolbar
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.appbar.CollapsingToolbarLayout
import com.google.android.material.tabs.TabLayout
import com.nicetas.movieapp3.R
import com.nicetas.movieapp3.databinding.FragmentMovieTabsBinding

class ViewsBindingMovieTabs(
    private val movieId: Int,
) : ViewsProviderMovieTabs {

    private var _binding: FragmentMovieTabsBinding? = null
    val binding
        get() = _binding!!

    override val coordinator: CoordinatorLayout
        get() = binding.coordinator
    override val ablExtended: AppBarLayout
        get() = binding.ablExtended
    override val ctlExtended: CollapsingToolbarLayout
        get() = binding.ctlExtended
    override val ivMovie: AppCompatImageView
        get() = binding.ivMovie
    override val toolbar: Toolbar
        get() = binding.toolbar
    override val tabLayout: TabLayout
        get() = binding.tabLayout
    override val viewPager: ViewPager2
        get() = binding.viewPager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?): View {
        val view = inflater.inflate(R.layout.fragment_movie_tabs, container, false)
        _binding = FragmentMovieTabsBinding.bind(view)
        return view
    }

    override fun onViewCreated(view: View) {
        /* no-op */
    }

    override fun onDestroyView() {
        _binding = null
    }

}
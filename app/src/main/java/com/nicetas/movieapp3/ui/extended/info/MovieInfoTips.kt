@file:Suppress("INVISIBLE_MEMBER", "INVISIBLE_REFERENCE")

package com.nicetas.movieapp3.ui.extended.info

import android.content.pm.ActivityInfo
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.PointF
import android.view.View
import android.view.ViewGroup
import android.view.animation.LinearInterpolator
import android.widget.TextView
import androidx.activity.OnBackPressedCallback
import androidx.core.view.isVisible
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.google.android.material.button.MaterialButton
import com.nicetas.movieapp3.R
import com.nicetas.movieapp3.base.adapter.ClickDebounce
import com.nicetas.movieapp3.base.extensions.dp
import com.nicetas.movieapp3.base.extensions.lockOrientationForTips
import com.nicetas.movieapp3.base.extensions.onClickDebounce
import com.nicetas.movieapp3.base.extensions.privateField
import com.takusemba.spotlight.OnSpotlightListener
import com.takusemba.spotlight.OnTargetListener
import com.takusemba.spotlight.Spotlight
import com.takusemba.spotlight.SpotlightView
import com.takusemba.spotlight.Target
import com.takusemba.spotlight.shape.RoundedRectangle
import com.takusemba.spotlight.shape.Shape


class MovieInfoTips(
    private val fragment: Fragment,
    private val scrollView: NestedScrollView?,
    private val tvReleaseDate: TextView?,
    private val vgKeywords: ViewGroup?,
    private val vgLinks: ViewGroup?,
    private val vBottomSpaceForTips: View?,
    private val onTipsEnd: () -> Unit,
) {

    private val activity: FragmentActivity?
        get() = fragment.activity

    private var spotlight: Spotlight? = null

    private val onBackPressedCallback = object : OnBackPressedCallback(enabled = true) {
        override fun handleOnBackPressed() {
            spotlight?.finish()
            spotlight = null
        }
    }

    fun onDestroyView() {
        spotlight?.finish()
        spotlight = null
    }

    fun showTips() {
        if (activity == null || activity!!.lockOrientationForTips()) {
            return
        }

        activity!!.onBackPressedDispatcher.addCallback(
            owner = fragment.viewLifecycleOwner,
            onBackPressedCallback = onBackPressedCallback
        )

        vBottomSpaceForTips!!.isVisible = true

        val point3 = PointF(0F, 0F)
        val point4 = PointF(0F, 0F)

        val targets = ArrayList<Target>(4).apply {
            add(target1())
            add(target2(point3, point4))
            add(target3(point3))
            add(target4(point4))
        }

        spotlight = Spotlight.Builder(fragment.requireActivity())
            .setTargets(targets)
            .setBackgroundColorRes(R.color.black_alpha_45)
            .setDuration(700L)
            .setOnSpotlightListener(object : OnSpotlightListener {
                override fun onStarted() = Unit

                override fun onEnded() {
                    onBackPressedCallback.remove()
                    if (scrollView == null || activity == null) {
                        return
                    }
                    spotlight = null
                    if (scrollView.scrollY == 0) {
                        vBottomSpaceForTips.isVisible = false
                    } else {
                        scrollView.smoothScrollTo(0, 0, 700)
                        vBottomSpaceForTips.postDelayed({
                            vBottomSpaceForTips?.isVisible = false
                        }, 800)
                    }
                    onTipsEnd.invoke()
                    activity!!.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED
                }
            })
            .build()

        val spotlightView = spotlight!!.privateField<SpotlightView>("spotlight")
        spotlightView.setOnClickListener(View.OnClickListener {
            /* prevent clicking through overlay */
        })

        spotlight?.start()
    }

    private fun target1(): Target {
        val shape1 = RoundedRectangle(
            height = tvReleaseDate!!.height * 1.5F,
            width = tvReleaseDate.width * 1.5F,
            radius = 36F,
        )

        val overlay1 = fragment.layoutInflater.inflate(R.layout.layout_movie_info_tip_1, null)

        val clickDebounce = ClickDebounce(1000)

        overlay1.findViewById<MaterialButton>(R.id.btnNext).setOnClickListener {
            if (clickDebounce.canClick()) {
                spotlight?.next()
            }
        }
        overlay1.findViewById<MaterialButton>(R.id.btnClose).setOnClickListener {
            if (clickDebounce.canClick()) {
                spotlight?.finish()
            }
        }

        return Target.Builder()
            .setAnchor(tvReleaseDate)
            .setShape(shape1)
            .setOverlay(overlay1)
            .setOnTargetListener(object : OnTargetListener {
                override fun onStarted() {
                    scrollView?.smoothScrollTo(0, 0)
                }

                override fun onEnded() = Unit
            })
            .build()
    }

    /**
     * Fake target to wait "smoothScrollTo" and get locations of views on screen after that
     */
    private fun target2(point3: PointF, point4: PointF): Target {
        val shape2 = object : Shape {
            override val duration = 0L
            override val interpolator = LinearInterpolator()
            override fun draw(canvas: Canvas, point: PointF, value: Float, paint: Paint) = Unit
        }

        return Target.Builder()
            .setAnchor(0F, 0F)
            .setShape(shape2)
            .setOverlay(View(fragment.requireContext()))
            .setOnTargetListener(object : OnTargetListener {
                override fun onStarted() {
                    val toY = scrollView?.bottom ?: return
                    scrollView.smoothScrollTo(0, toY, 700)

                    scrollView.postDelayed({
                        if (spotlight != null) {
                            point3.fill(vgKeywords!!)
                            point4.fill(vgLinks!!)
                            spotlight!!.next()
                        }
                    }, 800)
                }

                override fun onEnded() = Unit
            })
            .build()
    }

    private fun target3(point3: PointF): Target {
        val shape3 = RoundedRectangle(
            height = vgKeywords!!.height * 1.5F,
            width = vgKeywords.width.toFloat() - 8F.dp,
            radius = 36F,
        )

        val overlay3 = fragment.layoutInflater.inflate(R.layout.layout_movie_info_tip_3, null)

        overlay3.findViewById<MaterialButton>(R.id.btnNext).onClickDebounce(1000) {
            spotlight?.next()
        }

        return Target.Builder()
            .setAnchor(point3)
            .setShape(shape3)
            .setOverlay(overlay3)
            .build()
    }

    private fun target4(point4: PointF): Target {
        val shape4 = RoundedRectangle(
            height = vgLinks!!.height * 1.3F,
            width = vgLinks.width.toFloat() - 8F.dp,
            radius = 36F,
        )

        val overlay4 = fragment.layoutInflater.inflate(R.layout.layout_movie_info_tip_4, null)

        overlay4.findViewById<MaterialButton>(R.id.btnClose).onClickDebounce(1000) {
            spotlight?.finish()
        }

        return Target.Builder()
            .setAnchor(point4)
            .setShape(shape4)
            .setOverlay(overlay4)
            .build()
    }

    private fun PointF.fill(view: View) {
        val location = IntArray(2)
        view.getLocationInWindow(location)
        this.x = location[0] + view.width / 2f
        this.y = location[1] + view.height / 2f
    }

}
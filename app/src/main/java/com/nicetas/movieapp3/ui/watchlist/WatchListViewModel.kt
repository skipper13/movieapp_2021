package com.nicetas.movieapp3.ui.watchlist

import androidx.collection.MutableObjectList
import androidx.collection.ObjectList
import androidx.lifecycle.MutableLiveData
import com.nicetas.bus.WatchListUpdateBus
import com.nicetas.movieapp3.base.Logger
import com.nicetas.movieapp3.base.viewmodel.BaseViewModel
import com.nicetas.movieapp3.base.viewmodel.LiveEvent
import com.nicetas.movieapp3.ui.search.adapter.MovieHolder
import com.nicetas.movieapp3.ui.watchlist.sort.WatchlistSort
import com.nicetas.prefs.Prefs
import com.nicetas.prefs.PrefsKey
import com.nicetas.repos.movie.MovieRepository
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers
import java.time.LocalDate
import java.time.LocalDateTime

class WatchListViewModel(
    private val movieRepository: MovieRepository,
    private val watchListUpdateBus: WatchListUpdateBus,
    private val prefs: Prefs,
) : BaseViewModel() {

    class MovieItem(
        override val movieId: Int,
        override val title: String,
        override val voteAverage: Double?,
        override val releaseDate: LocalDate?,
        override val backdropPath: String?,
        override val posterPath: String?,
        override var isInWatchlist: Boolean,
        val dateAdded: LocalDateTime?,
    ) : MovieHolder.Item(
        movieId = movieId,
        title = title,
        voteAverage = voteAverage,
        releaseDate = releaseDate,
        backdropPath = backdropPath,
        posterPath = posterPath,
        isInWatchlist = isInWatchlist,
    ) {
        override fun toString(): String {
            return "movieId = $movieId, title = $title"
        }
    }

    val onGetMovies = MutableLiveData<ObjectList<Any>>()
    val scrollToTop = LiveEvent<Unit>()
    val removeMovieById = LiveEvent<Int>()

    private val itemsToSort = MutableObjectList<MovieItem>()

    var sort: @WatchlistSort Int = prefs.getInt(PrefsKey.WatchlistSort, WatchlistSort.DEFAULT)
    var isDescending: Boolean = prefs.getBoolean(PrefsKey.WatchlistIsDesc, true)

    private var wasInit = false

    fun onViewCreated() {
        if (wasInit) {
            getMoviesFromDb()
            return
        }
        wasInit = true
        lce.loading()
        getMoviesFromDb()
    }

    override fun onCleared() {
        super.onCleared()
        itemsToSort.clear()
    }

    fun reloadMovies() {
        lce.loading()
        getMoviesFromDb()
    }

    private fun getMoviesFromDb() {
        disposables += movieRepository.getWatchList()
            .map { movies ->
                itemsToSort.clear()
                for (i in movies.indices) {
                    val movie = movies[i]
                    val item = MovieItem(
                        movieId = movie.id,
                        title = movie.title,
                        voteAverage = movie.voteAverage,
                        releaseDate = movie.releaseDate,
                        backdropPath = movie.backdropPath,
                        posterPath = movie.posterPath,
                        dateAdded = movie.dateAdded,
                        isInWatchlist = true,
                    )
                    itemsToSort.add(item)
                }
                sortedItems()
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ items ->
                onGetMovies.value = items
                if (items.isEmpty()) {
                    lce.empty()
                } else {
                    lce.content()
                }
            }, { error ->
                Logger.e(error)
                lce.error(message = error.message ?: "")
            })
    }

    fun changeSort(sort: @WatchlistSort Int, isDescending: Boolean) {
        if (this.sort == sort && this.isDescending == isDescending) {
            return
        }
        this.sort = sort
        this.isDescending = isDescending

        disposables += Single.fromCallable { sortedItems() }
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ items ->
                scrollToTop.setValue(Unit)
                onGetMovies.value = items
            }, { error ->
                Logger.e(error)
                lce.error(message = error.message ?: "")
            })

        prefs.edit().commitIO {
            putBoolean(PrefsKey.WatchlistIsDesc, isDescending)
            putInt(PrefsKey.WatchlistSort, sort)
        }
    }

    @Suppress("UNCHECKED_CAST")
    private fun sortedItems(): ObjectList<Any> {
        return when (itemsToSort.size) {
            0 -> MutableObjectList(0)
            1 -> itemsToSort as ObjectList<Any>
            else -> itemsToSort.asMutableList().sort() as ObjectList<Any>
        }
    }

    private fun List<MovieItem>.sort(): ObjectList<MovieItem> {
        return if (isDescending) {
            when (sort) {
                WatchlistSort.DATE_ADDED -> sortMovies(compareByDescending { it.dateAdded })
                WatchlistSort.RELEASE_DATE -> sortMovies(compareByDescending { it.releaseDate })
                WatchlistSort.TITLE -> sortMovies(compareByDescending { it.title })
                WatchlistSort.RATING -> sortMovies(compareByDescending { it.voteAverage })
                else -> throw IllegalStateException()
            }
        } else {
            when (sort) {
                WatchlistSort.DATE_ADDED -> sortMovies(compareBy { it.dateAdded })
                WatchlistSort.RELEASE_DATE -> sortMovies(compareBy { it.releaseDate })
                WatchlistSort.TITLE -> sortMovies(compareBy { it.title })
                WatchlistSort.RATING -> sortMovies(compareBy { it.voteAverage })
                else -> throw IllegalStateException()
            }
        }
    }

    private fun <T> List<T>.sortMovies(comparator: Comparator<in T>): ObjectList<T> {
        val array = toTypedArray<Any?>() as Array<T>
        array.sortWith(comparator)
        return MutableObjectList<T>().also { it.addAll(array) }
    }

    fun removeFromWatchList(movieId: Int) {
        disposables += movieRepository.deleteMovieToWatch(movieId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                val index = itemsToSort.indexOfFirst { it.movieId == movieId }
                itemsToSort.removeAt(index)
                removeMovieById.setValue(movieId)

                watchListUpdateBus.onRemove(movieId)

                if (itemsToSort.isEmpty()) {
                    lce.empty()
                }
            }, { error ->
                Logger.e(error)
            })
    }

}
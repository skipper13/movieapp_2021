package com.nicetas.movieapp3.ui.search.options.certification.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.nicetas.movieapp3.base.adapter.ClickDebounce
import com.nicetas.movieapp3.base.adapter.AdapterItems

class CertificationAdapter(
    private val countryCallback: CertificationCountryHolder.Callback,
    private val classificationCallback: ClassificationHolder.Callback,
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        private const val TYPE_COUNTRY = 0
        private const val TYPE_CLASSIFICATION = 1
    }

    private val debounce = ClickDebounce()
    val items = AdapterItems<Any>(this)

    override fun getItemCount(): Int = items.list.size

    override fun getItemViewType(position: Int): Int {
        return when (items.list[position]) {
            is CertificationCountryHolder.Item -> TYPE_COUNTRY
            is ClassificationHolder.Item -> TYPE_CLASSIFICATION
            else -> throw IllegalStateException("item is null for position = $position")
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_COUNTRY -> CertificationCountryHolder(
                parent = parent,
                debounce = debounce,
                callback = countryCallback,
            )
            TYPE_CLASSIFICATION -> ClassificationHolder(
                parent = parent,
                debounce = debounce,
                callback = classificationCallback,
            )
            else -> throw IllegalStateException("Unknown holder type")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = items.list[position]
        when (holder) {
            is CertificationCountryHolder -> holder.bind(item as CertificationCountryHolder.Item)
            is ClassificationHolder -> holder.bind(item as ClassificationHolder.Item)
        }
    }

}
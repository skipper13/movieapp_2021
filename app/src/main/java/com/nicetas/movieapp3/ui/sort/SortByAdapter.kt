package com.nicetas.movieapp3.ui.sort

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.nicetas.movieapp3.base.adapter.ClickDebounce
import com.nicetas.movieapp3.base.adapter.AdapterItems

class SortByAdapter<T>(
    private val selectedId: T,
    private val onItemClick: SortByHolder.Callback<T>,
    private val onRadioClick: SortByHeaderHolder.Callback,
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        private const val TYPE_HEADER = 0
        private const val TYPE_ITEM = 1
    }

    private val debounce = ClickDebounce()
    val items = AdapterItems<Any>(this)

    fun onRadioClick(recyclerView: RecyclerView?, isDescending: Boolean) {
        recyclerView?.isVerticalScrollBarEnabled = false
        items.replace(
            item = SortByHeaderHolder.Item(isDescending),
            position = 0,
            payload = 0,
        )
        recyclerView?.postDelayed({ recyclerView?.isVerticalScrollBarEnabled = true }, 200)
    }

    override fun getItemCount(): Int = items.list.size

    override fun getItemViewType(position: Int): Int {
        return when (items.list[position]) {
            is SortByHeaderHolder.Item -> TYPE_HEADER
            is SortByHolder.Item<*> -> TYPE_ITEM
            else -> throw IllegalStateException("item is null for position = $position")
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_HEADER -> SortByHeaderHolder(
                parent = parent,
                debounce = debounce,
                callback = onRadioClick,
            )
            TYPE_ITEM -> SortByHolder(
                selectedId = selectedId,
                parent = parent,
                debounce = debounce,
                callback = onItemClick,
            )
            else -> throw IllegalStateException("Unknown holder type")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = items.list[position]
        when (holder) {
            is SortByHeaderHolder -> holder.bind(item as SortByHeaderHolder.Item)
            is SortByHolder<*> -> holder.bind(item)
        }
    }

}
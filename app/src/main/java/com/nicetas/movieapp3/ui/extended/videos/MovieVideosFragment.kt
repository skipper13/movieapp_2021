package com.nicetas.movieapp3.ui.extended.videos

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.widget.FrameLayout
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.nicetas.movieapp3.base.adapter.GridSpacingItemDecoration
import com.nicetas.movieapp3.base.adapter.RvScrollbarsHelper
import com.nicetas.movieapp3.base.extensions.dp
import com.nicetas.movieapp3.base.extensions.isLandscape
import com.nicetas.movieapp3.base.fragment.BaseFragment
import com.nicetas.movieapp3.base.states.ViewState
import com.nicetas.movieapp3.ui.extended.videos.adapter.MovieVideosAdapter
import com.nicetas.movieapp3.ui.extended.videos.adapter.VideosHeaderHolder
import org.koin.androidx.viewmodel.ext.android.viewModel

class MovieVideosFragment : BaseFragment() {

    companion object {
        private const val ARG_MOVIE_ID = "ARG_MOVIE_ID"

        fun newInstance(
            movieId: Int,
        ) = MovieVideosFragment().apply {
            arguments = Bundle(1).apply {
                putInt(ARG_MOVIE_ID, movieId)
            }
        }
    }

    private val viewModel: MovieVideosViewModel by viewModel()

    private var rvContent: RecyclerView? = null
    private var adapter: MovieVideosAdapter? = null

    private val movieId: Int by lazy { requireArguments().getInt(ARG_MOVIE_ID) }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val vgRoot = FrameLayout(requireContext())
        vgRoot.layoutParams = ViewGroup.LayoutParams(MATCH_PARENT, MATCH_PARENT)

        rvContent = RecyclerView(requireContext(), RvScrollbarsHelper.attrs).apply {
            id = viewModel.recyclerViewId
            layoutParams = ViewGroup.LayoutParams(MATCH_PARENT, MATCH_PARENT)
            setPadding(0, 8F.dp, 0, 8F.dp)
            clipToPadding = false
        }
        vgRoot.addView(rvContent)
        return vgRoot
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewState()
        initRecyclerView()
        setupObservers()
        viewModel.onViewCreated(movieId)
    }

    override fun onDestroyView() {
        adapter?.onDestroyView()
        adapter = null
        rvContent?.adapter = null
        rvContent = null
        super.onDestroyView()
    }

    private fun initViewState() {
        ViewState.Builder(rvContent!!)
            .load()
            .error(onClick = { viewModel.reload() })
            .build()
            .observeLce(viewModel.lce, viewLifecycleOwner)
    }

    private fun initRecyclerView() {
        adapter = MovieVideosAdapter(
            inflater = layoutInflater,
            onPlayClick = { key ->
                onPlayClick(key)
            },
            onVideosHeaderClick = { type ->
                viewModel.onVideosHeaderClick(type)
            }
        )
        val rvContent = rvContent ?: return
        rvContent.adapter = adapter

        val spanCount = if (resources.isLandscape()) 2 else 1

        val gridLayoutManager = GridLayoutManager(requireContext(), spanCount)

        gridLayoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                val adapter = adapter ?: return spanCount

                return when (adapter.items.list[position]) {
                    is VideosHeaderHolder.Item -> spanCount
                    else -> 1
                }
            }
        }
        rvContent.layoutManager = gridLayoutManager

        rvContent.addItemDecoration(GridSpacingItemDecoration(spanCount, 4f.dp, false))
    }

    private fun onPlayClick(key: String) {
        try {
            val intentApp = Intent(
                Intent.ACTION_VIEW,
                Uri.parse("vnd.youtube:$key")
            )
            startActivity(intentApp)
        } catch (ex: ActivityNotFoundException) {
            val intentBrowser = Intent(
                Intent.ACTION_VIEW,
                Uri.parse("http://www.youtube.com/watch?v=$key")
            )
            startActivity(intentBrowser)
        }
    }

    private fun setupObservers() {
        viewModel.updateItems.observe(viewLifecycleOwner) { items ->
            adapter?.update(items)
        }
    }

}
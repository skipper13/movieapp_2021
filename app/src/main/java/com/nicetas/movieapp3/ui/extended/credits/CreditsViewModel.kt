package com.nicetas.movieapp3.ui.extended.credits

import android.view.View
import androidx.collection.MutableObjectList
import androidx.collection.MutableScatterSet
import androidx.collection.ObjectList
import androidx.lifecycle.MutableLiveData
import com.nicetas.movieapp3.base.Logger
import com.nicetas.movieapp3.base.viewmodel.BaseViewModel
import com.nicetas.movieapp3.ui.extended.credits.adapter.CastHolder
import com.nicetas.movieapp3.ui.extended.credits.adapter.CrewHolder
import com.nicetas.movieapp3.ui.extended.credits.adapter.DepartmentHolder
import com.nicetas.repos.movie.MovieRepository
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.internal.operators.single.SingleFromCallable
import io.reactivex.rxjava3.schedulers.Schedulers
import java.util.Optional

class CreditsViewModel(
    private val movieRepository: MovieRepository,
) : BaseViewModel() {

    companion object {
        private const val CAST_DEPARTMENT = "Cast"
    }

    val updateCredits = MutableLiveData<ObjectList<Any>>()

    private var wasInit = false
    private var movieId = 0
    private val expandedDepartments = MutableScatterSet<String>().apply {
        add(CAST_DEPARTMENT)
    }
    var recyclerViewId = View.generateViewId()

    fun onViewCreated(movieId: Int) {
        if (wasInit) {
            return
        }
        wasInit = true
        this.movieId = movieId
        lce.loading()
        loadCredits()
    }

    override fun onCleared() {
        super.onCleared()
        expandedDepartments.clear()
    }

    fun reload() {
        lce.loading()
        loadCredits()
    }

    private fun loadCredits() {
        disposables += SingleFromCallable {
            val cache = movieRepository.getMovieCreditsFromCache(movieId)
            Optional.ofNullable(cache)
        }.flatMap { optional ->
            if (optional.isEmpty) {
                movieRepository.loadMovieCredits(movieId).map { it.body }
            } else {
                Single.just(optional.get())
            }
        }.map { credits ->

            val items = MutableObjectList<Any>()

            val cast = credits.cast

            if (cast != null && cast.isEmpty().not()) {
                val isExpanded = expandedDepartments.contains(CAST_DEPARTMENT)

                val departmentItem = DepartmentHolder.Item(
                    isExpanded = isExpanded,
                    department = CAST_DEPARTMENT,
                )
                items.add(departmentItem)

                if (isExpanded) {
                    for (i in cast.indices) {
                        val actor = cast[i]
                        val item = CastHolder.Item(
                            actorId = actor.id,
                            name = actor.name,
                            character = actor.character,
                            profilePath = actor.profilePath,
                        )
                        items.add(item)
                    }
                }
            }

            val crew = credits.crew?.sortedBy { it.department }
            var department = ""
            crew?.forEach { person ->
                val isExpanded = expandedDepartments.contains(person.department)

                if (person.department != department) {
                    department = person.department

                    val departmentItem = DepartmentHolder.Item(
                        isExpanded = isExpanded,
                        department = department,
                    )
                    items.add(departmentItem)
                }
                if (isExpanded) {
                    val item = CrewHolder.Item(
                        id = person.id,
                        name = person.name,
                        job = person.job,
                        department = person.department,
                        profilePath = person.profilePath,
                    )
                    items.add(item)
                }
            }

            items
        }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ credits ->
                updateCredits.value = credits
                lce.content()
            }, { error ->
                Logger.e(error)
                lce.error(message = error.message ?: "")
            })
    }

    fun onDepartmentClick(department: String) {
        if (expandedDepartments.remove(department).not()) {
            expandedDepartments.add(department)
        }
        loadCredits()
    }

}
package com.nicetas.movieapp3.ui.extended.videos.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.collection.ObjectList
import androidx.recyclerview.widget.RecyclerView
import com.nicetas.movieapp3.base.adapter.AdapterItems
import com.nicetas.movieapp3.base.adapter.BaseDiffUtilCallback
import com.nicetas.movieapp3.base.adapter.ClickDebounce

class MovieVideosAdapter(
    private var inflater: LayoutInflater?,
    private val onPlayClick: MovieVideosHolder.Callback,
    private val onVideosHeaderClick: VideosHeaderHolder.Callback,
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        private const val TYPE_VIDEO = 0
        private const val TYPE_VIDEOS_HEADER = 1
    }

    class DiffUtilCallback() : BaseDiffUtilCallback<Any>() {

        override fun areItemsTheSame(oldItem: Any, newItem: Any): Boolean {
            if (oldItem is MovieVideosHolder.Item && newItem is MovieVideosHolder.Item) {
                return oldItem.id == newItem.id
            }
            if (oldItem is VideosHeaderHolder.Item && newItem is VideosHeaderHolder.Item) {
                return oldItem.type == newItem.type
            }
            return false
        }

        override fun areContentsTheSame(oldItem: Any, newItem: Any): Boolean {
            if (oldItem is MovieVideosHolder.Item && newItem is MovieVideosHolder.Item) {
                return oldItem.youTubeKey == newItem.youTubeKey
            }
            if (oldItem is VideosHeaderHolder.Item && newItem is VideosHeaderHolder.Item) {
                return oldItem.isExpanded == newItem.isExpanded
            }
            return false
        }
    }

    private val debounce = ClickDebounce()
    private val diffUtilCallback = DiffUtilCallback()
    val items = AdapterItems<Any>(this)

    fun update(newItems: ObjectList<Any>) {
        items.update(newItems, diffUtilCallback)
    }

    fun onDestroyView() {
        items.list.clear()
        inflater = null
    }

    override fun getItemCount(): Int = items.list.size

    override fun getItemViewType(position: Int): Int {
        return when (items.list[position]) {
            is MovieVideosHolder.Item -> TYPE_VIDEO
            is VideosHeaderHolder.Item -> TYPE_VIDEOS_HEADER
            else -> throw IllegalStateException("item is null for position = $position")
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_VIDEO -> MovieVideosHolder(
                parent = parent,
                inflater = inflater ?: /*cannot happen*/ LayoutInflater.from(parent.context),
                debounce = debounce,
                callback = onPlayClick,
            )
            TYPE_VIDEOS_HEADER -> VideosHeaderHolder(
                parent = parent,
                inflater = inflater ?: /*cannot happen*/ LayoutInflater.from(parent.context),
                debounce = debounce,
                callback = onVideosHeaderClick,
            )
            else -> throw IllegalStateException("Unknown holder type")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = items.list[position]
        when (holder) {
            is MovieVideosHolder -> holder.bind(item as MovieVideosHolder.Item)
            is VideosHeaderHolder -> holder.bind(item as VideosHeaderHolder.Item)
        }
    }

}
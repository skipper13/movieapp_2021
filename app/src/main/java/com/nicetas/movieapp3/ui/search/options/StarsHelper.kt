package com.nicetas.movieapp3.ui.search.options

import android.content.Context
import android.view.ViewGroup
import androidx.annotation.IntRange
import androidx.appcompat.content.res.AppCompatResources.getDrawable
import androidx.appcompat.widget.AppCompatImageView
import com.nicetas.movieapp3.R
import com.nicetas.movieapp3.databinding.LayoutStarsBinding

class StarsHelper(
    private val binding: LayoutStarsBinding,
    private val updateMin: (star: Double?) -> Unit,
    private val updateMax: (star: Double?) -> Unit,
) {

    private val context: Context
        get() = binding.root.context

    private val iconStar = getDrawable(context, R.drawable.ic_rating_star)
    private val iconStarHalf = getDrawable(context, R.drawable.ic_rating_star_half)
    private val iconStarFilled = getDrawable(context, R.drawable.ic_rating_star_filled)

    private var isHalfMinStar = false

    @IntRange(from = 1, to = 10)
    private var starMin: Int? = null

    @IntRange(from = 3, to = 10)
    private var starMax: Int? = null

    private val viewGroups = ArrayList<ViewGroup>(10).apply {
        add(binding.vgStar1)
        add(binding.vgStar2)
        add(binding.vgStar3)
        add(binding.vgStar4)
        add(binding.vgStar5)
        add(binding.vgStar6)
        add(binding.vgStar7)
        add(binding.vgStar8)
        add(binding.vgStar9)
        add(binding.vgStar10)
    }
    private val imageViews = ArrayList<AppCompatImageView>(10).apply {
        add(binding.ivStar1)
        add(binding.ivStar2)
        add(binding.ivStar3)
        add(binding.ivStar4)
        add(binding.ivStar5)
        add(binding.ivStar6)
        add(binding.ivStar7)
        add(binding.ivStar8)
        add(binding.ivStar9)
        add(binding.ivStar10)
    }

    init {
        setListeners()
        updateViewsMin()
    }

    private fun setListeners() {
        binding.vgStar1.setOnClickListener { onStarClick(null) }

        for (i in 1 until viewGroups.size) {
            viewGroups[i].setOnClickListener { onStarClick(i + 1) }
        }

        for (i in 2 until viewGroups.size) {
            viewGroups[i].setOnLongClickListener {
                onStarLongClick(i + 1)
                return@setOnLongClickListener true
            }
        }
    }

    private fun onStarClick(@IntRange(from = 1, to = 10) star: Int?) {
        if (star == starMin && starMax == null) {
            isHalfMinStar = isHalfMinStar.not()
        } else {
            isHalfMinStar = false
        }
        starMin = star
        starMax = null

        updateViewsMin()

        if (starMin != null && isHalfMinStar) {
            updateMin.invoke(starMin!!.toDouble() - 0.5)
        } else {
            updateMin.invoke(starMin?.toDouble())
        }
        updateMax.invoke(starMax?.toDouble())
    }

    private fun onStarLongClick(@IntRange(from = 3, to = 10) star: Int) {
        if (starMin == null || starMin!! >= star) {
            return
        }
        starMax = star
        updateViewsMax()
        updateMax.invoke(star.toDouble())

        if (isHalfMinStar) {
            isHalfMinStar = false
            updateMin.invoke(starMin!!.toDouble())
        }
    }

    fun update(
        starMinNew: Double?,
        starMaxNew: Double?,
    ) {
        isHalfMinStar = if (starMinNew == null) {
            false
        } else {
            starMinNew % 1 != 0.0
        }

        starMin = if (isHalfMinStar) {
            // Set 8 instead of 7.5 to show 'half star' at the 8th place
            // (7.5).toInt() = 7,   (7).plus(1) = 8
            starMinNew?.toInt()?.plus(1)
        } else {
            starMinNew?.toInt()
        }

        starMax = starMaxNew?.toInt()

        if (starMax == null) {
            updateViewsMin()
        } else {
            updateViewsMax()
        }
    }

    private fun updateViewsMin() {
        val starMin = starMin
        if (starMin == null) {
            for (i in 0 until imageViews.size) {
                imageViews[i].setImageDrawable(iconStar)
            }
            return
        }
        for (i in 0 until starMin) {
            if (isHalfMinStar && i == starMin - 1) {
                imageViews[i].setImageDrawable(iconStarHalf)
            } else {
                imageViews[i].setImageDrawable(iconStarFilled)
            }
        }
        for (i in starMin until imageViews.size) {
            imageViews[i].setImageDrawable(iconStar)
        }
    }

    private fun updateViewsMax() {
        val starMin = starMin
        val starMax = starMax
        if (starMin == null || starMax == null) {
            return
        }
        for (i in 0 .. starMin - 2) {
            imageViews[i].setImageDrawable(iconStar)
        }
        for (i in starMin - 1 until starMax) {
            imageViews[i].setImageDrawable(iconStarFilled)
        }
        for (i in starMax until imageViews.size) {
            imageViews[i].setImageDrawable(iconStar)
        }
    }

}
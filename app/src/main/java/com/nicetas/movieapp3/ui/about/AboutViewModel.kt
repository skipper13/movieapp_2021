package com.nicetas.movieapp3.ui.about

import androidx.lifecycle.MutableLiveData
import com.nicetas.movieapp3.R
import com.nicetas.movieapp3.base.Logger
import com.nicetas.movieapp3.base.viewmodel.BaseViewModel
import com.nicetas.movieapp3.base.viewmodel.LiveEvent
import com.nicetas.movieapp3.ui.ImageUrl
import com.nicetas.prefs.Prefs
import com.nicetas.prefs.PrefsKey
import com.nicetas.repos.configuration.ConfigRepository
import com.nicetas.repos.network.OfflineModeStorage
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers

class AboutViewModel(
    private val configRepository: ConfigRepository,
    private val offlineModeStorage: OfflineModeStorage,
    private val prefs: Prefs,
) : BaseViewModel() {

    val isOfflineMode = MutableLiveData(OfflineModeStorage.isOfflineMode)
    val showReloadConfigMessage = LiveEvent<Int>()

    fun updateOfflineMode(isOffline: Boolean) {
        isOfflineMode.value = isOffline
        offlineModeStorage.updateOfflineMode(isOffline)
    }

    fun resetTips() {
        prefs.edit().commitIO {
            remove(PrefsKey.IsSearchOptionsTipsShown)
            remove(PrefsKey.IsMovieInfoTipsShown)
        }
    }

    fun reloadConfig() {
        disposables += configRepository.loadConfig()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ config ->
                ImageUrl.init(config.images.secureBaseUrl)
                showReloadConfigMessage.setValue(R.string.about_reload_config_success)
            }, { error ->
                Logger.e(error)
                showReloadConfigMessage.setValue(R.string.about_reload_config_error)
            })
    }

}
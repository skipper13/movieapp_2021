package com.nicetas.movieapp3.ui.search.options

import androidx.collection.MutableObjectList
import androidx.collection.ObjectList
import com.nicetas.database.search.SearchCompanyEntity
import com.nicetas.database.search.SearchKeywordEntity
import com.nicetas.domain.DateFormatInteractor
import com.nicetas.models.IdName
import com.nicetas.models.genres.Genre
import com.nicetas.movieapp3.base.Logger
import com.nicetas.movieapp3.base.viewmodel.BaseViewModel
import com.nicetas.movieapp3.base.viewmodel.LiveEvent
import com.nicetas.movieapp3.ui.search.options.keywords.adapter.AddKeywordHolder
import com.nicetas.movieapp3.ui.search.options.keywords.adapter.AddKeywordSmallHolder
import com.nicetas.movieapp3.ui.search.options.keywords.adapter.KeywordHolder
import com.nicetas.movieapp3.ui.search.options.keywords.adapter.SetupKeywordsHolder
import com.nicetas.movieapp3.ui.search.options.keywords.search.SearchWord
import com.nicetas.prefs.Prefs
import com.nicetas.prefs.PrefsKey
import com.nicetas.repos.configuration.ConfigRepository
import com.nicetas.repos.genre.GenresRepository
import com.nicetas.repos.search.SearchKit
import com.nicetas.repos.search.SearchRepository
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers

class SearchOptionsViewModel(
    private val searchRepository: SearchRepository,
    private val genresRepository: GenresRepository,
    private val configRepository: ConfigRepository,
    private val dateFormat: DateFormatInteractor,
    private val prefs: Prefs,
) : BaseViewModel() {

    class RemoveKeywords(
        val items: List<IdName>,
        val type: @SearchWord Int,
    )

    val showTips = LiveEvent<Unit>()
    val updateViews = LiveEvent<Unit>()
    val updateKeywordsWith = LiveEvent<ObjectList<Any>>()
    val updateKeywordsWithout = LiveEvent<ObjectList<Any>>()
    val updateCompaniesWith = LiveEvent<ObjectList<Any>>()
    val updateCompaniesWithout = LiveEvent<ObjectList<Any>>()
    val removeKeywordWith = LiveEvent<Int>()
    val removeKeywordWithout = LiveEvent<Int>()
    val removeCompanyWith = LiveEvent<Int>()
    val removeCompanyWithout = LiveEvent<Int>()
    val showRemoveKeywordsDialog = LiveEvent<RemoveKeywords>()

    private var wasInit = false
    val genres = arrayListOf<Genre>()

    var currentTab: Int
        get() = searchRepository.currentSearchOptionsTab
        set(value) {
            searchRepository.currentSearchOptionsTab = value
        }

    val options: SearchKit
        get() = searchRepository.searchKit

    fun dateOrder() = dateFormat.dateOrder
    fun dateSeparator() = dateFormat.dateSeparator

    fun onViewCreated() {
        if (wasInit) {
            updateViews.setValue(Unit)
            checkTips()
            return
        }
        wasInit = true
        loadGenres()
    }

    override fun onCleared() {
        super.onCleared()
        genres.clear()
    }

    fun findCountryName(iso_3166_1: String): String? {
        return configRepository.findCountryName(iso_3166_1)
    }

    fun updateSearchKitDiscover() {
        searchRepository.updateSearchKitDiscover()
    }

    fun loadGenres() {
        lce.loading()
        disposables += genresRepository.getGenres()
            .map { genres ->
                this.genres.addAll(genres)
                dateFormat.updateFromPrefs()
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                updateViews.setValue(Unit)
                lce.content()
                checkTips()
            }, { error ->
                Logger.e(error)
                lce.error(message = error.message ?: "")
            })
    }

    private fun checkTips() {
        val isTipsShown = prefs.getBoolean(PrefsKey.IsSearchOptionsTipsShown, false)
        if (isTipsShown.not()) {
            showTips.setValue(Unit)
        }
    }

    fun setTipsShown() {
        prefs.edit().commitIO {
            putBoolean(PrefsKey.IsSearchOptionsTipsShown, true)
        }
    }

    fun updateKeywords(type: @SearchWord Int) {
        disposables += searchWordList(type)
            .map { list ->
                val showSetupButton =
                    type == SearchWord.KEYWORDS_WITH || type == SearchWord.COMPANIES_WITH

                var size = list.size + 1
                if (list.isNotEmpty()) size++
                if (showSetupButton) size++

                val items = MutableObjectList<Any>(size)

                if (list.isNotEmpty()) {
                    items += AddKeywordSmallHolder.Item()
                }
                for (keyword in list.sortedBy { it.iName() }) {
                    val item = KeywordHolder.Item(
                        id = keyword.identifier(),
                        text = keyword.iName(),
                    )
                    items += item
                }
                items += AddKeywordHolder.Item()
                if (showSetupButton) {
                    items += SetupKeywordsHolder.Item()
                }
                items
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ items ->
                when (type) {
                    SearchWord.KEYWORDS_WITH -> updateKeywordsWith.setValue(items)
                    SearchWord.KEYWORDS_WITHOUT -> updateKeywordsWithout.setValue(items)
                    SearchWord.COMPANIES_WITH -> updateCompaniesWith.setValue(items)
                    SearchWord.COMPANIES_WITHOUT -> updateCompaniesWithout.setValue(items)
                }
            }, { error ->
                Logger.e(error)
            })
    }

    fun removeKeywordWith(id: Int) {
        options.withKeywords.remove(id)
        removeKeywordWith.setValue(id)
    }

    fun removeKeywordWithout(id: Int) {
        options.withoutKeywords.remove(id)
        removeKeywordWithout.setValue(id)
    }

    fun removeCompanyWith(id: Int) {
        options.withCompanies.remove(id)
        removeCompanyWith.setValue(id)
    }

    fun removeCompanyWithout(id: Int) {
        options.withoutCompanies.remove(id)
        removeCompanyWithout.setValue(id)
    }

    fun showRemoveKeywords(type: @SearchWord Int) {
        disposables += searchWordList(type)
            .map { it.sortedBy { it.iName() } }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ items ->
                showRemoveKeywordsDialog.setValue(RemoveKeywords(items, type))
            }, { error ->
                Logger.e(error)
            })
    }

    private fun searchWordList(type: @SearchWord Int): Single<List<IdName>> {
        val ids = when (type) {
            SearchWord.KEYWORDS_WITH -> options.withKeywords
            SearchWord.KEYWORDS_WITHOUT -> options.withoutKeywords
            SearchWord.COMPANIES_WITH -> options.withCompanies
            SearchWord.COMPANIES_WITHOUT -> options.withoutCompanies
            else -> throw IllegalStateException()
        }
        val idsList = ArrayList<Int>(ids.size).apply {
            addAll(ids)
        }
        return when {
            SearchWord.isKeyword(type) -> {
                searchRepository.getKeywordsForKits(idsList).map { mapWordList(it, idsList, type) }
            }
            SearchWord.isCompany(type) -> {
                searchRepository.getCompaniesForKits(idsList).map { mapWordList(it, idsList, type) }
            }
            else -> throw IllegalStateException()
        }
    }

    private fun mapWordList(
        entities: List<IdName>,
        idsList: List<Int>,
        type: @SearchWord Int,
    ): List<IdName> {
        val resultList = ArrayList<IdName>(idsList.size)
        idsLoop@ for (i in idsList.indices) {
            if (i < entities.size) {
                resultList.add(entities[i])
            }
            val id = idsList[i]
            for (j in entities.indices) {
                if (id == entities[j].identifier()) {
                    continue@idsLoop
                }
            }
            when {
                SearchWord.isKeyword(type) -> {
                    resultList.add(SearchKeywordEntity(id = id, name = ""))
                }
                SearchWord.isCompany(type) -> {
                    resultList.add(SearchCompanyEntity(id = id, name = ""))
                }
            }
        }
        return resultList
    }

}
package com.nicetas.movieapp3.ui.lists

import androidx.collection.MutableObjectList
import androidx.collection.ObjectList
import androidx.lifecycle.MutableLiveData
import com.nicetas.movieapp3.base.Logger
import com.nicetas.movieapp3.base.viewmodel.BaseViewModel
import com.nicetas.movieapp3.ui.lists.adapter.ListsHolder
import com.nicetas.repos.list.ListRepository
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers

class ListsViewModel(
    private val listRepository: ListRepository,
) : BaseViewModel() {

    val onGetLists = MutableLiveData<ObjectList<Any>>()

    private var wasInit = false

    fun onViewCreated() {
        if (wasInit) {
            getMovies()
            return
        }
        wasInit = true
        lce.loading()
        getMovies()
    }

    fun reloadMovies() {
        lce.loading()
        getMovies()
    }

    private fun getMovies() {
        disposables += listRepository.getLists()
            .map { lists ->
                val items = MutableObjectList<Any>(lists.size)
                for (i in lists.indices) {
                    val list = lists[i]
                    val item = ListsHolder.Item(
                        listId = list.id,
                        name = list.name,
                        itemCount = list.itemCount,
                        backdropPath = list.backdropPath,
                    )
                    items.add(item)
                }
                items
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ items ->
                onGetLists.value = items
                if (items.isEmpty()) {
                    lce.empty()
                } else {
                    lce.content()
                }
            }, { error ->
                Logger.e(error)
                lce.error(message = error.message ?: "")
            })
    }

}
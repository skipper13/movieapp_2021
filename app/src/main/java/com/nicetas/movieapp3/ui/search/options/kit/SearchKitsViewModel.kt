package com.nicetas.movieapp3.ui.search.options.kit

import android.view.View
import androidx.collection.MutableObjectList
import androidx.collection.ObjectList
import androidx.lifecycle.MutableLiveData
import com.nicetas.database.search.SearchKitEntity
import com.nicetas.movieapp3.base.Logger
import com.nicetas.movieapp3.base.viewmodel.BaseViewModel
import com.nicetas.movieapp3.base.viewmodel.LiveEvent
import com.nicetas.movieapp3.ui.search.options.kit.adapter.SearchKitHolder
import com.nicetas.prefs.Prefs
import com.nicetas.prefs.PrefsKey
import com.nicetas.repos.search.SearchRepository
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers

class SearchKitsViewModel(
    private val searchRepository: SearchRepository,
    private val prefs: Prefs,
) : BaseViewModel() {

    companion object {
        private const val DEFAULT_KIT_ID = SearchRepository.DEFAULT_KIT_ID
    }

    val showOptions = MutableLiveData<ObjectList<Any>>()
    val closeDialog = LiveEvent<Unit>()
    val updateOptionsScreen = LiveEvent<Unit>()

    private val kits = MutableObjectList<SearchKitEntity>()
    private var wasInit = false
    var recyclerViewId = View.generateViewId()

    fun onViewCreated() {
        if (wasInit) {
            return
        }
        wasInit = true
        getKits()
    }

    override fun onCleared() {
        super.onCleared()
        kits.clear()
    }

    private fun getKits() {
        disposables += searchRepository.getUserSearchKits()
            .map { userKits ->
                kits.clear()
                kits.addDefaults()
                kits.addAll(userKits)

                val selectedKitId = searchRepository.getSelectedKitId()

                val items = MutableObjectList<Any>(kits.size)

                for (i in 0 until kits.size) {
                    val kit = kits[i]
                    val item = SearchKitHolder.Item(
                        id = kit.id,
                        iconId = kit.iconId,
                        text = kit.title,
                        isSelected = kit.id == selectedKitId,
                    )
                    items.add(item)
                }
                items
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ items ->
                showOptions.value = items
            }, { error ->
                Logger.e(error)
            })
    }

    private fun MutableObjectList<SearchKitEntity>.addDefaults() {
        add(SearchKitEntity.createPopular())
        add(SearchKitEntity.createTop())
        add(SearchKitEntity.createUpcoming())
        if (prefs.getBoolean(PrefsKey.IsKitAnimationHided, false).not()) {
            add(SearchKitEntity.createAnimation())
        }
    }

    fun selectKit(id: Int) {
        searchRepository.searchKit.update(kits.first { it.id == id })

        disposables += searchRepository.saveSelectedKitId(id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ _ ->
                closeDialog.setValue(Unit)
            }, { error ->
                Logger.e(error)
            })
    }

    fun deleteKit(id: Int) {
        disposables += searchRepository.deleteSearchKit(kitId = id)
            .flatMap { affectedRows ->
                if (id == searchRepository.getSelectedKitId()) {
                    val entity = kits.first { it.id == DEFAULT_KIT_ID }
                    searchRepository.searchKit.update(entity)
                    searchRepository.saveSelectedKitId(id = DEFAULT_KIT_ID)
                } else {
                    Single.just(true)
                }
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ _ ->
                getKits()
                updateOptionsScreen.setValue(Unit)
            }, { error ->
                Logger.e(error)
            })
    }

}
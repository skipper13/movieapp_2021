package com.nicetas.movieapp3.ui.watchlist.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.collection.ObjectList
import androidx.recyclerview.widget.RecyclerView
import com.nicetas.movieapp3.base.adapter.BaseDiffUtilCallback
import com.nicetas.movieapp3.base.adapter.ClickDebounce
import com.nicetas.movieapp3.base.adapter.AdapterItems
import com.nicetas.movieapp3.ui.search.adapter.MovieHolder

class WatchListAdapter(
    private var inflater: LayoutInflater?,
    private val movieCallback: MovieHolder.Callback
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        private const val TYPE_MOVIE = 0
    }

    class DiffUtilCallback() : BaseDiffUtilCallback<Any>() {

        private val movieDifHelper = MovieHolder.DifHelper()

        override fun areItemsTheSame(oldItem: Any, newItem: Any): Boolean {
            if (oldItem is MovieHolder.Item && newItem is MovieHolder.Item) {
                return movieDifHelper.areItemsTheSame(oldItem, newItem)
            }
            return false
        }

        override fun areContentsTheSame(oldItem: Any, newItem: Any): Boolean {
            if (oldItem is MovieHolder.Item && newItem is MovieHolder.Item) {
                return movieDifHelper.areContentsTheSame(oldItem, newItem)
            }
            return false
        }
    }

    private val voteFormatter = MovieHolder.VoteFormatter()
    private val debounce = ClickDebounce()
    private val diffUtilCallback = DiffUtilCallback()
    val items = AdapterItems<Any>(this)

    fun update(newItems: ObjectList<Any>) {
        items.update(newItems, diffUtilCallback)
    }

    fun removeByMovieId(movieId: Int) {
        val index = items.list.indexOfFirst { it is MovieHolder.Item && it.movieId == movieId }
        items.remove(index)
    }

    fun onDestroyView() {
        items.list.clear()
        inflater = null
    }

    override fun getItemCount(): Int = items.list.size

    override fun getItemViewType(position: Int): Int {
        return when (items.list[position]) {
            is MovieHolder.Item -> TYPE_MOVIE
            else -> throw IllegalStateException("item is null for position = $position")
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_MOVIE -> MovieHolder(
                parent = parent,
                inflater = inflater ?: /*cannot happen*/ LayoutInflater.from(parent.context),
                debounce = debounce,
                callback = movieCallback,
                voteFormatter = voteFormatter,
            )
            else -> throw IllegalStateException("Unknown holder type")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = items.list[position]
        when (holder) {
            is MovieHolder -> holder.bind(item as MovieHolder.Item)
        }
    }

}


package com.nicetas.movieapp3.ui.extended.actor.cast

import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import com.nicetas.models.persons.credits.PersonCast
import com.nicetas.movieapp3.base.Logger
import com.nicetas.movieapp3.base.viewmodel.BaseViewModel
import com.nicetas.repos.person.PersonRepository
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.internal.operators.single.SingleFromCallable
import io.reactivex.rxjava3.schedulers.Schedulers
import java.time.LocalDate
import java.util.Optional

class ActorCastViewModel(
    private val personRepository: PersonRepository,
) : BaseViewModel() {

    class TitleItem(val year: String)

    class MovieItem(
        val movieId: Int,
        val title: String,
        val voteAverage: Double?,
        val releaseDate: LocalDate?,
        val backdropPath: String?,
        val posterPath: String?,
    )

    private var wasInit = false
    private var actorId: Int = -1

    val itemsSnapshot = mutableStateListOf<Any>()
    var toolbarTitleSnapshot = mutableStateOf<String>("")

    fun onViewCreated(actorId: Int, actorName: String) {
        if (wasInit) {
            return
        }
        wasInit = true
        this.actorId = actorId
        toolbarTitleSnapshot.value = actorName
        getMovies()
    }

    fun getMovies() {
        lce.loading()
        disposables += SingleFromCallable {
            val cache = personRepository.getActorFromCache(actorId)
            Optional.ofNullable(cache)
        }.flatMap { optional ->
            if (optional.isEmpty) {
                personRepository.loadActorInfo(actorId).map { it.body }
            } else {
                Single.just(optional.get())
            }
        }.map { actor ->
            val castList = actor.movieCredits.cast?.sortedByDescending { it.releaseDate }
            if (castList.isNullOrEmpty()) {
                emptyList()
            } else {
                createDateTitles(castList)
            }
        }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ items ->
                itemsSnapshot.addAll(items)
                lce.content()
            }, { error ->
                Logger.e(error)
                lce.error(message = error.message ?: "")
            })
    }

    private fun createDateTitles(castList: List<PersonCast>): List<Any> {
        var isOtherTitleAdded = false

        val firstCast = castList[0]
        var prevDate: LocalDate = firstCast.releaseDate
            ?: return castList.map { movieFromCast(it) }

        val items = ArrayList<Any>().apply {
            add(createTitle(prevDate))
            add(movieFromCast(firstCast))
        }
        for (i in 1 until castList.size) {
            val cast = castList[i]
            val date = cast.releaseDate
            if (date == null) {
                if (isOtherTitleAdded.not()) {
                    isOtherTitleAdded = true
                    items.add(TitleItem("other"))
                }
                items.add(movieFromCast(cast))
                continue
            }
            if (date.year < prevDate.year) {
                items.add(createTitle(date))
            }
            items.add(movieFromCast(cast))
            prevDate = cast.releaseDate!!
        }
        return items
    }

    private fun movieFromCast(cast: PersonCast): MovieItem {
        return MovieItem(
            movieId = cast.id,
            title = cast.title,
            voteAverage = cast.voteAverage,
            releaseDate = cast.releaseDate,
            backdropPath = cast.backdropPath,
            posterPath = cast.posterPath,
        )
    }

    private fun createTitle(releaseDate: LocalDate) =
        TitleItem(releaseDate.year.toString())

}
package com.nicetas.movieapp3.ui.search.options.keywords.search

import androidx.collection.MutableObjectList
import androidx.collection.ObjectList
import androidx.lifecycle.MutableLiveData
import com.nicetas.database.search.SearchCompanyEntity
import com.nicetas.database.search.SearchKeywordEntity
import com.nicetas.models.extensions.isBlankF
import com.nicetas.models.extensions.isNotBlankF
import com.nicetas.models.movies.SearchPaginationList
import com.nicetas.movieapp3.base.Logger
import com.nicetas.movieapp3.base.states.Lce
import com.nicetas.movieapp3.base.viewmodel.BaseViewModel
import com.nicetas.movieapp3.base.viewmodel.LiveEvent
import com.nicetas.movieapp3.ui.search.options.keywords.search.adapter.WordHolder
import com.nicetas.repos.search.SearchRepository
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.internal.functions.Functions
import io.reactivex.rxjava3.schedulers.Schedulers
import java.util.concurrent.TimeUnit

class SearchWordViewModel(
    private val searchRepository: SearchRepository,
) : BaseViewModel() {

    class Replace(val item: Any, val position: Int)

    val scrollToTop = LiveEvent<Unit>()
    val updateList = LiveEvent<ObjectList<Any>>()
    val clearList = LiveEvent<Unit>()
    val replaceItem = LiveEvent<Replace>()
    val addKeywords = LiveEvent<ObjectList<Any>>()
    val initPaginateIfNeed = LiveEvent<Unit>()
    val setHasMoreDataToLoad = LiveEvent<Boolean>()
    val showSearchLoad = MutableLiveData<Boolean>()
    val showPaginationError = LiveEvent<Unit>()
    val initEditText = LiveEvent<CharSequence>()

    private var wasInit = false
    private var page = 1
    private var totalPages = Int.MAX_VALUE
    private val searchDisposables = CompositeDisposable()
    private val items = MutableObjectList<Any>()

    val hasLoadedAllItems: Boolean
        get() = page > totalPages
    var isLoading = false
        private set
    var paginationError: Lce.Error? = null
        private set

    private var searchText = ""
    private var screenType: @SearchWord Int = SearchWord.KEYWORDS_WITH

    fun onViewCreated(screenType: @SearchWord Int) {
        this.screenType = screenType
        initEditText.setValue(searchText)
        if (wasInit) {
            updateList.setValue(items)
            if (searchText.isNotBlankF() && searchText.length > 1) {
                initPaginateIfNeed.setValue(Unit)
            }
            return
        }
        wasInit = true
    }

    override fun onCleared() {
        searchDisposables.clear()
        super.onCleared()
        items.clear()
    }

    fun search(text: String) {
        if (text == searchText) {
            // copy-paste of the same text
            return
        }
        if (text.isBlankF() || text.length < 2) {
            // when user clear all text, there is no need to load something for "first two letters"
            searchDisposables.clear()
            setHasMoreDataToLoad.setValue(false)
            showSearchLoad.value = false
            // let search "ma" after "magic" word clearing
            searchText = text
            page = 1
            totalPages = 0
            return
        }
        searchText = text
        page = 1
        totalPages = Int.MAX_VALUE
        isLoading = true
        showSearchLoad.value = true
        searchDisposables.clear()
        searchDisposables += Single.timer(1, TimeUnit.SECONDS)
            .flatMap { searchRequest() }
            .doOnEvent { response, error ->
                // let user click on keywords during load and clear items only after response
                items.clear()
            }
            .map { mapKeywords(it) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doAfterTerminate {
                showSearchLoad.value = false
            }
            .subscribe({ newItems ->
                scrollToTop.setValue(Unit)
                updateList.setValue(newItems)
                initPaginateIfNeed.setValue(Unit)
                if (newItems.isEmpty()) {
                    setHasMoreDataToLoad.setValue(false)
                }
                isLoading = false
            }, { error ->
                Logger.e(error)
                clearList.setValue(Unit)
                initPaginateIfNeed.setValue(Unit)
                paginationError = Lce.Error(error.message ?: "")
                showPaginationError.setValue(Unit)
                isLoading = false
            })
    }

    fun loadMore() {
        isLoading = true
        searchDisposables += searchRequest()
            .map { mapKeywords(it) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ newItems ->
                addKeywords.setValue(newItems)
                if (newItems.isEmpty()) {
                    setHasMoreDataToLoad.setValue(false)
                }
                isLoading = false
            }, { error ->
                Logger.e(error)
                paginationError = Lce.Error(error.message ?: "")
                showPaginationError.setValue(Unit)
                isLoading = false
            })
    }

    private fun mapKeywords(result: SearchPaginationList): ObjectList<Any> {
        page = result.getPaginationPage() + 1
        totalPages = result.getPaginationTotalPages()
        val keywords = result.getList()

        val newItems = MutableObjectList<Any>(keywords.size)

        for (i in keywords.indices) {
            val keyword = keywords[i]
            val item = WordHolder.Item(
                id = keyword.identifier(),
                text = keyword.iName(),
                isSelected = getSearchKitItems().contains(keyword.identifier()),
            )
            newItems.add(item)
        }
        items.addAll(newItems)
        return newItems
    }

    private fun searchRequest(): Single<SearchPaginationList> {
        return when {
            SearchWord.isKeyword(screenType) -> {
                searchRepository.searchKeyword(query = searchText, page = page).map { it.body }
            }
            SearchWord.isCompany(screenType) -> {
                searchRepository.searchCompany(query = searchText, page = page).map { it.body }
            }
            else -> throw IllegalStateException()
        }
    }

    fun onKeywordClick(item: WordHolder.Item) {
        val searchKitItems = getSearchKitItems()
        val wasSelected = searchKitItems.contains(item.id)
        if (wasSelected) {
            searchKitItems.remove(item.id)
        } else {
            searchKitItems.add(item.id)
            when {
                SearchWord.isKeyword(screenType) -> saveKeywordForKits(item.id, item.text)
                SearchWord.isCompany(screenType) -> saveCompanyForKits(item.id, item.text)
            }
        }
        val index = items.indexOfFirst { it is WordHolder.Item && it.id == item.id }
        val item2 = items[index] as WordHolder.Item
        item2.isSelected = wasSelected.not()
        replaceItem.setValue(Replace(item2, index))
    }

    private fun getSearchKitItems(): HashSet<Int> {
        val searchKit = searchRepository.searchKit
        return when (screenType) {
            SearchWord.KEYWORDS_WITH -> searchKit.withKeywords
            SearchWord.KEYWORDS_WITHOUT -> searchKit.withoutKeywords
            SearchWord.COMPANIES_WITH -> searchKit.withCompanies
            SearchWord.COMPANIES_WITHOUT -> searchKit.withoutCompanies
            else -> throw IllegalStateException()
        }
    }

    private fun saveKeywordForKits(id: Int, text: String) {
        val keyword = SearchKeywordEntity(id = id, name = text)
        disposables += searchRepository.saveKeywordForKits(keyword = keyword)
            .subscribeOn(Schedulers.io())
            .subscribe(Functions.emptyConsumer()) { Logger.e(it) }
    }

    private fun saveCompanyForKits(id: Int, text: String) {
        val company = SearchCompanyEntity(id = id, name = text)
        disposables += searchRepository.saveCompanyForKits(company = company)
            .subscribeOn(Schedulers.io())
            .subscribe(Functions.emptyConsumer()) { Logger.e(it) }
    }

}
package com.nicetas.movieapp3.ui.search.options.origincountry.adapter

import android.content.Context
import android.os.Build
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.DrawableRes
import androidx.appcompat.content.res.AppCompatResources.getDrawable
import androidx.recyclerview.widget.RecyclerView
import com.nicetas.movieapp3.R
import com.nicetas.movieapp3.base.adapter.ClickDebounce
import com.nicetas.movieapp3.databinding.ItemOriginCountryBinding

class OriginCountryHolder(
    parent: ViewGroup,
    private var binding: ItemOriginCountryBinding = ItemOriginCountryBinding.inflate(
        LayoutInflater.from(parent.context),
        parent,
        false
    ),
    debounce: ClickDebounce,
    private val callback: Callback,
) : RecyclerView.ViewHolder(binding.root) {

    interface Callback {
        fun onClick(item: Item)
        fun isSelected(id: String): Boolean
    }

    class Item(
        val iso_3166_1: String,
        val text: String,
    )

    private lateinit var item: Item
    private val context: Context
        get() = itemView.context

    init {
        binding.root.setOnClickListener {
            if (debounce.canClick()) {
                callback.onClick(item)
            }
        }
    }

    fun bind(item: Item) {
        this.item = item
        binding.root.text = item.text

        val isSelected = callback.isSelected(item.iso_3166_1)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            binding.root.background = getDrawable(context, R.drawable.dialog_list_ripple)
            if (isSelected) {
                binding.root.foreground = getDrawable(context, R.drawable.dialog_list_checked)
            } else {
                binding.root.foreground = null
            }
        } else {
            @DrawableRes val resId = if (isSelected) {
                R.drawable.dialog_list_checked
            } else {
                R.drawable.dialog_list_ripple
            }
            binding.root.background = getDrawable(context, resId)
        }
    }

}
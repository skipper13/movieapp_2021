package com.nicetas.movieapp3.ui.extended.actor.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.content.res.AppCompatResources.getDrawable
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.nicetas.movieapp3.R
import com.nicetas.movieapp3.base.adapter.ClickDebounce
import com.nicetas.movieapp3.databinding.ItemActorKnownForBinding
import com.nicetas.movieapp3.ui.ImageUrl
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.time.LocalDate
import java.util.Locale

class KnownForHolder(
    parent: ViewGroup,
    inflater: LayoutInflater,
    private var binding: ItemActorKnownForBinding =
        ItemActorKnownForBinding.inflate(inflater, parent, false),
    debounce: ClickDebounce,
    callback: Callback,
) : RecyclerView.ViewHolder(binding.root) {

    fun interface Callback {
        fun onMovieClick(item: Item)
    }

    class Item(
        val movieId: Int,
        val title: String,
        val posterPath: String?,
        val backdropPath: String?,
        val releaseDate: LocalDate?,
        val voteAverage: Double?,
        val character: String?,
    )

    private lateinit var item: Item
    private val voteFormat = DecimalFormat("#.0", DecimalFormatSymbols(Locale.UK).apply {
        groupingSeparator = '.'
    })

    init {
        binding.root.setOnClickListener {
            if (debounce.canClick()) {
                callback.onMovieClick(item)
            }
        }
    }

    fun bind(item: Item) {
        this.item = item
        val context = itemView.context

        binding.tvTitle.text = item.title
        binding.tvCharacter.text = item.character

        bindReleaseDate(item.releaseDate)
        bindVoteAverage(item.voteAverage)

        Glide.with(context)
            .load(ImageUrl.w342(item.posterPath))
            .transition(DrawableTransitionOptions.withCrossFade())
            .placeholder(getDrawable(context, R.drawable.placeholder_130x200))
            .centerCrop()
            .into(binding.ivPoster)
    }

    private fun bindVoteAverage(voteAverage: Double?) {
        if (voteAverage != null && voteAverage > 0.0) {
            binding.ivVoteAverage.isVisible = true
            binding.tvVoteAverage.isVisible = true
            binding.tvVoteAverage.text = voteFormat.format(voteAverage)
        } else {
            binding.tvVoteAverage.isInvisible = true
            binding.ivVoteAverage.isInvisible = true
        }
    }

    private fun bindReleaseDate(releaseDate: LocalDate?) {
        if (releaseDate == null) {
            binding.ivReleaseDate.isVisible = false
            binding.tvReleaseDate.isVisible = false
        } else {
            binding.ivReleaseDate.isVisible = true
            binding.tvReleaseDate.isVisible = true
            binding.tvReleaseDate.text = releaseDate.year.toString()
        }
    }

}
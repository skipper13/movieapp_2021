package com.nicetas.movieapp3.ui.search.options.keywords.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.nicetas.movieapp3.base.adapter.ClickDebounce
import com.nicetas.movieapp3.databinding.ItemAddKeywordBinding

class AddKeywordHolder(
    parent: ViewGroup,
    private var binding: ItemAddKeywordBinding = ItemAddKeywordBinding.inflate(
        LayoutInflater.from(parent.context),
        parent,
        false
    ),
    debounce: ClickDebounce,
    callback: Callback,
) : RecyclerView.ViewHolder(binding.root) {

    fun interface Callback {
        fun onAddClick()
    }

    class Item()

    private lateinit var item: Item

    init {
        binding.root.setOnClickListener {
            if (debounce.canClick()) {
                callback.onAddClick()
            }
        }
    }

    fun bind(item: Item) {
        this.item = item
    }

}
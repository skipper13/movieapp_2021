package com.nicetas.movieapp3.ui.extended.images

import android.view.View
import androidx.collection.MutableObjectList
import androidx.collection.ObjectList
import androidx.lifecycle.MutableLiveData
import com.nicetas.movieapp3.base.Logger
import com.nicetas.movieapp3.base.viewmodel.BaseViewModel
import com.nicetas.movieapp3.ui.extended.images.adapter.MovieImagesHolder
import com.nicetas.repos.movie.MovieRepository
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.internal.operators.single.SingleFromCallable
import io.reactivex.rxjava3.schedulers.Schedulers
import java.util.Optional

class MovieImagesViewModel(
    private val movieRepository: MovieRepository,
) : BaseViewModel() {

    val onGetImages = MutableLiveData<ObjectList<Any>>()

    private var wasInit = false
    val backdrops = arrayListOf<String>()
    var recyclerViewId = View.generateViewId()

    fun onViewCreated(movieId: Int) {
        if (wasInit) {
            return
        }
        wasInit = true
        loadImages(movieId)
    }

    override fun onCleared() {
        super.onCleared()
        backdrops.clear()
    }

    fun loadImages(movieId: Int) {
        lce.loading()
        disposables += SingleFromCallable {
            val cache = movieRepository.getMovieImagesFromCache(movieId)
            Optional.ofNullable(cache)
        }.flatMap { optional ->
            if (optional.isEmpty) {
                movieRepository.loadMovieImages(movieId).map { it.body }
            } else {
                Single.just(optional.get())
            }
        }.map { images ->
            val backdrops = images.backdrops ?: emptyList()

            this@MovieImagesViewModel.backdrops.apply {
                clear()
                addAll(backdrops.map { it.filePath })
            }

            val items = MutableObjectList<Any>(backdrops.size)
            for (i in backdrops.indices) {
                items += MovieImagesHolder.Item(filePath = backdrops[i].filePath)
            }
            items
        }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ items ->
                onGetImages.value = items
                lce.content()
            }, { error ->
                Logger.e(error)
                lce.error(message = error.message ?: "")
            })
    }

}
package com.nicetas.movieapp3.ui.search.options.kit.adapter

import android.content.Context
import android.os.Build
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.DrawableRes
import androidx.appcompat.content.res.AppCompatResources.getDrawable
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.nicetas.database.search.SearchKitEntity
import com.nicetas.movieapp3.R
import com.nicetas.movieapp3.base.adapter.ClickDebounce
import com.nicetas.movieapp3.databinding.ItemSearchKitBinding
import com.nicetas.movieapp3.ui.search.options.kit.create.adapter.KitIcon

class SearchKitHolder(
    parent: ViewGroup,
    private var binding: ItemSearchKitBinding = ItemSearchKitBinding.inflate(
        LayoutInflater.from(parent.context),
        parent,
        false
    ),
    debounce: ClickDebounce,
    callback: Callback,
) : RecyclerView.ViewHolder(binding.root) {

    interface Callback {
        fun onKitClick(id: Int)
        fun deleteKit(item: Item)
    }

    class Item(
        val id: Int,
        val iconId: Int,
        val text: CharSequence,
        val isSelected: Boolean,
    )

    private lateinit var item: Item
    private val context: Context
        get() = itemView.context

    init {
        binding.root.setOnClickListener {
            if (debounce.canClick()) {
                callback.onKitClick(item.id)
            }
        }
        binding.ivDelete.setOnClickListener {
            if (debounce.canClick()) {
                callback.deleteKit(item)
            }
        }
    }

    fun bind(item: Item) {
        this.item = item
        binding.tvTitle.text = when (item.id) {
            SearchKitEntity.ID_POPULAR -> context.getString(R.string.search_kits_popular)
            SearchKitEntity.ID_TOP -> context.getString(R.string.search_kits_top)
            SearchKitEntity.ID_UPCOMING -> context.getString(R.string.search_kits_upcoming)
            else -> item.text
        }

        val icon = getDrawable(context, KitIcon.getIconById(item.iconId))
        binding.ivIcon.setImageDrawable(icon)

        binding.ivDelete.isVisible = item.id != SearchKitEntity.ID_POPULAR
                && item.id != SearchKitEntity.ID_TOP
                && item.id != SearchKitEntity.ID_UPCOMING

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            binding.root.background = getDrawable(context, R.drawable.dialog_list_ripple)
            if (item.isSelected) {
                binding.root.foreground = getDrawable(context, R.drawable.dialog_list_checked)
            } else {
                binding.root.foreground = null
            }
        } else {
            @DrawableRes val resId = if (item.isSelected) {
                R.drawable.dialog_list_checked
            } else {
                R.drawable.dialog_list_ripple
            }
            binding.root.background = getDrawable(context, resId)
        }
    }
}
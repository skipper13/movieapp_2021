package com.nicetas.movieapp3.ui.search.options.kit.create.adapter

import androidx.annotation.DrawableRes
import com.nicetas.database.search.SearchKitEntity
import com.nicetas.movieapp3.R

object KitIcon {

    fun getIconsCount() = 28

    @DrawableRes fun getIconById(id: Int): Int {
        return when (id) {
            SearchKitEntity.IconId.POPULAR -> R.drawable.ic_kit_0_popular
            SearchKitEntity.IconId.TOP -> R.drawable.ic_kit_1_top
            SearchKitEntity.IconId.ANIMATION -> R.drawable.ic_kit_2_animation
            3 -> R.drawable.ic_kit_3
            4 -> R.drawable.ic_kit_4
            5 -> R.drawable.ic_kit_5
            6 -> R.drawable.ic_kit_6
            7 -> R.drawable.ic_kit_7
            8 -> R.drawable.ic_kit_8
            9 -> R.drawable.ic_kit_9
            10 -> R.drawable.ic_kit_10
            11 -> R.drawable.ic_kit_11
            12 -> R.drawable.ic_kit_12
            13 -> R.drawable.ic_kit_13
            14 -> R.drawable.ic_kit_14
            15 -> R.drawable.ic_kit_15
            16 -> R.drawable.ic_kit_16
            17 -> R.drawable.ic_kit_17
            SearchKitEntity.IconId.UPCOMING -> R.drawable.ic_kit_18_upcoming
            19 -> R.drawable.ic_kit_19
            20 -> R.drawable.ic_kit_20
            21 -> R.drawable.ic_kit_21
            22 -> R.drawable.ic_kit_22
            23 -> R.drawable.ic_kit_23
            24 -> R.drawable.ic_kit_24
            25 -> R.drawable.ic_kit_25
            26 -> R.drawable.ic_kit_26
            27 -> R.drawable.ic_kit_27
            else -> R.drawable.ic_kit_1_top
        }
    }

}
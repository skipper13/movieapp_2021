package com.nicetas.movieapp3.ui.search.options.keywords.adapter

import android.view.ViewGroup
import androidx.collection.ObjectList
import androidx.recyclerview.widget.RecyclerView
import com.nicetas.movieapp3.base.adapter.BaseDiffUtilCallback
import com.nicetas.movieapp3.base.adapter.ClickDebounce
import com.nicetas.movieapp3.base.adapter.AdapterItems

class KeywordsAdapter(
    private val debounce: ClickDebounce,
    private val onDeleteClick: KeywordHolder.Callback,
    private val onAddClick: AddKeywordHolder.Callback,
    private val onSetupClick: SetupKeywordsHolder.Callback,
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        private const val TYPE_KEYWORD = 0
        private const val TYPE_ADD_KEYWORD_SMALL = 1
        private const val TYPE_ADD_KEYWORD = 2
        private const val TYPE_SETUP_KEYWORDS = 3
    }

    class DiffUtilCallback() : BaseDiffUtilCallback<Any>() {

        override fun areItemsTheSame(oldItem: Any, newItem: Any): Boolean {
            if (oldItem is AddKeywordSmallHolder.Item && newItem is AddKeywordSmallHolder.Item) {
                return true
            }
            if (oldItem is AddKeywordHolder.Item && newItem is AddKeywordHolder.Item) {
                return true
            }
            if (oldItem is SetupKeywordsHolder.Item && newItem is SetupKeywordsHolder.Item) {
                return true
            }
            if (oldItem is KeywordHolder.Item && newItem is KeywordHolder.Item) {
                return oldItem.id == newItem.id
            }
            return false
        }

        override fun areContentsTheSame(oldItem: Any, newItem: Any): Boolean {
            if (oldItem is AddKeywordSmallHolder.Item && newItem is AddKeywordSmallHolder.Item) {
                return true
            }
            if (oldItem is AddKeywordHolder.Item && newItem is AddKeywordHolder.Item) {
                return true
            }
            if (oldItem is SetupKeywordsHolder.Item && newItem is SetupKeywordsHolder.Item) {
                return true
            }
            if (oldItem is KeywordHolder.Item && newItem is KeywordHolder.Item) {
                return oldItem.text == newItem.text
            }
            return false
        }
    }

    private val diffUtilCallback = DiffUtilCallback()
    val items = AdapterItems<Any>(this)

    fun update(newItems: ObjectList<Any>) {
        if (items.list.isEmpty()) {
            items.list.addAll(newItems)
            // noinspection NotifyDataSetChanged
            notifyDataSetChanged()
        } else {
            items.update(newItems, diffUtilCallback)
        }
    }

    fun removeById(id: Int) {
        val indexWord = items.list.indexOfFirst { item ->
            item is KeywordHolder.Item && item.id == id
        }
        if (indexWord == -1) {
            return
        }
        items.remove(indexWord)

        if (items.list.firstOrNull { it is KeywordHolder.Item } == null) {
            val indexAdd = items.list.indexOfFirst { it is AddKeywordSmallHolder.Item }
            if (indexAdd != -1) {
                items.remove(indexAdd)
            }
        }
    }

    override fun getItemCount(): Int = items.list.size

    override fun getItemViewType(position: Int): Int {
        return when (items.list[position]) {
            is KeywordHolder.Item -> TYPE_KEYWORD
            is AddKeywordSmallHolder.Item -> TYPE_ADD_KEYWORD_SMALL
            is AddKeywordHolder.Item -> TYPE_ADD_KEYWORD
            is SetupKeywordsHolder.Item -> TYPE_SETUP_KEYWORDS
            else -> throw IllegalStateException("item is null for position = $position")
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_KEYWORD -> KeywordHolder(
                parent = parent,
                debounce = debounce,
                callback = onDeleteClick,
            )
            TYPE_ADD_KEYWORD_SMALL -> AddKeywordSmallHolder(
                parent = parent,
                debounce = debounce,
                callback = { onAddClick.onAddClick() },
            )
            TYPE_ADD_KEYWORD -> AddKeywordHolder(
                parent = parent,
                debounce = debounce,
                callback = onAddClick,
            )
            TYPE_SETUP_KEYWORDS -> SetupKeywordsHolder(
                parent = parent,
                debounce = debounce,
                callback = onSetupClick,
            )
            else -> throw IllegalStateException("Unknown holder type")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = items.list[position]
        when (holder) {
            is KeywordHolder -> holder.bind(item as KeywordHolder.Item)
            is AddKeywordHolder -> holder.bind(item as AddKeywordHolder.Item)
            is AddKeywordSmallHolder -> holder.bind(item as AddKeywordSmallHolder.Item)
            is SetupKeywordsHolder -> holder.bind(item as SetupKeywordsHolder.Item)
        }
    }

}
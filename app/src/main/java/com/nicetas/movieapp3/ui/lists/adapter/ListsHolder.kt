package com.nicetas.movieapp3.ui.lists.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.nicetas.movieapp3.R
import com.nicetas.movieapp3.base.adapter.ClickDebounce
import com.nicetas.movieapp3.base.extensions.share
import com.nicetas.movieapp3.databinding.ItemListsBinding
import com.nicetas.movieapp3.ui.ImageUrl

class ListsHolder(
    parent: ViewGroup,
    private var binding: ItemListsBinding = ItemListsBinding.inflate(
        LayoutInflater.from(parent.context),
        parent,
        false
    ),
    debounce: ClickDebounce,
    callback: Callback,
) : RecyclerView.ViewHolder(binding.root) {

    class DifHelper {
        fun areItemsTheSame(oldItem: Item, newItem: Item): Boolean {
            return oldItem.listId == newItem.listId
        }

        fun areContentsTheSame(oldItem: Item, newItem: Item): Boolean {
            return oldItem.name == newItem.name
                    && oldItem.itemCount == newItem.itemCount
                    && oldItem.backdropPath == newItem.backdropPath
        }
    }

    fun interface Callback {
        fun onListClick(listId: Int, name: String)
    }

    class Item(
        val listId: Int,
        val name: String,
        val itemCount: Int,
        val backdropPath: String?,
    )

    private lateinit var item: Item

    init {
        binding.root.setOnClickListener {
            if (debounce.canClick()) {
                callback.onListClick(listId = item.listId, name = item.name)
            }
        }
        binding.ivShare.setOnClickListener {
            if (debounce.canClick()) {
                binding.root.context?.share("https://www.themoviedb.org/list/${item.listId}")
            }
        }
    }

    fun bind(item: Item) {
        this.item = item
        binding.tvTitle.text = item.name
        binding.tvItemCount.text = item.itemCount.toString()

        Glide.with(binding.root.context)
            .load(ImageUrl.w500(item.backdropPath))
            .transition(DrawableTransitionOptions.withCrossFade())
            .placeholder(R.drawable.movie_image_placeholder)
            .centerCrop()
            .into(binding.ivPoster)
    }

}
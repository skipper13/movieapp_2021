package com.nicetas.movieapp3.ui.lists.list.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.nicetas.movieapp3.base.adapter.ClickDebounce
import com.nicetas.movieapp3.base.adapter.AdapterItems
import com.nicetas.movieapp3.ui.search.adapter.MovieHolder

class ListAdapter(
    private var inflater: LayoutInflater?,
    private val movieCallback: MovieHolder.Callback
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        private const val TYPE_MOVIE = 0
    }

    private val voteFormatter = MovieHolder.VoteFormatter()
    private val debounce = ClickDebounce()
    val items = AdapterItems<Any>(this)

    fun onDestroyView() {
        items.list.clear()
        inflater = null
    }

    fun removeMovieFromWatchlist(movieId: Int) {
        for (i in 0 until items.list.size) {
            val item = items.list[i]
            if (item is MovieHolder.Item && item.movieId == movieId) {
                item.isInWatchlist = false
                items.replace(item, i, -1)
                break
            }
        }
    }

    override fun getItemCount(): Int = items.list.size

    override fun getItemViewType(position: Int): Int {
        return when (items.list[position]) {
            is MovieHolder.Item -> TYPE_MOVIE
            else -> throw IllegalStateException("item is null for position = $position")
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_MOVIE -> MovieHolder(
                parent = parent,
                inflater = inflater ?: /*cannot happen*/ LayoutInflater.from(parent.context),
                debounce = debounce,
                callback = movieCallback,
                voteFormatter = voteFormatter,
            )
            else -> throw IllegalStateException("Unknown holder type")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = items.list[position]
        when (holder) {
            is MovieHolder -> holder.bind(item as MovieHolder.Item)
        }
    }

}


package com.nicetas.movieapp3.ui.extended.info.links

import android.content.Context
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.nicetas.models.extensions.isNotNullOrBlankF
import com.nicetas.models.movies.Movie
import com.nicetas.movieapp3.R
import com.nicetas.movieapp3.base.adapter.ClickDebounce
import com.nicetas.movieapp3.base.adapter.AdapterItems

class LinksAdapter() : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        private const val TYPE_LINK = 0
    }

    private val debounce = ClickDebounce()
    val items = AdapterItems<Any>(this)

    fun fill(
        context: Context,
        movieId: Int,
        imdbId: String?,
        ids: Movie.ExternalIds?,
    ) {
        val tmdbUrl = "https://www.themoviedb.org/movie/$movieId"
        val tmdbItem = LinkHolder.Item(
            text = context.getString(R.string.movie_info_link_tmdb),
            link = tmdbUrl,
        )
        items.add(tmdbItem)

        if (ids?.imdbId.isNotNullOrBlankF() || imdbId.isNotNullOrBlankF()) {
            val id = ids?.imdbId ?: imdbId
            val imdbUrl = "https://www.imdb.com/title/$id"
            val imdbItem = LinkHolder.Item(
                text = context.getString(R.string.movie_info_link_imdb),
                link = imdbUrl,
            )
            items.add(imdbItem)
        }
        if (ids?.instagramId.isNotNullOrBlankF()) {
            val instagramUrl = "https://www.instagram.com/${ids!!.instagramId}"
            val instagramItem = LinkHolder.Item(
                text = context.getString(R.string.movie_info_link_instagram),
                link = instagramUrl,
            )
            items.add(instagramItem)
        }
        if (ids?.facebookId.isNotNullOrBlankF()) {
            val facebookUrl = "https://www.facebook.com/${ids!!.facebookId}"
            val facebookItem = LinkHolder.Item(
                text = context.getString(R.string.movie_info_link_facebook),
                link = facebookUrl,
            )
            items.add(facebookItem)
        }
        if (ids?.twitterId.isNotNullOrBlankF()) {
            val twitterUrl = "https://www.twitter.com/${ids!!.twitterId}"
            val twitterItem = LinkHolder.Item(
                text = context.getString(R.string.movie_info_link_twitter),
                link = twitterUrl,
            )
            items.add(twitterItem)
        }
    }

    override fun getItemCount(): Int = items.list.size

    override fun getItemViewType(position: Int): Int {
        return when (items.list[position]) {
            is LinkHolder.Item -> TYPE_LINK
            else -> throw IllegalStateException("item is null for position = $position")
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_LINK -> LinkHolder(
                parent = parent,
                debounce = debounce,
            )
            else -> throw IllegalStateException("Unknown holder type")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = items.list[position]
        when (holder) {
            is LinkHolder -> holder.bind(item as LinkHolder.Item)
        }
    }

}
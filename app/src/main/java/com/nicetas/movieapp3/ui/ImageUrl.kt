package com.nicetas.movieapp3.ui

//    https://developer.themoviedb.org/reference/configuration-details
//    https://api.themoviedb.org/3/configuration?api_key=

object ImageUrl {

    const val w185 = "w185"
    const val w342 = "w342"
    const val w500 = "w500"
    const val w780 = "w780"
    const val w1280 = "w1280"
    const val original = "original"

    var baseUrl = "https://image.tmdb.org/t/p/"
        private set
    var isFromConfig = false
        private set

    fun init(baseUrl: String) {
        isFromConfig = true
        this.baseUrl = baseUrl
    }

    fun w185(path: String?): String? {
        if (path == null) return null
        return "${baseUrl}w185${path}"
    }

    fun w342(path: String?): String? {
        if (path == null) return null
        return "${baseUrl}w342${path}"  // ~ 20 kb
    }

    fun w500(path: String?): String? {
        if (path == null) return null
        return "${baseUrl}w500${path}"  // ~ 40 kb
    }

    fun w780(path: String?): String? {
        if (path == null) return null
        return "${baseUrl}w780${path}"  // ~ 86 kb
    }

    fun w1280(path: String?): String? {
        if (path == null) return null
        return "${baseUrl}w1280${path}"  // ~ 200 kb
    }

    fun original(path: String?): String? {
        if (path == null) return null
        return "${baseUrl}original${path}"  // ~ 200 kb
    }

}
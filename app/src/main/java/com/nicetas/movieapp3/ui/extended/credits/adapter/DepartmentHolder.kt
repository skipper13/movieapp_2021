package com.nicetas.movieapp3.ui.extended.credits.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.nicetas.movieapp3.base.adapter.ClickDebounce
import com.nicetas.movieapp3.databinding.ItemDepartmentBinding

class DepartmentHolder(
    parent: ViewGroup,
    inflater: LayoutInflater,
    private var binding: ItemDepartmentBinding =
        ItemDepartmentBinding.inflate(inflater, parent, false),
    debounce: ClickDebounce,
    callback: Callback,
) : RecyclerView.ViewHolder(binding.root) {

    fun interface Callback {
        fun onDepartmentClick(department: String)
    }

    class Item(
        val isExpanded: Boolean,
        val department: String,
    )

    init {
        binding.root.setOnClickListener {
            if (debounce.canClick()) {
                callback.onDepartmentClick(department = item.department)
            }
        }
    }

    private lateinit var item: Item

    fun bind(item: Item) {
        this.item = item
        binding.tvTitle.text = item.department
        binding.ivArrow.rotation = if (item.isExpanded) 180F else 0F
    }

}
package com.nicetas.movieapp3.ui.lists.list

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup.MarginLayoutParams
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat.getColor
import androidx.core.view.MenuProvider
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.updateLayoutParams
import androidx.fragment.app.setFragmentResultListener
import androidx.lifecycle.Lifecycle
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.nicetas.models.search.MediaType
import com.nicetas.movieapp3.R
import com.nicetas.movieapp3.base.adapter.GridSpacingItemDecoration
import com.nicetas.movieapp3.base.MoviesSpanCount
import com.nicetas.movieapp3.base.adapter.ScrollOnTapHelper
import com.nicetas.movieapp3.base.adapter.pagination.Paginate
import com.nicetas.movieapp3.base.extensions.dp
import com.nicetas.movieapp3.base.extensions.getIdFromDeeplink
import com.nicetas.movieapp3.base.fragment.BaseFragment
import com.nicetas.movieapp3.base.states.ViewState
import com.nicetas.movieapp3.databinding.FragmentListBinding
import com.nicetas.movieapp3.ui.extended.tabs.MovieTabsFragment
import com.nicetas.movieapp3.ui.lists.list.adapter.ListAdapter
import com.nicetas.movieapp3.ui.search.adapter.MovieHolder
import com.nicetas.movieapp3.ui.tv.TvInfoFragment
import org.koin.androidx.viewmodel.ext.android.viewModel

class ListFragment : BaseFragment(R.layout.fragment_list) {

    companion object {
        private const val ARG_LIST_ID_DEEPLINK = "ARG_LIST_ID_DEEPLINK"
        private const val ARG_LIST_ID = "ARG_LIST_ID"
        private const val ARG_LIST_NAME = "ARG_LIST_NAME"

        fun args(
            listId: Int,
            name: String,
        ) = Bundle(2).apply {
            putInt(ARG_LIST_ID, listId)
            putString(ARG_LIST_NAME, name)
        }
    }

    private var _binding: FragmentListBinding? = null
    private val binding
        get() = _binding!!

    private val listId: Int by lazy {
        val args = requireArguments()
        if (args.containsKey(ARG_LIST_ID)) {
            args.getInt(ARG_LIST_ID)
        } else {
            args.getString(ARG_LIST_ID_DEEPLINK, "0").getIdFromDeeplink()
        }
    }
    private val listName: String by lazy { requireArguments().getString(ARG_LIST_NAME, "") }

    private val viewModel: ListViewModel by viewModel()

    private var adapter: ListAdapter? = null
    private var paginate: Paginate? = null
    private var scrollOnTapHelper: ScrollOnTapHelper? = null

    private val paginateCallbacks = object : Paginate.Callbacks {
        override fun onLoadMore() = viewModel.loadMoreMovies(isFullReload = false)
        override fun isLoading() = viewModel.isLoading
        override fun hasLoadedAllItems() = viewModel.hasLoadedAllItems
    }

    private var menuItemAdd: MenuItem? = null
    private var menuItemRemove: MenuItem? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setFragmentResultListener(ListSortDialog.FRAGMENT_RESULT_KEY) { requestKey, bundle ->
            val sort = ListSortDialog.sortBy(bundle)
            viewModel.changeSort(sort)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentListBinding.bind(view)
        scrollOnTapHelper = ScrollOnTapHelper(savedInstanceState)
        setupOptionsMenu()
        initViewState()
        setupViews()
        initRecyclerView()
        setupObservers()
        viewModel.onViewCreated(listId)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        scrollOnTapHelper?.onSaveInstanceState(outState)
    }

    override fun onDestroyView() {
        paginate?.unbind()
        paginate = null
        adapter?.onDestroyView()
        adapter = null
        binding.rvContent.adapter = null
        scrollOnTapHelper = null
        super.onDestroyView()
        _binding = null
    }

    private fun setupOptionsMenu() {
        requireActivity().addMenuProvider(object : MenuProvider {
            override fun onCreateMenu(menu: Menu, menuInflater: MenuInflater) {
                menuInflater.inflate(R.menu.menu_list, menu)

                val isInit = viewModel.isInLists.isInitialized

                menuItemAdd = menu.findItem(R.id.action_add_to_bookmarks).apply {
                    if (isInit) {
                        isVisible = viewModel.isInLists.value!!.not()
                    } else {
                        isVisible = false
                    }
                }
                menuItemRemove = menu.findItem(R.id.action_remove_from_bookmarks).apply {
                    if (isInit) {
                        isVisible = viewModel.isInLists.value!!
                    } else {
                        isVisible = false
                    }
                }
            }

            override fun onMenuItemSelected(menuItem: MenuItem): Boolean {
                return when (menuItem.itemId) {
                    R.id.action_sort_type -> {
                        val dialog = ListSortDialog.newInstance(sortBy = viewModel.sortBy)
                        dialog.show(parentFragmentManager, null)
                        true
                    }
                    R.id.action_add_to_bookmarks -> {
                        viewModel.addToLists()
                        true
                    }
                    R.id.action_remove_from_bookmarks -> {
                        viewModel.removeFromLists()
                        true
                    }
                    else -> false
                }
            }
        }, viewLifecycleOwner, Lifecycle.State.RESUMED)
    }

    private fun initViewState() {
        ViewState.Builder(binding.rvContent)
            .load()
            .error(onClick = { viewModel.reloadMovies() })
            .empty(binding.tvEmptyState)
            .build()
            .observeLce(viewModel.lce, viewLifecycleOwner) { lceState, viewState ->
                when (lceState) {
                    is ListViewModel.LceIsPrivate -> viewState.error(getString(R.string.list_is_private))
                }
            }
    }

    private fun setupViews() {
        setToolbarTitle(listName)
        setStatusBarColor(getColor(requireContext(), R.color.colorPrimary))

        ViewCompat.setOnApplyWindowInsetsListener(binding.root) { _, insets ->
            val statusBarHeight = insets.getInsets(WindowInsetsCompat.Type.statusBars()).top
            binding.appBar.updateLayoutParams<MarginLayoutParams> {
                topMargin = statusBarHeight
            }
            binding.rvContent.updateLayoutParams<MarginLayoutParams> {
                bottomMargin = statusBarHeight
            }
            insets
        }
        binding.toolbar.setOnClickListener {
            scrollOnTapHelper?.scroll(binding.rvContent)
        }
    }

    private fun initRecyclerView() {
        adapter = ListAdapter(
            inflater = layoutInflater,
            movieCallback = object : MovieHolder.Callback {
                override val isMultiWindow: Boolean
                    get() = isInMultiWindowMode

                override fun onMovieClick(movie: MovieHolder.Item) {
                    if (movie !is ListViewModel.MovieItem) {
                        return
                    }
                    when (movie.mediaType) {
                        MediaType.MOVIE -> {
                            val args = MovieTabsFragment.args(
                                movieId = movie.movieId,
                                title = movie.title,
                                backdropPath = movie.backdropPath,
                                posterPath = movie.posterPath,
                                releaseDate = movie.releaseDate,
                                voteAverage = movie.voteAverage,
                            )
                            findNavController().navigate(R.id.movieTabs, args)
                        }
                        MediaType.TV -> {
                            val args = TvInfoFragment.args(
                                tvId = movie.movieId,
                                name = movie.title,
                                originalName = movie.originalTitle,
                                posterPath = movie.posterPath,
                                firstAirDate = movie.releaseDate,
                                voteAverage = movie.voteAverage ?: 0.0,
                                popularity = movie.popularity ?: 0.0,
                                voteCount = movie.voteCount ?: 0,
                                overview = movie.overview,
                            )
                            findNavController().navigate(R.id.tvInfoScreen, args)
                        }
                        else -> Unit
                    }
                }

                override fun removeFromBookmarks(movie: MovieHolder.Item) {
                    AlertDialog.Builder(requireContext())
                        .setTitle(R.string.search_remove_from_bookmarks)
                        .setMessage(movie.title)
                        .setPositiveButton(android.R.string.ok) { _, _ ->
                            viewModel.removeFromWatchList(movie.movieId)
                        }
                        .setNegativeButton(android.R.string.cancel, null)
                        .show()
                }
            }
        )

        binding.rvContent.adapter = adapter

        val spanCount = MoviesSpanCount().get(requireActivity(), false)
        binding.rvContent.layoutManager = GridLayoutManager(requireContext(), spanCount)
        binding.rvContent.addItemDecoration(GridSpacingItemDecoration(spanCount, 4f.dp, false))
    }

    private fun initPaginate() {
        if (paginate == null) {
            paginate = Paginate.with(binding.rvContent, paginateCallbacks)
                .setLoadingTriggerThreshold(3)
                .addLoadingListItem(true)
                .onRetryClick { viewModel.loadMoreMovies(isFullReload = false) }
                .errorText { viewModel.paginationError?.message(requireContext()) ?: "" }
                .build()
        }
    }

    private fun setupObservers() {
        viewModel.onGetMovies.observe(viewLifecycleOwner) { result ->
            if (result.isFullReload) {
                adapter?.items?.replaceAll(result.items)

                binding.rvContent.scrollToPosition(0)

                scrollOnTapHelper?.savedBottomPos = 0
                binding.appBar.setExpanded(true, false)
            } else {
                adapter?.items?.addAll(result.items)
            }
            initPaginate()
        }
        viewModel.onMovieFromWatchlistRemoved.observe(viewLifecycleOwner) { movieId ->
            adapter?.removeMovieFromWatchlist(movieId)
        }
        viewModel.isInLists.observe(viewLifecycleOwner) { isInLists ->
            menuItemAdd?.isVisible = isInLists.not()
            menuItemRemove?.isVisible = isInLists
        }
        viewModel.setToolbarTitle.observe(viewLifecycleOwner) { title ->
            setToolbarTitle(title)
        }
        viewModel.showPaginationError.observe(viewLifecycleOwner) {
            paginate?.setError()
        }
    }

}
package com.nicetas.movieapp3.ui.extended.videos

import android.view.View
import androidx.collection.MutableObjectList
import androidx.collection.MutableScatterSet
import androidx.collection.ObjectList
import androidx.lifecycle.MutableLiveData
import com.nicetas.movieapp3.base.Logger
import com.nicetas.movieapp3.base.viewmodel.BaseViewModel
import com.nicetas.movieapp3.ui.extended.videos.adapter.MovieVideosHolder
import com.nicetas.movieapp3.ui.extended.videos.adapter.VideosHeaderHolder
import com.nicetas.repos.movie.MovieRepository
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.internal.operators.single.SingleFromCallable
import io.reactivex.rxjava3.schedulers.Schedulers
import java.util.Optional

class MovieVideosViewModel(
    private val movieRepository: MovieRepository,
) : BaseViewModel() {

    val updateItems = MutableLiveData<ObjectList<Any>>()

    private var wasInit = false
    private var movieId = 0
    private val collapsedHeaders = MutableScatterSet<String>()
    var recyclerViewId = View.generateViewId()

    fun onViewCreated(movieId: Int) {
        if (wasInit) {
            return
        }
        wasInit = true
        this.movieId = movieId
        lce.loading()
        loadVideos()
    }

    override fun onCleared() {
        super.onCleared()
        collapsedHeaders.clear()
    }

    fun reload() {
        lce.loading()
        loadVideos()
    }

    private fun loadVideos() {
        disposables += SingleFromCallable {
            val cache = movieRepository.getMovieVideosFromCache(movieId)
            Optional.ofNullable(cache)
        }.flatMap { optional ->
            if (optional.isEmpty) {
                movieRepository.loadMovieVideos(movieId).map { it.body }
            } else {
                Single.just(optional.get())
            }
        }.map { movieVideos ->

            val items = MutableObjectList<Any>()

            val videos = movieVideos.results.sortedBy { it.type.order }

            var videoType = ""
            for (i in videos.indices) {
                val video = videos[i]
                val isCollapsed = collapsedHeaders.contains(video.type.value)

                if (video.type.value != videoType) {
                    videoType = video.type.value

                    val header = VideosHeaderHolder.Item(
                        isExpanded = isCollapsed.not(),
                        type = videoType,
                    )
                    items.add(header)
                }
                if (isCollapsed.not()) {
                    val videoItem = MovieVideosHolder.Item(id = video.id, youTubeKey = video.key)
                    items.add(videoItem)
                }
            }

            items
        }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ items ->
                updateItems.value = items
                lce.content()
            }, { error ->
                Logger.e(error)
                lce.error(message = error.message ?: "")
            })
    }

    fun onVideosHeaderClick(type: String) {
        if (collapsedHeaders.remove(type).not()) {
            collapsedHeaders.add(type)
        }
        loadVideos()
    }

}
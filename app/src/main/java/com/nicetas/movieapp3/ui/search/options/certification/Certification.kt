package com.nicetas.movieapp3.ui.search.options.certification

import androidx.collection.ObjectList
import androidx.collection.objectListOf
import com.nicetas.movieapp3.R
import com.nicetas.movieapp3.ui.search.options.certification.adapter.ClassificationHolder

interface Certification {

    companion object {
        fun getById(countryId: String): Certification {
            return when (countryId) {
                Germany.getId() -> Germany
                UnitedStates.getId() -> UnitedStates
                Australia.getId() -> Australia
                else -> default
            }
        }

        val default: Certification
            get() = Germany
    }

    fun getId(): String
    fun sortedServerNames(): ObjectList<String>
    fun getClassifications(): ObjectList<ClassificationHolder.Item>


    // DE:  0|6|12|16|18
    object Germany : Certification {

        private const val G0 = "0"
        private const val G6 = "6"
        private const val G12 = "12"
        private const val G16 = "16"
        private const val G18 = "18"

        override fun getId(): String = "DE"

        override fun sortedServerNames(): ObjectList<String> {
            return objectListOf(G0, G6, G12, G16, G18)
        }

        override fun getClassifications(): ObjectList<ClassificationHolder.Item> {
            val g0 = ClassificationHolder.Item(
                serverName = G0,
                descriptionRes = R.string.certification_dialog_germany_0,
                iconRes = R.drawable.classification_german_0,
            )
            val g6 = ClassificationHolder.Item(
                serverName = G6,
                descriptionRes = R.string.certification_dialog_germany_6,
                iconRes = R.drawable.classification_german_6,
            )
            val g12 = ClassificationHolder.Item(
                serverName = G12,
                descriptionRes = R.string.certification_dialog_germany_12,
                iconRes = R.drawable.classification_german_12,
            )
            val g16 = ClassificationHolder.Item(
                serverName = G16,
                descriptionRes = R.string.certification_dialog_germany_16,
                iconRes = R.drawable.classification_german_16,
            )
            val g18 = ClassificationHolder.Item(
                serverName = G18,
                descriptionRes = R.string.certification_dialog_germany_18,
                iconRes = R.drawable.classification_german_18,
            )
            return objectListOf(g0, g6, g12, g16, g18)
        }
    }

    // US:  NR|G|PG|PG-13|R|NC-17
    object UnitedStates : Certification {

        private const val NR = "NR"
        private const val G = "G"
        private const val PG = "PG"
        private const val PG13 = "PG-13"
        private const val R_ = "R"
        private const val NC17 = "NC-17"

        override fun getId(): String = "US"

        override fun sortedServerNames(): ObjectList<String> {
            return objectListOf(NR, G, PG, PG13, R_, NC17)
        }

        override fun getClassifications(): ObjectList<ClassificationHolder.Item> {
            val nr = ClassificationHolder.Item(
                serverName = NR,
                descriptionRes = R.string.certification_dialog_usa_nr,
                iconRes = R.drawable.classification_usa_not_rated,
            )
            val g = ClassificationHolder.Item(
                serverName = G,
                descriptionRes = R.string.certification_dialog_usa_g,
                iconRes = R.drawable.classification_usa_general_g,
            )
            val pg = ClassificationHolder.Item(
                serverName = PG,
                descriptionRes = R.string.certification_dialog_usa_pg,
                iconRes = R.drawable.classification_usa_parental_guidance_pg,
            )
            val pg13 = ClassificationHolder.Item(
                serverName = PG13,
                descriptionRes = R.string.certification_dialog_usa_pg13,
                iconRes = R.drawable.classification_usa_parental_guidance_pg_13,
            )
            val r = ClassificationHolder.Item(
                serverName = R_,
                descriptionRes = R.string.certification_dialog_usa_r,
                iconRes = R.drawable.classification_usa_restricted_r,
            )
            val nc17 = ClassificationHolder.Item(
                serverName = NC17,
                descriptionRes = R.string.certification_dialog_usa_nc17,
                iconRes = R.drawable.classification_usa_nc_17,
            )
            return objectListOf(nr, g, pg, pg13, r, nc17)
        }
    }

    // AU:  E|G|PG|M|MA15+|R18+|X18+|RC
    object Australia : Certification {

        private const val E = "E"
        private const val G = "G"
        private const val PG = "PG"
        private const val M = "M"
        private const val MA15 = "MA15+"
        private const val R18 = "R18+"
        private const val X18 = "X18+"
        private const val RC = "RC"

        override fun getId(): String = "AU"

        override fun sortedServerNames(): ObjectList<String> {
            return objectListOf(E, G, PG, M, MA15, R18, X18, RC)
        }

        override fun getClassifications(): ObjectList<ClassificationHolder.Item> {
            val e = ClassificationHolder.Item(
                serverName = E,
                descriptionRes = R.string.certification_dialog_australia_e,
                iconRes = R.drawable.classification_australian_exempt_e,
            )
            val g = ClassificationHolder.Item(
                serverName = G,
                descriptionRes = R.string.certification_dialog_australia_g,
                iconRes = R.drawable.classification_australian_general_g,
            )
            val pg = ClassificationHolder.Item(
                serverName = PG,
                descriptionRes = R.string.certification_dialog_australia_pg,
                iconRes = R.drawable.classification_australian_parental_guidance_pg,
            )
            val m = ClassificationHolder.Item(
                serverName = M,
                descriptionRes = R.string.certification_dialog_australia_m,
                iconRes = R.drawable.classification_australian_mature_m,
            )
            val ma15 = ClassificationHolder.Item(
                serverName = MA15,
                descriptionRes = R.string.certification_dialog_australia_ma,
                iconRes = R.drawable.classification_australian_mature_ma_15,
            )
            val r18 = ClassificationHolder.Item(
                serverName = R18,
                descriptionRes = R.string.certification_dialog_australia_r18,
                iconRes = R.drawable.classification_australian_restricted_r_18,
            )
            val x18 = ClassificationHolder.Item(
                serverName = X18,
                descriptionRes = R.string.certification_dialog_australia_x18,
                iconRes = R.drawable.classification_australian_restricted_x_18,
            )
            val rc = ClassificationHolder.Item(
                serverName = RC,
                descriptionRes = R.string.certification_dialog_australia_rc,
                iconRes = R.drawable.classification_australian_refused_classification_rc,
            )
            return objectListOf(e, g, pg, m, ma15, r18, x18, rc)
        }
    }

}
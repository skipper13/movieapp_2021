package com.nicetas.movieapp3.ui.search.options.certification

import android.app.Dialog
import android.os.Bundle
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import androidx.appcompat.app.AlertDialog
import androidx.collection.ObjectList
import androidx.core.view.postDelayed
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.setFragmentResult
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.nicetas.movieapp3.R
import com.nicetas.movieapp3.base.adapter.RvScrollbarsHelper
import com.nicetas.movieapp3.base.extensions.dp
import com.nicetas.movieapp3.ui.search.options.certification.adapter.CertificationAdapter
import com.nicetas.movieapp3.ui.search.options.certification.adapter.CertificationCountryHolder
import com.nicetas.movieapp3.ui.search.options.certification.adapter.ClassificationHolder

class SearchCertificationDialog : DialogFragment() {

    companion object {
        private const val OUT_SELECTED = "OUT_SELECTED"
        private const val OUT_COUNTRY = "OUT_COUNTRY"
        const val FRAGMENT_RESULT_KEY = "FRAGMENT_RESULT_KEY_SearchCertificationDialog"
        private const val ARG_SELECTED = "ARG_SELECTED"
        private const val ARG_COUNTRY = "ARG_COUNTRY"
        private const val STATE_COUNTRY = "STATE_COUNTRY"
        private const val STATE_CLASSIFICATION = "STATE_CLASSIFICATION"

        fun newInstance(
            selected: HashSet<String>,
            certificationCountry: String?,
        ) = SearchCertificationDialog().apply {
            arguments = Bundle(2).apply {
                putStringArrayList(ARG_SELECTED, ArrayList(selected))
                putString(ARG_COUNTRY, certificationCountry)
            }
        }

        fun selected(bundle: Bundle): List<String> {
            return bundle.getStringArrayList(OUT_SELECTED)!!
        }

        fun country(bundle: Bundle): String {
            return bundle.getString(OUT_COUNTRY)!!
        }
    }

    private var adapter: CertificationAdapter? = null
    private val selected = ArrayList<String>()
    private var countryId = ""

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val recyclerView = RecyclerView(requireContext(), RvScrollbarsHelper.attrs).apply {
            layoutParams = ViewGroup.LayoutParams(MATCH_PARENT, MATCH_PARENT)
            setPadding(0, 8F.dp, 0, 8F.dp)
            clipToPadding = false
            isVerticalScrollBarEnabled = false
            postDelayed(200) { isVerticalScrollBarEnabled = true }
        }

        val initialSelected: ArrayList<String>?
        if (savedInstanceState != null) {
            initialSelected = savedInstanceState.getStringArrayList(STATE_CLASSIFICATION)
            countryId = savedInstanceState.getString(STATE_COUNTRY)!!
        } else {
            val args = requireArguments()
            initialSelected = args.getStringArrayList(ARG_SELECTED)
            countryId = args.getString(ARG_COUNTRY, Certification.default.getId())
        }
        if (initialSelected != null) {
            selected.addAll(initialSelected)
        }

        adapter = CertificationAdapter(
            countryCallback = object : CertificationCountryHolder.Callback {
                override fun onClick() {
                    showCountryDialog()
                }
            },
            classificationCallback = object : ClassificationHolder.Callback {
                override fun isSelected(item: ClassificationHolder.Item): Boolean {
                    return selected.contains(item.serverName)
                }

                override fun onItemClick(position: Int, item: ClassificationHolder.Item) {
                    val index = selected.indexOfFirst { it == item.serverName }
                    if (index == -1) {
                        selected.add(item.serverName)
                    } else {
                        selected.removeAt(index)
                    }
                    adapter?.notifyItemChanged(position, 0)
                }
            }
        )

        val items = Certification.getById(countryId).getClassifications()
        adapter!!.items.add(CertificationCountryHolder.Item(countryId))
        adapter!!.items.addAll(items as ObjectList<Any>)

        recyclerView.layoutManager = LinearLayoutManager(requireContext())
        recyclerView.adapter = adapter

        return AlertDialog.Builder(requireActivity())
            .setView(recyclerView)
            .setPositiveButton(android.R.string.ok) { _, _ ->
                val bundle = Bundle(2).apply {
                    putString(OUT_COUNTRY, countryId)
                    putStringArrayList(OUT_SELECTED, selected)
                }
                setFragmentResult(FRAGMENT_RESULT_KEY, bundle)
            }
            .setNegativeButton(android.R.string.cancel, null)
            .setNeutralButton(R.string.reset_selected, null)
            .create()
    }

    override fun onStart() {
        super.onStart()
        (dialog as AlertDialog).getButton(AlertDialog.BUTTON_NEUTRAL).setOnClickListener {
            selected.clear()
            adapter?.notifyItemRangeChanged(1, adapter!!.itemCount, 0)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString(STATE_COUNTRY, countryId)
        outState.putStringArrayList(STATE_CLASSIFICATION, selected)
    }

    override fun onDestroyView() {
        adapter?.run { items.list.clear() }
        adapter = null
        super.onDestroyView()
    }

    private fun showCountryDialog() {
        val choiceItems = arrayOf(
            getString(R.string.certification_dialog_title_germany),
            getString(R.string.certification_dialog_title_united_states),
            getString(R.string.certification_dialog_title_australia),
        )
        var selectedIndex = when (countryId) {
            Certification.Germany.getId() -> 0
            Certification.UnitedStates.getId() -> 1
            Certification.Australia.getId() -> 2
            else -> 0
        }
        AlertDialog.Builder(requireContext())
            .setTitle(R.string.certification_settings_title)
            .setSingleChoiceItems(choiceItems, selectedIndex) { _, index ->
                selectedIndex = index
            }
            .setPositiveButton(android.R.string.ok) { _, _ ->
                val newCountry = when (selectedIndex) {
                    0 -> Certification.Germany.getId()
                    1 -> Certification.UnitedStates.getId()
                    2 -> Certification.Australia.getId()
                    else -> throw IllegalStateException()
                }
                if (countryId != newCountry) {
                    countryId = newCountry
                    selected.clear()

                    adapter?.let { adapter ->
                        val item = adapter.items.list[0] as CertificationCountryHolder.Item
                        item.countryId = newCountry

                        val size = adapter.items.list.size
                        for (i in size - 1 downTo 1) {
                            adapter.items.list.removeAt(i)
                        }

                        val items = Certification.getById(countryId).getClassifications()
                        adapter.items.list.addAll(1, items as ObjectList<Any>)

                        // noinspection NotifyDataSetChanged
                        adapter.notifyDataSetChanged()
                    }
                }
            }
            .setNegativeButton(android.R.string.cancel, null)
            .show()
    }

}
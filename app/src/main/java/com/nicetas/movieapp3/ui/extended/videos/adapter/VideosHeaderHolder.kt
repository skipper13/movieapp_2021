package com.nicetas.movieapp3.ui.extended.videos.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.nicetas.movieapp3.base.adapter.ClickDebounce
import com.nicetas.movieapp3.databinding.ItemVideosHeaderBinding

class VideosHeaderHolder(
    parent: ViewGroup,
    inflater: LayoutInflater,
    private var binding: ItemVideosHeaderBinding =
        ItemVideosHeaderBinding.inflate(inflater, parent, false),
    debounce: ClickDebounce,
    callback: Callback,
) : RecyclerView.ViewHolder(binding.root) {

    fun interface Callback {
        fun onVideosHeaderClick(type: String)
    }

    class Item(
        val isExpanded: Boolean,
        val type: String,
    )

    init {
        binding.root.setOnClickListener {
            if (debounce.canClick()) {
                callback.onVideosHeaderClick(type = item.type)
            }
        }
    }

    private lateinit var item: Item

    fun bind(item: Item) {
        this.item = item
        binding.tvTitle.text = item.type
        binding.ivArrow.rotation = if (item.isExpanded) 180F else 0F
    }

}
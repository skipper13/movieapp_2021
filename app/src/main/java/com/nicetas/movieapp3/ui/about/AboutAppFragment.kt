package com.nicetas.movieapp3.ui.about

import android.os.Build
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup.MarginLayoutParams
import androidx.annotation.RequiresApi
import androidx.appcompat.view.menu.MenuBuilder
import androidx.core.content.ContextCompat.getColor
import androidx.core.text.scale
import androidx.core.view.MenuProvider
import androidx.core.view.ViewCompat
import androidx.core.view.updateLayoutParams
import androidx.lifecycle.Lifecycle
import com.google.android.material.snackbar.Snackbar
import com.nicetas.movieapp3.BuildConfig
import com.nicetas.movieapp3.R
import com.nicetas.movieapp3.base.extensions.snackBarTMDB
import com.nicetas.movieapp3.base.extensions.statusBarHeight
import com.nicetas.movieapp3.base.fragment.BaseFragment
import com.nicetas.movieapp3.databinding.FragmentAboutAppBinding
import com.nicetas.movieapp3.ui.about.alerts.AlertAboutDeeplinks
import com.nicetas.movieapp3.ui.about.alerts.AlertChangeLanguage
import com.nicetas.movieapp3.ui.main.StartDestinationTabHelper
import org.koin.androidx.viewmodel.ext.android.viewModel

class AboutAppFragment : BaseFragment(R.layout.fragment_about_app) {

    private var _binding: FragmentAboutAppBinding? = null
    private val binding
        get() = _binding!!

    private val viewModel: AboutViewModel by viewModel()

    private val startDestinationTabHelper = StartDestinationTabHelper()
    private val alertChangeLanguage = AlertChangeLanguage()
    @RequiresApi(Build.VERSION_CODES.S)
    private val alertAboutDeeplinks = AlertAboutDeeplinks()
    private var menuItemEnableOffline: MenuItem? = null
    private var menuItemDisableOffline: MenuItem? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        startDestinationTabHelper.onBackPressedDispatcher(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentAboutAppBinding.bind(view)
        setStatusBarColor(getColor(requireContext(), R.color.colorPrimary))
        setToolbarTitle("")
        setupOptionsMenu()
        setupViews()
        setupObservers()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
        menuItemEnableOffline = null
        menuItemDisableOffline = null
    }

    private fun setupOptionsMenu() {
        requireActivity().addMenuProvider(object : MenuProvider {
            override fun onCreateMenu(menu: Menu, menuInflater: MenuInflater) {
                menuInflater.inflate(R.menu.menu_about, menu)
                // noinspection RestrictedApi
                if (menu is MenuBuilder) {
                    menu.setOptionalIconsVisible(true)
                }
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.S) {
                    menu.findItem(R.id.action_deeplink_associate).isVisible = false
                }
                menuItemEnableOffline = menu.findItem(R.id.action_enable_offline).apply {
                    isVisible = viewModel.isOfflineMode.value!!.not()
                }
                menuItemDisableOffline = menu.findItem(R.id.action_disable_offline).apply {
                    isVisible = viewModel.isOfflineMode.value!!
                }
            }

            override fun onMenuItemSelected(menuItem: MenuItem): Boolean {
                return when (menuItem.itemId) {
                    R.id.action_deeplink_associate -> {
                        // noinspection NewApi
                        alertAboutDeeplinks.show(requireContext())
                        true
                    }
                    R.id.action_language_change -> {
                        alertChangeLanguage.show(requireContext())
                        true
                    }
                    R.id.action_enable_offline -> {
                        viewModel.updateOfflineMode(isOffline = true)
                        true
                    }
                    R.id.action_disable_offline -> {
                        viewModel.updateOfflineMode(isOffline = false)
                        true
                    }
                    R.id.action_reset_tips -> {
                        viewModel.resetTips()
                        true
                    }
                    R.id.action_reload_config -> {
                        viewModel.reloadConfig()
                        true
                    }
                    else -> false
                }
            }
        }, viewLifecycleOwner, Lifecycle.State.RESUMED)
    }

    private fun setupObservers() {
        viewModel.isOfflineMode.observe(viewLifecycleOwner) { isOffline ->
            menuItemEnableOffline?.isVisible = isOffline.not()
            menuItemDisableOffline?.isVisible = isOffline
        }
        viewModel.showReloadConfigMessage.observe(viewLifecycleOwner) { textRes ->
            binding.root.snackBarTMDB(getString(textRes), Snackbar.LENGTH_SHORT).show()
        }
    }

    private fun setupViews() {
        ViewCompat.setOnApplyWindowInsetsListener(binding.root) { _, insets ->
            val statusBarHeight = insets.statusBarHeight()
            binding.appBar.updateLayoutParams<MarginLayoutParams> {
                topMargin = statusBarHeight
            }
            binding.scrollView.updateLayoutParams<MarginLayoutParams> {
                bottomMargin = statusBarHeight
            }
            insets
        }

        binding.tvAppNameAndVersion.text = SpannableStringBuilder()
            .append(getString(R.string.app_name))
            .append("  ")
            .scale(0.7F) {
                append('(')
                append(BuildConfig.VERSION_NAME)
                append(')')
            }

        val a = "\n\u2022"
        binding.tvDependencies.text = StringBuilder("Main dependencies:")
            .append("$a androidx.navigation")
            .append("$a androidx.room")
            .append("$a androidx.compose (one screen)")
            .append("$a desugar_jdk_libs")
            .append("$a io.reactivex.rxjava3")
            .append("$a com.squareup.retrofit2")
            .append("$a io.insert-koin")
            .append("$a bumptech.glide")
            .append("\ncopied & edited:")
            .append("$a chrisbanes:photoview")
            .append("$a markomilos:paginate")
    }

}
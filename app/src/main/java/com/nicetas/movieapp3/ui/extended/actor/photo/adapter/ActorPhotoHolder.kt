package com.nicetas.movieapp3.ui.extended.actor.photo.adapter

import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import androidx.appcompat.widget.AppCompatImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.nicetas.movieapp3.R
import com.nicetas.movieapp3.base.adapter.ClickDebounce
import com.nicetas.movieapp3.base.extensions.dp
import com.nicetas.movieapp3.ui.ImageUrl

class ActorPhotoHolder(
    parent: ViewGroup,
    private var imageView: AppCompatImageView = AppCompatImageView(parent.context).apply {
        layoutParams = ViewGroup.LayoutParams(MATCH_PARENT, 250F.dp)
    },
    debounce: ClickDebounce,
    callback: Callback,
) : RecyclerView.ViewHolder(imageView) {

    fun interface Callback {
        fun onPhotoClick(position: Int)
    }

    class Item(val filePath: String)

    private lateinit var item: Item

    init {
        imageView.setOnClickListener {
            if (debounce.canClick()) {
                callback.onPhotoClick(bindingAdapterPosition)
            }
        }
    }

    fun bind(item: Item) {
        this.item = item

        Glide.with(imageView.context)
            .load(ImageUrl.w342(item.filePath))
            .transition(DrawableTransitionOptions.withCrossFade())
            .placeholder(R.drawable.movie_image_placeholder)
            .centerCrop()
            .into(imageView)
    }

}
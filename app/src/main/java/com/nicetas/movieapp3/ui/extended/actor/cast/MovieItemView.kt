package com.nicetas.movieapp3.ui.extended.actor.cast

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.ElevatedCard
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.ripple
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.bumptech.glide.integration.compose.CrossFade
import com.bumptech.glide.integration.compose.ExperimentalGlideComposeApi
import com.bumptech.glide.integration.compose.GlideImage
import com.nicetas.movieapp3.R
import com.nicetas.movieapp3.base.extensions.isLandscape
import com.nicetas.movieapp3.ui.ImageUrl
import com.nicetas.movieapp3.ui.theme.ColorCompose
import com.nicetas.movieapp3.ui.theme.MovieApp3Theme
import java.time.LocalDate

@OptIn(ExperimentalGlideComposeApi::class)
@Composable
fun MovieItemView(
    movie: ActorCastViewModel.MovieItem,
    isMultiWindow: () -> Boolean,
    onClick: () -> Unit,
) {
    ElevatedCard(
        shape = RectangleShape,
        colors = CardDefaults.cardColors(containerColor = ColorCompose.grey_424242),
        modifier = Modifier
            .height(245.dp)
            .fillMaxWidth()
            .clickable(
                interactionSource = remember { MutableInteractionSource() },
                indication = ripple(color = ColorCompose.white),
                onClick = onClick
            )
    ) {
        GlideImage(
            model = ImageUrl.w500(movie.backdropPath),
            contentDescription = "Movie backdrop image",
            contentScale = ContentScale.Crop,
            transition = CrossFade,
            modifier = Modifier
                .fillMaxWidth()
                .height(195.dp)
        )
        Row(
            modifier = Modifier.fillMaxSize()
        ) {
            Text(
                text = movie.title,
                color = ColorCompose.white,
                textAlign = TextAlign.Center,
                fontSize = 14.sp,
                lineHeight = 16.0.sp,
                maxLines = 2,
                overflow = TextOverflow.Ellipsis,
                modifier = Modifier
                    .weight(1F)
                    .align(Alignment.CenterVertically)
                    .padding(horizontal = 8.dp, vertical = 8.dp)
            )

            val hideIcons = isMultiWindow.invoke() && LocalContext.current.isLandscape()

            if (movie.releaseDate != null) {
                if (!hideIcons) {
                    Image(
                        painter = painterResource(id = R.drawable.ic_release_date_alpha_50),
                        contentDescription = "Release date icon",
                        modifier = Modifier
                            .align(Alignment.CenterVertically)
                            .padding(end = 8.dp)
                    )
                }
                Text(
                    text = movie.releaseDate.year.toString(),
                    color = ColorCompose.white,
                    textAlign = TextAlign.Center,
                    style = MaterialTheme.typography.bodyMedium,
                    modifier = Modifier
                        .wrapContentWidth(Alignment.CenterHorizontally)
                        .align(Alignment.CenterVertically)
                        .padding(end = 20.dp)
                )
            }

            val voteAverage = movie.voteAverage
            if (voteAverage != null && voteAverage > 0.0) {
                if (!hideIcons) {
                    Image(
                        painter = painterResource(id = R.drawable.ic_votes_alpha_50),
                        contentDescription = "Movie rating icon",
                        modifier = Modifier
                            .align(Alignment.CenterVertically)
                            .padding(end = 8.dp)
                    )
                }
                Text(
                    text = movie.voteAverage.toString(),
                    color = ColorCompose.white,
                    textAlign = TextAlign.Center,
                    style = MaterialTheme.typography.bodyMedium,
                    modifier = Modifier
                        .wrapContentWidth(Alignment.CenterHorizontally)
                        .align(Alignment.CenterVertically)
                        .padding(end = 10.dp)
                )
            }
        }
    }
}


@Preview
@Composable
private fun MovieItemViewPreview() {
    MovieApp3Theme {
        val movie = ActorCastViewModel.MovieItem(
            movieId = 10,
            title = "Title",
            voteAverage = 9.0,
            releaseDate = LocalDate.of(2013, 8, 1),
            backdropPath = "",
            posterPath = "",
        )
        MovieItemView(
            movie = movie,
            isMultiWindow = { false },
            onClick = {},
        )
    }
}
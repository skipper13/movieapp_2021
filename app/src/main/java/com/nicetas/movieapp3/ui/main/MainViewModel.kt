package com.nicetas.movieapp3.ui.main

import com.nicetas.bus.KeyboardBus
import com.nicetas.movieapp3.base.viewmodel.BaseViewModel
import com.nicetas.repos.configuration.ConfigRepository

class MainViewModel(
    private val keyboardBus: KeyboardBus,
) : BaseViewModel() {

    fun updateKeyboardVisibility(isOpen: Boolean) {
        if (isOpen) {
            keyboardBus.onOpen()
        } else {
            keyboardBus.onClose()
        }
    }

}
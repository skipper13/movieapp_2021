package com.nicetas.movieapp3.ui.lists.list

import androidx.collection.MutableObjectList
import androidx.collection.ObjectList
import androidx.lifecycle.MutableLiveData
import com.nicetas.bus.WatchListUpdateBus
import com.nicetas.database.lists.ListsEntity
import com.nicetas.models.search.MediaType
import com.nicetas.movieapp3.base.Logger
import com.nicetas.movieapp3.base.states.Lce
import com.nicetas.movieapp3.base.viewmodel.BaseViewModel
import com.nicetas.movieapp3.base.viewmodel.LiveEvent
import com.nicetas.movieapp3.ui.ImageUrl
import com.nicetas.movieapp3.ui.search.adapter.MovieHolder
import com.nicetas.repos.configuration.ConfigRepository
import com.nicetas.repos.list.ListRepository
import com.nicetas.repos.movie.MovieRepository
import com.nicetas.repos.network.ErrorResponse
import com.nicetas.repos.search.SortBy
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.schedulers.Schedulers
import java.time.LocalDate

class ListViewModel(
    private val configRepository: ConfigRepository,
    private val listRepository: ListRepository,
    private val movieRepository: MovieRepository,
    private val watchListUpdateBus: WatchListUpdateBus,
) : BaseViewModel() {

    object LceIsPrivate : Lce.State

    class GetMoviesResult(val items: ObjectList<Any>, val isFullReload: Boolean)

    class MovieItem(
        override val movieId: Int,
        override val title: String,
        override val voteAverage: Double?,
        override val releaseDate: LocalDate?,
        override val backdropPath: String?,
        override var isInWatchlist: Boolean,
        val mediaType: MediaType,
        val originalTitle: String?,
        override val posterPath: String?,
        val popularity: Double?,
        val voteCount: Int?,
        val overview: String?,
    ) : MovieHolder.Item(
        movieId = movieId,
        title = title,
        voteAverage = voteAverage,
        releaseDate = releaseDate,
        backdropPath = backdropPath,
        posterPath = posterPath,
        isInWatchlist = isInWatchlist,
    )

    val onGetMovies = LiveEvent<GetMoviesResult>()
    val isInLists = MutableLiveData<Boolean>()
    val setToolbarTitle = MutableLiveData<String>()
    val showPaginationError = LiveEvent<Unit>()
    val onMovieFromWatchlistRemoved = LiveEvent<Int>()

    private var wasInit = false
    private val items = MutableObjectList<Any>()
    private var listId = 0
    var sortBy: SortBy? = null
        private set
    private var backdropPath = ""
    private var itemCount = 0
    private var listName = ""
        get() = field.ifBlank { "$listId" }

    private var page = 1
    private var totalPages = Int.MAX_VALUE

    val hasLoadedAllItems: Boolean
        get() = page > totalPages
    var isLoading = false
        private set
    var paginationError: Lce.Error? = null
        private set

    fun onViewCreated(listId: Int) {
        checkInLists(listId)
        if (wasInit) {
            onGetMovies.setValue(GetMoviesResult(items, false))
            return
        }
        wasInit = true
        this.listId = listId
        subscribeOnWatchlist()
        lce.loading()
        loadImgBaseUrlThenMovies(isFullReload = false)
    }

    override fun onCleared() {
        super.onCleared()
        items.clear()
    }

    fun changeSort(sortBy: SortBy?) {
        if (this.sortBy != sortBy) {
            this.sortBy = sortBy
            reloadMovies()
        }
    }

    fun reloadMovies() {
        disposables.clear()
        page = 1
        totalPages = Int.MAX_VALUE
        lce.loading()
        if (ImageUrl.isFromConfig) {
            loadMoreMovies(isFullReload = true)
        } else {
            loadImgBaseUrlThenMovies(isFullReload = true)
        }
    }

    private fun loadImgBaseUrlThenMovies(isFullReload: Boolean) {
        disposables += configRepository.getOrLoadImgBaseUrl()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ url ->
                ImageUrl.init(url)
                loadMoreMovies(isFullReload)
            }, { error ->
                Logger.e(error)
                lce.error(message = error.message ?: "")
            })
    }

    private class InWatchList(val id: Int, val inList: Boolean)

    fun loadMoreMovies(isFullReload: Boolean) {
        isLoading = true
        disposables += listRepository.getDetails(sortBy = sortBy, listId = listId, page = page)
            .flatMap { response ->
                Observable.fromIterable(response.body.results.map { it.id })
                    .flatMapSingle { id ->
                        movieRepository.isInWatchList(id).map { InWatchList(id, it) }
                    }
                    .toList()
                    .map { watchList ->
                        val result = response.body

                        page = result.page + 1
                        totalPages = result.totalPages
                        backdropPath = result.backdropPath ?: ""
                        itemCount = result.itemCount
                        listName = result.name

                        // Do not loop inside "movies.map" without need.
                        val isSomethingInWatchList = watchList.any { it.inList }

                        val newItems = MutableObjectList<Any>()

                        val movies = result.results

                        for (i in movies.indices) {
                            val movie = movies[i]
                            val item = MovieItem(
                                mediaType = movie.mediaType,
                                movieId = movie.id,
                                title = movie.title ?: movie.name ?: "",
                                voteAverage = movie.voteAverage,
                                releaseDate = movie.releaseDate ?: movie.firstAirDate,
                                backdropPath = movie.backdropPath,
                                originalTitle = movie.originalTitle ?: movie.originalName,
                                posterPath = movie.posterPath,
                                popularity = movie.popularity,
                                voteCount = movie.voteCount,
                                overview = movie.overview,
                                isInWatchlist = isSomethingInWatchList
                                        && watchList.any { it.id == movie.id && it.inList },
                            )
                            newItems.add(item)
                        }
                        if (isFullReload) {
                            items.clear()
                        }
                        items.addAll(newItems)
                        newItems
                    }
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ newItems ->
                onGetMovies.setValue(GetMoviesResult(newItems, isFullReload))
                setToolbarTitle.value = listName
                updateList()

                if (items.isEmpty()) {
                    lce.empty()
                } else {
                    lce.content()
                }
                isLoading = false
            }, { error ->
                Logger.e(error)

                if (error is ErrorResponse && error.serverCode == 3) {
                    lce.state(LceIsPrivate)
                } else {
                    if (items.isEmpty()) {
                        lce.error(message = error.message ?: "")
                    } else {
                        paginationError = Lce.Error(error.message ?: "")
                        showPaginationError.setValue(Unit)
                    }
                }
                isLoading = false
            })
    }

    private fun checkInLists(listId: Int) {
        disposables += listRepository.isInLists(listId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ isIn ->
                isInLists.value = isIn
            }, { error ->
                Logger.e(error)
            })
    }

    fun addToLists() {
        val list = ListsEntity(
            id = listId,
            name = listName,
            backdropPath = backdropPath,
            itemCount = itemCount,
        )
        disposables += listRepository.insertList(list)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                isInLists.value = true
            }, { error ->
                Logger.e(error)
            })
    }

    private fun updateList() {
        val list = ListsEntity(
            id = listId,
            name = listName,
            backdropPath = backdropPath,
            itemCount = itemCount,
        )
        disposables += listRepository.updateList(list)
            .subscribeOn(Schedulers.io())
            .subscribe({
                /* no-op */
            }, { error ->
                Logger.e(error)
            })
    }

    fun removeFromLists() {
        disposables += listRepository.deleteList(listId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                isInLists.value = false
            }, { error ->
                Logger.e(error)
            })
    }

    fun removeFromWatchList(movieId: Int) {
        disposables += movieRepository.deleteMovieToWatch(movieId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                val index = items.indexOfFirst { it is MovieItem && it.movieId == movieId }
                val item = items[index] as MovieItem
                item.isInWatchlist = false
                onMovieFromWatchlistRemoved.setValue(item.movieId)
            }, { error ->
                Logger.e(error)
            })
    }

    private fun subscribeOnWatchlist() {
        disposables += watchListUpdateBus.subscribeOnWatchlist()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ event ->
                val index = items.indexOfFirst { it is MovieItem && it.movieId == event.movieId }
                val item = items[index] as MovieItem
                when (event) {
                    is WatchListUpdateBus.OnRemove -> {
                        item.isInWatchlist = false
                    }
                    is WatchListUpdateBus.OnAdd -> {
                        item.isInWatchlist = true
                    }
                }
            }, { error ->
                Logger.e(error)
            })
    }

}
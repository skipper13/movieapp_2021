package com.nicetas.movieapp3.ui.search.options.keywords.search

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.core.view.postDelayed
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.setFragmentResult
import com.nicetas.movieapp3.R
import com.nicetas.movieapp3.base.adapter.pagination.Paginate
import com.nicetas.movieapp3.base.extensions.showKeyboard
import com.nicetas.movieapp3.databinding.FragmentSearchKeywordsDialogBinding
import com.nicetas.movieapp3.ui.search.options.keywords.search.adapter.SearchWordAdapter
import org.koin.androidx.viewmodel.ext.android.viewModel

class SearchWordDialog : DialogFragment() {

    companion object {
        const val FRAGMENT_RESULT_KEY = "FRAGMENT_RESULT_KEY_SearchWordDialog"
        private const val ARG_SCREEN_TYPE = "ARG_SCREEN_TYPE"

        fun newInstance(type: @SearchWord Int) = SearchWordDialog().apply {
            arguments = Bundle(1).apply {
                putInt(ARG_SCREEN_TYPE, type)
            }
        }

        fun screenType(bundle: Bundle): @SearchWord Int {
            return bundle.getInt(ARG_SCREEN_TYPE)
        }
    }

    private var _binding: FragmentSearchKeywordsDialogBinding? = null
    private val binding
        get() = _binding!!

    private val viewModel: SearchWordViewModel by viewModel()

    private val screenType: @SearchWord Int by lazy {
        requireArguments().getInt(ARG_SCREEN_TYPE)
    }
    private var adapter: SearchWordAdapter? = null
    private var paginate: Paginate? = null
    private var textWatcher: TextWatcher? = null

    private val paginateCallbacks = object : Paginate.Callbacks {
        override fun onLoadMore() = viewModel.loadMore()
        override fun isLoading() = viewModel.isLoading
        override fun hasLoadedAllItems() = viewModel.hasLoadedAllItems
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        _binding = FragmentSearchKeywordsDialogBinding.inflate(layoutInflater)

        adapter = SearchWordAdapter(
            onKeywordClick = { item ->
                viewModel.onKeywordClick(item)
            }
        )
        binding.rvItems.adapter = adapter

        return AlertDialog.Builder(requireActivity())
            .setView(binding.root)
            .setPositiveButton(android.R.string.ok, null)
            .create()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupObservers()
        setupViews()

        binding.editText.apply {
            requestFocus()
            postDelayed(150) {
                showKeyboard()
            }
        }
        viewModel.onViewCreated(screenType)
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        val bundle = Bundle(1).apply {
            putInt(ARG_SCREEN_TYPE, screenType)
        }
        setFragmentResult(FRAGMENT_RESULT_KEY, bundle)
    }

    override fun onDestroyView() {
        binding.editText.removeTextChangedListener(textWatcher)
        textWatcher = null
        paginate?.unbind()
        paginate = null
        adapter?.run { items.list.clear() }
        adapter = null
        binding.rvItems.adapter = null
        super.onDestroyView()
        _binding = null
    }

    private fun setupViews() {
        val hint = when {
            SearchWord.isKeyword(screenType) -> getString(R.string.search_keywords_hint)
            SearchWord.isCompany(screenType) -> getString(R.string.search_keywords_hint_company)
            else -> throw IllegalStateException()
        }
        binding.editText.hint = hint
    }

    private fun setupObservers() {
        viewModel.setHasMoreDataToLoad.observe(viewLifecycleOwner) { has ->
            paginate?.setHasMoreDataToLoad(has)
        }
        viewModel.scrollToTop.observe(viewLifecycleOwner) {
            binding.rvItems.scrollToPosition(0)
        }
        viewModel.replaceItem.observe(viewLifecycleOwner) { replace ->
            adapter?.replace(binding.rvItems, replace)
        }
        viewModel.updateList.observe(viewLifecycleOwner) { items ->
            adapter?.update(items)
        }
        viewModel.clearList.observe(viewLifecycleOwner) {
            adapter?.items?.clear()
        }
        viewModel.addKeywords.observe(viewLifecycleOwner) { items ->
            adapter?.items?.addAll(items)
        }
        viewModel.initPaginateIfNeed.observe(viewLifecycleOwner) {
            if (paginate == null) {
                paginate = Paginate.with(binding.rvItems, paginateCallbacks)
                    .onRetryClick { viewModel.loadMore() }
                    .errorText { viewModel.paginationError?.message(requireContext()) ?: "" }
                    .build()
            }
        }
        viewModel.showPaginationError.observe(viewLifecycleOwner) {
            paginate?.setError()
        }
        viewModel.initEditText.observe(viewLifecycleOwner) { initialText ->
            binding.editText.setText(initialText)
            binding.editText.setSelection(initialText.length)
            textWatcher = binding.editText.addTextChangedListener(
                onTextChanged = { text: CharSequence?, start: Int, before: Int, count: Int ->
                    viewModel.search(text.toString())
                }
            )
        }
        viewModel.showSearchLoad.observe(viewLifecycleOwner) { show ->
            if (show) {
                binding.vProgressBar.show()
            } else {
                binding.vProgressBar.hide()
            }
        }
    }

}
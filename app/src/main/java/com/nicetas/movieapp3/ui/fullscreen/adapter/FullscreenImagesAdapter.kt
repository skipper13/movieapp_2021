package com.nicetas.movieapp3.ui.fullscreen.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class FullscreenImagesAdapter(
    private val images: List<String>,
    private val imageSize: String,
    private val onImageClick: () -> Unit,
) : RecyclerView.Adapter<FullscreenImagesHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FullscreenImagesHolder {
        return FullscreenImagesHolder(
            parent = parent,
            imageSize = imageSize,
            onImageClick = onImageClick,
        )
    }

    override fun onBindViewHolder(holder: FullscreenImagesHolder, position: Int) {
        holder.bind(images[position])
    }

    override fun getItemCount(): Int = images.size

}
package com.nicetas.movieapp3.ui.about.alerts

import android.content.Context
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.app.LocaleManagerCompat
import androidx.core.os.LocaleListCompat
import com.nicetas.movieapp3.R

class AlertChangeLanguage {

    fun show(context: Context) {
        val choiceItems = arrayOf(
            context.getString(R.string.language_english),
            context.getString(R.string.language_french),
            context.getString(R.string.language_german),
            context.getString(R.string.language_italian),
            context.getString(R.string.language_portuguese),
            context.getString(R.string.language_russian),
            context.getString(R.string.language_serbian),
            context.getString(R.string.language_spanish),
            context.getString(R.string.language_turkish),
        )
        // https://developer.android.com/guide/topics/resources/app-languages#androidx-impl
        val appLocaleList = AppCompatDelegate.getApplicationLocales()
        val currentLocale =
            if (appLocaleList.isEmpty.not()) {
                appLocaleList.get(0)!!
            } else {
                LocaleManagerCompat.getSystemLocales(context).get(0)!!
            }
        val checkedItem = when (currentLocale.language) {
            "en" -> 0
            "fr" -> 1
            "de" -> 2
            "it" -> 3
            "pt" -> 4
            "ru" -> 5
            "sr" -> 6
            "es" -> 7
            "tr" -> 8
            else -> 0
        }

        var selectedIndex = checkedItem

        AlertDialog.Builder(context)
            .setTitle(R.string.language_change_title)
            .setSingleChoiceItems(choiceItems, checkedItem) { _, index ->
                selectedIndex = index
            }
            .setPositiveButton(android.R.string.ok) { _, _ ->
                val language = when (selectedIndex) {
                    0 -> "en"
                    1 -> "fr"
                    2 -> "de"
                    3 -> "it"
                    4 -> "pt"
                    5 -> "ru"
                    6 -> "sr"
                    7 -> "es"
                    8 -> "tr"
                    else -> "en"
                }
                // https://developer.android.com/guide/topics/resources/app-languages#androidx-impl
                val appLocale = LocaleListCompat.forLanguageTags(language)
                AppCompatDelegate.setApplicationLocales(appLocale)
            }
            .setNegativeButton(android.R.string.cancel, null)
            .show()
    }

}
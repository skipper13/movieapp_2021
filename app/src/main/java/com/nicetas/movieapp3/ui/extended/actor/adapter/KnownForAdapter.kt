package com.nicetas.movieapp3.ui.extended.actor.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.nicetas.movieapp3.base.adapter.AdapterItems
import com.nicetas.movieapp3.base.adapter.ClickDebounce

class KnownForAdapter(
    private var inflater: LayoutInflater?,
    private val debounce: ClickDebounce,
    private val onMovieClick: KnownForHolder.Callback,
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        private const val TYPE_MOVIE = 0
    }

    val items = AdapterItems<Any>(this)

    override fun getItemCount(): Int = items.list.size

    override fun getItemViewType(position: Int): Int {
        return when (items.list[position]) {
            is KnownForHolder.Item -> TYPE_MOVIE
            else -> throw IllegalStateException("item is null for position = $position")
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_MOVIE -> KnownForHolder(
                parent = parent,
                inflater = inflater ?: /*cannot happen*/ LayoutInflater.from(parent.context),
                debounce = debounce,
                callback = onMovieClick,
            )
            else -> throw IllegalStateException("Unknown holder type")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = items.list[position]
        when (holder) {
            is KnownForHolder -> holder.bind(item as KnownForHolder.Item)
        }
    }

}
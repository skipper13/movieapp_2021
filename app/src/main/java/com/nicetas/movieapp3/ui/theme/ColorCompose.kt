package com.nicetas.movieapp3.ui.theme

import androidx.compose.ui.graphics.Color

object ColorCompose {

    val white = Color(0xFFFFFFFF)

    val grey_424242 = Color(0xFF424242)
    val grey_606060 = Color(0xFF606060)

    val yellow_CAC683 = Color(0xFFCAC683)

}
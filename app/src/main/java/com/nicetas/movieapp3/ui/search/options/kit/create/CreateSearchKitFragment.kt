package com.nicetas.movieapp3.ui.search.options.kit.create

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup.MarginLayoutParams
import androidx.collection.ObjectList
import androidx.core.content.ContextCompat.getColor
import androidx.core.view.MenuProvider
import androidx.core.view.ViewCompat
import androidx.core.view.postDelayed
import androidx.core.view.updateLayoutParams
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.Lifecycle
import androidx.navigation.fragment.findNavController
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexWrap
import com.google.android.flexbox.FlexboxLayoutManager
import com.google.android.flexbox.JustifyContent
import com.nicetas.movieapp3.R
import com.nicetas.movieapp3.base.extensions.showKeyboard
import com.nicetas.movieapp3.base.extensions.statusBarHeight
import com.nicetas.movieapp3.base.fragment.BaseFragment
import com.nicetas.movieapp3.databinding.FragmentCreateSearchKitBinding
import com.nicetas.movieapp3.ui.search.options.kit.create.adapter.KitIconsAdapter
import org.koin.androidx.viewmodel.ext.android.viewModel

class CreateSearchKitFragment : BaseFragment(R.layout.fragment_create_search_kit) {

    private var _binding: FragmentCreateSearchKitBinding? = null
    private val binding
        get() = _binding!!

    private val viewModel: CreateSearchKitViewModel by viewModel()

    private var adapter: KitIconsAdapter? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentCreateSearchKitBinding.bind(view)

        setupViews()
        setupOptionsMenu()
        setupObservers()
        viewModel.onViewCreated()

        binding.editText.apply {
            requestFocus()
            postDelayed(100) {
                showKeyboard()
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun setupOptionsMenu() {
        requireActivity().addMenuProvider(object : MenuProvider {
            override fun onCreateMenu(menu: Menu, menuInflater: MenuInflater) {
                menuInflater.inflate(R.menu.menu_create_search_kit, menu)
            }

            override fun onMenuItemSelected(menuItem: MenuItem): Boolean {
                return when (menuItem.itemId) {
                    R.id.action_create -> {
                        viewModel.create()
                        true
                    }
                    else -> false
                }
            }
        }, viewLifecycleOwner, Lifecycle.State.RESUMED)
    }

    private fun setupViews() {
        setStatusBarColor(getColor(requireContext(), R.color.colorPrimary))
        setToolbarTitle(getString(R.string.search_kits_create))

        ViewCompat.setOnApplyWindowInsetsListener(binding.root) { _, insets ->
            val statusBarHeight = insets.statusBarHeight()
            binding.appBar.updateLayoutParams<MarginLayoutParams> {
                topMargin = statusBarHeight
            }
            insets
        }

        binding.editText.addTextChangedListener(
            onTextChanged = { text: CharSequence?, start: Int, before: Int, count: Int ->
                viewModel.title = text?.toString() ?: ""
            }
        )
        adapter = KitIconsAdapter(
            iconCallback = { id -> viewModel.updateSelectedIcon(id) }
        )
        binding.rvIcons.adapter = adapter
        val layoutManager = FlexboxLayoutManager(context, FlexDirection.ROW, FlexWrap.WRAP)
        binding.rvIcons.layoutManager = layoutManager
        layoutManager.justifyContent = JustifyContent.FLEX_START
    }

    private fun setupObservers() {
        viewModel.closeScreen.observe(viewLifecycleOwner) {
            findNavController().popBackStack()
        }
        viewModel.showIcons.observe(viewLifecycleOwner) { items ->
            adapter?.items?.replaceAll(items as ObjectList<Any>)
        }
        viewModel.updateSelected.observe(viewLifecycleOwner) { selected ->
            adapter?.updateSelected(selected.first, selected.second)
        }
    }

}
package com.nicetas.movieapp3.ui.extended.actor.cast

import android.content.res.Configuration
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.WindowInsets
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.statusBars
import androidx.compose.foundation.layout.statusBarsPadding
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.GridItemSpan
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ArrowBack
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.nicetas.movieapp3.ui.theme.ColorCompose
import com.nicetas.movieapp3.ui.theme.MovieApp3Theme
import java.time.LocalDate

@Composable
fun ActorCastScreen(
    viewModel: ActorCastViewModel,
    onMovieClick: (ActorCastViewModel.MovieItem) -> Unit,
    isMultiWindow: () -> Boolean,
    onUpClick: () -> Unit = {},
) {
    ActorCastScreenView(
        items = viewModel.itemsSnapshot,
        toolbarTitleSnapshot = viewModel.toolbarTitleSnapshot,
        onMovieClick = onMovieClick,
        isMultiWindow = isMultiWindow,
        onUpClick = onUpClick,
    )
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ActorCastScreenView(
    items: SnapshotStateList<Any>,
    toolbarTitleSnapshot: MutableState<String>,
    onMovieClick: (ActorCastViewModel.MovieItem) -> Unit,
    isMultiWindow: () -> Boolean,
    onUpClick: () -> Unit = {},
) {
    val scrollBehavior = TopAppBarDefaults.enterAlwaysScrollBehavior()

    Scaffold(
        containerColor = ColorCompose.grey_606060,
        contentWindowInsets = WindowInsets.statusBars,
        topBar = {
            TopAppBar(
                title = {
                    Text(toolbarTitleSnapshot.value)
                },
                colors = TopAppBarDefaults.topAppBarColors(
                    containerColor = ColorCompose.grey_424242,
                    scrolledContainerColor = ColorCompose.grey_424242,
                    titleContentColor = ColorCompose.white,
                    navigationIconContentColor = ColorCompose.white,
                    actionIconContentColor = ColorCompose.white
                ),
                scrollBehavior = scrollBehavior,
                navigationIcon = {
                    IconButton(onClick = onUpClick) {
                        Icon(
                            Icons.AutoMirrored.Filled.ArrowBack,
                            contentDescription = null
                        )
                    }
                },
                modifier = Modifier
                    .statusBarsPadding()
                    .shadow(elevation = 5.dp)
            )
        },
        modifier = Modifier
            .fillMaxSize()
            .nestedScroll(scrollBehavior.nestedScrollConnection),
    ) { padding ->
        val orientation = LocalContext.current.resources.configuration.orientation

        // Scrollbars - do not even hope
        // https://developer.android.com/jetpack/androidx/compose-roadmap
        // TODO NR: 19.05.2024  try this for Scrollbar
        // https://gist.github.com/mxalbert1996/33a360fcab2105a31e5355af98216f5a
        LazyVerticalGrid(
            columns = GridCells.Fixed(2),
            verticalArrangement = Arrangement.spacedBy(8.dp),
            horizontalArrangement = Arrangement.spacedBy(4.dp),
            contentPadding = PaddingValues(bottom = 8.dp),
            modifier = Modifier
                .padding(padding)
        ) {
            items(
                items = items,
                key = { item ->
                    when (item) {
                        is ActorCastViewModel.MovieItem -> item.movieId
                        is ActorCastViewModel.TitleItem -> item.year
                        else -> throw IllegalStateException()
                    }
                },
                span = { item ->
                    val currentLineSpan = when (item) {
                        is ActorCastViewModel.MovieItem -> {
                            if (orientation == Configuration.ORIENTATION_PORTRAIT) 2 else 1
                        }
                        is ActorCastViewModel.TitleItem -> 2
                        else -> 2
                    }
                    GridItemSpan(currentLineSpan)
                },
                contentType = { item ->
                    when (item) {
                        is ActorCastViewModel.MovieItem -> 1
                        is ActorCastViewModel.TitleItem -> 2
                        else -> throw IllegalStateException()
                    }
                },
            ) { item ->
                when (item) {
                    is ActorCastViewModel.MovieItem -> {
                        MovieItemView(
                            movie = item,
                            isMultiWindow = isMultiWindow,
                            onClick = { onMovieClick.invoke(item) },
                        )
                    }
                    is ActorCastViewModel.TitleItem -> {
                        ActorCastTitleItemView(text = item.year)
                    }
                    else -> throw IllegalStateException()
                }
            }
        }
    }
}


@Preview
@Composable
private fun ActorCastScreenPreview() {
    MovieApp3Theme {
        val items = SnapshotStateList<Any>()
        items.add(ActorCastViewModel.TitleItem("2014"))
        for (i in 5 until 7) {
            val item = ActorCastViewModel.MovieItem(
                movieId = i,
                title = "Title $i",
                voteAverage = i.toDouble(),
                releaseDate = LocalDate.of(2014, 8, 1),
                backdropPath = "",
                posterPath = "",
            )
            items.add(item)
        }
        items.add(ActorCastViewModel.TitleItem("2013"))
        val item = ActorCastViewModel.MovieItem(
            movieId = 10,
            title = "Title 2013",
            voteAverage = 9.0,
            releaseDate = LocalDate.of(2013, 8, 1),
            backdropPath = "",
            posterPath = "",
        )
        items.add(item)

        ActorCastScreenView(
            items = items,
            toolbarTitleSnapshot = remember { mutableStateOf("Actor Name") },
            onMovieClick = {},
            isMultiWindow = { false },
            onUpClick = {},
        )
    }
}
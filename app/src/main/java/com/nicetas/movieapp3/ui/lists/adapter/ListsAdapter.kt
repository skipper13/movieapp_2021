package com.nicetas.movieapp3.ui.lists.adapter

import android.view.ViewGroup
import androidx.collection.ObjectList
import androidx.recyclerview.widget.RecyclerView
import com.nicetas.movieapp3.base.adapter.BaseDiffUtilCallback
import com.nicetas.movieapp3.base.adapter.ClickDebounce
import com.nicetas.movieapp3.base.adapter.AdapterItems

class ListsAdapter(
    private val listsCallback: ListsHolder.Callback
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        private const val TYPE_LIST = 0
    }

    class DiffUtilCallback() : BaseDiffUtilCallback<Any>() {

        private val listsDifHelper = ListsHolder.DifHelper()

        override fun areItemsTheSame(oldItem: Any, newItem: Any): Boolean {
            if (oldItem is ListsHolder.Item && newItem is ListsHolder.Item) {
                return listsDifHelper.areItemsTheSame(oldItem, newItem)
            }
            return false
        }

        override fun areContentsTheSame(oldItem: Any, newItem: Any): Boolean {
            if (oldItem is ListsHolder.Item && newItem is ListsHolder.Item) {
                return listsDifHelper.areContentsTheSame(oldItem, newItem)
            }
            return false
        }
    }

    private val debounce = ClickDebounce()
    private val diffUtilCallback = DiffUtilCallback()
    val items = AdapterItems<Any>(this)

    fun update(newItems: ObjectList<Any>) {
        items.update(newItems, diffUtilCallback)
    }

    override fun getItemCount(): Int = items.list.size

    override fun getItemViewType(position: Int): Int {
        return when (items.list[position]) {
            is ListsHolder.Item -> TYPE_LIST
            else -> throw IllegalStateException("item is null for position = $position")
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_LIST -> ListsHolder(
                parent = parent,
                debounce = debounce,
                callback = listsCallback,
            )
            else -> throw IllegalStateException("Unknown holder type")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = items.list[position]
        when (holder) {
            is ListsHolder -> holder.bind(item as ListsHolder.Item)
        }
    }

}


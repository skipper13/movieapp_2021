package com.nicetas.movieapp3.ui.watchlist.sort

import android.app.Dialog
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.content.res.AppCompatResources.getDrawable
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.setFragmentResult
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.nicetas.movieapp3.R
import com.nicetas.movieapp3.base.extensions.dp
import com.nicetas.movieapp3.ui.sort.SortByAdapter
import com.nicetas.movieapp3.ui.sort.SortByHeaderHolder
import com.nicetas.movieapp3.ui.sort.SortByHolder

class WatchlistSortDialog : DialogFragment() {

    companion object {
        private const val SORT_TYPE = "sort_type"
        private const val IS_DESC = "is_desc"
        const val FRAGMENT_RESULT_KEY = "FRAGMENT_RESULT_KEY_WatchlistSortDialog"
        private const val ARG_SELECTED_SORT = "ARG_SELECTED_SORT"
        private const val ARG_SELECTED_IS_DESC = "ARG_SELECTED_IS_DESC"
        private const val STATE_IS_DESC = "STATE_IS_DESC"

        fun newInstance(
            selectedSort: Int,
            isDescending: Boolean,
        ) = WatchlistSortDialog().apply {
            arguments = Bundle(2).apply {
                putInt(ARG_SELECTED_SORT, selectedSort)
                putBoolean(ARG_SELECTED_IS_DESC, isDescending)
            }
        }

        fun sort(bundle: Bundle): @WatchlistSort Int {
            return bundle.getInt(SORT_TYPE, WatchlistSort.DEFAULT)
        }

        fun isDescending(bundle: Bundle): Boolean {
            return bundle.getBoolean(IS_DESC, false)
        }
    }

    private val selectedId: Int by lazy {
        requireArguments().getInt(ARG_SELECTED_SORT, 0)
    }

    private var adapter: SortByAdapter<Int>? = null
    private var isDesc = true

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val recyclerView = RecyclerView(requireContext()).apply {
            layoutParams = ViewGroup.LayoutParams(MATCH_PARENT, MATCH_PARENT)
            setPadding(0, 8F.dp, 0, 8F.dp)
            clipToPadding = false
            overScrollMode = View.OVER_SCROLL_NEVER
        }

        val isSelectedDesc = requireArguments().getBoolean(ARG_SELECTED_IS_DESC, true)
        isDesc = savedInstanceState?.getBoolean(STATE_IS_DESC, isSelectedDesc)
            ?: isSelectedDesc

        adapter = SortByAdapter(
            selectedId = selectedId,
            onItemClick = { position, item ->
                val bundle = Bundle(2).apply {
                    putInt(SORT_TYPE, item.id)
                    putBoolean(IS_DESC, isDesc)
                }
                setFragmentResult(FRAGMENT_RESULT_KEY, bundle)
                dismiss()
            },
            onRadioClick = { isDescending ->
                adapter?.onRadioClick(recyclerView, isDescending)
                isDesc = isDescending
            }
        )
        fillAdapter(adapter!!)

        recyclerView.layoutManager = LinearLayoutManager(requireContext())
        recyclerView.adapter = adapter

        return AlertDialog.Builder(requireActivity())
            .setTitle(R.string.sort_dialog_title)
            .setView(recyclerView)
            .create()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putBoolean(STATE_IS_DESC, isDesc)
    }

    override fun onDestroyView() {
        adapter?.run { items.list.clear() }
        adapter = null
        super.onDestroyView()
    }

    private fun fillAdapter(adapter: SortByAdapter<Int>) {
        adapter.items.add(SortByHeaderHolder.Item(isDesc))

        val dateAdded = SortByHolder.Item(
            text = getString(R.string.watchlist_dialog_sort_by_date_added),
            icon = getDrawable(requireContext(), R.drawable.ic_watchlist_sort_date_added)!!,
            id = WatchlistSort.DATE_ADDED,
        )
        adapter.items.add(dateAdded)

        val releaseDate = SortByHolder.Item(
            text = getString(R.string.watchlist_dialog_sort_by_release_date),
            icon = getDrawable(requireContext(), R.drawable.ic_watchlist_sort_release_date)!!,
            id = WatchlistSort.RELEASE_DATE,
        )
        adapter.items.add(releaseDate)

        val title = SortByHolder.Item(
            text = getString(R.string.watchlist_dialog_sort_by_title),
            icon = getDrawable(requireContext(), R.drawable.ic_watchlist_sort_title)!!,
            id = WatchlistSort.TITLE,
        )
        adapter.items.add(title)

        val rating = SortByHolder.Item(
            text = getString(R.string.watchlist_dialog_sort_by_rating),
            icon = getDrawable(requireContext(), R.drawable.ic_watchlist_sort_rating)!!,
            id = WatchlistSort.RATING,
        )
        adapter.items.add(rating)
    }

}
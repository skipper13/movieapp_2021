package com.nicetas.movieapp3.ui.extended.date

import com.nicetas.movieapp3.base.viewmodel.BaseViewModel
import com.nicetas.movieapp3.base.viewmodel.LiveEvent
import com.nicetas.prefs.Prefs
import com.nicetas.prefs.PrefsKey
import com.nicetas.prefs.PrefsKey.DateFormatOrder.D_M_Y

class DateFormatViewModel(
    private val prefs: Prefs,
) : BaseViewModel() {

    class Format(val order: Int, val separator: String)

    val update = LiveEvent<Format>()
    val onRestoreScreen = LiveEvent<String>()

    private var wasInit = false

    var selectedSeparator = "."

    fun onViewCreated() {
        if (wasInit) {
            onRestoreScreen.setValue(selectedSeparator)
            return
        }
        wasInit = true

        val format = Format(
            order = prefs.getInt(PrefsKey.DateFormatOrder, D_M_Y),
            separator = prefs.getString(PrefsKey.DateFormatSeparator, ".") ?: ".",
        )
        update.setValue(format)
    }

    fun saveFormat(order: Int, separator: String) {
        prefs.edit().commitIO {
            putInt(PrefsKey.DateFormatOrder, order)
            putString(PrefsKey.DateFormatSeparator, separator)
        }
    }

}
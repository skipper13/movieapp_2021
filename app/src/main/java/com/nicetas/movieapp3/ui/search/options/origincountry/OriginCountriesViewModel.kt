package com.nicetas.movieapp3.ui.search.options.origincountry

import androidx.collection.MutableObjectList
import androidx.collection.ObjectList
import androidx.lifecycle.MutableLiveData
import com.nicetas.movieapp3.base.Logger
import com.nicetas.movieapp3.base.viewmodel.BaseViewModel
import com.nicetas.movieapp3.ui.search.options.origincountry.adapter.OriginCountryHolder
import com.nicetas.repos.configuration.ConfigRepository
import com.nicetas.repos.network.LanguageProvider
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import java.text.Collator
import java.util.Locale

class OriginCountriesViewModel(
    private val configRepository: ConfigRepository,
) : BaseViewModel() {

    val showCountries = MutableLiveData<ObjectList<Any>>()

    private var wasInit = false
    private var language = ""

    fun onViewCreated() {
        if (wasInit && language == LanguageProvider.language) {
            return
        }
        wasInit = true
        language = LanguageProvider.language
        getCountries()
    }

    private fun getCountries() {
        disposables += configRepository.getOrReadCountries().map { countries ->

            val items = MutableObjectList<Any>(countries.size)

            val collator = Collator.getInstance(Locale(language))
            val sorted = countries.asList().sortedWith(compareBy(collator) { it.nativeName })

            for (i in sorted.indices) {
                val country = sorted[i]
                items += OriginCountryHolder.Item(country.iso_3166_1, country.nativeName)
            }
            items
        }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ items ->
                showCountries.value = items
            }, { error ->
                Logger.e(error)
            })
    }

}
package com.nicetas.movieapp3.ui.search.options

import android.app.DatePickerDialog
import android.os.Build
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.DatePicker
import androidx.appcompat.app.AlertDialog
import androidx.collection.MutableObjectList
import androidx.core.content.ContextCompat.getColor
import androidx.core.text.color
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.setFragmentResultListener
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.slider.RangeSlider
import com.google.android.material.slider.Slider
import com.nicetas.database.search.SearchKitEntity
import com.nicetas.models.IdName
import com.nicetas.models.extensions.format
import com.nicetas.movieapp3.R
import com.nicetas.movieapp3.base.adapter.ClickDebounce
import com.nicetas.movieapp3.base.states.Lce
import com.nicetas.movieapp3.base.states.ViewState
import com.nicetas.movieapp3.databinding.SearchOptionsBottomSheetBinding
import com.nicetas.movieapp3.ui.search.options.certification.Certification
import com.nicetas.movieapp3.ui.search.options.certification.SearchCertificationDialog
import com.nicetas.movieapp3.ui.search.options.keywords.adapter.KeywordsAdapter
import com.nicetas.movieapp3.ui.search.options.keywords.search.SearchWord
import com.nicetas.movieapp3.ui.search.options.keywords.search.SearchWordDialog
import com.nicetas.movieapp3.ui.search.options.kit.SearchKitsDialog
import com.nicetas.movieapp3.ui.search.options.origincountry.OriginCountriesDialog
import com.nicetas.repos.search.SearchKit
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.time.LocalDate


class SearchOptionsFragment : BottomSheetDialogFragment() {

    companion object {
        private const val FRAGMENT_RESULT_NEED_RELOAD = "FRAGMENT_RESULT_NEED_RELOAD"
        const val FRAGMENT_RESULT_KEY = "SearchOptionsFragment_FRAGMENT_RESULT"

        fun isNeedReload(bundle: Bundle): Boolean {
            return bundle.getBoolean(FRAGMENT_RESULT_NEED_RELOAD, false)
        }
    }

    private var _binding: SearchOptionsBottomSheetBinding? = null
    private val binding
        get() = _binding!!

    private val viewModel: SearchOptionsViewModel by viewModel()

    private var adapterKeywordsWith: KeywordsAdapter? = null
    private var adapterKeywordsWithout: KeywordsAdapter? = null
    private var adapterCompaniesWith: KeywordsAdapter? = null
    private var adapterCompaniesWithout: KeywordsAdapter? = null
    private var starsHelper: StarsHelper? = null
    private var sortByHelper: SortByHelper? = null
    private var tips: SearchOptionsTips? = null
    private var _debounce: ClickDebounce? = null
    private val debounce
        get() = _debounce!!

    private var isBehaviorDraggable: Boolean
        get() = (dialog as BottomSheetDialog).behavior.isDraggable
        set(value) {
            (dialog as BottomSheetDialog).behavior.isDraggable = value
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setFragmentResultListener(SearchKitsDialog.FRAGMENT_RESULT_KEY) { requestKey, bundle ->
            updateViews()
        }
        setFragmentResultListener(SearchSortDialog.FRAGMENT_RESULT_KEY) { requestKey, bundle ->
            val sort = SearchSortDialog.sortBy(bundle)
            viewModel.options.sortBy = sort
            binding.tab1.btnSortBy.text = sortByHelper!!.getButtonText(sort)
        }
        setFragmentResultListener(SearchWordDialog.FRAGMENT_RESULT_KEY) { requestKey, bundle ->
            viewModel.updateKeywords(SearchWordDialog.screenType(bundle))
        }
        setFragmentResultListener(OriginCountriesDialog.FRAGMENT_RESULT_KEY) { requestKey, bundle ->
            viewModel.options.withOriginCountry = OriginCountriesDialog.countryISO(bundle)
            binding.tab2.btnOriginCountry.text = OriginCountriesDialog.countryName(bundle)
                ?: getString(R.string.discover_origin_country)
        }
        setFragmentResultListener(SearchCertificationDialog.FRAGMENT_RESULT_KEY) { requestKey, bundle ->
            val selected = SearchCertificationDialog.selected(bundle)
            val certification = viewModel.options.certification
            certification.clear()
            certification.addAll(selected)
            viewModel.options.certificationCountry = SearchCertificationDialog.country(bundle)
            updateCertificationButton()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = SearchOptionsBottomSheetBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (dialog as BottomSheetDialog).behavior.apply {
            state = BottomSheetBehavior.STATE_EXPANDED
            skipCollapsed = true
        }
        _debounce = ClickDebounce(delay = 200)
        initViewState()
        setupViews()
        setupObservers()
        viewModel.onViewCreated()
    }

    override fun onDestroyView() {
        adapterKeywordsWith?.run { items.list.clear() }
        adapterKeywordsWith = null
        adapterKeywordsWithout?.run { items.list.clear() }
        adapterKeywordsWithout = null
        adapterCompaniesWith?.run { items.list.clear() }
        adapterCompaniesWith = null
        adapterCompaniesWithout?.run { items.list.clear() }
        adapterCompaniesWithout = null
        binding.tab2.rvKeywordsWith.adapter = null
        binding.tab2.rvKeywordsWithout.adapter = null
        binding.tab2.rvCompaniesWith.adapter = null
        binding.tab2.rvCompaniesWithout.adapter = null
        starsHelper = null
        sortByHelper = null
        _debounce = null
        tips?.onDestroyView()
        tips = null
        super.onDestroyView()
        _binding = null
    }

    override fun getTheme() = R.style.TransparentBottomSheetDialog

    private fun initViewState() {
        val viewState = ViewState.Builder(binding.vgScrollView)
            .load()
            .error(onClick = { viewModel.loadGenres() })
            .build()

        viewModel.lce.liveData.observe(viewLifecycleOwner) { state ->
            when (state) {
                is Lce.Loading -> viewState.load()
                is Lce.Content -> viewState.content()
                is Lce.Error -> viewState.error(state.message(requireContext()))
            }
        }
    }

    private fun canScroll(scrollView: NestedScrollView): Boolean {
        val child = scrollView.getChildAt(0)
        if (child != null) {
            return scrollView.height < (child.height + scrollView.paddingTop + scrollView.paddingBottom)
        }
        return false
    }

    private fun updateNestedScroll() {
        _binding?.vgScrollView?.postDelayed({
            _binding?.vgScrollView?.apply {
                isNestedScrollingEnabled = canScroll(binding.vgScrollView)
                requestLayout()
            }
        }, 100)
    }

    private fun blockScrollOnTopEdge() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.R) {
            return
        }
        val onNestedScrollListener = object : NestedScrollView.OnScrollChangeListener {
            override fun onScrollChange(
                v: NestedScrollView,
                scrollX: Int,
                scrollY: Int,
                oldScrollX: Int,
                oldScrollY: Int
            ) {
                isBehaviorDraggable = false
                if (scrollY == 0) {
                    binding.vgScrollView.postDelayed({ isBehaviorDraggable = true }, 300)
                }
            }
        }
        binding.vgScrollView.setOnScrollChangeListener(onNestedScrollListener)
    }

    private fun setupViews() {
        updateNestedScroll()
        blockScrollOnTopEdge()

        binding.tvTab1.setOnClickListener {
            viewModel.currentTab = 1
            updateTabs()
        }
        binding.tvTab2.setOnClickListener {
            viewModel.currentTab = 2
            updateTabs()
        }
        updateTabs()

        binding.tvSearchKits.setOnClickListener {
            if (debounce.canClick()) {
                val dialog = SearchKitsDialog.newInstance()
                dialog.show(parentFragmentManager, null)
            }
        }

        val options = viewModel.options

        starsHelper = StarsHelper(
            binding = binding.tab1.includeStars,
            updateMin = { star: Double? ->
                options.voteAverageMin = star
            },
            updateMax = { star: Double? ->
                options.voteAverageMax = star
            },
        )
        starsHelper!!.update(
            starMinNew = options.voteAverageMin,
            starMaxNew = options.voteAverageMax,
        )

        setupGenres(options)
        setupReleaseDates(options)
        setupRuntime(options)
        setupVotesCount(options)
        setupSortBy(options)
        setupKeywords(options)

        binding.btnDiscoverMovies.setOnClickListener {
            if (debounce.canClick().not()) {
                return@setOnClickListener
            }
            viewModel.updateSearchKitDiscover()
            val bundle = Bundle(1).apply {
                putBoolean(FRAGMENT_RESULT_NEED_RELOAD, true)
            }
            setFragmentResult(FRAGMENT_RESULT_KEY, bundle)
            dismiss()
        }

        binding.tab2.btnOriginCountry.setOnClickListener {
            if (debounce.canClick()) {
                val dialog = OriginCountriesDialog.newInstance(options.withOriginCountry)
                dialog.show(parentFragmentManager, null)
            }
        }

        binding.tab2.btnCertification.setOnClickListener {
            if (debounce.canClick()) {
                val dialog = SearchCertificationDialog.newInstance(
                    selected = viewModel.options.certification,
                    certificationCountry = viewModel.options.certificationCountry,
                )
                dialog.show(parentFragmentManager, null)
            }
        }
    }

    private fun updateTabs() {
        if (viewModel.currentTab == 1) {
            binding.tab1.root.isVisible = true
            binding.tab2.root.isVisible = false
            binding.tvTab1.isClickable = false
            binding.tvTab2.isClickable = true
            binding.tvTab1.setTextColor(getColor(requireContext(), R.color.white))
            binding.tvTab2.setTextColor(getColor(requireContext(), R.color.grey_d9d9d9))
            binding.vTab1Selected.isVisible = true
            binding.vTab2Selected.isInvisible = true
        } else {
            binding.tab1.root.isVisible = false
            binding.tab2.root.isVisible = true
            binding.tvTab1.isClickable = true
            binding.tvTab2.isClickable = false
            binding.tvTab1.setTextColor(getColor(requireContext(), R.color.grey_d9d9d9))
            binding.tvTab2.setTextColor(getColor(requireContext(), R.color.white))
            binding.vTab1Selected.isInvisible = true
            binding.vTab2Selected.isVisible = true
        }
    }

    private fun updateViews() {
        updateReleaseDatesViews()

        viewModel.updateKeywords(type = SearchWord.KEYWORDS_WITH)
        viewModel.updateKeywords(type = SearchWord.KEYWORDS_WITHOUT)
        viewModel.updateKeywords(type = SearchWord.COMPANIES_WITH)
        viewModel.updateKeywords(type = SearchWord.COMPANIES_WITHOUT)

        val options = viewModel.options

        starsHelper?.update(
            starMinNew = options.voteAverageMin,
            starMaxNew = options.voteAverageMax,
        )

        binding.tvSearchKits.text = when (options.id) {
            SearchKitEntity.ID_POPULAR -> getString(R.string.search_kits_popular)
            SearchKitEntity.ID_TOP -> getString(R.string.search_kits_top)
            SearchKitEntity.ID_UPCOMING -> getString(R.string.search_kits_upcoming)
            else -> options.title
        }

        binding.tab1.btnSortBy.text = sortByHelper!!.getButtonText(options.sortBy)

        binding.tab1.sliderVotesCount.value = options.voteCountMin.toFloat()

        val default = resources.getIntArray(R.array.initial_runtime_slider_values)
        binding.tab1.sliderRuntime.values = listOf(
            options.withRuntimeMinimum?.toFloat() ?: default[0].toFloat(),
            options.withRuntimeMaximum?.toFloat() ?: default[1].toFloat()
        )

        updateCertificationButton()
        updateOriginCountryButton(options)
    }

    private fun updateCertificationButton() {
        val certification = viewModel.options.certification

        if (certification.isEmpty()) {
            binding.tab2.btnCertification.text = getString(R.string.discover_certification)

        } else {
            val country = viewModel.options.certificationCountry ?: return
            val sortedNames = Certification.getById(country).sortedServerNames()

            val certificationSorted = MutableObjectList<String>(sortedNames.size)

            for (i in sortedNames.indices) {
                val rating = sortedNames[i]
                if (certification.any { it == rating }) {
                    certificationSorted.add(rating)
                }
            }
            binding.tab2.btnCertification.text = SpannableStringBuilder()
                .append(getString(R.string.discover_certification))
                .append(": ")
                .color(getColor(requireContext(), R.color.yellow_CAC683)) {
                    append(certificationSorted.joinToString { it })
                }
        }
    }

    private fun updateOriginCountryButton(options: SearchKit) {
        val text = when (val iso_3166_1 = options.withOriginCountry) {
            null -> getString(R.string.discover_origin_country)
            else -> viewModel.findCountryName(iso_3166_1) ?: iso_3166_1
        }
        binding.tab2.btnOriginCountry.text = text
    }

    private fun setupKeywords(options: SearchKit) {
        adapterKeywordsWith = KeywordsAdapter(
            debounce = debounce,
            onDeleteClick = { id ->
                viewModel.removeKeywordWith(id)
            },
            onAddClick = {
                val dialog = SearchWordDialog.newInstance(type = SearchWord.KEYWORDS_WITH)
                dialog.show(parentFragmentManager, null)
            },
            onSetupClick = {
                val choiceItems = arrayOf(
                    getString(R.string.setup_with_keywords_and),
                    getString(R.string.setup_with_keywords_or)
                )
                var selectedIndex = if (options.isWithKeywordsOperatorOR) 1 else 0
                AlertDialog.Builder(requireContext())
                    .setTitle(R.string.discover_settings_and_or)
                    .setSingleChoiceItems(choiceItems, selectedIndex) { _, index ->
                        selectedIndex = index
                    }
                    .setPositiveButton(android.R.string.ok) { _, _ ->
                        options.isWithKeywordsOperatorOR = selectedIndex == 1
                    }
                    .show()
            }
        )
        binding.tab2.rvKeywordsWith.adapter = adapterKeywordsWith

        adapterKeywordsWithout = KeywordsAdapter(
            debounce = debounce,
            onDeleteClick = { id ->
                viewModel.removeKeywordWithout(id)
            },
            onAddClick = {
                val dialog = SearchWordDialog.newInstance(type = SearchWord.KEYWORDS_WITHOUT)
                dialog.show(parentFragmentManager, null)
            },
            onSetupClick = { /* no-op */ }
        )
        binding.tab2.rvKeywordsWithout.adapter = adapterKeywordsWithout

        adapterCompaniesWith = KeywordsAdapter(
            debounce = debounce,
            onDeleteClick = { id ->
                viewModel.removeCompanyWith(id)
            },
            onAddClick = {
                val dialog = SearchWordDialog.newInstance(type = SearchWord.COMPANIES_WITH)
                dialog.show(parentFragmentManager, null)
            },
            onSetupClick = {
                val choiceItems = arrayOf(
                    getString(R.string.setup_with_companies_and),
                    getString(R.string.setup_with_companies_or)
                )
                var selectedIndex = if (options.isWithCompaniesOperatorOR) 1 else 0
                AlertDialog.Builder(requireContext())
                    .setTitle(R.string.discover_settings_and_or)
                    .setSingleChoiceItems(choiceItems, selectedIndex) { _, index ->
                        selectedIndex = index
                    }
                    .setPositiveButton(android.R.string.ok) { _, _ ->
                        options.isWithCompaniesOperatorOR = selectedIndex == 1
                    }
                    .show()
            }
        )
        binding.tab2.rvCompaniesWith.adapter = adapterCompaniesWith

        adapterCompaniesWithout = KeywordsAdapter(
            debounce = debounce,
            onDeleteClick = { id ->
                viewModel.removeCompanyWithout(id)
            },
            onAddClick = {
                val dialog = SearchWordDialog.newInstance(type = SearchWord.COMPANIES_WITHOUT)
                dialog.show(parentFragmentManager, null)
            },
            onSetupClick = { /* no-op */ }
        )
        binding.tab2.rvCompaniesWithout.adapter = adapterCompaniesWithout

        binding.tab2.tvKeywordsWith.setOnClickListener {
            if (debounce.canClick() && options.withKeywords.isNotEmpty()) {
                viewModel.showRemoveKeywords(type = SearchWord.KEYWORDS_WITH)
            }
        }
        binding.tab2.tvKeywordsWithout.setOnClickListener {
            if (debounce.canClick() && options.withoutKeywords.isNotEmpty()) {
                viewModel.showRemoveKeywords(type = SearchWord.KEYWORDS_WITHOUT)
            }
        }
        binding.tab2.tvCompaniesWith.setOnClickListener {
            if (debounce.canClick() && options.withCompanies.isNotEmpty()) {
                viewModel.showRemoveKeywords(type = SearchWord.COMPANIES_WITH)
            }
        }
        binding.tab2.tvCompaniesWithout.setOnClickListener {
            if (debounce.canClick() && options.withoutCompanies.isNotEmpty()) {
                viewModel.showRemoveKeywords(type = SearchWord.COMPANIES_WITHOUT)
            }
        }
    }

    private fun setupGenres(options: SearchKit) {
        binding.tab1.btnWithGenres.setOnClickListener {
            if (debounce.canClick().not()) {
                return@setOnClickListener
            }
            showMultiChoiceAlert(
                items = viewModel.genres,
                selectedIds = options.withGenresIds,
                title = getString(R.string.discover_with),
            )
        }
        binding.tab1.btnWithGenresSettings.setOnClickListener {
            if (debounce.canClick().not()) {
                return@setOnClickListener
            }
            val choiceItems = arrayOf(
                getString(R.string.setup_with_genres_and),
                getString(R.string.setup_with_genres_or)
            )
            var selectedIndex = if (viewModel.options.isWithGenresOperatorOR) 1 else 0
            AlertDialog.Builder(requireContext())
                .setTitle(R.string.discover_settings_and_or)
                .setSingleChoiceItems(choiceItems, selectedIndex) { _, index ->
                    selectedIndex = index
                }
                .setPositiveButton(android.R.string.ok) { _, _ ->
                    viewModel.options.isWithGenresOperatorOR = selectedIndex == 1
                }
                .show()
        }
        binding.tab1.btnWithoutGenres.setOnClickListener {
            if (debounce.canClick().not()) {
                return@setOnClickListener
            }
            showMultiChoiceAlert(
                items = viewModel.genres,
                selectedIds = options.withoutGenresIds,
                title = getString(R.string.discover_without),
            )
        }
    }

    private fun setupSortBy(options: SearchKit) {
        sortByHelper = SortByHelper(requireContext())

        binding.tab1.btnSortBy.setOnClickListener {
            if (debounce.canClick()) {
                val dialog = SearchSortDialog.newInstance(sortBy = options.sortBy)
                dialog.show(parentFragmentManager, null)
            }
        }
    }

    // https://github.com/material-components/material-components-android/blob/master/docs/components/Slider.md
    private fun setupRuntime(options: SearchKit) {
        val default = resources.getIntArray(R.array.initial_runtime_slider_values)
        val defaultMax = default[1]

        // Set separation to avoid setting of max by user to "0".
        // Otherwise, we need to call "getValues()" which
        // will return "new ArrayList<>(values);" on each LabelFormatter call.
        binding.tab1.sliderRuntime.setMinSeparationValue(10F)

        binding.tab1.sliderRuntime.setLabelFormatter { value: Float ->
            val runtime = value.toInt()
            when {
                runtime == defaultMax -> {
                    "\u221E" // infinity symbol
                }
                runtime > 59 -> {
                    val hours = runtime / 60
                    val min = runtime - (hours * 60)
                    val minText = if (min < 10) "0$min" else min
                    "$hours:$minText"
                }
                else -> {
                    runtime.toString()
                }
            }
        }

        val sliderTouchListener = object : RangeSlider.OnSliderTouchListener {
            override fun onStartTrackingTouch(slider: RangeSlider) = Unit

            override fun onStopTrackingTouch(slider: RangeSlider) {
                val values = slider.values

                val min = values[0].toInt()
                options.withRuntimeMinimum = if (min == 0) null else min

                val max = values[1].toInt()
                options.withRuntimeMaximum = if (max == defaultMax) null else max
            }
        }
        binding.tab1.sliderRuntime.addOnSliderTouchListener(sliderTouchListener)
    }

    private fun setupVotesCount(options: SearchKit) {
        binding.tab1.sliderVotesCount.setLabelFormatter { value: Float ->
            value.toInt().toString()
        }
        binding.tab1.sliderVotesCount.addOnSliderTouchListener(object :
            Slider.OnSliderTouchListener {
            override fun onStartTrackingTouch(slider: Slider) = Unit

            override fun onStopTrackingTouch(slider: Slider) {
                options.voteCountMin = slider.value.toInt()
            }
        })
    }

    private fun setupReleaseDates(options: SearchKit) {
        binding.tab1.btnReleaseDateFrom.setOnClickListener {
            if (debounce.canClick().not()) {
                return@setOnClickListener
            }
            val listener = object : DatePickerDialog.OnDateSetListener {
                override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
                    options.releaseDateFrom = LocalDate.of(year, month + 1, dayOfMonth)
                    updateReleaseDatesViews()
                }
            }
            val from = options.releaseDateFrom ?: LocalDate.of(2000, 1, 1)
            val dialog = DatePickerDialog(
                requireContext(),
                R.style.DarkAlertDialogStyle,
                listener,
                from.year,
                from.monthValue - 1,
                from.dayOfMonth
            )
            dialog.datePicker.touchables[0].performClick()
            dialog.show()
        }

        binding.tab1.btnReleaseDateUntil.setOnClickListener {
            if (debounce.canClick().not()) {
                return@setOnClickListener
            }
            val listener = object : DatePickerDialog.OnDateSetListener {
                override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
                    options.releaseDateUntil = LocalDate.of(year, month + 1, dayOfMonth)
                    updateReleaseDatesViews()
                }
            }
            val lastDayOfThisYear = LocalDate.now().withMonth(12).withDayOfMonth(31)
            val until = options.releaseDateUntil ?: lastDayOfThisYear
            val dialog = DatePickerDialog(
                requireContext(),
                R.style.DarkAlertDialogStyle,
                listener,
                until.year,
                until.monthValue - 1,
                until.dayOfMonth
            )
            dialog.datePicker.touchables[0].performClick()
            dialog.show()
        }

        binding.tab1.vReleaseDateFromReset.setOnClickListener {
            if (debounce.canClick()) {
                options.releaseDateFrom = null
                updateReleaseDatesViews()
            }
        }
        binding.tab1.vReleaseDateUntilReset.setOnClickListener {
            if (debounce.canClick()) {
                options.releaseDateUntil = null
                updateReleaseDatesViews()
            }
        }
    }

    private fun updateReleaseDatesViews() {
        binding.tab1.ivReleaseDateFromReset.isVisible = false
        binding.tab1.vReleaseDateFromReset.isVisible = false
        binding.tab1.ivReleaseDateUntilReset.isVisible = false
        binding.tab1.vReleaseDateUntilReset.isVisible = false

        val options = viewModel.options

        val text: String

        val order = viewModel.dateOrder()
        val separator = viewModel.dateSeparator()

        if (options.releaseDateFrom != null && options.releaseDateUntil != null) {
            val from = options.releaseDateFrom.format(order, separator)
            val to = options.releaseDateUntil.format(order, separator)
            text = getString(R.string.discover_release_dates_from_to, from, to)

            binding.tab1.ivReleaseDateFromReset.isVisible = true
            binding.tab1.vReleaseDateFromReset.isVisible = true
            binding.tab1.ivReleaseDateUntilReset.isVisible = true
            binding.tab1.vReleaseDateUntilReset.isVisible = true

        } else if (options.releaseDateFrom != null) {
            val from = options.releaseDateFrom.format(order, separator)
            text = getString(R.string.discover_release_dates_from, from)

            binding.tab1.ivReleaseDateFromReset.isVisible = true
            binding.tab1.vReleaseDateFromReset.isVisible = true

        } else if (options.releaseDateUntil != null) {
            val until = options.releaseDateUntil.format(order, separator)
            text = getString(R.string.discover_release_dates_until, until)

            binding.tab1.ivReleaseDateUntilReset.isVisible = true
            binding.tab1.vReleaseDateUntilReset.isVisible = true

        } else {
            text = getString(R.string.discover_release_dates)
        }

        binding.tab1.tvReleaseDates.text = text
    }

    private fun showMultiChoiceAlert(
        items: List<IdName>,
        selectedIds: HashSet<Int>,
        title: CharSequence,
        onOkClick: (() -> Unit)? = null,
    ) {
        val isSelectedArray = BooleanArray(items.size)
        for (i in 0 until items.size) {
            isSelectedArray[i] = selectedIds.contains(items[i].identifier())
        }

        val prevSelectedIds = HashSet(selectedIds)

        val multiChoiceItems = items.map { it.iName() }.toTypedArray()

        val builder = AlertDialog.Builder(requireContext())
        builder
            .setTitle(title)
            .setMultiChoiceItems(multiChoiceItems, isSelectedArray) { _, index, isChecked ->
                val id = items[index].identifier()
                if (isChecked) {
                    selectedIds.add(id)
                } else {
                    selectedIds.remove(id)
                }
            }
            .setPositiveButton(android.R.string.ok) { _, _ ->
                onOkClick?.invoke()
            }
            .setNegativeButton(android.R.string.cancel) { _, _ ->
                selectedIds.clear()
                selectedIds.addAll(prevSelectedIds)
            }
            .setNeutralButton(R.string.reset_selected, null)
            .setOnCancelListener {
                selectedIds.clear()
                selectedIds.addAll(prevSelectedIds)
            }
        val dialog = builder.create()
        dialog.show()

        dialog.getButton(AlertDialog.BUTTON_NEUTRAL).setOnClickListener {
            selectedIds.clear()

            for (pos in 0 until dialog.listView.adapter.count) {
                isSelectedArray[pos] = false
                dialog.listView.setItemChecked(pos, false)
            }
        }
    }

    private fun setupObservers() {
        viewModel.updateViews.observe(viewLifecycleOwner) {
            updateViews()
        }
        viewModel.showTips.observe(viewLifecycleOwner) {
            // ".post" to wait for locations of views on screen
            binding.root.post {
                tips = SearchOptionsTips(
                    fragment = this,
                    frameLayoutContainer = binding.root,
                    tvSearchKits = binding.tvSearchKits,
                    vgStars = binding.tab1.includeStars.root,
                    onTipsEnd = {
                        viewModel.setTipsShown()
                    }
                )
                tips!!.showTips()
            }
        }
        viewModel.showRemoveKeywordsDialog.observe(viewLifecycleOwner) { result ->
            when (result.type) {
                SearchWord.KEYWORDS_WITH -> showMultiChoiceAlert(
                    items = result.items,
                    selectedIds = viewModel.options.withKeywords,
                    title = getString(R.string.discover_keywords_with),
                    onOkClick = {
                        viewModel.updateKeywords(type = SearchWord.KEYWORDS_WITH)
                    },
                )
                SearchWord.KEYWORDS_WITHOUT -> showMultiChoiceAlert(
                    items = result.items,
                    selectedIds = viewModel.options.withoutKeywords,
                    title = getString(R.string.discover_keywords_without),
                    onOkClick = {
                        viewModel.updateKeywords(type = SearchWord.KEYWORDS_WITHOUT)
                    },
                )
                SearchWord.COMPANIES_WITH -> showMultiChoiceAlert(
                    items = result.items,
                    selectedIds = viewModel.options.withCompanies,
                    title = getString(R.string.discover_companies_with),
                    onOkClick = {
                        viewModel.updateKeywords(type = SearchWord.COMPANIES_WITH)
                    },
                )
                SearchWord.COMPANIES_WITHOUT -> showMultiChoiceAlert(
                    items = result.items,
                    selectedIds = viewModel.options.withoutCompanies,
                    title = getString(R.string.discover_companies_without),
                    onOkClick = {
                        viewModel.updateKeywords(type = SearchWord.COMPANIES_WITHOUT)
                    },
                )
            }
        }
        viewModel.updateKeywordsWith.observe(viewLifecycleOwner) { items ->
            adapterKeywordsWith?.update(items)
        }
        viewModel.updateKeywordsWithout.observe(viewLifecycleOwner) { items ->
            adapterKeywordsWithout?.update(items)
        }
        viewModel.removeKeywordWith.observe(viewLifecycleOwner) { id ->
            adapterKeywordsWith?.removeById(id)
        }
        viewModel.removeKeywordWithout.observe(viewLifecycleOwner) { id ->
            adapterKeywordsWithout?.removeById(id)
        }
        viewModel.updateCompaniesWith.observe(viewLifecycleOwner) { items ->
            adapterCompaniesWith?.update(items)
        }
        viewModel.updateCompaniesWithout.observe(viewLifecycleOwner) { items ->
            adapterCompaniesWithout?.update(items)
        }
        viewModel.removeCompanyWith.observe(viewLifecycleOwner) { id ->
            adapterCompaniesWith?.removeById(id)
        }
        viewModel.removeCompanyWithout.observe(viewLifecycleOwner) { id ->
            adapterCompaniesWithout?.removeById(id)
        }
    }

}
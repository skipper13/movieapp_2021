package com.nicetas.movieapp3.ui.search.options.keywords.search.adapter

import android.view.ViewGroup
import androidx.collection.ObjectList
import androidx.core.view.postDelayed
import androidx.recyclerview.widget.RecyclerView
import com.nicetas.movieapp3.base.adapter.BaseDiffUtilCallback
import com.nicetas.movieapp3.base.adapter.ClickDebounce
import com.nicetas.movieapp3.base.adapter.AdapterItems
import com.nicetas.movieapp3.ui.search.options.keywords.search.SearchWordViewModel

class SearchWordAdapter(
    private val onKeywordClick: WordHolder.Callback,
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        private const val TYPE_KEYWORD = 0
    }

    class DiffUtilCallback() : BaseDiffUtilCallback<Any>() {

        override fun areItemsTheSame(oldItem: Any, newItem: Any): Boolean {
            if (oldItem is WordHolder.Item && newItem is WordHolder.Item) {
                return oldItem.id == newItem.id
            }
            return false
        }

        override fun areContentsTheSame(oldItem: Any, newItem: Any): Boolean {
            if (oldItem is WordHolder.Item && newItem is WordHolder.Item) {
                return oldItem.text == newItem.text
                        && oldItem.isSelected == newItem.isSelected
            }
            return false
        }
    }

    private val debounce = ClickDebounce()
    private val diffUtilCallback = DiffUtilCallback()
    val items = AdapterItems<Any>(this)

    fun update(newItems: ObjectList<Any>) {
        items.update(newItems, diffUtilCallback)
    }

    fun replace(recyclerView: RecyclerView?, replace: SearchWordViewModel.Replace) {
        recyclerView?.isVerticalScrollBarEnabled = false
        items.replace(replace.item, replace.position, 0)
        recyclerView?.postDelayed(200) { recyclerView?.isVerticalScrollBarEnabled = true }
    }

    override fun getItemCount(): Int = items.list.size

    override fun getItemViewType(position: Int): Int {
        return when (items.list[position]) {
            is WordHolder.Item -> TYPE_KEYWORD
            else -> throw IllegalStateException("item is null for position = $position")
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_KEYWORD -> WordHolder(
                parent = parent,
                debounce = debounce,
                callback = onKeywordClick,
            )
            else -> throw IllegalStateException("Unknown holder type")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = items.list[position]
        when (holder) {
            is WordHolder -> holder.bind(item as WordHolder.Item)
        }
    }

}
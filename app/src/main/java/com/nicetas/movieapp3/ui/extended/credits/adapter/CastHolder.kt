package com.nicetas.movieapp3.ui.extended.credits.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.content.res.AppCompatResources.getDrawable
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.nicetas.movieapp3.R
import com.nicetas.movieapp3.base.adapter.ClickDebounce
import com.nicetas.movieapp3.databinding.ItemCastBinding
import com.nicetas.movieapp3.ui.ImageUrl

class CastHolder(
    parent: ViewGroup,
    inflater: LayoutInflater,
    private var binding: ItemCastBinding = ItemCastBinding.inflate(inflater, parent, false),
    debounce: ClickDebounce,
    callback: Callback,
) : RecyclerView.ViewHolder(binding.root) {

    fun interface Callback {
        fun onCastClick(actorId: Int, name: String)
    }

    class Item(
        val actorId: Int,
        val name: String,
        val character: String,
        val profilePath: String?,
    )

    private lateinit var item: Item

    init {
        binding.root.setOnClickListener {
            if (debounce.canClick()) {
                callback.onCastClick(actorId = item.actorId, name = item.name)
            }
        }
    }

    fun bind(item: Item) {
        this.item = item
        val context = itemView.context

        binding.tvName.text = item.name
        binding.tvCharacter.text = item.character

        Glide.with(context)
            .load(ImageUrl.w342(item.profilePath))
            .transition(DrawableTransitionOptions.withCrossFade())
            .placeholder(getDrawable(context, R.drawable.placeholder_135x200))
            .centerCrop()
            .into(binding.ivPhoto)
    }

}
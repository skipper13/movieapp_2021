package com.nicetas.movieapp3.ui.search.options.certification.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.appcompat.content.res.AppCompatResources.getDrawable
import androidx.recyclerview.widget.RecyclerView
import com.nicetas.movieapp3.base.adapter.ClickDebounce
import com.nicetas.movieapp3.databinding.ItemClassificationBinding

class ClassificationHolder(
    parent: ViewGroup,
    private var binding: ItemClassificationBinding = ItemClassificationBinding.inflate(
        LayoutInflater.from(parent.context),
        parent,
        false
    ),
    debounce: ClickDebounce,
    private val callback: Callback,
) : RecyclerView.ViewHolder(binding.root) {

    interface Callback {
        fun isSelected(item: Item): Boolean
        fun onItemClick(position: Int, item: Item)
    }

    class Item(
        val serverName: String,
        @StringRes val descriptionRes: Int,
        @DrawableRes val iconRes: Int,
    )

    private lateinit var item: Item
    private val context: Context
        get() = itemView.context

    init {
        binding.root.setOnClickListener {
            if (debounce.canClick()) {
                callback.onItemClick(bindingAdapterPosition, item)
            }
        }
    }

    fun bind(item: Item) {
        this.item = item
        binding.ivIcon.setImageDrawable(getDrawable(context, item.iconRes))
        binding.tvTitle.text = context.getString(item.descriptionRes)
        binding.checkBox.isChecked = callback.isSelected(item)
    }
}
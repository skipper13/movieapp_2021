package com.nicetas.movieapp3.ui.fullscreen

import android.Manifest
import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ObjectAnimator
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup.MarginLayoutParams
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.view.menu.MenuBuilder
import androidx.collection.MutableObjectList
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.MenuProvider
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat
import androidx.core.view.isVisible
import androidx.core.view.updateLayoutParams
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.widget.ViewPager2.OnPageChangeCallback
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.google.android.material.snackbar.Snackbar
import com.nicetas.movieapp3.R
import com.nicetas.movieapp3.base.ImageSaveHelper
import com.nicetas.movieapp3.base.extensions.snackBarTMDB
import com.nicetas.movieapp3.base.extensions.statusBarHeight
import com.nicetas.movieapp3.base.fragment.BaseFragment
import com.nicetas.movieapp3.databinding.FragmentFullscreenImagesBinding
import com.nicetas.movieapp3.ui.ImageUrl
import com.nicetas.movieapp3.ui.fullscreen.adapter.FullscreenImagesAdapter
import org.koin.androidx.viewmodel.ext.android.viewModel

class FullscreenImagesFragment : BaseFragment(R.layout.fragment_fullscreen_images) {

    companion object {
        private const val ARG_IMAGE_SIZE = "ARG_IMAGE_SIZE"
        private const val ARG_IMAGE_PATH = "ARG_IMAGE_PATH"
        private const val ARG_POSITION = "ARG_POSITION"
        private const val ARG_TITLE = "ARG_TITLE"
        private const val DEFAULT_POS = 0

        fun args(
            title: String,
            imageSize: String,
            images: List<String>,
            position: Int = DEFAULT_POS
        ) = Bundle(4).apply {
            putString(ARG_TITLE, title)
            putString(ARG_IMAGE_SIZE, imageSize)
            putStringArrayList(ARG_IMAGE_PATH, ArrayList(images))
            putInt(ARG_POSITION, position)
        }
    }

    private var _binding: FragmentFullscreenImagesBinding? = null
    private val binding
        get() = _binding!!

    private val viewModel: FullscreenImagesViewModel by viewModel()

    private var adapter: FullscreenImagesAdapter? = null
    private var isSystemBarsHidden = false
    private val imageSaveHelper = ImageSaveHelper()
    private val animators = MutableObjectList<ObjectAnimator>(4)

    private val args: Bundle
        get() = requireArguments()

    private val imageSize: String by lazy {
        args.getString(ARG_IMAGE_SIZE, "")
    }
    private val images: ArrayList<String> by lazy {
        args.getStringArrayList(ARG_IMAGE_PATH)!!
    }
    private val title: String by lazy {
        args.getString(ARG_TITLE, "")
    }

    private val imageSavePermissionLauncher =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { isGranted ->
            if (isGranted) {
                saveImage()
            } else {
                showReqPermissionAlert()
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.onCreate(
            initialPosition = args.getInt(ARG_POSITION, DEFAULT_POS)
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentFullscreenImagesBinding.bind(view)
        setupOptionsMenu()
        setupViews()
        isBottomNavigationLockedHidden = true
        isBottomNavigationVisible = false
    }

    override fun onDestroyView() {
        animators.forEach { it.removeAllListeners() }
        animators.clear()
        showSystemBars()
        adapter = null
        binding.viewPager.adapter = null
        isBottomNavigationLockedHidden = false
        isBottomNavigationVisible = true
        super.onDestroyView()
        _binding = null
    }

    private fun setupOptionsMenu() {
        requireActivity().addMenuProvider(object : MenuProvider {
            override fun onCreateMenu(menu: Menu, menuInflater: MenuInflater) {
                menuInflater.inflate(R.menu.menu_fullscreen, menu)
                // noinspection RestrictedApi
                if (menu is MenuBuilder) {
                    menu.setOptionalIconsVisible(true)
                }
            }

            override fun onMenuItemSelected(menuItem: MenuItem): Boolean {
                return when (menuItem.itemId) {
                    R.id.action_share -> {
                        selectImageSize { size, url ->
                            val sendIntent = Intent().apply {
                                action = Intent.ACTION_SEND
                                putExtra(Intent.EXTRA_TEXT, url)
                                type = "text/plain"
                            }
                            val shareIntent = Intent.createChooser(sendIntent, null)
                            startActivity(shareIntent)
                        }
                        true
                    }
                    R.id.action_save -> {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                            && Build.VERSION.SDK_INT < Build.VERSION_CODES.Q
                        ) {
                            saveImageWithPermission()
                        } else {
                            saveImage()
                        }
                        true
                    }
                    else -> false
                }
            }
        }, viewLifecycleOwner, Lifecycle.State.RESUMED)
    }

    private fun saveImageWithPermission() {
        when {
            ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) == PackageManager.PERMISSION_GRANTED -> {
                saveImage()
            }
            ActivityCompat.shouldShowRequestPermissionRationale(
                requireActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) -> {
                showReqPermissionAlert()
            }
            else -> {
                imageSavePermissionLauncher.launch(Manifest.permission.WRITE_EXTERNAL_STORAGE)
            }
        }
    }

    private fun showReqPermissionAlert() {
        AlertDialog.Builder(requireContext())
            .setTitle(R.string.fullscreen_save_permission_required_title)
            .setMessage(R.string.fullscreen_save_permission_required_message)
            .setPositiveButton(R.string.fullscreen_save_permission_required_grant) { _, _ ->
                imageSavePermissionLauncher.launch(Manifest.permission.WRITE_EXTERNAL_STORAGE)
            }
            .setNegativeButton(android.R.string.cancel, null)
            .show()
    }

    private fun saveImage() {
        selectImageSize { size, url ->
            saveImage(size = size, url = url)
        }
    }

    private fun saveImage(size: String, url: String) {
        Glide.with(requireContext())
            .asBitmap()
            .load(url)
            .into(object : CustomTarget<Bitmap>() {
                override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                    val imageId = images[viewModel.position]
                        .replaceAfter('.', "")
                        .filter { it.isLetterOrDigit() }

                    val uri = imageSaveHelper.saveImage(
                        context = requireContext(),
                        bitmap = resource,
                        folderName = "MovieApp3",
                        fileName = "${imageId}_${size}",
                    )
                    binding.root.snackBarTMDB(
                        text = if (uri != null) {
                            getString(R.string.fullscreen_save_success)
                        } else {
                            getString(R.string.fullscreen_save_error)
                        },
                        duration = Snackbar.LENGTH_SHORT
                    ).show()
                }

                override fun onLoadCleared(placeholder: Drawable?) = Unit
            })
    }

    private fun selectImageSize(urlSelected: (size: String, url: String) -> Unit) {
        val choiceItems = arrayOf("original", "780", "500", "400", "300")
        var selectedIndex = 0
        AlertDialog.Builder(requireContext())
            .setTitle(R.string.fullscreen_share_title)
            .setSingleChoiceItems(choiceItems, selectedIndex) { _, index ->
                selectedIndex = index
            }
            .setPositiveButton(android.R.string.ok) { _, _ ->
                val size = when (selectedIndex) {
                    0 -> "original"
                    1 -> "w780"
                    2 -> "w500"
                    3 -> "w400"
                    else -> "w300"
                }
                val url = StringBuilder()
                    .append(ImageUrl.baseUrl)
                    .append(size)
                    .append(images[viewModel.position])
                    .toString()
                urlSelected(size, url)
            }
            .setNegativeButton(android.R.string.cancel, null)
            .show()
    }

    private fun setupViews() {
        setStatusBarColor(Color.TRANSPARENT)
        setToolbarTitle(title)
        updateSubtitle(viewModel.position)

        ViewCompat.setOnApplyWindowInsetsListener(binding.root) { _, insets ->
            val statusBarHeight = insets.statusBarHeight()
            binding.toolbar.updateLayoutParams<MarginLayoutParams> {
                topMargin = statusBarHeight
            }
            insets
        }

        val showToolbarOverlayAnim = showToolbarAnim(binding.vToolbarBlackOverlay)
        val showToolbarAnim = showToolbarAnim(binding.toolbar)
        val hideToolbarOverlayAnim = hideToolbarAnim(binding.vToolbarBlackOverlay)
        val hideToolbarAnim = hideToolbarAnim(binding.toolbar)

        adapter = FullscreenImagesAdapter(
            images = images,
            imageSize = imageSize,
            onImageClick = {
                if (isSystemBarsHidden) {
                    isSystemBarsHidden = false
                    showSystemBars()
                    showToolbarOverlayAnim.start()
                    showToolbarAnim.start()
                } else {
                    isSystemBarsHidden = true
                    hideSystemBars()
                    hideToolbarOverlayAnim.start()
                    hideToolbarAnim.start()
                }
            },
        )
        binding.viewPager.adapter = adapter
        binding.viewPager.setCurrentItem(viewModel.position, false)
        binding.viewPager.registerOnPageChangeCallback(object : OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                viewModel.position = position
                updateSubtitle(position)
            }
        })
    }

    private fun updateSubtitle(position: Int) {
        if (images.size < 2) {
            return
        }
        val text = getString(R.string.fullscreen_subtitle_from, position + 1, images.size)
        binding.toolbar.subtitle = text
    }

    // https://stackoverflow.com/a/64828028
    private fun getInsetsController(): WindowInsetsControllerCompat? {
        val activity = activity ?: return null
        return WindowInsetsControllerCompat(activity.window, activity.window.decorView)
    }

    private fun hideSystemBars() {
        val controller = getInsetsController()
        controller?.systemBarsBehavior =
            WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
        controller?.hide(WindowInsetsCompat.Type.systemBars())
    }

    private fun showSystemBars() {
        val controller = getInsetsController()
        controller?.systemBarsBehavior = WindowInsetsControllerCompat.BEHAVIOR_DEFAULT
        controller?.show(WindowInsetsCompat.Type.systemBars())
    }

    private fun hideToolbarAnim(view: View) = toolbarAnim(view, 1f, 0f, View.GONE)
    private fun showToolbarAnim(view: View) = toolbarAnim(view, 0f, 1f, View.VISIBLE)

    private fun toolbarAnim(view: View, from: Float, to: Float, endVis: Int): ObjectAnimator {
        val animator = ObjectAnimator.ofFloat(view, View.ALPHA, from, to)
        animator.duration = 300
        animator.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationStart(animation: Animator) {
                view.isVisible = true
            }

            override fun onAnimationEnd(animation: Animator) {
                view.visibility = endVis
            }
        })
        animators += animator
        return animator
    }

}
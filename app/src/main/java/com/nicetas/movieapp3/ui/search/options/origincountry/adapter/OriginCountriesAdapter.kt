package com.nicetas.movieapp3.ui.search.options.origincountry.adapter

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.nicetas.movieapp3.base.adapter.ClickDebounce
import com.nicetas.movieapp3.base.adapter.AdapterItems
import me.zhanghai.android.fastscroll.PopupTextProvider

class OriginCountriesAdapter(
    private val countryCallback: OriginCountryHolder.Callback,
) : RecyclerView.Adapter<RecyclerView.ViewHolder>(),
    PopupTextProvider {

    companion object {
        private const val TYPE_COUNTRY = 0
    }

    private val debounce = ClickDebounce()
    val items = AdapterItems<Any>(this)

    override fun getItemCount(): Int = items.list.size

    override fun getItemViewType(position: Int): Int {
        return when (items.list[position]) {
            is OriginCountryHolder.Item -> TYPE_COUNTRY
            else -> throw IllegalStateException("item is null for position = $position")
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_COUNTRY -> OriginCountryHolder(
                parent = parent,
                debounce = debounce,
                callback = countryCallback,
            )
            else -> throw IllegalStateException("Unknown holder type")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = items.list[position]
        when (holder) {
            is OriginCountryHolder -> holder.bind(item as OriginCountryHolder.Item)
        }
    }

    override fun getPopupText(view: View, position: Int): CharSequence {
        val item = items.list[position]
        if (item is OriginCountryHolder.Item) {
            return item.text[0].toString()
        }
        return "-"
    }

}
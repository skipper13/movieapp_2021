package com.nicetas.movieapp3.ui.search

import android.view.MenuItem
import android.view.MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW
import android.view.MenuItem.SHOW_AS_ACTION_IF_ROOM
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView

class SearchHelper {

    fun init(
        activity: AppCompatActivity,
        item: MenuItem,
        onSearchRequest: (searchText: String) -> Unit,
        getSearchText: () -> String,
    ) {
        val toolbar = activity.supportActionBar ?: return
        val searchView = SearchView(toolbar.themedContext)

        item.setShowAsAction(SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW or SHOW_AS_ACTION_IF_ROOM)
        item.actionView = searchView

        searchView.setOnSearchClickListener {
            searchView.setQuery(getSearchText(), false)
        }

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                onSearchRequest(newText)
                return true
            }
        })
    }

}
package com.nicetas.movieapp3.ui.extended.tabs

import androidx.lifecycle.MutableLiveData
import com.nicetas.bus.WatchListUpdateBus
import com.nicetas.database.watchlist.MovieToWatchEntity
import com.nicetas.movieapp3.base.Logger
import com.nicetas.movieapp3.base.viewmodel.BaseViewModel
import com.nicetas.repos.movie.MovieRepository
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import java.time.LocalDate
import java.time.LocalDateTime

class MovieTabsViewModel(
    private val movieRepository: MovieRepository,
    private val watchListUpdateBus: WatchListUpdateBus,
) : BaseViewModel() {

    val isInWatchlist = MutableLiveData<Boolean>()

    fun onViewCreated(movieId: Int) {
        checkInWatchlist(movieId)
    }

    private fun checkInWatchlist(movieId: Int) {
        disposables += movieRepository.isInWatchList(movieId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ isInWatchList ->
                isInWatchlist.value = isInWatchList
            }, { error ->
                Logger.e(error)
            })
    }

    fun addToWatchlist(
        movieId: Int,
        title: String,
        backdropPath: String?,
        posterPath: String?,
        releaseDate: LocalDate?,
        voteAverage: Double?,
    ) {
        val movie = MovieToWatchEntity(
            id = movieId,
            dateAdded = LocalDateTime.now(),
            title = title,
            backdropPath = backdropPath,
            posterPath = posterPath,
            releaseDate = releaseDate,
            voteAverage = voteAverage,
        )
        disposables += movieRepository.insertMovieToWatch(movie)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                watchListUpdateBus.onAdd(movieId)
                isInWatchlist.value = true
            }, { error ->
                Logger.e(error)
            })
    }

    fun removeFromWatchlist(movieId: Int) {
        disposables += movieRepository.deleteMovieToWatch(movieId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                watchListUpdateBus.onRemove(movieId)
                isInWatchlist.value = false
            }, { error ->
                Logger.e(error)
            })
    }

}
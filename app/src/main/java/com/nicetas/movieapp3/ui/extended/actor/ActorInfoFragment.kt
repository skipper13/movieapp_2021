package com.nicetas.movieapp3.ui.extended.actor

import android.os.Bundle
import android.view.View
import android.view.ViewGroup.MarginLayoutParams
import androidx.collection.ObjectList
import androidx.collection.objectListOf
import androidx.core.content.ContextCompat.getColor
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.updateLayoutParams
import androidx.navigation.fragment.findNavController
import com.nicetas.models.extensions.DateOrder
import com.nicetas.models.extensions.isBlankF
import com.nicetas.movieapp3.R
import com.nicetas.movieapp3.base.adapter.enforceSingleScrollDirection
import com.nicetas.movieapp3.base.fragment.BaseFragment
import com.nicetas.movieapp3.base.states.ViewState
import com.nicetas.movieapp3.databinding.FragmentActorInfoBinding
import com.nicetas.movieapp3.ui.extended.actor.adapter.ActorInfoAdapter
import com.nicetas.movieapp3.ui.extended.actor.adapter.ActorInfoHolder
import com.nicetas.movieapp3.ui.extended.actor.adapter.KnownForHolder
import com.nicetas.movieapp3.ui.extended.actor.cast.ActorCastFragment
import com.nicetas.movieapp3.ui.extended.actor.photo.ActorPhotosFragment
import com.nicetas.movieapp3.ui.extended.date.DateFormatDialog
import com.nicetas.movieapp3.ui.extended.tabs.MovieTabsFragment
import org.koin.androidx.viewmodel.ext.android.viewModel


class ActorInfoFragment : BaseFragment(R.layout.fragment_actor_info) {

    companion object {
        private const val ARG_ACTOR_ID_DEEPLINK = "ARG_ACTOR_ID_DEEPLINK"
        private const val ARG_ACTOR_ID = "ARG_ACTOR_ID"
        private const val ARG_ACTOR_NAME = "ARG_ACTOR_NAME"

        fun args(
            actorId: Int,
            actorName: String
        ) = Bundle(2).apply {
            putInt(ARG_ACTOR_ID, actorId)
            putString(ARG_ACTOR_NAME, actorName)
        }
    }

    private var _binding: FragmentActorInfoBinding? = null
    private val binding
        get() = _binding!!

    private val viewModel: ActorInfoViewModel by viewModel()

    private var adapter: ActorInfoAdapter? = null

    private val actorName by lazy { requireArguments().getString(ARG_ACTOR_NAME, "") }

    private val actorId: Int by lazy {
        val args = requireArguments()
        if (args.containsKey(ARG_ACTOR_ID)) {
            args.getInt(ARG_ACTOR_ID)
        } else {
            val idText = args.getString(ARG_ACTOR_ID_DEEPLINK, "0")
            if (idText[0].isDigit()) {
                idText.takeWhile { it.isDigit() }.toInt()
            } else {
                0
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (actorId == 0) {
            findNavController().navigateUp()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentActorInfoBinding.bind(view)
        setupViews()
        initViewState()
        initRecyclerView()
        setupObservers()
        viewModel.onViewCreated(actorId)
    }

    override fun onDestroyView() {
        adapter?.onDestroyView()
        adapter = null
        binding.rvContent.adapter = null
        super.onDestroyView()
        _binding = null
    }

    private fun setupViews() {
        setStatusBarColor(getColor(requireContext(), R.color.colorPrimary))

        ViewCompat.setOnApplyWindowInsetsListener(binding.root) { _, insets ->
            val statusBarHeight = insets.getInsets(WindowInsetsCompat.Type.statusBars()).top
            binding.appBar.updateLayoutParams<MarginLayoutParams> {
                topMargin = statusBarHeight
            }
            binding.rvContent.updateLayoutParams<MarginLayoutParams> {
                bottomMargin = statusBarHeight
            }
            insets
        }
        setToolbarTitle(actorName)
    }

    private fun initViewState() {
        ViewState.Builder(binding.rvContent)
            .loadDelay(100)
            .error(onClick = { viewModel.loadActor(actorId) })
            .build()
            .observeLce(viewModel.lce, viewLifecycleOwner)
    }

    private fun initRecyclerView() {
        adapter = ActorInfoAdapter(
            inflater = layoutInflater,
            callbackInfo = object : ActorInfoHolder.Callback {
                override fun onPhotoClick(actorId: Int, name: String) {
                    val args = ActorPhotosFragment.args(
                        actorId = actorId,
                        name = name,
                    )
                    findNavController().navigate(R.id.action_actor_to_photos, args)
                }

                override fun onMovieClick(movie: KnownForHolder.Item) {
                    val args = MovieTabsFragment.args(
                        movieId = movie.movieId,
                        title = movie.title,
                        backdropPath = movie.backdropPath,
                        posterPath = movie.posterPath,
                        releaseDate = movie.releaseDate,
                        voteAverage = movie.voteAverage,
                    )
                    findNavController().navigate(R.id.movieTabs, args)
                }

                override fun onAllMoviesClick(actorId: Int, name: String) {
                    val args = ActorCastFragment.args(
                        actorId = actorId,
                        name = name,
                    )
                    findNavController().navigate(R.id.action_actor_to_cast, args)
                }

                override fun getCast(): ObjectList<Any> {
                    return viewModel.getCast()
                }

                override fun getMoviesScrollOffset(): Int {
                    return viewModel.moviesScrollOffset
                }

                override fun getDateOrder(): DateOrder {
                    return viewModel.dateOrder()
                }

                override fun getDateSeparator(): Char {
                    return viewModel.dateSeparator()
                }

                override fun showDateFormatDialog() {
                    DateFormatDialog.newInstance().show(parentFragmentManager, null)
                }
            },
            saveMoviesScrollOffset = { offset ->
                viewModel.moviesScrollOffset = offset
            },
        )
        binding.rvContent.adapter = adapter
        binding.rvContent.enforceSingleScrollDirection()
    }

    private fun setupObservers() {
        viewModel.onGetActor.observe(viewLifecycleOwner) { actor ->
            if (actorName.isBlankF()) {
                setToolbarTitle(actor.name)
            }
            adapter?.items?.replaceAll(objectListOf(actor))
        }
    }


}
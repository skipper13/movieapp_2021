package com.nicetas.movieapp3.ui.extended.videos.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.nicetas.movieapp3.R
import com.nicetas.movieapp3.base.adapter.ClickDebounce
import com.nicetas.movieapp3.databinding.ItemMovieVideoBinding

class MovieVideosHolder(
    parent: ViewGroup,
    inflater: LayoutInflater,
    private var binding: ItemMovieVideoBinding =
        ItemMovieVideoBinding.inflate(inflater, parent, false),
    debounce: ClickDebounce,
    callback: Callback,
) : RecyclerView.ViewHolder(binding.root) {

    fun interface Callback {
        fun onPlayClick(key: String)
    }

    class Item(
        val id: String,
        val youTubeKey: String,
    )

    private lateinit var item: Item

    init {
        binding.vPlay.setOnClickListener {
            if (debounce.canClick()) {
                callback.onPlayClick(key = item.youTubeKey)
            }
        }
    }

    fun bind(item: Item) {
        this.item = item

        Glide.with(binding.root.context)
            .load("https://img.youtube.com/vi/${item.youTubeKey}/0.jpg")
            .transition(DrawableTransitionOptions.withCrossFade())
            .placeholder(R.drawable.movie_image_placeholder)
            .centerCrop()
            .into(binding.ivBackdrop)
    }

}
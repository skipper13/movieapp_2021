package com.nicetas.movieapp3.ui.search

import androidx.collection.MutableObjectList
import androidx.collection.ObjectList
import com.nicetas.bus.KeyboardBus
import com.nicetas.bus.WatchListUpdateBus
import com.nicetas.models.extensions.isBlankF
import com.nicetas.models.movies.MoviesList
import com.nicetas.movieapp3.base.Logger
import com.nicetas.movieapp3.base.states.Lce
import com.nicetas.movieapp3.base.viewmodel.BaseViewModel
import com.nicetas.movieapp3.base.viewmodel.LiveEvent
import com.nicetas.movieapp3.ui.ImageUrl
import com.nicetas.movieapp3.ui.search.adapter.MovieHolder
import com.nicetas.prefs.Prefs
import com.nicetas.prefs.PrefsKey
import com.nicetas.repos.configuration.ConfigRepository
import com.nicetas.repos.movie.MovieRepository
import com.nicetas.repos.network.SuccessResponse
import com.nicetas.repos.search.SearchRepository
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.internal.functions.Functions
import io.reactivex.rxjava3.schedulers.Schedulers
import java.util.concurrent.TimeUnit

class SearchViewModel(
    private val configRepository: ConfigRepository,
    private val searchRepository: SearchRepository,
    private val movieRepository: MovieRepository,
    private val keyboardBus: KeyboardBus,
    private val watchListUpdateBus: WatchListUpdateBus,
    private val prefs: Prefs,
) : BaseViewModel() {

    enum class LoadMoreType {
        Discover,
        Search
    }

    class GetMoviesResult(val items: ObjectList<Any>, val isFullReload: Boolean)

    val onGetMovies = LiveEvent<GetMoviesResult>()
    val isKeyboardOpen = LiveEvent<Boolean>()
    val showPaginationError = LiveEvent<Unit>()
    val onMovieFromWatchlistRemoved = LiveEvent<Int>()
    val updateSpanCount = LiveEvent<Unit>()

    private var loadMoreType = LoadMoreType.Discover
    var searchText = ""
        private set
    private var page = 1
    private var totalPages = Int.MAX_VALUE
    private val searchDisposables = CompositeDisposable()

    private val items = MutableObjectList<Any>()

    private var wasInit = false
    var isPosters = false
        private set

    val hasLoadedAllItems: Boolean
        get() = page > totalPages
    var isLoading = false
        private set
    var paginationError: Lce.Error? = null
        private set

    fun onViewCreated() {
        if (wasInit) {
            updateSpanCount.setValue(Unit)
            onGetMovies.setValue(GetMoviesResult(items, false))
        } else {
            wasInit = true
            lce.loading()
            subscribeOnKeyboard()
            subscribeOnWatchlist()
            loadImgBaseUrlThenMovies(isFullReload = false)
        }
        preloadCountries()
    }

    override fun onCleared() {
        searchDisposables.clear()
        super.onCleared()
        items.clear()
    }

    private fun loadImgBaseUrlThenMovies(isFullReload: Boolean) {
        disposables += Single.fromCallable {
            isPosters = prefs.getBoolean(PrefsKey.ShowPostersForSearch, false)
        }.flatMap {
            configRepository.getOrLoadImgBaseUrl()
        }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ url ->
                ImageUrl.init(url)
                updateSpanCount.setValue(Unit)
                loadMoreMovies(isFullReload)
            }, { error ->
                Logger.e(error)
            })
    }

    private fun subscribeOnKeyboard() {
        disposables += keyboardBus.subscribeOnKeyboard()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                isKeyboardOpen.setValue(it.isOpen)
            }, { error ->
                Logger.e(error)
            })
    }

    private fun subscribeOnWatchlist() {
        disposables += watchListUpdateBus.subscribeOnWatchlist()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ event ->
                val index =
                    items.indexOfFirst { it is MovieHolder.Item && it.movieId == event.movieId }
                val item = items[index] as MovieHolder.Item
                when (event) {
                    is WatchListUpdateBus.OnRemove -> {
                        item.isInWatchlist = false
                    }
                    is WatchListUpdateBus.OnAdd -> {
                        item.isInWatchlist = true
                    }
                }
            }, { error ->
                Logger.e(error)
            })
    }

    fun discover() {
        loadMoreType = LoadMoreType.Discover
        reloadMovies()
    }

    fun search(text: String) {
        if (text.isBlankF() || text == searchText) {
            return
        }
        searchText = text
        loadMoreType = LoadMoreType.Search
        searchDisposables.clear()
        searchDisposables += Single.timer(1, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                reloadMovies()
            }, { error ->
                Logger.e(error)
            })
    }

    fun reloadMovies() {
        page = 1
        totalPages = Int.MAX_VALUE
        lce.loading()
        if (ImageUrl.isFromConfig) {
            loadMoreMovies(isFullReload = true)
        } else {
            loadImgBaseUrlThenMovies(isFullReload = true)
        }
    }

    fun loadMoreMovies(isFullReload: Boolean) {
        isLoading = true
        when (loadMoreType) {
            LoadMoreType.Discover -> {
                searchDisposables += searchRepository.discoverMovie(page = page)
                    .subscribeOnMovies(isFullReload)
            }
            LoadMoreType.Search -> {
                searchDisposables += searchRepository.searchMovie(page = page, query = searchText)
                    .subscribeOnMovies(isFullReload)
            }
        }
    }

    private class InWatchList(val id: Int, val inList: Boolean)

    private fun Single<SuccessResponse<MoviesList>>.subscribeOnMovies(
        isFullReload: Boolean,
    ): Disposable {
        return this.flatMap { response ->
            Observable.fromIterable(response.body.movies?.map { it.id } ?: emptyList())
                .flatMapSingle { id ->
                    movieRepository.isInWatchList(id).map { InWatchList(id, it) }
                }
                .toList()
                .map { watchList ->
                    val model = response.body
                    page = model.page + 1
                    totalPages = model.totalPages
                    val movies = model.movies ?: emptyList()

                    // Do not loop inside "movies.map" without need.
                    val isSomethingInWatchList = watchList.any { it.inList }

                    val newItems = MutableObjectList<Any>(movies.size)
                    for (i in movies.indices) {
                        val movie = movies[i]
                        val item = MovieHolder.Item(
                            movieId = movie.id,
                            title = movie.title,
                            voteAverage = movie.voteAverage,
                            releaseDate = movie.releaseDate,
                            backdropPath = movie.backdropPath,
                            posterPath = movie.posterPath,
                            isInWatchlist = isSomethingInWatchList
                                    && watchList.any { it.id == movie.id && it.inList },
                        )
                        newItems.add(item)
                    }
                    if (isFullReload) {
                        items.clear()
                    }
                    items.addAll(newItems)
                    newItems
                }
        }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ newItems ->
                onGetMovies.setValue(GetMoviesResult(newItems, isFullReload))
                if (items.isEmpty()) {
                    lce.empty()
                } else {
                    lce.content()
                }
                isLoading = false
            }, { error ->
                Logger.e(error)
                isLoading = false

                if (isFullReload || items.isEmpty()) {
                    lce.error(message = error.message ?: "")
                } else {
                    paginationError = Lce.Error(error.message ?: "")
                    showPaginationError.setValue(Unit)
                }
            })
    }

    private fun preloadCountries() {
        disposables += configRepository.getOrReadCountries()
            .subscribeOn(Schedulers.io())
            .subscribe(Functions.emptyConsumer()) { Logger.e(it) }
    }

    fun removeFromWatchList(movieId: Int) {
        disposables += movieRepository.deleteMovieToWatch(movieId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                val index = items.indexOfFirst { it is MovieHolder.Item && it.movieId == movieId }
                val item = items[index] as MovieHolder.Item
                item.isInWatchlist = false
                onMovieFromWatchlistRemoved.setValue(item.movieId)
            }, { error ->
                Logger.e(error)
            })
    }

    fun onGridTypeClick() {
        isPosters = !isPosters
        updateSpanCount.setValue(Unit)
        prefs.edit().commitIO {
            putBoolean(PrefsKey.ShowPostersForSearch, isPosters)
        }
    }

}
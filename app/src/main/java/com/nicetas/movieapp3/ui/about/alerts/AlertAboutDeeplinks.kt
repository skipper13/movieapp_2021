package com.nicetas.movieapp3.ui.about.alerts

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.pm.verify.domain.DomainVerificationManager
import android.content.pm.verify.domain.DomainVerificationUserState
import android.net.Uri
import android.os.Build
import android.provider.Settings
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import com.nicetas.movieapp3.R

@RequiresApi(Build.VERSION_CODES.S)
class AlertAboutDeeplinks {

    fun show(context: Context) {
        val manager = context.getSystemService(DomainVerificationManager::class.java)
        val userState = manager.getDomainVerificationUserState(context.packageName) ?: return

        val isAssociated = userState.hostToStateMap.any { host ->
            host.key == "www.themoviedb.org"
                    && host.value == DomainVerificationUserState.DOMAIN_STATE_SELECTED
        }

        val message = if (isAssociated) {
            context.getString(R.string.about_deeplinks_are_associated)
        } else {
            context.getString(R.string.about_deeplinks_are_not_associated)
        }

        AlertDialog.Builder(context)
            .setTitle(R.string.about_deeplinks_associate)
            .setMessage(message)
            .setPositiveButton(R.string.about_deeplinks_settings) { _, _ ->
                onPositiveButtonClick(context)
            }
            .setNegativeButton(android.R.string.cancel, null)
            .show()
    }

    private fun onPositiveButtonClick(context: Context) {
        val isSamsung = Build.MANUFACTURER.equals("samsung", ignoreCase = true)
        if (isSamsung) {
            openSystemSettings(context)
        } else {
            try {
                val intent = Intent(
                    Settings.ACTION_APP_OPEN_BY_DEFAULT_SETTINGS,
                    Uri.parse("package:${context.packageName}")
                )
                context.startActivity(intent)
            } catch (ex: ActivityNotFoundException) {
                openSystemSettings(context)
            }
        }
    }

    private fun openSystemSettings(context: Context) {
        try {
            val intent = Intent(
                Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                Uri.parse("package:${context.packageName}")
            )
            context.startActivity(intent)
        } catch (ex: ActivityNotFoundException) {
            try {
                val intent = Intent(Settings.ACTION_SETTINGS)
                context.startActivity(intent)
            } catch (ex: ActivityNotFoundException) {
                Toast.makeText(context, R.string.about_deeplinks_settings_open_error, Toast.LENGTH_SHORT).show()
            }
        }
    }

}
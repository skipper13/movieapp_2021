package com.nicetas.models.adapters

import com.google.gson.TypeAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonWriter
import com.nicetas.models.movies.extended.videos.VideoType

class VideoTypeJsonAdapter : TypeAdapter<VideoType>() {

    override fun read(reader: JsonReader): VideoType {
        val type = reader.nextString()
        return when (type) {
            VideoType.Trailer.value -> VideoType.Trailer
            VideoType.Teaser.value -> VideoType.Teaser
            VideoType.Clip.value -> VideoType.Clip
            VideoType.BehindTheScenes.value -> VideoType.BehindTheScenes
            VideoType.Bloopers.value -> VideoType.Bloopers
            VideoType.Featurette.value -> VideoType.Featurette
            else -> VideoType.Other(value = type, order = 7)
        }
    }

    override fun write(out: JsonWriter?, type: VideoType) {
        out?.value(type.value)
    }

}
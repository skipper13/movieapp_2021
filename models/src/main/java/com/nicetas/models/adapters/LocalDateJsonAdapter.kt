package com.nicetas.models.adapters

import com.google.gson.TypeAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonToken
import com.google.gson.stream.JsonWriter
import com.nicetas.models.extensions.isBlankF
import com.nicetas.models.extensions.toLocalDate
import java.time.LocalDate

class LocalDateJsonAdapter : TypeAdapter<LocalDate?>() {

    override fun read(jsonReader: JsonReader): LocalDate? {
        when (jsonReader.peek()) {
            JsonToken.STRING -> {
                val dateStr = jsonReader.nextString()
                if (dateStr.isBlankF()) {
                    return null
                } else {
                    return dateStr.toLocalDate()
                }
            }
            else -> {
                jsonReader.skipValue()
                return null
            }
        }
    }

    override fun write(out: JsonWriter, value: LocalDate?) {
        out.value(value?.toString())
    }
}
package com.nicetas.models.adapters

import com.google.gson.TypeAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonToken
import com.google.gson.stream.JsonWriter
import com.nicetas.models.search.MediaType

class MediaTypeJsonAdapter : TypeAdapter<MediaType>() {

    companion object {
        private const val PERSON = "person"
        private const val MOVIE = "movie"
        private const val TV = "tv"
    }

    override fun read(reader: JsonReader): MediaType {
        when (reader.peek()) {
            JsonToken.STRING -> {
                return when (reader.nextString()) {
                    PERSON -> MediaType.PERSON
                    MOVIE -> MediaType.MOVIE
                    TV -> MediaType.TV
                    else -> MediaType.UNKNOWN
                }
            }
            else -> {
                reader.skipValue()
                return MediaType.UNKNOWN
            }
        }
    }

    override fun write(out: JsonWriter?, value: MediaType) {
        val outValue = when (value) {
            MediaType.PERSON -> PERSON
            MediaType.MOVIE -> MOVIE
            MediaType.TV -> TV
            else -> ""
        }
        out?.value(outValue)
    }

}
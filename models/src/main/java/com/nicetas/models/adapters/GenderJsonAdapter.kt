package com.nicetas.models.adapters

import com.google.gson.TypeAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonToken
import com.google.gson.stream.JsonWriter
import com.nicetas.models.search.person.Gender

class GenderJsonAdapter : TypeAdapter<Gender>() {

    companion object {
        private const val NOT_SPECIFIED = 0
        private const val FEMALE = 1
        private const val MALE = 2
    }

    override fun read(reader: JsonReader): Gender {
        when (reader.peek()) {
            JsonToken.NUMBER -> {
                return when (reader.nextInt()) {
                    FEMALE -> Gender.FEMALE
                    MALE -> Gender.MALE
                    else -> Gender.NOT_SPECIFIED
                }
            }
            else -> {
                reader.skipValue()
                return Gender.NOT_SPECIFIED
            }
        }
    }

    override fun write(out: JsonWriter?, value: Gender) {
        val outValue = when (value) {
            Gender.FEMALE -> FEMALE
            Gender.MALE -> MALE
            else -> NOT_SPECIFIED
        }
        out?.value(outValue)
    }

}
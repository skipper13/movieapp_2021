package com.nicetas.models.lists

import com.google.gson.annotations.JsonAdapter
import com.google.gson.annotations.SerializedName
import com.nicetas.models.adapters.LocalDateJsonAdapter
import com.nicetas.models.adapters.MediaTypeJsonAdapter
import com.nicetas.models.search.MediaType
import java.time.LocalDate

class ListDetailsResult(
    @SerializedName("id")
    val id: Int,
    @SerializedName("title") // "media_type": "movie"
    val title: String?,
    @SerializedName("original_title") // "media_type": "movie"
    val originalTitle: String?,
    @SerializedName("name") // "media_type": "tv"
    val name: String?,
    @SerializedName("original_name") // "media_type": "tv"
    val originalName: String?,
    @JsonAdapter(value = MediaTypeJsonAdapter::class, nullSafe = false)
    @SerializedName("media_type")
    val mediaType: MediaType,
    @SerializedName("overview")
    val overview: String,
    @SerializedName("poster_path") // movie + tv
    val posterPath: String?,
    @SerializedName("backdrop_path") // movie + tv
    val backdropPath: String?,
    @SerializedName("popularity")
    val popularity: Double,
    @SerializedName("genre_ids")
    val genreIds: List<Int>,
    @SerializedName("vote_average")
    val voteAverage: Double,
    @SerializedName("vote_count")
    val voteCount: Int,
    @SerializedName("adult")
    val adult: Boolean,
    @JsonAdapter(value = LocalDateJsonAdapter::class, nullSafe = false)
    @SerializedName("release_date") // "media_type": "movie"
    val releaseDate: LocalDate?,
    @JsonAdapter(value = LocalDateJsonAdapter::class, nullSafe = false)
    @SerializedName("first_air_date") // "media_type": "tv"
    val firstAirDate: LocalDate?,
    @SerializedName("original_language")
    val originalLanguage: String, // "en"

//    @SerializedName("video") // "media_type": "movie"
//    val video: Boolean?,
//    @SerializedName("origin_country") // "media_type": "tv"
//    val originCountry: List<String>?, // ["US"]
)
package com.nicetas.models.lists

import com.google.gson.annotations.SerializedName

class ListDetails(
    @SerializedName("id")
    val id: Int,
    @SerializedName("page")
    val page: Int,
    @SerializedName("name")
    val name: String,
    @SerializedName("backdrop_path")
    val backdropPath: String?,
    @SerializedName("item_count")
    val itemCount: Int,
    @SerializedName("total_pages")
    val totalPages: Int,
    @SerializedName("results")
    val results: List<ListDetailsResult>,
    /*
    @SerializedName("iso_3166_1")
    val iso_3166_1: String, // "RU"
    @SerializedName("iso_639_1")
    val iso_639_1: String, // "ru"
    @SerializedName("poster_path")
    val posterPath: String?,
    @SerializedName("revenue")
    val revenue: Long,
    @SerializedName("runtime")
    val runtime: Int,
    @SerializedName("sort_by")
    val sortBy: String,
    @SerializedName("public")
    val public: Boolean,
    @SerializedName("total_results")
    val totalResults: Int,
    @SerializedName("description")
    val description: String,
    @SerializedName("average_rating")
    val averageRating: Double,
    @SerializedName("created_by")
    val createdBy: CreatedBy,
    @SerializedName("comments")
    val comments: List<Any>
     */
) {

    /*
    class CreatedBy(
        @SerializedName("id")
        val id: String,
        @SerializedName("avatar_path")
        val avatarPath: String?,
        @SerializedName("gravatar_hash")
        val gravatarHash: String,
        @SerializedName("name")
        val name: String,
        @SerializedName("username")
        val username: String,
    )
     */

}
package com.nicetas.models.extensions

import kotlin.contracts.ExperimentalContracts
import kotlin.contracts.InvocationKind
import kotlin.contracts.contract

/**
 * Postfix F stands for fast
 */

// https://www.romainguy.dev/posts/2024/speeding-up-isblank/
fun CharSequence.isBlankF(): Boolean {
    for (i in 0..<length) {
        val c = this[i]
        if (!Character.isWhitespace(c) && c != '\u00a0' && c != '\u2007' && c != '\u202f') {
            return false
        }
    }
    return true
}

inline fun CharSequence.isNotBlankF(): Boolean = !isBlankF()

@OptIn(ExperimentalContracts::class)
inline fun CharSequence?.isNullOrBlankF(): Boolean {
    contract {
        returns(false) implies (this@isNullOrBlankF != null)
    }
    return this == null || this.isBlankF()
}

inline fun CharSequence?.isNotNullOrBlankF(): Boolean {
    return this != null && !this.isBlankF()
}

// Suppress: https://youtrack.jetbrains.com/issue/KT-19215/Relax-restriction-on-type-parameters-that-are-constrained-by-other-type-parameters
@Suppress("BOUNDS_NOT_ALLOWED_IF_BOUNDED_BY_TYPE_PARAMETER")
@OptIn(ExperimentalContracts::class)
inline fun <C, R> C.ifBlankF(defaultValue: () -> R): R where C : CharSequence, C : R {
    contract {
        callsInPlace(defaultValue, InvocationKind.AT_MOST_ONCE)
    }
    return if (isBlankF()) defaultValue() else this
}
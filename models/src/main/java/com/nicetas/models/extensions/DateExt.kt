package com.nicetas.models.extensions

import java.time.LocalDate
import java.time.format.DateTimeFormatter

enum class DateOrder {
    DayMonthYear,
    MonthDayYear,
    YearMonthDay;
}

fun LocalDate?.format(
    order: DateOrder = DateOrder.DayMonthYear,
    separator: Char = '.',
): String? {
    return when (order) {
        DateOrder.DayMonthYear -> dayMonthYear(separator)
        DateOrder.MonthDayYear -> monthDayYear(separator)
        DateOrder.YearMonthDay -> yearMonthDay(separator)
    }
}

fun LocalDate?.dayMonthYear(separator: Char): String? {
    this ?: return null
    val s = StringBuilder(10)
    if (dayOfMonth < 10) {
        s.append('0')
    }
    s.append(dayOfMonth)
    s.append(separator)
    if (monthValue < 10) {
        s.append('0')
    }
    s.append(monthValue)
    s.append(separator)
    s.append(year)
    return s.toString()
}

fun LocalDate?.monthDayYear(separator: Char): String? {
    this ?: return null
    val s = StringBuilder(10)
    if (monthValue < 10) {
        s.append('0')
    }
    s.append(monthValue)
    s.append(separator)
    if (dayOfMonth < 10) {
        s.append('0')
    }
    s.append(dayOfMonth)
    s.append(separator)
    s.append(year)
    return s.toString()
}

fun LocalDate?.yearMonthDay(separator: Char): String? {
    this ?: return null
    val s = StringBuilder(10)
    s.append(year)
    s.append(separator)
    if (monthValue < 10) {
        s.append('0')
    }
    s.append(monthValue)
    s.append(separator)
    if (dayOfMonth < 10) {
        s.append('0')
    }
    s.append(dayOfMonth)
    return s.toString()
}

/**
 *  "yyyy-MM-dd"
 *  @see DateTimeFormatter.ISO_LOCAL_DATE
 */
fun String?.toLocalDate(): LocalDate? {
    this ?: return null
    return LocalDate.of(
        Integer.parseInt(substring(0, 4)),
        Integer.parseInt(substring(5, 7)),
        Integer.parseInt(substring(8, 10))
    )
}
package com.nicetas.models.configuration

import com.google.gson.annotations.SerializedName

class ConfigurationTmdb(
    @SerializedName("images")
    val images: Images,
) {
    class Images(
        @SerializedName("secure_base_url")
        val secureBaseUrl: String,
//        @SerializedName("backdrop_sizes")
//        val backdropSizes: List<String>,
//        @SerializedName("profile_sizes")
//        val profileSizes: List<String>,
    )
}
package com.nicetas.models.configuration

import com.google.gson.annotations.SerializedName

class CountryTmdb(
    @SerializedName("iso_3166_1")
    val iso_3166_1: String, // "US"
    @SerializedName("native_name")
    val nativeName: String,
//    @SerializedName("english_name")
//    val englishName: String,
)
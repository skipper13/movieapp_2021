package com.nicetas.models.movies.extended.videos

sealed interface VideoType {

    val value: String
    val order: Int

    object Trailer : VideoType {
        override val value: String = "Trailer"
        override val order: Int = 1
    }

    object Teaser : VideoType {
        override val value: String = "Teaser"
        override val order: Int = 2
    }

    object Clip : VideoType {
        override val value: String = "Clip"
        override val order: Int = 3
    }

    object BehindTheScenes : VideoType {
        override val value: String = "Behind the Scenes"
        override val order: Int = 4
    }

    object Bloopers : VideoType {
        override val value: String = "Bloopers"
        override val order: Int = 5
    }

    object Featurette : VideoType {
        override val value: String = "Featurette"
        override val order: Int = 6
    }

    class Other(
        override val value: String,
        override val order: Int,
    ) : VideoType

}
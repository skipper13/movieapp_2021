package com.nicetas.models.movies.companies

import com.google.gson.annotations.SerializedName
import com.nicetas.models.IdName
import com.nicetas.models.movies.SearchPaginationList

class SearchCompanies(
    @SerializedName("results")
    val keywords: List<Company>,
    @SerializedName("page")
    val page: Int,
    @SerializedName("total_pages")
    val totalPages: Int,
//    @SerializedName("total_results")
//    val totalResults: Int,
) : SearchPaginationList {

    override fun getList(): List<IdName> = keywords

    override fun getPaginationPage(): Int = page

    override fun getPaginationTotalPages(): Int = totalPages

}
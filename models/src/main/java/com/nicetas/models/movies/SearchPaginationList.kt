package com.nicetas.models.movies

import com.nicetas.models.IdName

interface SearchPaginationList {

    fun getList(): List<IdName>
    fun getPaginationPage(): Int
    fun getPaginationTotalPages(): Int

}
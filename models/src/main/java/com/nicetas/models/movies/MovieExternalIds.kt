package com.nicetas.models.movies

import com.google.gson.annotations.SerializedName

class MovieExternalIds(
    // https://www.themoviedb.org/movie/976573
    // https://www.themoviedb.org/movie/{id}
    @SerializedName("id")
    val movieId: Int,

    // https://www.imdb.com/title/tt15789038
    // https://www.imdb.com/title/{imdb_id}
    @SerializedName("imdb_id")
    val imdbId: String?,

    // https://www.facebook.com/PixarElemental
    // https://www.facebook.com/{facebook_id}
    @SerializedName("facebook_id")
    val facebookId: String?,

    // https://www.twitter.com/pixarelemental
    // https://twitter.com/{twitter_id}
    @SerializedName("twitter_id")
    val twitterId: String?,

    // https://www.instagram.com/pixarelemental
    // https://www.instagram.com/{instagram_id}
    @SerializedName("instagram_id")
    val instagramId: String?,

    // https://www.wikidata.org/wiki/Q112801489
    // https://www.wikidata.org/wiki/{wikidata_id}
//    @SerializedName("wikidata_id")
//    val wikidataId: String?,
)
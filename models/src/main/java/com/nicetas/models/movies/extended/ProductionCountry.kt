package com.nicetas.models.movies.extended

import com.google.gson.annotations.SerializedName

class ProductionCountry(
    @SerializedName("iso_3166_1")
    val iso_3166_1: String,
    @SerializedName("name")
    var name: String,
) {
    override fun toString(): String {
        return "iso_3166_1 = $iso_3166_1, name = $name; "
    }
}
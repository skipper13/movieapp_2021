package com.nicetas.models.movies.keywords

import com.google.gson.annotations.SerializedName
import com.nicetas.models.IdName

class Keyword(
    @SerializedName("id")
    val id: Int,
    @SerializedName("name")
    val name: String,
) : IdName {

    override fun identifier() = id

    override fun iName() = name

    override fun toString(): String {
        return "id = $id, name = $name; "
    }

}
package com.nicetas.models.movies.extended.credits

import com.google.gson.annotations.SerializedName

class MovieCast(
    @SerializedName("id")
    val id: Int,
    @SerializedName("name")
    val name: String,
    @SerializedName("character")
    val character: String,
    @SerializedName("profile_path")
    val profilePath: String?,
//    @SerializedName("cast_id")
//    val castId: Int,
//    @SerializedName("credit_id")
//    val creditId: String,
//    @SerializedName("order")
//    val order: Int,
//    @JsonAdapter(value = GenderJsonAdapter::class, nullSafe = false)
//    @SerializedName("gender")
//    val gender: Gender,
//    @SerializedName("popularity")
//    val popularity: Double,
//    @SerializedName("original_name")
//    val originalName: String,
)
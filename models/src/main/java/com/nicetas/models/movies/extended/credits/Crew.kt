package com.nicetas.models.movies.extended.credits

import com.google.gson.annotations.SerializedName

class Crew(
    @SerializedName("id")
    val id: Int,
    @SerializedName("name")
    val name: String,
    @SerializedName("credit_id")
    val creditId: String?,
    @SerializedName("profile_path")
    val profilePath: String?,
    @SerializedName("job")
    val job: String,
    @SerializedName("department")
    val department: String,
//    @SerializedName("known_for_department")
//    val knownForDepartment: String,
//    @JsonAdapter(value = GenderJsonAdapter::class, nullSafe = false)
//    @SerializedName("gender")
//    val gender: Gender,
//    @SerializedName("popularity")
//    val popularity: Double,
//    @SerializedName("original_name")
//    val originalName: String,
)
package com.nicetas.models.movies.extended.images

import com.google.gson.annotations.SerializedName

class MovieImages(
    @SerializedName("id")
    val id: Int,
    @SerializedName("backdrops")
    val backdrops: List<MovieImage>?,
    // There is no need for this at the moment
//    @SerializedName("posters")
//    val posters: List<MovieImage>?
)
package com.nicetas.models.movies.extended.videos

import com.google.gson.annotations.JsonAdapter
import com.google.gson.annotations.SerializedName
import com.nicetas.models.adapters.VideoTypeJsonAdapter

class MovieVideo(
    @SerializedName("id")
    val id: String,

    @SerializedName("key")
    val key: String,     // k-0XcjGqq1E

    @JsonAdapter(value = VideoTypeJsonAdapter::class)
    @SerializedName("type")
    val type: VideoType,    // Trailer

//    @SerializedName("iso_639_1")
//    val iso_639_1: String,     // ru
//    @SerializedName("iso_3166_1")
//    val iso_3166_1: String,   // RU
//    @SerializedName("name")
//    val name: String,
//    @SerializedName("site")
//    val site: String,   // YouTube
//    @SerializedName("size")
//    val size: String,   // 1080
)
package com.nicetas.models.movies.companies

import com.google.gson.annotations.SerializedName
import com.nicetas.models.IdName

class Company(
    @SerializedName("id")
    val id: Int,
    @SerializedName("name")
    val name: String,
//    @SerializedName("logo_path")
//    val logoPath: String?, // "/1TjvGVDMYsj6JBxOAkUHpPEwLf7.png"
//    @SerializedName("origin_country")
//    val originCountry: String, // "US" || ""
) : IdName {

    override fun identifier() = id

    override fun iName() = name

    override fun toString(): String {
        return "id = $id, name = $name; "
    }

}
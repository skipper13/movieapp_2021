package com.nicetas.models.movies

import com.google.gson.annotations.JsonAdapter
import com.google.gson.annotations.SerializedName
import com.nicetas.models.adapters.LocalDateJsonAdapter
import com.nicetas.models.genres.Genre
import com.nicetas.models.movies.companies.Company
import com.nicetas.models.movies.extended.ProductionCountry
import com.nicetas.models.movies.keywords.Keyword
import java.time.LocalDate

class Movie(

    @SerializedName("id")
    val id: Int,
    @SerializedName("title")
    val title: String?,
    @SerializedName("original_title")
    val originalTitle: String?,
    @SerializedName("overview")
    val overview: String?,
    @SerializedName("backdrop_path")
    val backdropPath: String?,
    @JsonAdapter(value = LocalDateJsonAdapter::class, nullSafe = false)
    @SerializedName("release_date")
    val releaseDate: LocalDate?,
    @SerializedName("poster_path")
    val posterPath: String?,
    @SerializedName("vote_average")
    val voteAverage: Double?,
    @SerializedName("vote_count")
    val voteCount: Int?,
    @SerializedName("popularity")
    val popularity: Double?,
    @SerializedName("budget")
    val budget: Long?,
    @SerializedName("tagline")
    val tagline: String?,
    @SerializedName("revenue")
    val revenue: Long?,
    @SerializedName("runtime")
    val runtime: Int?,
    @SerializedName("genres")
    val genres: List<Genre>,
    @SerializedName("production_companies")
    val productionCompanies: List<Company>,
    @SerializedName("production_countries")
    val productionCountries: List<ProductionCountry>,
    @SerializedName("keywords")
    val keywords: Keywords,
    @SerializedName("external_ids")
    val externalIds: ExternalIds?,
    @SerializedName("imdb_id")
    val imdbId: String?,

//    @SerializedName("homepage")
//    val homepage: String?,
//    @SerializedName("status")
//    val status: String?, // "Released"
//    @SerializedName("spoken_languages")
//    val spokenLanguages: List<SpokenLanguage>,
//    @SerializedName("original_language")
//    val originalLanguage: String?, // "en"
//    @SerializedName("adult")
//    val adult: Boolean,

) {
    class Keywords(
        @SerializedName("keywords")
        val keywords: List<Keyword>,
    )

    class ExternalIds(
        @SerializedName("imdb_id")
        val imdbId: String?,
        @SerializedName("facebook_id")
        val facebookId: String?,
        @SerializedName("twitter_id")
        val twitterId: String?,
        @SerializedName("instagram_id")
        val instagramId: String?,
    )
}
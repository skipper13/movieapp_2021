package com.nicetas.models.movies.extended.images

import com.google.gson.annotations.SerializedName

class MovieImage(
    @SerializedName("file_path")
    val filePath: String,

//    @SerializedName("width")
//    val width: Int,
//    @SerializedName("height")
//    val height: Int,
//    @SerializedName("aspect_ratio")
//    val aspectRatio: Float,
//    @SerializedName("vote_average")
//    val voteAverage: Double,
//    @SerializedName("vote_count")
//    val voteCount: Int,
//    @SerializedName("iso_639_1")
//    val iso_639_1: String?
)
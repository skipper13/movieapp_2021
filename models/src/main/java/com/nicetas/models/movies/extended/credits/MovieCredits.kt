package com.nicetas.models.movies.extended.credits

import com.google.gson.annotations.SerializedName

class MovieCredits(
    @SerializedName("id")
    val id: Int,
    @SerializedName("cast")
    val cast: List<MovieCast>?,
    @SerializedName("crew")
    val crew: List<Crew>?,
)
package com.nicetas.models.movies

import com.google.gson.annotations.JsonAdapter
import com.google.gson.annotations.SerializedName
import com.nicetas.models.adapters.LocalDateJsonAdapter
import java.time.LocalDate

class MoviesList(
    @SerializedName("page")
    val page: Int,
    @SerializedName("results")
    val movies: List<Movie>?,
    @SerializedName("total_pages")
    val totalPages: Int,
//    @SerializedName("total_results")
//    val totalResults: Int,
) {

    class Movie(
        @SerializedName("id")
        val id: Int,
        @SerializedName("title")
        val title: String,
        @SerializedName("original_title")
        val originalTitle: String?,
        @SerializedName("backdrop_path")
        val backdropPath: String?,
        @SerializedName("poster_path")
        val posterPath: String?,
        @JsonAdapter(value = LocalDateJsonAdapter::class, nullSafe = false)
        @SerializedName("release_date")
        val releaseDate: LocalDate?,
        @SerializedName("vote_average")
        val voteAverage: Double?,
        /*
        @SerializedName("overview")
        val overview: String?,
        @SerializedName("vote_count")
        val voteCount: Int?,
        @SerializedName("popularity")
        val popularity: Double?,
        @SerializedName("genre_ids")
        val genreIds: List<Int>?,
        @SerializedName("original_language")
        val originalLanguage: String?,
        @SerializedName("adult")
        val adult: Boolean,
        @SerializedName("video")
        val video: Boolean,
         */
    )

}
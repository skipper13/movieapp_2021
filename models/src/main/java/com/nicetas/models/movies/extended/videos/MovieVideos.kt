package com.nicetas.models.movies.extended.videos

import com.google.gson.annotations.SerializedName

class MovieVideos(
    @SerializedName("id")
    val movieId: Int,
    @SerializedName("results")
    val results: List<MovieVideo>
)
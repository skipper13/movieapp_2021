package com.nicetas.models.movies.extended

import com.google.gson.annotations.SerializedName

class SpokenLanguage(
    @SerializedName("english_name")
    val englishName: String, // "English"
    @SerializedName("iso_639_1")
    val iso_639_1: String, // "en"
    @SerializedName("name")
    val name: String, // "English
) {
    override fun toString(): String {
        return "iso_3166_1 = $iso_639_1, name = $name; "
    }
}
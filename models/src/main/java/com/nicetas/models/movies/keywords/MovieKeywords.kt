package com.nicetas.models.movies.keywords

import com.google.gson.annotations.SerializedName

class MovieKeywords(
    @SerializedName("id")
    val movieId: Int,
    @SerializedName("keywords")
    val keywords: List<Keyword>,
)
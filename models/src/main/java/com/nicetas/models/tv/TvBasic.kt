package com.nicetas.models.tv

import com.google.gson.annotations.JsonAdapter
import com.google.gson.annotations.SerializedName
import com.nicetas.models.adapters.LocalDateJsonAdapter
import java.time.LocalDate

class TvBasic(

    @SerializedName("id")
    val id: Int,
    @SerializedName("name")
    val name: String?,
    @SerializedName("overview")
    val overview: String?,
    @SerializedName("popularity")
    val popularity: Double?,
    @SerializedName("original_name")
    val originalName: String?,
    @SerializedName("poster_path")
    val posterPath: String?,
    @SerializedName("backdrop_path")
    val backdropPath: String?,
    @SerializedName("vote_average")
    val voteAverage: Double?,
    @SerializedName("genre_ids")
    val genreIds: List<Int>,
    @SerializedName("vote_count")
    val voteCount: Int?,
    @SerializedName("original_language")
    val originalLanguage: String?,
    @JsonAdapter(value = LocalDateJsonAdapter::class, nullSafe = false)
    @SerializedName("first_air_date")
    val firstAirDate: LocalDate?,
    @SerializedName("origin_country")
    val originCountry: List<String>

)
package com.nicetas.models.persons

import com.google.gson.annotations.JsonAdapter
import com.google.gson.annotations.SerializedName
import com.nicetas.models.adapters.GenderJsonAdapter
import com.nicetas.models.adapters.LocalDateJsonAdapter
import com.nicetas.models.persons.credits.PersonCast
import com.nicetas.models.persons.credits.PersonCredits
import com.nicetas.models.search.person.Gender
import java.time.LocalDate

class Person(
    @SerializedName("id")
    val id: Int,
    @SerializedName("name")
    val name: String,
    @SerializedName("biography")
    val biography: String?,
    @SerializedName("place_of_birth")
    val placeOfBirth: String?,
    @JsonAdapter(value = LocalDateJsonAdapter::class, nullSafe = false)
    @SerializedName("birthday")
    val birthday: LocalDate?,
    @JsonAdapter(value = LocalDateJsonAdapter::class, nullSafe = false)
    @SerializedName("deathday")
    val deathday: LocalDate?,
    @SerializedName("homepage")
    val homepage: String?,
    @SerializedName("profile_path")
    val profilePath: String?,
    @SerializedName("popularity")
    val popularity: Double,
    @SerializedName("imdb_id")
    val imdbId: String?,
    @JsonAdapter(value = GenderJsonAdapter::class, nullSafe = false)
    @SerializedName("gender")
    val gender: Gender,
    @SerializedName("also_known_as")
    val alsoKnownAs: List<String>,
    @SerializedName("movie_credits")
    val movieCredits: MovieCredits,
) {
   class MovieCredits(
       @SerializedName("cast")
       val cast: List<PersonCast>?,
   )
}
package com.nicetas.models.persons.images

import com.google.gson.annotations.SerializedName

class PersonImage(
    @SerializedName("file_path")
    val filePath: String,
    /*
    @SerializedName("aspect_ratio")
    val aspectRatio: Float,
    @SerializedName("iso_639_1")
    val iso_639_1: String,
    @SerializedName("vote_average")
    val voteAverage: Double,
    @SerializedName("vote_count")
    val voteCount: Int,
    @SerializedName("height")
    val height: Int,
    @SerializedName("width")
    val width: Int,
     */
)
package com.nicetas.models.persons.images

import com.google.gson.annotations.SerializedName

class PersonImages(
    @SerializedName("id")
    val id: Int,
    @SerializedName("profiles")
    val images: List<PersonImage>,
)
package com.nicetas.models.persons.credits

import com.google.gson.annotations.SerializedName

class PersonCrew(
        @SerializedName("id")
        val id: Int,
        @SerializedName("department")
        val department: String,
        @SerializedName("job")
        var job: String?,
        @SerializedName("adult")
        var adult: Boolean,
        @SerializedName("credit_id")
        var creditId: String?,
        @SerializedName("profile_path")
        val profilePath: String?,
        @SerializedName("poster_path")
        val posterPath: String?,
        @SerializedName("release_date")
        var releaseDate: String?,
        @SerializedName("original_title")
        var originalTitle: String?,
)
package com.nicetas.models.persons.credits

import com.google.gson.annotations.JsonAdapter
import com.google.gson.annotations.SerializedName
import com.nicetas.models.adapters.LocalDateJsonAdapter
import java.time.LocalDate

class PersonCast(

        @SerializedName("id")
        val id: Int,
        @SerializedName("character")
        val character: String?,
        @SerializedName("title")
        val title: String,
        @SerializedName("original_title")
        val originalTitle: String?,
        @SerializedName("backdrop_path")
        val backdropPath: String?,
        @JsonAdapter(value = LocalDateJsonAdapter::class, nullSafe = false)
        @SerializedName("release_date")
        val releaseDate: LocalDate?,
        @SerializedName("poster_path")
        val posterPath: String?,
        @SerializedName("vote_average")
        val voteAverage: Double?,
        @SerializedName("vote_count")
        val voteCount: Int?,
        @SerializedName("popularity")
        val popularity: Double?,
//        @SerializedName("order")
//        val order: Int,
//        @SerializedName("overview")
//        val overview: String?,
//        @SerializedName("adult")
//        val adult: Boolean,
//        @SerializedName("video")
//        val video: Boolean,
//        @SerializedName("credit_id")
//        val creditId: String?,
//        @SerializedName("genre_ids")
//        val genreIds: List<Int>,
//        @SerializedName("original_language")
//        val originalLanguage: String?,

)
package com.nicetas.models.persons.credits

import com.google.gson.annotations.SerializedName

class PersonCredits(
    @SerializedName("id")
    val id: Int,
    @SerializedName("cast")
    val cast: List<PersonCast>?,
    // There is no need for this at the moment
//    @SerializedName("crew")
//    val crew: List<PersonCrew>?,
)
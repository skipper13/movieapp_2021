package com.nicetas.models.search.person

import com.google.gson.annotations.SerializedName

class SearchPerson(
    @SerializedName("id")
    val id: Int,
    @SerializedName("profile_path")
    val profilePath: String?,
    @SerializedName("name")
    val name: String,
    @SerializedName("adult")
    val adult: Boolean,
    @SerializedName("known_for")
    val knownFor: List<KnownFor>,
)
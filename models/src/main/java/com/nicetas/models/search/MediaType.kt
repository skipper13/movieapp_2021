package com.nicetas.models.search

enum class MediaType {
    PERSON,
    MOVIE,
    TV,
    // Just in case. Actually there are always "person/movie/tv" types.
    UNKNOWN;
}
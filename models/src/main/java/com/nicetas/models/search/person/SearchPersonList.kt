package com.nicetas.models.search.person

import com.google.gson.annotations.SerializedName


class SearchPersonList(
    @SerializedName("page")
    val page: Int,
    @SerializedName("results")
    val actors: List<SearchPerson>?,
    @SerializedName("total_pages")
    val totalPages: Int,
    @SerializedName("total_results")
    val totalResults: Int
)
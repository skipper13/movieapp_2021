package com.nicetas.models.search.multi

import com.google.gson.annotations.JsonAdapter
import com.google.gson.annotations.SerializedName
import com.nicetas.models.adapters.LocalDateJsonAdapter
import com.nicetas.models.adapters.MediaTypeJsonAdapter
import com.nicetas.models.search.MediaType
import com.nicetas.models.search.person.KnownFor
import java.time.LocalDate

class MultiSearch(
    @JsonAdapter(value = MediaTypeJsonAdapter::class, nullSafe = false)
    @SerializedName("media_type")
    val mediaType: MediaType,
    @SerializedName("id")
    val id: Int,

//        Person / Movie
    @SerializedName("adult")
    val adult: Boolean?,

//        Person
    @SerializedName("profile_path")
    val profilePath: String?,
    @SerializedName("known_for")
    val knownFor: List<KnownFor>?,

//        Person / Tv
    @SerializedName("name")
    val name: String?,

//        Movie
    @SerializedName("title")
    val title: String?,
    @SerializedName("original_title")
    val originalTitle: String?,
    @SerializedName("video")
    val video: Boolean?,

//        Movie / Tv
    @SerializedName("overview")
    val overview: String?,
    @SerializedName("backdrop_path")
    val backdropPath: String?,
    @SerializedName("release_date")
    val releaseDate: String?,
    @SerializedName("poster_path")
    val posterPath: String?,
    @SerializedName("vote_average")
    val voteAverage: Double?,
    @SerializedName("vote_count")
    val voteCount: Int?,
    @SerializedName("popularity")
    val popularity: Double?,
    @SerializedName("genre_ids")
    val genreIds: List<Int>?,
    @SerializedName("original_language")
    val originalLanguage: String?,

//        Tv
    @SerializedName("original_name")
    val originalName: String?,
    @JsonAdapter(value = LocalDateJsonAdapter::class, nullSafe = false)
    @SerializedName("first_air_date")
    val firstAirDate: LocalDate?,
    @SerializedName("origin_country")
    val originCountry: List<String>?,
)
package com.nicetas.models.search.multi

import com.google.gson.annotations.SerializedName

class MultiSearchList(
    @SerializedName("page")
    val page: Int,
    @SerializedName("results")
    val results: List<MultiSearch>?,
    @SerializedName("total_pages")
    val totalPages: Int,
    @SerializedName("total_results")
    val totalResults: Int,
)
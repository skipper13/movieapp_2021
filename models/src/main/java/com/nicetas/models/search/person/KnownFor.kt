package com.nicetas.models.search.person

import com.google.gson.annotations.JsonAdapter
import com.google.gson.annotations.SerializedName
import com.nicetas.models.adapters.LocalDateJsonAdapter
import com.nicetas.models.adapters.MediaTypeJsonAdapter
import com.nicetas.models.search.MediaType
import java.time.LocalDate

class KnownFor(
    @SerializedName("vote_average")
    val voteAverage: Double?,
    @SerializedName("vote_count")
    val voteCount: Int?,
    @SerializedName("id")
    val id: Int,
    @SerializedName("video")
    val video: Boolean,
    @JsonAdapter(value = MediaTypeJsonAdapter::class, nullSafe = false)
    @SerializedName("media_type")
    val mediaType: MediaType,
    @SerializedName("title")
    val title: String?,
    @SerializedName("popularity")
    val popularity: Double?,
    @SerializedName("poster_path")
    val posterPath: String?,
    @SerializedName("original_language")
    val originalLanguage: String?,
    @SerializedName("original_title")
    val originalTitle: String?,
    @SerializedName("genre_ids")
    val genreIds: List<Int>?,
    @SerializedName("backdrop_path")
    val backdropPath: String?,
    @SerializedName("adult")
    val adult: Boolean,
    @SerializedName("overview")
    val overview: String?,
    @JsonAdapter(value = LocalDateJsonAdapter::class, nullSafe = false)
    @SerializedName("release_date")
    val releaseDate: LocalDate?
)
package com.nicetas.models.search.person

enum class Gender {
    NOT_SPECIFIED,
    FEMALE,
    MALE;
}
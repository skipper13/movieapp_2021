package com.nicetas.models.genres

import com.google.gson.annotations.SerializedName

class Genres(
    @SerializedName("genres")
    val genres: List<Genre>?,
)
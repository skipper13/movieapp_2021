package com.nicetas.models.genres

import com.google.gson.annotations.SerializedName
import com.nicetas.models.IdName

class Genre(
    @SerializedName("id")
    val id: Int,
    @SerializedName("name")
    val name: String,
) : IdName {

    companion object {
        const val ANIMATION_ID = 16
        const val HORROR_ID = 27
    }

    override fun identifier() = id

    override fun iName() = name

    override fun toString(): String {
        return "id = $id, name = $name; "
    }
}

/*
    id = 28, name = Action
    id = 12, name = Adventure
    id = 16, name = Animation
    id = 35, name = Comedy
    id = 80, name = Crime
    id = 99, name = Documentary
    id = 18, name = Drama
    id = 10751, name = Family
    id = 14, name = Fantasy
    id = 36, name = History
    id = 27, name = Horror
    id = 10402, name = Music
    id = 9648, name = Mystery
    id = 10749, name = Romance
    id = 878, name = Science Fiction
    id = 10770, name = TV Movie
    id = 53, name = Thriller
    id = 10752, name = War
    id = 37, name = Western
 */
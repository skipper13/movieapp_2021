package com.nicetas.models

interface IdName {

    fun identifier(): Int
    fun iName(): String

}